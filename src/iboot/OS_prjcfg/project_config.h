/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#ifndef __PROJECT_CONFFIG_H__
#define __PROJECT_CONFFIG_H__

#include <stdint.h>
#include <stddef.h>

#define    CN_RUNMODE_IBOOT                0                 //IBOOT模式运行
#define    CN_RUNMODE_BOOTSELF             1                 //无须IBOOT，自启动模式APP
#define    CN_RUNMODE_APP                  2                 //由IBOOT加载的APP
#define    CFG_RUNMODE                     CN_RUNMODE_IBOOT              //由IBOOT加载的APP
//*******************************  Configure device file system  ******************************************//
#define    CFG_MODULE_ENABLE_DEVICE_FILE_SYSTEM  true
#define CFG_DEVFILE_LIMIT       10      //定义设备数量
//*******************************  Configure int  ******************************************//
#define    CFG_MODULE_ENABLE_INT           true
//*******************************  Configure kernel object system  ******************************************//
#define    CFG_MODULE_ENABLE_KERNEL_OBJECT_SYSTEM  true
#define CFG_OBJECT_LIMIT        8   //用完会自动扩充
#define CFG_HANDLE_LIMIT        8   //用完会自动扩充
//*******************************  Configure memory pool  ******************************************//
#define    CFG_MODULE_ENABLE_MEMORY_POOL   true
#define CFG_MEMPOOL_LIMIT       20      //
//*******************************  Configure message queue  ******************************************//
#define    CFG_MODULE_ENABLE_MESSAGE_QUEUE true
//*******************************  Configure multiplex  ******************************************//
#define    CFG_MODULE_ENABLE_MULTIPLEX     true
//*******************************  Configure ring buffer and line buffer  ******************************************//
#define    CFG_MODULE_ENABLE_RING_BUFFER_AND_LINE_BUFFER  true
//*******************************  Configure xip app file system  ******************************************//
#define    CFG_MODULE_ENABLE_XIP_APP_FILE_SYSTEM  true
//*******************************  Configure fat file system  ******************************************//
#define    CFG_MODULE_ENABLE_FAT_FILE_SYSTEM  true
#define CFG_FAT_MS_INSTALLUSE       MS_INSTALLUSE   //参考filesystem.h中的MS_INSTALLUSE等定义
#define CFG_FAT_MEDIA_KIND          "SD"      //（如"MSC" "EMMC"
#define CFG_FAT_MOUNT_POINT         "SD"      //FAT文件系统安装目录
//*******************************  Configure cpu onchip gpio  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_GPIO  true
//*******************************  Configure cpu onchip adc  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_ADC  true
//*******************************  Configure board config  ******************************************//
#define    CFG_MODULE_ENABLE_BOARD_CONFIG  true
//*******************************  Configure sdcard power  ******************************************//
#define    CFG_MODULE_ENABLE_SDCARD_POWER  true
//*******************************  Configure file system  ******************************************//
#define    CFG_MODULE_ENABLE_FILE_SYSTEM   true
#define CFG_CLIB_BUFFERSIZE            512      //
//*******************************  Configure kernel  ******************************************//
#define    CFG_MODULE_ENABLE_KERNEL        true
#define CFG_INIT_STACK_SIZE     4096    //定义初始化过程使用的栈空间一般按默认值就可以了
#define CFG_EVENT_LIMIT         15      //事件数量
#define CFG_EVENT_TYPE_LIMIT    15      //事件类型数
#define CFG_MAINSTACK_LIMIT     4096    //main函数运行所需的栈尺寸
#define CFG_IDLESTACK_LIMIT     1024    //IDLE事件处理函数运行的栈尺寸一般按默认值就可以了
#define CFG_IDLE_MONITOR_CYCLE  30      //"IDLE监视周期",监视IDLE事件持续低于1/16 CPU占比的时间，秒数，0=不监视
#define CFG_OS_TINY             false   //true=用于资源紧缺的场合内核会裁剪掉：事件类型名字事件处理时间统计。
//*******************************  Configure lock  ******************************************//
#define    CFG_MODULE_ENABLE_LOCK          true
#define CFG_LOCK_LIMIT          40      //定义锁的数量包含信号量和互斥量
//*******************************  Configure heap  ******************************************//
#define    CFG_MODULE_ENABLE_HEAP          true
#define CFG_DYNAMIC_MEM true  //即使选false也允许使用malloc-free分配内存但使用有差别详见 《……用户手册》内存分配章节
//*******************************  Configure cpu onchip qspi  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_QSPI  true
//*******************************  Configure uart device file  ******************************************//
#define    CFG_MODULE_ENABLE_UART_DEVICE_FILE  true
//*******************************  Configure cpu onchip uart  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_UART  true
#define CFG_UART1_SENDBUF_LEN       64      //
#define CFG_UART1_RECVBUF_LEN       64      //
#define CFG_UART2_SENDBUF_LEN       64      //
#define CFG_UART2_RECVBUF_LEN       64      //
#define CFG_UART1_ENABLE           false        //
#define CFG_UART2_ENABLE           true       //
//*******************************  Configure time  ******************************************//
#define    CFG_MODULE_ENABLE_TIME          true
#define CFG_LOCAL_TIMEZONE      8      //北京时间是东8区
//*******************************  Configure cpu onchip boot  ******************************************//
#define    CFG_MODULE_ENABLE_BOOT          true
#define    CFG_POWER_ON_RESET_TO_BOOT      true //控制上电复位后是否强制运行iboot
//*******************************  Configure cpu onchip flash  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_FLASH  true
//*******************************  Configure emflash insatall xip  ******************************************//
#define    CFG_MODULE_ENABLE_EMFLASH_INSATALL_XIP  true
#define    CFG_EFLASH_XIP_PART_START       80                //"分区起始块号，含",填写块号，块号从0开始计算
#define    CFG_EFLASH_XIP_PART_END         945               //"分区结束块号，不含",-1表示最后一块
#define    CFG_EFLASH_XIP_PART_FORMAT      false             //是否需要格式化该分区。
#define    CFG_EFLASH_XIPFSMOUNT_NAME      "xip-app"         //
//*******************************  Configure loader  ******************************************//
#define    CFG_MODULE_ENABLE_LOADER        true
#define CFG_UPDATEIBOOT_EN      false       //
#define  CFG_APP_RUNMODE  EN_DIRECT_RUN     //EN_DIRECT_RUN=直接从flash中运行；EN_FORM_FILE=从文件系统加载到内存运行
#define  CFG_APP_VERIFICATION   VERIFICATION_MD5   //
#define CFG_IBOOT_VERSION_SMALL       00        //xx.xx.__APP忽略
#define CFG_IBOOT_VERSION_MEDIUM      00        //xx.__.xxAPP忽略
#define CFG_IBOOT_VERSION_LARGE       01        //__.xx.xxAPP忽略
#define CFG_IBOOT_UPDATE_NAME      "/efs/iboot.bin"           //
#define CFG_APP_UPDATE_NAME        "/SD/app.bin"            //
#define CFG_FORCED_UPDATE_PATH     "/SD/djyapp.bin"           //
//*******************************  Configure debug information  ******************************************//
#define    CFG_MODULE_ENABLE_DEBUG_INFORMATION  true
//*******************************  Configure stdio  ******************************************//
#define    CFG_MODULE_ENABLE_STDIO         true
#define CFG_STDIO_STDIN_MULTI      true         //
#define CFG_STDIO_STDOUT_FOLLOW    true         //
#define CFG_STDIO_STDERR_FOLLOW    true         //
#define CFG_STDIO_FLOAT_PRINT      true         //
#define CFG_STDIO_STDIOFILE        true         //
#define CFG_STDIO_IN_NAME              "/dev/UART2"    //
#define CFG_STDIO_OUT_NAME             "/dev/UART2"    //
#define CFG_STDIO_ERR_NAME             "/dev/UART2"    //
//*******************************  Configure shell  ******************************************//
#define    CFG_MODULE_ENABLE_SHELL         true
#define CFG_SHELL_STACK            0x1000      //
#define CFG_ADD_ROUTINE_SHELL      true        //
#define CFG_ADD_EXPAND_SHELL       true        //
#define CFG_ADD_GLOBAL_FUN         false       //
#define CFG_SHOW_ADD_SHEELL        true        //
//*******************************  Core Clock  ******************************************//
#define    CFG_CORE_MCLK                   (180.0*Mhz)       //主频，内核要用，必须定义
//*******************************  DjyosProduct Configuration  ******************************************//
#define    PRODUCT_MANUFACTURER_NAME       "深圳市天材教育科技有限公司"   //厂商名称
#define    PRODUCT_PRODUCT_CLASSIFY        "口语机"             //产品分类
#define    PRODUCT_PRODUCT_MODEL           "TC-1"            //产品型号
#define    PRODUCT_VERSION_LARGE           1                 //版本号,__.xx.xx
#define    PRODUCT_VERSION_MEDIUM          0                 //版本号,xx.__.xx
#define    PRODUCT_VERSION_SMALL           40                //版本号,xx.xx.__
#define    PRODUCT_PRODUCT_MODEL_CODE      "4HH1HU"          //产品型号编码
#define    PRODUCT_PASSWORD                "12345678"        //产品秘钥
#define    PRODUCT_OTA_ADDRESS             "factory.cpolar.cn"    //OTA服务器地址
#define    PRODUCT_BOARD_TYPE              "AiEasyTalk"      //板件类型
#define    PRODUCT_CPU_TYPE                "bk7251"          //CPU类型


#endif
