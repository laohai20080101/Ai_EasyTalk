
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */
#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"
#include <sys/socket.h>
#include <gpio_pub.h>
#include <app_flash.h>
#include "wlan_ui_pub.h"
#include "project_config.h"
#include "board.h"
#include "djyos.h"
#include "shell.h"
#include "mem/mem_mgr.h"
#include "GuiWin/inc/GuiInfo.h"
#include "GuiWin/inc/WinSwitch.h"
//#include <filesystems.h>
#include <Iboot_info.h>
#include <upgrade.h>
#include <wdt_soft.h>
#include <wdt_pub.h>
#include <qspi_pub.h>
#include <bk_timer_pub.h>

#define OPEN_INTERNAL_CHARGE      0
#define   RC_DEBUG_OFF     1
#define LONG_DOWN_TURN 0
#define ASM_GET_LR(fun)   asm( "mov %0, LR\n" :"=r"(fun):)

char power_percentage = 0;
int login_ok = 0;
s64 login_time = 0;
char start_user_loading = 0;
volatile int flag_connected = 0;

extern  WIFI_CFG_T  LoadWifi;

extern void usb_charge_check_cb(void);
extern void usb_plug_func_open(void);
//extern int vbat_voltage_get(void);
//extern char Get_UpdateRunModet(void);
extern char vol_to_percentage(int assign);
extern enum UpgradeMode GetUpgradeMode(void);
extern void flash_protection_op(UINT8 mode, PROTECT_TYPE type);

extern int wav_play_from_file(char *path);
extern int mp3_play_from_file(char *path);
extern int WavIsBusy();
extern int Mp3IsBusy();
extern int mp3_module_init();
extern int wav_module_init();
extern int record_init();
extern int DevMgrInit();
extern bool_t oss_player_event_init(void);
extern bool_t oss_download_event_init(void);
extern int web_upgrade_firmware(int check);
extern int Update_ServerInfo();

extern OSStatus bk_wlan_get_link_status(LinkStatusTypeDef *outStatus);
extern bool_t DhcpModeInit(int dhcp_mode);
void dhcpd_route_add_default();
extern void DelayCloseWiFi(int delay_ms);
extern void WiFiPowerUp();
extern bool_t Wifi_Connectedflag(u8 flag);

extern int SetWiFiStrength(int level);
extern void Set_Time_Ctrl();
extern bool_t Refresh_SoftUpdateWin(void);
extern bool_t Set_UpdateSource(char *param);
extern int vbat_voltage_get(void);
extern void deep_sleep(void);
#if LONG_DOWN_TURN
//typedef enum
//{
//    NO_KEY = 0,
////    PAUSE_PLAY_KEY,
////    VOL_UP_KEY,
////    VOL_DOWN_KEY,
////    RECORD_KEY,
//    POWER_KEY,
//} KEY_ID;

static char key_recognition(void)
{
    char current_key;

    if(djy_gpio_read(13))
        current_key = POWER_KEY;
    else
        current_key = NO_KEY;

//    printf("key = %d\r\n", current_key);
    return current_key;
}
#endif

char current_power(void)
{
    return power_percentage;
}

void set_cell_power(void)
{
    char i = 0, per = -1;

    while(i++ < 3)
    {
        per = vol_to_percentage(0);
        if(per != -1)
            break;
    }

    if(i <= 3)
        power_percentage = per;

//    printf(" ====== 拔充电器是的电量   = %d ======\r\n", power_percentage);
}

int is_wifi_connected()
{
    return flag_connected;
}

bool_t UsbIsCharge = false;
bool_t IsCharge(void)
{
    return UsbIsCharge;
}
void SetChargeFlag(enum ChargeFlag flag)
{
    if(flag == NoCharge)
        UsbIsCharge = false;
    else if(flag == OnCharge)
        UsbIsCharge = true;
}

ptu32_t Key_EventMain(void)
{
    while (1) {

        wav_play_from_file("/SD/16D.wav");
        while (WavIsBusy()) {
            printf("wait wav end ..\r\n");
            Djy_EventDelay(500*1000);
        }
        mp3_play_from_file("/SD/1.mp3");
        while (Mp3IsBusy()) {
            printf("wait mp3 end ..\r\n");
            Djy_EventDelay(500*1000);
        }
    }
    return 0;

}


ptu32_t timer_EventMain(void)
{
    u8 count = 0;
    int local_hour = 0;
    int local_min = 0;
    int hour, min;
    int ret = 0;
    int signal_level = 0;
    char local_Percentage = 0;
    char Percentage = 0;
    char local_Power_flag = 0;
    char Power_flag = 0;
    LinkStatusTypeDef outStatus;
    while (1) {

        DelayCloseWiFi(1000*15);

//        vol2 = vbat_voltage_get();
//        if((vol2 != 0) && (vol1 != 0))
//        {
//            if((vol2 - vol1) > 30)
//                UsbIsCharge = true;
//            else if((vol2 - vol1) < -50)
//                UsbIsCharge = false;
//        }
//        vol1 = vol2;

        //30分钟重新登录一下防止token过期
        if((DjyGetSysTime() - login_time) > 30*60*1000*mS)
        {
            login_time = DjyGetSysTime();
            login_ok = 0;
        }
        if (flag_connected && !login_ok) {
            printf("==user_login==\r\n");
            int user_login();
            ret = user_login();
            printf("login message: ret = %d!\r\n", ret);
            if (ret == 1) {
                login_ok = 1;
                login_time = DjyGetSysTime();
            }
        }

        if (login_ok==1 && start_user_loading){
//              Update_ServerInfo();
                 Homework_HomeWorkPull(0);
                start_user_loading = 0;
        }


        Djy_EventDelay(1000*1000);

        int GetTimeHourMinute(int *hour, int *min);
        /************更新当前显示时间*******************/
        int SetClock(int hour, int min);
        if (flag_connected) {
            GetTimeHourMinute(&hour, &min);
            if (local_hour != hour || local_min != min) {
                local_hour = hour;
                local_min = min;
                SetClock(hour, min);
            }
        }
        /******************************************/

        /************更新当前显示电量*******************/
        if(Get_PowerFlag()==true)
        {
            Set_PowerFlag(false);
            Power_Lower();
        }
        if(IsCharge() == false)
            Power_flag =0;
        else
            Power_flag = 1;
        if(count==0 || count >= 60 ||Power_flag != local_Power_flag)
        {
            count = 1;
            Percentage = current_power();
            if(Percentage != local_Percentage || Power_flag != local_Power_flag)
            {
                local_Power_flag = Power_flag;
                local_Percentage = Percentage;
                Set_MainWinPower(local_Percentage);
            }
        }
        count++;
        /******************************************/

        /************更新当前wifi显示图标*******************/
        switch(mhdr_get_station_status()) {
        case MSG_GOT_IP:
            if (flag_connected != 1) {
                flag_connected = 1;
                Wifi_Connectedflag(1);
                DjyWifi_StaConnectDone();
            }
            break;
        case MSG_PASSWD_WRONG:
            printf(">>>info: MSG_PASSWD_WRONG<<<\r\n");
        case MSG_IDLE:
        case MSG_CONNECTING:
        //case MSG_PASSWD_WRONG:
        case MSG_NO_AP_FOUND:
        case MSG_CONN_FAIL:
        case MSG_CONN_SUCCESS:
        default:
            if (flag_connected != 0) {
                flag_connected = 0;
                Wifi_Connectedflag(0);
            }
            break;
        }
        if (flag_connected && kNoErr == bk_wlan_get_link_status(&outStatus)) {
            //printf("--wifi strength--: %d !\r\n", outStatus.wifi_strength);
            if (outStatus.wifi_strength < -100) {  // 0: wifi break out  < -100
                signal_level = 0;
            }
            else if (outStatus.wifi_strength < -88) { // 1: wifi strength -100 ~ -88
                signal_level = 1;
            }
            else if (outStatus.wifi_strength < -77) { // 2: wifi strength - 88 ~ -77
                signal_level = 2;
            }
            else if (outStatus.wifi_strength < -55)  { // 3: wifi strength - 77 ~ -55
                signal_level = 3;
            }
            else if (outStatus.wifi_strength < 0) { // 4: wifi strength -  0 ~ -55
                signal_level = 4;
            }
        }
        else { // wifi connected failed will set be -1
            signal_level = -1;
        }
        SetWiFiStrength(signal_level);
        /******************************************/
        if(is_wifi_connected())
        {
//            if (WiFiOpen(10*1000) > 0) {

            if((Get_update_flag() == ReadyUpdate) && (GetUpgradeMode() == CMD_CoerceUpgrade))
            {
                Refresh_SoftUpdateWin();
            }

            if((Get_update_flag() == UpgradInProgress) || (GetUpgradeMode() == CMD_CoerceUpgrade))
            {
                Set_Time_Ctrl();
                if(web_upgrade_firmware(0) > 0)
                    network_update_exe();
            }
//            }
//            WiFiClose(10);
        }
    }
}

bool_t timer_event_init(void)
{
    u16 evtt_timer;
    evtt_timer = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS + 1, 0, 0, timer_EventMain, 0, 0x1000, "timer_event");
    if(evtt_timer != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_timer, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_timer Event Start Fail!\r\n");
    }

    return true;
}

bool_t Key_event_init(void)
{
    u16 evtt_Key;
    evtt_Key = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0, Key_EventMain, NULL, 0x1000, "Key_evevt");
    if(evtt_Key != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_Key, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_recorder Event Start Fail!\r\n");
    }

    return true;
}

//void battery_low_voltage(int vol)
//{
//    char Vbat;
//    Vbat = vol_to_percentage(vol);
//    while(Vbat < 5)
//    {
//        Djy_EventDelay(30000*1000);
//        Vbat = vol_to_percentage(0);
//        printf("percent = %d %%  \r\n", Vbat);    //显示当前的电池电压
//        if(IsCharge() == false)
//        {
//            printf("电量不足5 %%，准备关机\r\n");
//            Djy_EventDelay(3000*1000);
//            deep_sleep();
//        }
//    }
//}

void CheckUpdate(void)
{   //开机时等待1分钟，判断是否连上网，连上网后是否需要进行升级
    Set_update_flag(NoUpdate);
//    for(i = 0; i < 60; i++)
//    {
//        Djy_EventDelay(1000*1000);
        if(flag_connected)
        {
            if((IsCharge()) || (current_power() >= 50))//电量大于50%才可以进行升级
            {
    //            if (WiFiOpen(10*1000) > 0) {
                if(web_upgrade_firmware(1) == 1)
                {
                    Set_update_flag(ReadyUpdate);
                }
                printf("info: download firmware from the network done!\r\n");
    //            }
    //            WiFiClose(10);
//                break;
            }
            else
            {
                printf("info: battery is low and cannot be Update!\r\n");
//                break;
            }
        }
//    }
}

void Power_on_control(void)
{
    int vol = 0;

#if OPEN_INTERNAL_CHARGE
    usb_plug_func_open();   //充电功能开启函数
//    Vbat = vbat_voltage_get();
//    printf("读到的指定的Vbat = %d  \r\n", vol);
    usb_charge_check_cb();      //判断是否开始和结束充电的函数
#endif
    if(usb_is_plug_in() != 0)
    {
        SetChargeFlag(OnCharge);
        vol = vbat_voltage_get();
        vol -= 150;
    }
    else
        SetChargeFlag(NoCharge);
    power_percentage = vol_to_percentage(vol);
    printf("========= percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压
    if(power_percentage < 1)
    {
        if(usb_is_plug_in() == 0)
        {
            if(vbat_voltage_get() < 3470)
            {
                printf("电量不足，准备关机\r\n");
                Djy_EventDelay(3000*1000);
                deep_sleep();
            }
        }
    }

}

tagWdt *ptWdtHard;
u32 wdtcounter = 0;
u32 WdtYipHook(tagWdt *wdt)
{
    wdtcounter = 0;
    return EN_BLACKBOX_DEAL_IGNORE;
}

bool_t spyk(char *param);
bool_t eventk(char *param);

void timer0_isr(UINT8 param)
{
//    CloseBackLight();
    wdtcounter++;
    if (wdtcounter > 15)
    {
        wdtcounter = 0;
        printk("------wdt error------\r\n");
        eventk(NULL);
        spyk("0");
        spyk("1");
        spyk("2");
        spyk("3");
        spyk("4");
        spyk("5");
        spyk("6");
        spyk("7");
        spyk("8");
        spyk("9");
        spyk("10");
        spyk("11");
        spyk("12");
        spyk("13");
        spyk("14");
        spyk("15");
        spyk("16");
        spyk("17");
        spyk("18");
        spyk("19");
        spyk("20");
        spyk("21");
        spyk("22");
        spyk("23");
        spyk("24");
        spyk("25");
    }
//    Djy_DelayUs(1000);
//  printf("******timer interrupt\r\n");
//    OpenBackLight();
}

void mcu_init_timer0(void)
{
    timer_param_t param;
    param.channel = 0;
    param.div = 1;
    param.period = 1000000;
    param.t_Int_Handler= timer0_isr;

    sddev_control(TIMER_DEV_NAME, CMD_TIMER_INIT_PARAM_US, &param);
}
extern u8 StartBitmap[];
int GetNetInfo(char *ssid, int ssid_len, char *passwd, int pwd_len);
u32 xff8c,xff0c,xff8cnow,xff0cnow;
ptu32_t djy_main(void)
{
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 0, 0  };
    struct GkWinObj *desktop;
    struct RectBitmap   bitmap;
    s32 j;

    Heap_Add(QSPI_DCACHE_BASE, 0x7fff80, 64, 0, false, "PSRAM");       //把QSPI接的RAM添加到heap当中
    xff8c = *(u32*)0x37fff8c;

    desktop = GK_GetDesktop(CFG_DISPLAY_NAME);
    bitmap.bm_bits = StartBitmap;
    bitmap.linebytes = 480;
    bitmap.PixelFormat = CN_SYS_PF_RGB565;
    bitmap.ExColor = 0;
    bitmap.height=320;
    bitmap.width=240;
    bitmap.reversal = true;
    GK_DrawBitMap(desktop,&bitmap,0,0,0,RopCode,1000000);
    GK_SyncShow(1000000);
    OpenBackLight();
    Djy_EventDelay(500*1000);

    mcu_init_timer0( );
    ptWdtHard = Wdt_Create("testwdt",10000000,WdtYipHook,EN_BLACKBOX_DEAL_IGNORE, 0,0);
    OpenBackLight();
    MemMgrInit();
    int dhcp_mode = WifiLoad();
#if (RC_DEBUG_OFF==0)
    dhcp_mode = 1;
#endif
    if(dhcp_mode==1)
    {
        DhcpModeInit(0);//dhcp client
    }
    else
    {
        DhcpModeInit(1);//dhcp server
        dhcpd_route_add_default();
    }

//    Update_ServerInfo();
//  Heap_Add(0x3000000,0x800000,32,8,true,CFG_ST7796S_HEAP_NAME);
    gpio_config(13, GMODE_INPUT_PULLDOWN);
    LP_BSP_ResigerGpioToWakeUpL4(0x2000,0);
    Set_UpdateSource("0");  //设置默认的升级方式为文件系统升级
#if LONG_DOWN_TURN       //用于调试，发布时须打开
    if(IsCharge() == false)
    {
        if(Get_UpdateRunModet() == 0)
        {
            for(j = 0; j < 20; j++)
            {
                Djy_EventDelay(100*1000);   //程序开始运行前判断一下是否长按电源键2秒。是的话正常开机。不是的话继续休眠
                if(key_recognition() == POWER_KEY)
                    num++;
            }
            if(num < 20)
            {
                deep_sleep();
            }
        }
    }
#endif
    Power_on_control();
    DevMgrInit();
    timer_event_init();

#if 1
//    extern ptu32_t ModuleInstall_st7796s(const char *DisplayName,const char* HeapName);
//    ModuleInstall_st7796s(CFG_ST7796S_DISPLAY_NAME,CFG_ST7796S_HEAP_NAME);


//    extern void ModuleInstall_Gdd_AND_Desktop(void);
//    ModuleInstall_Gdd_AND_Desktop();
//
//    extern bool_t ModuleInstall_FT6236(void);
//    ModuleInstall_FT6236( );
//
//    extern bool_t GDD_AddInputDev(const char *InputDevName);
//    GDD_AddInputDev(CFG_KEYBOARD_NAME);
//    GDD_AddInputDev(CFG_FT6236_TOUCH_NAME);
//    battery_low_voltage(Vbat);    //判断电池电量是否过低，如果过低则显示正在充电。
    power_percentage = vol_to_percentage(0);
    void Main_GuiWin(void);
    Main_GuiWin();
#endif


#if RC_DEBUG_OFF
//        dhcp_db_flag = 0;
    if(LoadWifi.statue != 0x55)
    {
        char snbuf[30];
        char buf[100];
        int  Get_DeviceSn(char *buf,int buflen);
        Get_DeviceSn(snbuf,sizeof(snbuf));
//        memcpy(buf,"Geniustalk-",strlen("Geniustalk-"));
        sprintf(buf,"Geniustalk%s",snbuf+6);
        DjyWifi_ApOpen(buf,"");
        bool_t web_config_event_init();
        web_config_event_init();

        GetNetInfo(LoadWifi.WifiSsid, sizeof(LoadWifi.WifiSsid), LoadWifi.WifiPassWd, sizeof(LoadWifi.WifiPassWd));
        Djy_EventDelay(100*mS);
        DjyWifi_ApClose();
        WifiSave(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
        Djy_EventDelay(2000*1000);
        runapp(0);
    }
#endif
//        strcpy(LoadWifi.WifiSsid,"VVV");
//        strcpy(LoadWifi.WifiPassWd,"120503Aa");

    WiFiPowerUp();

    record_init();  //录音上传任务。
    mp3_module_init(); //mp3解码任务。
    wav_module_init(); //wav解码任务。
    oss_player_event_init(); //后台下载和播放任务。
    oss_download_event_init();//后台下载音频数据到缓冲区的任务。
    //Key_event_init();

//    comparison_Background();
//    CheckUpdate();
//    if(update_flag ==0)
    start_user_loading = 1;
    j = 450;
    while(1)
    {

//        if(djy_audio_dac_ctrl(AUD_DAC_CMD_GET_FREE_BUF_SIZE, NULL) != 8188)
//        {
////            printf(" (%d)",date);
//            if(djy_gpio_read(GPIO8))
//            {
//                if(GetSpeakerState() == Speaker_on)
//                {
//                    CloseSpeaker();    //有数据，连接耳机、关喇叭
//
//                }
//            }
//            else
//            {
//                if(GetSpeakerState() == Speaker_off)
//                {
//                    OpenSpeaker();    //有数据，断开耳机、开喇叭
//                }
//            }
//        }
//        else
//        {
//            if(GetSpeakerState() == Speaker_on)
//            {
//                CloseSpeaker();    //没有数据、关喇叭
//            }
//        }

        if(j++ > 450)
        {
//            Vbat = vol_to_percentage(0);
//            printf(" main  percent = %d %%  \r\n", Vbat);    //显示当前的电池电压
            power_percentage = vol_to_percentage(0);
//            printf(" main  percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压
#if OPEN_INTERNAL_CHARGE
            usb_charge_check_cb();      //判断是否开始和结束充电的函数
#endif
            j = 0;
        }

        Djy_EventDelay(100*1000);

    }
    return 0;
}

bool_t electric_quantity(char *param)
{
    (void)param;
    int vol;
    power_percentage = vol_to_percentage(0);
    printf("  percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压
    vol = vbat_voltage_get();
    printf("测量到的Vbat = %d  \r\n", vol);    //显示当前的电池电压

    return true;
}

bool_t PrintInfo(char * p)
{
    (void)p;
    printf_Infolist();
    return true;
}
//bool_t testupdate(char *param)
//{
//    (void)param;
//    Set_UpdateSource("1");  //设置升级方式为，从ram获取app数据
//    update_flag = 1;
//    return true;
//}
bool_t format_efs(char *param)
{
    (void)param;
    if(Format("/efs"))
    {
        printf("格式化efs文件系统失败\r\n");
        return false;
    }
    else
    {
        printf("格式化efs文件系统成功\r\n");
        return true;
    }
}

bool_t erase_efs(char *param)
{
    u32 i;
    (void)param;
    flash_protection_op(0,FLASH_PROTECT_NONE);
    for(i = CFG_EMBFLASH_EFS_PART_START; i < CFG_EMBFLASH_EFS_PART_END; i++)
    {
        djy_flash_erase(i * 4096);
    }
    flash_protection_op(0,FLASH_PROTECT_ALL);
    return true;
}
extern void stub_debug(void);
bool_t init_jtag(char *param)
{
    (void)param;
    sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);
    stub_debug();
    return true;
}

bool_t Show_Wininfo(char *param)
{
    (void) param;
    int print_Wininfo();
    print_Wininfo();
    return true;
}

ADD_TO_ROUTINE_SHELL(initjtag,init_jtag,"重新初始化 :COMMAND:init_jtag+enter");
ADD_TO_ROUTINE_SHELL(eraseefs,erase_efs,"擦除efs文件系统 :COMMAND:erase_efs+enter");
ADD_TO_ROUTINE_SHELL(formatefs,format_efs,"格式化efs文件系统 :COMMAND:formatefs+enter");
//ADD_TO_ROUTINE_SHELL(testupdate,testupdate,"帮助信息格式 :help [cmd]");
ADD_TO_ROUTINE_SHELL(printinfo,PrintInfo,"打印信息");
//ADD_TO_ROUTINE_SHELL(touchclean,touchclean,"帮助信息格式 :help [cmd]");
ADD_TO_ROUTINE_SHELL(battery,electric_quantity,"帮助信息格式 :help [cmd]");

ADD_TO_ROUTINE_SHELL(wininfo,Show_Wininfo,"帮助信息格式 :help [cmd]");
