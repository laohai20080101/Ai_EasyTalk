//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
//界面元素ID编号
enum MsgDetailedId{
    MsgDetailed_BACKGROUND, //背景
    MsgDetailed_WIFI,       //wifi
    MsgDetailed_TIME,       //时间
    MsgDetailed_POWER,      //电量
//    MsgDetailed_POWER_LOGO,//
    MsgDetailed_BACK,       //返回
    MsgDetailed_NAME,
//    MsgDetailed_DividingLine,//分割线

    MsgDetailed_Msginfo, //消息信息

    MsgDetailed_del, //查看作业

    MsgDetailed_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MsgDetailedCfgTab[MsgDetailed_MAXNUM] =
{
    [MsgDetailed_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "MsgDetailed",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [MsgDetailed_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [MsgDetailed_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [MsgDetailed_POWER] = {
        .position =  {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [MsgDetailed_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [MsgDetailed_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },
    [MsgDetailed_NAME] = {
        .position = {0,24,240,24+30},
        .name = "消息",
        .type = widget_type_text,
        .userParam = 0,
    },
    [MsgDetailed_Msginfo] = {
        .position = {8,62,240-8,260},
        .name = NULL,
        .type = widget_type_text,
        .userParam = 0,
    },

    [MsgDetailed_del] = {
        .position = {8,270,8+224,270+42},
        .name = "删除",
        .type = widget_type_button,
        .userParam = RGB(247,192,51),
    },

};

extern u32 SelectMsg_cnt;

//按钮控件创建函数
static bool_t  MsgDetailedButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,MsgDetailedCfgTab[MsgDetailed_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        else if (0==strcmp(buttoninfo->name,MsgDetailedCfgTab[MsgDetailed_del].name))//查看作业
        {
            SetTextColor(hdc,RGB(255,255,255));
//            SetFillColor(hdc,buttoninfo->userParam);
//            FillRect(hdc,&rc);
//            OffsetRect(&rc,1,1);
            Draw_Circle_Button(hdc,&rc,21,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }

    //  UpdateDisplay(0);
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_MsgD(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
//    printf("=================HmiCreate_MsgD \n\r");
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, MsgDetailedButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<MsgDetailed_MAXNUM;i++)
    {
        switch (MsgDetailedCfgTab[i].type)
        {
            case  widget_type_work :
                hwndButton = CreateButton(MsgDetailedCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             MsgDetailedCfgTab[i].position.left, MsgDetailedCfgTab[i].position.top,\
                             RectW(&MsgDetailedCfgTab[i].position),RectH(&MsgDetailedCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&MsgDetailedCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
//                     Widget_SetAttr(hwndButton,ENUM_WIDGET_HYALINE_COLOR,CN_COLOR_BLACK);
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(MsgDetailedCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         MsgDetailedCfgTab[i].position.left, MsgDetailedCfgTab[i].position.top,\
                         RectW(&MsgDetailedCfgTab[i].position),RectH(&MsgDetailedCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&MsgDetailedCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_MsgD(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    struct  MsgCb msg;
    int error;
    RECT rc;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<MsgDetailed_MAXNUM;i++)
        {
            switch (MsgDetailedCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,MsgDetailedCfgTab[i].userParam);
                        FillRect(hdc,&MsgDetailedCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(MsgDetailedCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,MsgDetailedCfgTab[i].position.left,\
                                MsgDetailedCfgTab[i].position.top,&MsgDetailedCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&MsgDetailedCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    DrawText(hdc,PowerProgressbar.text,-1,&MsgDetailedCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(MsgDetailedCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;

                case  widget_type_text :
                    if(i == MsgDetailed_Msginfo)
                    {
                        GetClientRect(hwnd,&rc);
                        msg =  Get_MsgNameOfcnt(SelectMsg_cnt,&error);
                        if(!error)
                        {
                            rc.left+=8;
                            rc.top+=62;
                            SetTextColor(hdc,RGB(178,178,178));
                            DrawText(hdc,"消息",-1,&rc,DT_LEFT|DT_TOP);
                            rc.top+=30;
                            SetTextColor(hdc,RGB(28,32,42));
                            size_t len = strlen(msg.msg) + 20;
                            char * str = malloc(len);
                            if(str != NULL)
                            {
                                Utf8_Typesetting(msg.msg,str,len,15);
                                struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                                struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                                DrawText(hdc,str,-1,&rc,DT_TOP|DT_LEFT);
                                SetCharset(hdc,Charsetbak);
                            }
                        }
                    }else{
                        DrawText(hdc,MsgDetailedCfgTab[i].name,-1,&MsgDetailedCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    }
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType HmiNotify_MsgD(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  MsgDetailed_del       :
                Del_MsgNameOfcnt(SelectMsg_cnt);
                NextUi = WIN_Messages;
                break;
            case  MsgDetailed_BACK       :
                NextUi = WIN_Messages;
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_Messages_delete()
{
    return Register_NewWin(WIN_Messages_delete,HmiCreate_MsgD,HmiPaint_MsgD,HmiNotify_MsgD);
}


