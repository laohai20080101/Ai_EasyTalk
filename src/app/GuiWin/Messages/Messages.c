//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
//界面元素ID编号
enum MessagesId{
    Messages_BACKGROUND, //背景
    Messages_WIFI,       //wifi
    Messages_TIME,       //时间
    Messages_POWER,      //电量
//    Messages_POWER_LOGO,
    Messages_BACK,      //返回
//    Messages_LOGO,      //过往作业
    Messages_NAME,
//    Messages_DividingLine,//分割线

    Messages_Msg1,  //班级
    Messages_Msg2,
    Messages_Msg3,
//    Messages_Msg4,
//    Messages_Msg5,

    Messages_SELECT, //选择
//    Messages_PreviousPage,//上一页

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MessagesCfgTab[ENUM_MAXNUM] =
{
    [Messages_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "Messages",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },

    [Messages_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [Messages_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [Messages_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [Messages_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [Messages_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

//    [Messages_LOGO] = {
//        .position = {194-24,20+10,194,20+10+24},
//        .name = "logo",
//        .type = widget_type_picture,
//        .userParam = BMP_MessageList_bmp,
//    },
    [Messages_NAME] = {
        .position = {0,24,240,24+30},
        .name = "消息",
        .type = widget_type_text,
        .userParam = 0,
    },
//    [Messages_DividingLine] = {
//        .position = {10,65,240-10,65},
//        .name = "DividingLine",
//        .type = widget_type_Line,
//        .userParam = 0x095b81,//划线的颜色
//    },

    [Messages_Msg1] = {
        .position = {8,62,8+224,62+62},
        .name = "Msg1",
        .type = widget_type_msg,
        .userParam = 0,
    },
    [Messages_Msg2] = {
        .position = {8,132,8+224,132+62},
        .name = "Msg2",
        .type = widget_type_msg,
        .userParam = 1,
    },
    [Messages_Msg3] = {
        .position = {8,202,8+224,202+62},
        .name = "Msg3",
        .type = widget_type_msg,
        .userParam = 2,
    },
//    [Messages_Msg4] = {
//        .position = {15,66+55*3,240-15,66+55*4},
//        .name = "Msg4",
//        .type = widget_type_msg,
//        .userParam = 3,
//    },
//    [Messages_Msg5] = {
//        .position = {0,66+52*4,240,66+52*5},
//        .name = "Msg5",
//        .type = widget_type_msg,
//        .userParam = 4,
//    },

    [Messages_SELECT] = {
            .position = {8,270,8+224,270+42},
            .name = "清除消息",
            .type = widget_type_button,
            .userParam = RGB(247,192,51),
    },
//
//    [Messages_PreviousPage] = {
//        .position = {0,60+40*5,120,320},
//        .name = "上一页",
//        .type = widget_type_button,
//        .userParam =  RGB(28,32,42),
//    },
};
struct MsgCB
{
   u32 MsgBase;
   HWND hwnd[4];
   u32 msgmax;
};
static struct MsgCB Msgcb = {0,{NULL,NULL,NULL,NULL},0xffffffff};
u32 SelectMsg_cnt;

//按钮控件创建函数
static bool_t  MessagesButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    struct  MsgCb Msg;
    const char * bmp;
    int error;
    SelectMsg_cnt = 0;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,MessagesCfgTab[Messages_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        else if(widget_type_msg == buttoninfo->type)//课程窗口
        {
//            bmp = Get_BmpBuf(BMP_Background_bmp);
//            if(bmp != NULL)
//            {
//                DrawBMP(hdc,-buttoninfo->position.left,-buttoninfo->position.top,bmp);
//            }

           Msg =  Get_MsgNameOfcnt(Msgcb.MsgBase+buttoninfo->userParam,&error);
           if(!error)
           {
               SetFillColor(hdc,RGB(255,225,147));
               FillRect(hdc,&rc);
//               rc.left+=8;
               rc.top+=8;
               SetTextColor(hdc,RGB(178,178,178));
               DrawText(hdc,"通知",-1,&rc,DT_LEFT|DT_TOP);
               struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
               struct Charset* Charsetbak = SetCharset(hdc,myCharset);
               rc.left+=93;
               DrawText(hdc,Msg.time,-1,&rc,DT_LEFT|DT_TOP);
               rc.left-=93;
               rc.top +=27;
               SetTextColor(hdc,RGB(28,32,42));
//               DrawText(hdc,Msg.msg,-1,&rc,DT_LEFT|DT_TOP);
               size_t len = strlen(Msg.msg) + 20;
               char * str = malloc(len);
               if(str != NULL)
               {
                   Utf8_Typesetting_2(Msg.msg,str,len,13);
                   struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                   struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                   DrawText(hdc,str,-1,&rc,DT_TOP|DT_LEFT);
                   SetCharset(hdc,Charsetbak);
               }
               if(Msg.newmsg)
               {
                   rc.top -=27;
                   rc.left+=34;
                   SetTextColor(hdc,RGB(255,0,0));
                   DrawText(hdc,"NEW",-1,&rc,DT_LEFT|DT_TOP);
               }
               SetCharset(hdc,Charsetbak);
           }else {
               SetFillColor(hdc,RGB(255,255,255));
               FillRect(hdc,&rc);
               SetTextColor(hdc,RGB(28,32,42));
               if(buttoninfo->userParam == 0)
               {
                   if(Msgcb.MsgBase)
                   {
                       DrawText(hdc,"这是最后一页，请往下滑！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }else{
                       DrawText(hdc,"当前暂时没有消息！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }
               }

           }
           if(error == -200)
           {
               if(Msgcb.msgmax == 0xffffffff)
                   Msgcb.msgmax = Msgcb.MsgBase+buttoninfo->userParam;
           }

        }
        else if((0==strcmp(buttoninfo->name,MessagesCfgTab[Messages_SELECT].name)))
        {
            SetTextColor(hdc,RGB(255,255,255));
//            SetFillColor(hdc,buttoninfo->userParam);
//            FillRect(hdc,&rc);
//            OffsetRect(&rc,1,1);
            Draw_Circle_Button(hdc,&rc,21,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }

    //  UpdateDisplay(0);
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static bool_t Update_MessagesMsg(void)
{
    u8 i;
    for(i=0;i<sizeof(Msgcb.hwnd)/sizeof(HWND);i++)
    {
        if(Msgcb.hwnd[i] !=NULL)
            PostMessage(Msgcb.hwnd[i],MSG_PAINT,0,0);
    }
    return true;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Msg(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    //消息处理函数表
    Msgcb.msgmax = 0xffffffff;
    Msgcb.hwnd[0] = NULL;
    Msgcb.hwnd[1] = NULL;
    Msgcb.hwnd[2] = NULL;
    Msgcb.hwnd[3] = NULL;
//    Msgcb.hwnd[4] = NULL;
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, MessagesButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (MessagesCfgTab[i].type)
        {
            case  widget_type_msg :
                hwndButton = CreateButton(MessagesCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             MessagesCfgTab[i].position.left, MessagesCfgTab[i].position.top,\
                             RectW(&MessagesCfgTab[i].position),RectH(&MessagesCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&MessagesCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
                     Msgcb.hwnd[MessagesCfgTab[i].userParam] = hwndButton;
//                     Widget_SetAttr(hwndButton,ENUM_WIDGET_HYALINE_COLOR,CN_COLOR_BLACK);
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(MessagesCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         MessagesCfgTab[i].position.left, MessagesCfgTab[i].position.top,\
                         RectW(&MessagesCfgTab[i].position),RectH(&MessagesCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&MessagesCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Msg(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (MessagesCfgTab[i].type)
            {
                case  type_background :
                    SetFillColor(hdc,MessagesCfgTab[i].userParam);
                    FillRect(hdc,&MessagesCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(MessagesCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,MessagesCfgTab[i].position.left,\
                                MessagesCfgTab[i].position.top,&MessagesCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&MessagesCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    DrawText(hdc,PowerProgressbar.text,-1,&MessagesCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(MessagesCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    DrawText(hdc,MessagesCfgTab[i].name,-1,&MessagesCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType HmiNotify_Message(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;

    u16 event,id;
    s16 x,y;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);
        printf("HmiNotify_TodayHomeWork,x= %d,y=%d\r\n",x,y);

        if(abs(y) > abs(x))
        {
            if(y >0){
                //下一页
                printf("TodayHomeWork_NextPage\r\n");
                if(Msgcb.msgmax > (Msgcb.MsgBase+sizeof(Msgcb.hwnd)/sizeof(HWND)))
                {
                    Msgcb.MsgBase+=sizeof(Msgcb.hwnd)/sizeof(HWND);
                    Update_MessagesMsg();
                }
            }
            else{
                //上一页
                printf("TodayHomeWork_PreviousPage\r\n");
                if(Msgcb.MsgBase>sizeof(Msgcb.hwnd)/sizeof(HWND))
                    Msgcb.MsgBase-=sizeof(Msgcb.hwnd)/sizeof(HWND);
                else
                    Msgcb.MsgBase=0;
                Update_MessagesMsg();
            }
        }
    }else if(event==MSG_BTN_UP){
        switch (id)
        {
            case  Messages_BACK       :
                NextUi = WIN_Main_WIN;
                break;
            case  Messages_Msg1      :
            case  Messages_Msg2      :
            case  Messages_Msg3      :
//            case  Messages_Msg4      :
//            case  Messages_Msg5      :
                if(Msgcb.msgmax > (Msgcb.MsgBase + MessagesCfgTab[id].userParam))
                {
                    SelectMsg_cnt = Msgcb.MsgBase + MessagesCfgTab[id].userParam;
                    Read_MsgNameOfcnt(SelectMsg_cnt);
                    NextUi = WIN_Messages_delete;
                }
                break;
            case  Messages_SELECT    :
                NextUi = WIN_Messages_Select;
                break;
//            case  Messages_PreviousPage:
//                if(Msgcb.MsgBase>sizeof(Msgcb.hwnd)/sizeof(HWND))
//                    Msgcb.MsgBase-=sizeof(Msgcb.hwnd)/sizeof(HWND);
//                else
//                    Msgcb.MsgBase=0;
//                Update_MessagesMsg();
//                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_Messages()
{
    return Register_NewWin(WIN_Messages,HmiCreate_Msg,HmiPaint_Msg,HmiNotify_Message);
}

