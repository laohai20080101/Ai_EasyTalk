/*
 * EngLishPlaza.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include "align.h"

static struct GuiWinCb winCtrl[WIN_Max];
/*static*/ enum WinType eng_CurrentWindow = WIN_Main_WIN;
/*static*/ enum WinType eng_CurrentWindowbak = WIN_Main_WIN;

static bool_t PoweringFlag = true;

enum WinType Get_SelectionWinType()
{
    return eng_CurrentWindow;
}

//更新完作业
bool_t Refresh_FromUpdateServer()
{
    if(WIN_NetLoading != Get_SelectionWinType())
        return false;

    eng_CurrentWindow = Get_NextWinType();
    if(eng_CurrentWindow != WIN_NotChange)
    {
        eng_CurrentWindowbak = eng_CurrentWindow;
        Refresh_GuiWin();
    }
    return true;
}

extern void Set_UpdateWinType(enum WinType state);
bool_t Set_CheckUpdateWin(void)
{
    Set_UpdateWinType(eng_CurrentWindow);
    eng_CurrentWindow = WIN_Check_Update;
    if(eng_CurrentWindow != WIN_NotChange)
    {
        eng_CurrentWindowbak = WIN_Check_Update;
        Refresh_GuiWin();
    }
    return true;
}

bool_t Refresh_SoftUpdateWin(void)
{
    eng_CurrentWindow = WIN_Update_Proram;
    if(eng_CurrentWindow != WIN_NotChange)
    {
        eng_CurrentWindowbak = WIN_Update_Proram;
        Refresh_GuiWin();
    }
    return true;
}

bool_t Refresh_SubmitStatueWin(void)
{
    eng_CurrentWindow = WIN_SubmitStatue;
    if(eng_CurrentWindow != WIN_NotChange)
    {
        eng_CurrentWindowbak = WIN_SubmitStatue;
        Refresh_GuiWin();
    }
    return true;
}
bool_t Refresh_MainWin(void)
{
    eng_CurrentWindow = WIN_Main_WIN;
    if(eng_CurrentWindow != WIN_NotChange)
    {
        eng_CurrentWindowbak = WIN_Main_WIN;
        Refresh_GuiWin();
    }
    return true;
}

bool_t SwitchToFriendlyReminders(enum enumFriendlyReminders Tips)
{
    if(eng_CurrentWindow == WIN_FriendlyReminders)
        return false;
    if(eng_CurrentWindow != eng_CurrentWindowbak)
        return false;

    bool_t Set_BrightStatusOpenScreen();
    Set_BrightStatusOpenScreen(); //开屏幕
    int Set_SelectionFriendlyReminders(enum enumFriendlyReminders  Tips,enum WinType switchfrom);
    Set_SelectionFriendlyReminders(Tips,eng_CurrentWindow);
    eng_CurrentWindow = WIN_FriendlyReminders;
    eng_CurrentWindowbak = WIN_FriendlyReminders;
    Refresh_GuiWin();
    return true;
}


#include "shell.h"
bool_t switchtest(char *param)
{

    char *word_addr,*next_param;
    word_addr = shell_inputs(param,&next_param);
    int key = strtoul(word_addr, (char **)NULL, 0);
    switch (key) {
        case 1: SwitchToFriendlyReminders(enum_Insufficient_buffer); break;
        case 2: SwitchToFriendlyReminders(enum_buf_overflow); break;
        case 3: SwitchToFriendlyReminders(enum_warning_Submit_homework); break;
        case 4: SwitchToFriendlyReminders(enum_configure_network); break;
        case 5: SwitchToFriendlyReminders(enum_Recording_upload_failed); break;
        case 6: SwitchToFriendlyReminders(enumFriendlyReminders_max); break;
        default:            break;
    }
    return true;
}
ADD_TO_ROUTINE_SHELL(test,switchtest,"switch test ");

extern struct WinTimer *tg_pCloseBrightTimer,*tg_pPowerDownTimer,*tg_pPoweringTimer;
//绘制图标
void Draw_Icon(HDC hdc,s32 x,s32 y,const RECT *rc,const char *bmp)
{
    struct RectBitmap bm;
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 1, 0  };

    bm.reversal = true;
    bm.PixelFormat =CN_SYS_PF_RGB565;
    bm.width  =rc->right - rc->left;
    bm.height =rc->bottom- rc->top;
    bm.linebytes =align_up(4,bm.width*2);
    bm.ExColor =(ptu32_t)0;
    bm.bm_bits =(u8*)bmp + 70;

//    printf("width = %d,height = %d,linebytes = %d\n\r",bm.width,bm.height,bm.linebytes);
    DrawBitmap(hdc,x,y,&bm,RGB(255,223,115),RopCode);
}

//绘制图标
void Draw_Icon_2(HDC hdc,s32 x,s32 y,s32 width,s32 height,const char *bmp)
{
    struct RectBitmap bm;
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 1, 0  };

    bm.reversal = true;
    bm.PixelFormat =CN_SYS_PF_RGB565;
    bm.width  =width;
    bm.height =height;
    bm.linebytes =align_up(4,bm.width*2);
    bm.ExColor =(ptu32_t)0;
    bm.bm_bits =(u8*)bmp + 70;

//    printf("width = %d,height = %d,linebytes = %d\n\r",bm.width,bm.height,bm.linebytes);
    DrawBitmap(hdc,x,y,&bm,RGB(255,223,115),RopCode);
}

void Draw_Circle_Button(HDC hdc,RECT *rc,s32 r,u32 color)
{
    RECT rc_rect;

    rc_rect = *rc;
    SetFillColor(hdc,color);

    rc_rect.left += r;
    rc_rect.right =rc_rect.right-r-2;
    FillRect(hdc,&rc_rect);

    FillCircle(hdc, rc->left+r, rc->top+r,r);
    FillCircle(hdc, rc->right-r-2,rc->top+r,r);
}

bool_t HWD_IsPlayOrRecord();
bool_t CWR_IsPlayOrRecord();
bool_t FCWR_IsPlayOrRecord();
bool_t Check_IsPlayOrRecord()
{
    int ret = false;
    switch (eng_CurrentWindow) {
        case WIN_HWL_Course_Work_Dictation:
            ret = HWD_IsPlayOrRecord();
            break;
        case WIN_HWL_Course_Work_Recite:
            ret = CWR_IsPlayOrRecord();
            break;
        case WIN_FHW_Course_Work_Recite:
            ret = FCWR_IsPlayOrRecord();
            break;
        default:    break;
    }
    return ret;
}

//主窗口消息分发
bool_t HmiNotify_EasyTalk(struct WindowMsg *pMsg)
{
    enum WinType NextWin;
    static u8 left_flag=0;
    static s64 timebak;

    GDD_ResetTimer(tg_pCloseBrightTimer,30000);
    GDD_ResetTimer(tg_pPowerDownTimer,600000);
    if(PoweringFlag)
    {
        PoweringFlag = false;
        GDD_ResetTimer(tg_pPoweringTimer,60000);
    }
    if((eng_CurrentWindow >= WIN_Max)||(winCtrl[eng_CurrentWindow].DoMsg==NULL))
    {
        eng_CurrentWindow = WIN_Main_WIN;
        return false;
    }
//=============================屏蔽误触发=======================================
    u16 event = HI16(pMsg->Param1);
    if(event==MSG_BTN_PEN_MOVE)
    {
        if((left_flag == 1)&&(DjyGetSysTime()-timebak<50*mS))
            return true;
        left_flag = 1;
    }

    if(left_flag && event==MSG_BTN_UP)
    {
        timebak =  DjyGetSysTime();
        left_flag = 0;
        return true;
    }
    //move 消息之后短时间不响应按下弹起消息
    if(((DjyGetSysTime() - timebak) < 50*mS)&&\
        ((event==MSG_BTN_PEN_MOVE)||(event==MSG_BTN_UP)||(event==MSG_BTN_DOWN)))
    {
        return true;
    }
//==============================================================================
    NextWin = winCtrl[eng_CurrentWindow].DoMsg(pMsg);
    if(NextWin != WIN_NotChange)
    {
        eng_CurrentWindowbak  = NextWin;
        DestroyAllChild(pMsg->hwnd);      //删除当前控件。
        PostMessage(pMsg->hwnd, MSG_REFRESH_UI, NextWin, 0);
    }
    return true;
}

//主窗口界面更新
bool_t HmiRefresh(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u32 recreat;
    const char * bmp ;
    RECT rc;

    hwnd =pMsg->hwnd;
    HDC hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        recreat = pMsg->Param2;
        eng_CurrentWindow = (enum WinType)pMsg->Param1;
        if(!((recreat == CN_RECREAT_WIDGET)&&(winCtrl[eng_CurrentWindow].HmiCreate != NULL)))//容错
        {
            eng_CurrentWindow = WIN_Main_WIN;
        }

        if(eng_CurrentWindow == WIN_Main_WIN)
        {
            bmp = Get_BmpBuf(BMP_Background_bmp);
            if(bmp != NULL)
            {
                DrawBMP(hdc,0,0,bmp);
            }
        }else if((eng_CurrentWindow != WIN_Check_Update)&&(eng_CurrentWindow != WIN_Check_None) \
                &&(eng_CurrentWindow != WIN_Update_Proram)&&(eng_CurrentWindow != WIN_Uploading)\
                &&(eng_CurrentWindow != WIN_RecordSubmitFail)&&(eng_CurrentWindow != WIN_PowerLower)){
           SetFillColor(hdc,RGB(255,255,255));
           FillRect(hdc,&rc);
        }

        if((recreat == CN_RECREAT_WIDGET)&&(winCtrl[eng_CurrentWindow].HmiCreate != NULL))
            winCtrl[eng_CurrentWindow].HmiCreate(pMsg);//创建本界面各控件

        if(winCtrl[eng_CurrentWindow].HmiPaint != NULL)
            winCtrl[eng_CurrentWindow].HmiPaint(pMsg); //绘制各控件

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//界面刷新
bool_t Refresh_SwitchWIn(HWND hwnd)
{
    DestroyAllChild(hwnd);      //删除当前控件。

    if(eng_CurrentWindowbak != eng_CurrentWindow)
        printf("error Refresh_SwitchWIn \n\r");

    PostMessage(hwnd, MSG_REFRESH_UI, eng_CurrentWindow, 0);
    return true;

}


//注册所有的界面
int Init_WinSwitch()
{
   Register_Main_WIN();
   Register_HomeWorkLists();
   Register_HWL_Course();
   Register_HWL_Course_Work();
   Register_HWL_Course_Work_Recite();
   Register_FHW_Course_Work_Recite();
   Register_HWL_Course_Work_Dictation();
   Register_HWL_Submit();
   Register_EngLishPlaza();
   Register_TodayHomeWork();
   Register_THW_Course();
   Register_THW_Course_Work();
   Register_Messages();
   Register_MessagesSelect();
   Register_Messages_delete();
   Register_Settings();
   Register_Check_Update();
   Register_Update_Proram();
   Register_Check_None();
   Register_Settings_WifiConnect();
   Register_Settings_TurnOff();
   Register_Settings_DeviceBinding();
   Register_NetLoading();
   Register_Uploading();
   Register_RecordSubmitFail();
   Register_PowerLower();
   Register_FriendlyReminders();
   Register_SubmitStatue();
   return 0;
}


//注册新的界面
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg)
{
    winCtrl[id].HmiCreate = HmiCreate;
    winCtrl[id].HmiPaint = HmiPaint;
    winCtrl[id].DoMsg = DoMsg;
    return 0;
}

int print_Wininfo()
{
    int flag = 0;
    printf("eng_CurrentWindow %d  eng_CurrentWindowbak %d \n\r",eng_CurrentWindow,eng_CurrentWindowbak);
    for(int i=0;i<WIN_Max;i++)
    {
        flag = 0;
        if((winCtrl[i].HmiCreate != NULL))
            flag |= 1;
        if(winCtrl[i].HmiPaint != NULL)
            flag |=2;
        if(winCtrl[i].DoMsg != NULL)
            flag |=2;

        printf("%d : flag: %d   \n\r",i,flag);
    }
    return 0;

}
