//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * BmpInfo.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *
 *  功能:对要显示的Bmp文件集中管理，提供获取获取相应Bmp文件的接口
 *
 */
#define  GUI_INFO_FILE_C_
#include "../inc/GuiInfo.h"
#include "../inc/ServerInfo.h"
#include "../inc/WinSwitch.h"
#include <gdd_widget.h>
#include <stddef.h>
#include "stdio.h"
#include "cJSON.h"
#include "string.h"
#include "stdlib.h"
#include "device_user.h"
#include "board.h"
#include <app_flash.h>
#include <dbug.h>
#include <minilzo.h>
#include "recorder.h"
#include "../inc/ServerEvent.h"
#include "../Homework/homeWork.h"

//============================所有压缩图片的文件数组======================================
static const char ArrowLeft_bmp[] = {
#include "../PictureMaterial/ArrowLeft_bmp"
};
static const char ArrowRight_bmp[] = {
#include "../PictureMaterial/ArrowRight_bmp"
};
//static const char MainInterface_bmp[] = {//开机界面图
//#include "../PictureMaterial/MainInterface_bmp"
//};

//static const char MainInterface_bmp_uncompression[] = {//未压缩，能正常显示的开机界面图
//#include "../PictureMaterial/MainInterface_bmp_djy"
//};
static const char PowerLogo1_bmp[] = {//冲电1%标志
#include "../PictureMaterial/PowerLogo_1_bmp"
};
static const char PowerLogo25_bmp[] = {//冲电25%标志
#include "../PictureMaterial/PowerLogo_25_bmp"
};
static const char PowerLogo50_bmp[] = {////冲电50%标志
#include "../PictureMaterial/PowerLogo_50_bmp"
};
static const char PowerLogo75_bmp[] = {////冲电75%标志
#include "../PictureMaterial/PowerLogo_75_bmp"
};
static const char PowerLogo100_bmp[] = {////冲电100%标志
#include "../PictureMaterial/PowerLogo_100_bmp"
};
static const char Powering25_bmp[] = {//充电中25%
#include "../PictureMaterial/Powering_25_bmp"
};
static const char Powering50_bmp[] = {//充电中50%
#include "../PictureMaterial/Powering_50_bmp"
};
static const char Powering75_bmp[] = {//充电中75%
#include "../PictureMaterial/Powering_75_bmp"
};
static const char Powering100_bmp[] = {//充电中100%
#include "../PictureMaterial/Powering_100_bmp"
};
static const char TodayHomeLogo_bmp[] = {
#include "../PictureMaterial/TodayHomeLogo_bmp"
};
static const char HomeWorkLogo_bmp[] = {
#include "../PictureMaterial/HomeWorkLogo_bmp"
};
static const char EngLishPlazaLogo_bmp[] = {
#include "../PictureMaterial/EngLishPlazaLogo_bmp"
};
static const char MessagesLogo_bmp[] = {
#include "../PictureMaterial/MessagesLogo_bmp"
};
static const char SetLogo_bmp[] = {
#include "../PictureMaterial/SetLogo_bmp"
};
//static const char NetLoading_bmp[] = {//网络加载背景图
//#include "../PictureMaterial/NetLoading_bmp"
//};
static const char WifiConnecting_bmp[] = {//请wifi配置背景图
#include "../PictureMaterial/WifiConnecting_bmp"
};
static const char Close_bmp[] = {//关闭背景图
#include "../PictureMaterial/Close_bmp"
};
static const char Background_bmp[] = {//背景Bmp格式文件
#include "../PictureMaterial/Background_bmp"
};
static const char WifiConnected_bmp[] = {
#include "../PictureMaterial/WifiConnected_bmp"
};
static const char WifiDisconnected_bmp[] = {
#include "../PictureMaterial/WifiDisconnected_bmp"
};
static const char HomeWorkLists_bmp[] = {
#include "../PictureMaterial/HomeWorkLists_bmp"
};
static const char EngLishPlaza_bmp[] = {
#include "../PictureMaterial/EngLishPlaza_bmp"
};
static const char TodayHomeWork_bmp[] = {
#include "../PictureMaterial/TodayHomeWork_bmp"
};
static const char Messages_bmp[] = {
#include "../PictureMaterial/Messages_bmp"
};
static const char Settings_bmp[] = {
#include "../PictureMaterial/Settings_bmp"
};

static const char ClassLogo_bmp[] = {
#include "../PictureMaterial/ClassLogo_bmp"
};
static const char GoBack_bmp[] = {
#include "../PictureMaterial/GoBack_bmp"
};
static const char BookLogo_bmp[] = {
#include "../PictureMaterial/BookLogo_bmp"
};
static const char UpgradeSetLogo_bmp[] = {
#include "../PictureMaterial/UpgradeSetLogo_bmp"
};
static const char Device_BindingLogo_bmp[] = {
#include "../PictureMaterial/Device_BindingLogo_bmp"
};
static const char CheckFileLogo_bmp[] = {
#include "../PictureMaterial/CheckFileLogo_bmp"
};
static const char StarDown_bmp[] = {
#include "../PictureMaterial/StarDown_bmp"
};
static const char StarUp_bmp[] = {
#include "../PictureMaterial/StarUp_bmp"
};
static const char TurnOffLogo_bmp[] = {
#include "../PictureMaterial/TurnOffLogo_bmp"
};
static const char WifiSetLogo_bmp[] = {
#include "../PictureMaterial/WifiSetLogo_bmp"
};
static const char Record_bmp[] = {
#include "../PictureMaterial/record_bmp"
};
//static const char Record_2_bmp[] = {
//#include "../PictureMaterial/record_2_bmp"
//};
static const char PlayPause_bmp[] = {
#include "../PictureMaterial/play_pause_bmp"
};
static const char PlayPause_2_bmp[] = {
#include "../PictureMaterial/play_pause_2_bmp"
};
static const char PlayPauseStop_bmp[] = {
#include "../PictureMaterial/play_pause_stop_bmp"
};

static const char PlayPauseBig_bmp[] = {
#include "../PictureMaterial/play_pause_big_bmp"
};
static const char PlayPauseBigStop_bmp[] = {
#include "../PictureMaterial/play_pause_big_stop_bmp"
};
static const char Volume_bmp[] = {
#include "../PictureMaterial/volume_bmp"
};
static const char VolumeStop_bmp[] = {
#include "../PictureMaterial/volume_stop_bmp"
};
static const char RecordDown_bmp[] = {
#include "../PictureMaterial/RecordDown_bmp"
};
//static const char HomeWorkBasemap_bmp[] = {
//#include "../PictureMaterial/HomeWorkBasemap_bmp"
//};
static const char Round_bmp[] = {
#include "../PictureMaterial/Round_bmp"
};
static const char RoundDot_bmp[] = {
#include "../PictureMaterial/RoundDot_bmp"
};
static const char Loading_bmp[] = {
#include "../PictureMaterial/Loading_bmp"
};
static const char Submitting_bmp[] = {
#include "../PictureMaterial/Submitting_bmp"
};
static const char SubmitSuccess_bmp[] = {
#include "../PictureMaterial/SubmitSuccess_bmp"
};
static const char SubmitFail_bmp[] = {
#include "../PictureMaterial/SubmitFail_bmp"
};
static const char VoicePlay_bmp[] = {
#include "../PictureMaterial/VoicePlay_bmp"
};
static const char WorkingHard_bmp[] = {
#include "../PictureMaterial/WorkingHard_bmp"
};
static const char VoiceStop_bmp[] = {
#include "../PictureMaterial/VoiceStop_bmp"
};

#define PICTURES_TOTAL_NUMBER   45         //所有要显示的图片总数
#define UNCOMPRESS_PICTURES_BUF_SIZE   1*1024*1024     //除开机界面外，解压后图片存在PSRAM中需要的缓冲区大小,根据图片大小自行调节
//#define MAININTERFACE_UNCOMPRESS_BUF_SIZE   200*1024//开机界面，解压后图片存在PSRAM中需要的缓冲区大小,根据图片大小自行调节
u32 PictureDateAddr[PICTURES_TOTAL_NUMBER]; //存所有解压后图片在PSRAM中的地址

static bool_t PowerLowerFlag = false;
static bool_t WifiConnectedflag = false;   //wifi链接标志
//static char Powertxtbuf[8] = {'2','5','%',0,0,0,0,0};//电量
struct WinTime GuiInfoTime = {
        .hour = 12,
        .minute = 20,
};
static u32 GuiInfoPower = 25;//电量占比 0~100
static char Timetxtbuf[16] = {'1','2',':','1','0',0,0,0,0,0,0,0,0,0,0,0};//时间
//PROGRESSBAR_DATA PowerProgressbar = //电量进度条
//{
//    .Flag    =PBF_SHOWTEXT|PBF_ORG_LEFT,
//    .Range   =100,
//    .Pos     = 25,
//    .FGColor = CN_COLOR_GREEN,
//    .BGColor = CN_COLOR_WHITE,
//    .TextColor =CN_COLOR_WHITE,
//    .DrawTextFlag =DT_VCENTER|DT_CENTER,
//    .text = Powertxtbuf,
//};
//HWND Powerhwnd;

extern bool_t IsCharge(void);
extern void deep_sleep(void);

extern  WIFI_CFG_T  LoadWifi;
bool_t Get_PowerFlag(void)
{
    return PowerLowerFlag;
}
void Set_PowerFlag(bool_t statue)
{
    PowerLowerFlag = statue;
}
/*****************************************************************************
 *  功能：更新wifi状态
 *  参数：flag ：false wifi未连接  true wifi已连接
 *  返回：
 *****************************************************************************/
bool_t Wifi_Connectedflag(u8 flag)
{
    WifiConnectedflag = flag;
    Refresh_GuiWin();
    return true;
}
bool_t Get_Wifi_Connectedflag()
{
    return WifiConnectedflag;
}

/*****************************************************************************
 *  功能：获取家庭作业背景
 *  参数：Course ：班级编号
 *        work ： 作业编号
 *        cnt ： 作业题目编号
 *  返回：对应图片的bmp数组、没有返回NULL
 *****************************************************************************/
//const char* Get_HomeWorkBasemap_bmp(u32 Course,u32 work,u32 cnt)
//{
//    (void) Course;    (void) work;    (void) cnt;
//
//    return HomeWorkBasemap_bmp;
//
//}

/*****************************************************************************
 *  功能：获取家庭作业背景
 *  参数：Course ：班级编号
 *        work ： 作业编号
 *        cnt ： 作业题目编号
 *  返回：对应图片的bmp数组、没有返回NULL
 *****************************************************************************/
const char * Get_BmpBuf(enum Bmptype type)
{
    switch (type)
    {
        case BMP_Close_bmp   :
          return Close_bmp;
        case BMP_ArrowLeft_bmp   :
          return ArrowLeft_bmp;
       case BMP_ArrowRight_bmp   :
          return ArrowRight_bmp;
//       case BMP_MainInterface_bmp   :
//          return MainInterface_bmp;
       case BMP_TodayHomeLogo_bmp   :
          return TodayHomeLogo_bmp;
       case BMP_HomeWorkLogo_bmp   :
          return HomeWorkLogo_bmp;
       case BMP_EngLishPlazaLogo_bmp   :
          return EngLishPlazaLogo_bmp;
       case BMP_MessagesLogo_bmp   :
          return MessagesLogo_bmp;
       case BMP_SetLogo_bmp   :
          return SetLogo_bmp;
       case BMP_NetLoading_bmp   :
           if(LoadWifi.statue != 0x55)
           {
               return WifiConnecting_bmp;
           }else{
               return Loading_bmp;
           }
        case BMP_Background_bmp   :
            return Background_bmp;
        case BMP_HomeWorkLists_bmp:
            return HomeWorkLists_bmp;
        case BMP_EngLishPlaza_bmp :
            return EngLishPlaza_bmp;
        case BMP_TodayHomeWork_bmp:
            return TodayHomeWork_bmp;
        case BMP_Messages_bmp     :
            return Messages_bmp;
        case BMP_Settings_bmp     :
            return Settings_bmp;
        case BMP_WIFI             :
            if(WifiConnectedflag)
            {
                return WifiConnected_bmp;
            }else{
                return WifiDisconnected_bmp;
            }
        case BMP_PowerLogo_bmp   :
            if(IsCharge() == false)
            {
                if(current_power() >= 100)
                    return PowerLogo100_bmp;
                else if(current_power() >= 75)
                    return PowerLogo75_bmp;
                else if(current_power() >= 50)
                    return PowerLogo50_bmp;
                else if(current_power() >= 25)
                    return PowerLogo25_bmp;
                else
                    return PowerLogo1_bmp;
            }else{
                if(current_power() >= 100)
                    return Powering100_bmp;
                else if(current_power() >= 75)
                    return Powering75_bmp;
                else if(current_power() >= 50)
                    return Powering50_bmp;
                else
                    return Powering25_bmp;
            }
        case BMP_courseLogo_bmp   :
            return ClassLogo_bmp;
        case BMP_GoBack_bmp   :
            return GoBack_bmp;
        case BMP_BookLogoDown_bmp   :
            return BookLogo_bmp;
        case BMP_UpgradeSetLogo_bmp  :
            return UpgradeSetLogo_bmp;
        case BMP_Device_BindingLogo_bmp :
            return Device_BindingLogo_bmp;
        case BMP_CheckFileLogo_bmp  :
            return CheckFileLogo_bmp;
        case BMP_StarDown_bmp       :
            return StarDown_bmp;
        case BMP_StarUp_bmp         :
            return StarUp_bmp;
        case BMP_TurnOffLogo_bmp    :
            return TurnOffLogo_bmp;
        case BMP_WifiSetLogo_bmp    :
            return WifiSetLogo_bmp;
        case BMP_Record_bmp    :
            return Record_bmp;
//        case BMP_Record_2_bmp    :
//            return Record_2_bmp;
        case BMP_PlayPause_bmp    :
            return PlayPause_bmp;
        case BMP_PlayPause_2_bmp    :
            return PlayPause_2_bmp;
        case BMP_PlayPauseStop_bmp    :
             return PlayPauseStop_bmp;
        case BMP_PlayPauseBig_bmp    :
            return PlayPauseBig_bmp;
        case BMP_PlayPauseBigStop_bmp    :
            return PlayPauseBigStop_bmp;
        case BMP_Volume_bmp    :
            return Volume_bmp;
        case BMP_VolumeStop_bmp    :
            return  VolumeStop_bmp;
        case BMP_RecordDown_bmp    :
            return RecordDown_bmp;
        case BMP_Round_bmp    :
            return Round_bmp;
        case BMP_RoundDot_bmp    :
            return RoundDot_bmp;
        case BMP_Submitting_bmp    :
            return Submitting_bmp;
        case BMP_SubmitSuccess_bmp    :
            return SubmitSuccess_bmp;
        case BMP_SubmitFail_bmp    :
            return  SubmitFail_bmp;
        case BMP_VoicePlay_bmp    :
            return VoicePlay_bmp;
        case BMP_WorkingHard_bmp    :
            return WorkingHard_bmp;
        case BMP_VoiceStop_bmp    :
            return VoiceStop_bmp;
        default:      break;
    }
    return NULL;
}
//void MainInterface(void)
//{
//    void DispMainInterface(unsigned char *pic);
//   DispMainInterface((unsigned char *)Get_BmpBuf(BMP_MainInterface_bmp));
//}
/**
 * 设置电量占比
 */
bool_t Set_MainWinPower(u32 Percentage)
{
    if(Percentage>100)
        return false;
    GuiInfoPower = Percentage;
//    PowerProgressbar.Pos = GuiInfoPower;
    printf("IsCharge()= %d, Percentage = %d\r\n",IsCharge(),Percentage);
    if(GuiInfoPower <= 1 && PowerLowerFlag==false && IsCharge() == false)
    {
        PowerLowerFlag = true;
//        Power_Lower();
    }
    Refresh_GuiWin();

    return true;
}
//获取电量
//u32 Win_GetPower()
//{
//    u32 powerCnt;
//    powerCnt = current_power();
//    printf(">powerCnt = %d\r\n",powerCnt);
//    return powerCnt;
//}

//获取窗口界面时间
char*  Win_GetTime()
{
    return Timetxtbuf;
}
/**
 * 设置界面时间
 */
bool_t Win_SetTime(struct WinTime time)
{
    printf("Win_SetTime  time.minute = %d\r\n",time.minute);
    if((time.hour<24)&&(time.minute<60))
    {
        GuiInfoTime = time;
        sprintf(Timetxtbuf,"%02d:%02d",time.hour,time.minute);
        Refresh_GuiWin();
        return true;
    }
    return false;
}

int SetClock(int hour, int min)
{
    struct WinTime t;
    t.hour = hour;
    t.minute = min;
    return Win_SetTime(t);
}

//=========================管理信息=========================================

/*
 *  删除第cnt条消息
 * */
bool_t Del_MsgNameOfcnt(u32 cnt)
{
    para_Del_MsgNameOfcnt para;
    para.cnt = cnt;
    return   Server_SendComd(cmd_Del_MsgNameOfcnt,&para,sizeof(para),100*mS);
}

bool_t Del_MsgNameOfBuf(u8 *buf,int len)
{
    return   Server_SendComd(cmd_Del_MsgNameOfBuf,buf,len,100*mS);

}
/*
 * 消息已读
 * */
bool_t Read_MsgNameOfcnt(u32 cnt)
{

    para_Read_MsgNameOfcnt para;
    para.cnt = cnt;
    return   Server_SendComd(cmd_Read_MsgNameOfcnt,&para,sizeof(para),100*mS);
}
/*
 * 获取第cnt条消息
 * */
struct  MsgCb Get_MsgNameOfcnt(u32 cnt,int *error)
{
    struct  MsgCb  mcb = {0};
    struct MessagesCB *pmsg = Get_MessagesOfCnt(cnt);
    if(pmsg == NULL)
    {
        *error = -200;//ID溢出
        return mcb;
    }

    mcb.msg = pmsg->content;
    mcb.time = pmsg->time;
    if(pmsg->read)
        mcb.newmsg = false;
    else
        mcb.newmsg = true;
    *error = 0;
    return mcb;
}


/*==============================================================================
 *
 *
 *
 * ===========================================================================*/
int Get_AudioLinkAddress(char**linkbuf,int bufnum)
{
    int ret = 0;
    struct HWorkCB* hwcb = Get_SelectHmoeWork();
    if(hwcb==NULL)
        goto Error_return;

    ret = Get_HomeWorkAllAudioLink(hwcb,linkbuf,bufnum);
    return ret;
Error_return:
    return ret;
}

struct  workcb Get_TodayHomeWorkCourseWorkOfcnt(u32 work,int *error)
{
    struct  workcb WCB = {0};
    struct HWorkCB*hwcb;
    struct classCB* pclass = Get_SelectClass();
    if(pclass == NULL)
        goto Error_return;
    if(Is_TodayHomeWorkType())
    {
        hwcb = Get_TodayHomeWorkOfCnt(pclass,work);
        if(hwcb==NULL)
        {
            *error = -1;
            return WCB;
        }
    }
    else
    {
        hwcb = Get_PastHomeWorkOfId(pclass,work);
        if(hwcb)
        {
            *error = -2;
            return WCB;
        }
    }

    WCB.work = hwcb->title;
    WCB.score = hwcb->score;
    WCB.type = hwcb->type;
    WCB.time = hwcb->time;

    switch (hwcb->status)
    {
    case 1:
        WCB.state = "未完成";
        WCB.complete = 0;
        break;
    case 2:
        WCB.state = "未批改";
        WCB.complete = 1;
        break;
    case 3:
        WCB.state = "已批改";
        WCB.complete = 2;
        break;
        default:
        WCB.state = "状态：未知";
            break;
    }
    WCB.is_read = 1;
    WCB.num = (u32)Get_HomeWorkTopicNUm(hwcb);

    *error = 0;
    return WCB;

Error_return:
    *error = -200;
    return WCB;
}

//================================work==========================================

bool_t Strat_PlayDetailsAudio(PlayMediaCallback fun)
{
    para_Details_PlayAudioStart para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.fun      = fun;
    return Server_SendComd(cmd_Details_PlayAudioStart,&para,sizeof(para),100*mS);
}
bool_t Stop_PlayDetailsAudio()
{
    para_Details_PlayAudioStop para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    return Server_SendComd(cmd_Details_PlayAudioStop,&para,sizeof(para),100*mS);
}

//提交作业课程ID 作业ID
bool_t Submit_JobOfid()
{
    para_Submit_JobOfid para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    return Server_SendComd(cmd_Submit_JobOfid,&para,sizeof(para),100*mS);
}

//播放音频
bool_t Homework_PlayAudio(struct TopicCB*Topic,PlayMediaCallback fun)
{
    para_Homework_PlayAudio para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;
    para.fun = fun;
    return Server_SendComd(cmd_Homework_PlayAudio,&para,sizeof(para),100*mS);
}

//停止播放音频
bool_t Homework_PlayAudioStop(struct TopicCB*Topic)
{
    para_Homework_PlayAudioStop para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;
    return Server_SendComd(cmd_Homework_PlayAudioStop,&para,sizeof(para),100*mS);
}
//停止录音
bool_t Homework_StopRecording(struct TopicCB*Topic)
{
    para_Homework_StopRecording para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;

    return Server_SendComd(cmd_Homework_StopRecording,&para,sizeof(para),100*mS);
}
//开始录音
bool_t Homework_StartRecording(struct TopicCB*Topic)
{
    para_Homework_StartRecording para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;

    return Server_SendComd(cmd_Homework_StartRecording,&para,sizeof(para),100*mS);
}
//播放录制的音频
bool_t Homework_listenRecording(struct TopicCB*Topic,PlayMediaCallback fun)
{
    para_Homework_listenRecording para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;
    para.fun = fun;

    return Server_SendComd(cmd_Homework_listenRecording,&para,sizeof(para),100*mS);
}
//停止播放录制的音频
bool_t Homework_listenRecordingStop(struct TopicCB*Topic)
{
    para_Homework_listenRecordingStop para;
    para.class    = Get_SelectClass();
    para.homework = Get_SelectHmoeWork();
    para.Topic = Topic;

    return Server_SendComd(cmd_Homework_listenRecordingStop,&para,sizeof(para),100*mS);
}

//更新作业
bool_t Homework_HomeWorkPull(int para)
{
    return Server_SendComd(cmd_HomeWorkPull,&para,sizeof(para),100*mS);
}

//关机
int Set_ShoutDown()
{
    printf(">power down\r\n");
        deep_sleep();
    return 0;
}


//标记作业完成
int mark_TopicSendOk(char *url)
{
    int ret = 0;
    int classid;
    int homeworkid;
    int topicid;
    char idbuf[30];
    char * p = strstr(url,"/tmp_");
    if(p == NULL)
    {
        ret = -1;
        goto mark_error;
    }
    p+=strlen("/tmp_");
    if(strlen(p)+1 > sizeof(idbuf))
    {
        ret = -2;
        goto mark_error;
    }
    strcpy(idbuf,p);
    int j = 0;
    int cnt=0;
    for(int i=0;i<sizeof(idbuf);i++)
    {
        if((idbuf[i] == '_')||(idbuf[i] == '.'))
        {
            idbuf[i] = '\0';
            if (cnt == 0)
                classid = strtoul(&idbuf[j], (char **)NULL, 0);
            else if(cnt == 1)
                homeworkid = strtoul(&idbuf[j], (char **)NULL, 0);
            else if(cnt == 2)
            {
                topicid = strtoul(&idbuf[j], (char **)NULL, 0); break;
            }
            j=i+1;
            cnt++;
        }
    }

    struct classCB*pclass =  Get_ClassOfId(classid);
    if(pclass == NULL)
    {
        ret = -3;
        goto mark_error;
    }

    struct HWorkCB* phwork = Get_TodayHomeWorkOfId(pclass,homeworkid);
    if(phwork == NULL)
    {
        ret = -4;
        goto mark_error;
    }
    struct TopicCB* topic = Get_TopicOfID(phwork,topicid);
    if(topic == NULL)
    {
        ret = -5;
        goto mark_error;
    }
    Makr_Topic(topic);//标记作业已完成

    printf("home work : %d mark ok \r\n",topic->id);
    //todo : 服务器标记已完成

    return true;
mark_error:
    printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,ret);
    return ret;
}



int Pop_CodeUploadedFail(char *url)
{
    int ret = 0;
    int classid;
    int homeworkid;
    int topicid;
    char idbuf[30];
    char * p = strstr(url,"/tmp_");
    if(p == NULL)
    {
        ret = -1;
        goto mark_error;
    }
    p+=strlen("/tmp_");
    if(strlen(p)+1 > sizeof(idbuf))
    {
        ret = -2;
        goto mark_error;
    }
    strcpy(idbuf,p);
    int j = 0;
    int cnt=0;
    for(unsigned int i=0;i<sizeof(idbuf);i++)
    {
        if((idbuf[i] == '_')||(idbuf[i] == '.'))
        {
            idbuf[i] = '\0';
            if (cnt == 0)
                classid = strtoul(&idbuf[j], (char **)NULL, 0);
            else if(cnt == 1)
                homeworkid = strtoul(&idbuf[j], (char **)NULL, 0);
            else if(cnt == 2)
            {
                topicid = strtoul(&idbuf[j], (char **)NULL, 0); break;
            }
            j=i+1;
            cnt++;
        }
    }

    struct classCB*pclass =  Get_ClassOfId(classid);
    if(pclass == NULL)
    {
        ret = -3;
        goto mark_error;
    }

    struct HWorkCB* phwork = Get_TodayHomeWorkOfId(pclass,homeworkid);
    if(phwork == NULL)
    {
        ret = -4;
        goto mark_error;
    }
    struct TopicCB* topic = Get_TopicOfID(phwork,topicid);
    if(topic == NULL)
    {
        ret = -5;
        goto mark_error;
    }
    int TopicCnt = 0;
    struct TopicCB* topictmp = phwork->Topic;
    while(topictmp != NULL)
    {
        TopicCnt++;
        if(topictmp == topic)
            break;
    }
    if(topictmp != topic)
    {
        ret = -6;
        goto mark_error;
    }
    int Set_ErrorTopicCnt(int cnt);
    Set_ErrorTopicCnt(TopicCnt);
    SwitchToFriendlyReminders(enum_Recording_upload_failed);
    return true;
mark_error:
    printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,ret);
    return ret;
}

