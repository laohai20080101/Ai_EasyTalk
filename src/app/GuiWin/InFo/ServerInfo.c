//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * REServerinfo.c
 *
 *  Created on: 2019年12月13日
 *      Author: c
 *      作业信息缓存
 */
#include <stddef.h>
#include "stdio.h"
#include "cJSON.h"
#include "string.h"
#include "stdlib.h"
#include "../inc/serverinfo.h"
#include "../inc/WinSwitch.h"
#include "lock.h"
#include "device_user.h"

struct SICB{
    char fistinit;
    int msgnum;
    int objnum;
    struct MutexLCB *mutex;
    struct classCB *classhead;
    struct MessagesCB *masghead;
};
static struct SICB ServerInfCb = {0};

#if 0
int serverinfo_userprintf (const char *format, ...){(void)format;return 0;}
#define  printf       serverinfo_userprintf
#endif

//==============================================================================
#define DEBUG_SWITCH   0
#if DEBUG_SWITCH
struct DebugMalloc{
    struct DebugMalloc*next;
    u32 lraddr;
    void* addr;
};
static struct DebugMalloc *p_debughead;

int NUM_CNT = 0;
int Debug_AddMalloc(void * addr,u32 lraddr)
{
    struct DebugMalloc *tmp = p_debughead;
    struct DebugMalloc *Addr = malloc(sizeof(struct DebugMalloc));
    if(Addr==NULL)
    {
        printf("malloc error :\n\r");
        return -1;
    }
    Addr->next = NULL;
    Addr->lraddr = lraddr;
    Addr->addr = addr;
    if(p_debughead==NULL)
    {
        p_debughead = Addr;
    }else
    {
        while(tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        tmp->next = Addr;
    }
    return 0;
}

int Debug_DelMalloc(void * addr)
{
    struct DebugMalloc *tmp = p_debughead;
    struct DebugMalloc *bak = p_debughead;
    if(tmp == NULL){
        printf("p_debughead is NULL \n\r");
        return -1;
    }else if(tmp->next==NULL){
        p_debughead = NULL;
        free(tmp);
    }else if(tmp->addr == addr) {

        p_debughead= tmp->next;
        free(tmp);
    }else{
        while(tmp!=NULL)
        {
            if(tmp->addr == addr)
            {
                bak->next = tmp->next;
                free(tmp);
                break;
            }
            bak = tmp;
            tmp = tmp->next;
        }
    }
    return 0;
}

bool_t Print_DebugMalloc(char * cmd)
{
    (void)cmd;
    struct DebugMalloc *tmp = p_debughead;
    int cnt=0;
    while(tmp!=NULL)
    {
        printf("0x%x-0x%x ",tmp->lraddr,(u32)tmp->addr);
        cnt++;
        if(cnt%8==0)
            printf("\n\r");
        tmp = tmp->next;
    }
    return true;
}
#include "shell.h"
ADD_TO_ROUTINE_SHELL(debug,Print_DebugMalloc,"打印Print_DebugMalloc信息");
#endif
/*==============================================================================
 *
 *
 *
 *
 *
 *============================================================================*/
static void * Malloc_ServerInfo(size_t size)
{
    u32 lraddr;
    asm("mov %0, LR\n":"=r"(lraddr):);
    void*ret = malloc(size);
    if(ret)
    {
        ServerInfCb.objnum++;
#if DEBUG_SWITCH
        Debug_AddMalloc(ret,lraddr);
#endif
    }
    return ret;
}
static void  Free_ServerInfo(void * obj)
{
    if(obj != NULL)
    {
         ServerInfCb.objnum--;
         free(obj);
#if DEBUG_SWITCH
         Debug_DelMalloc(obj);
#endif
    }
}

static int Del_TopicObj(struct HWorkCB*homework,struct TopicCB*topic)
{
    int ret =-100;
    if((homework==NULL)||(topic == NULL))
        return -1;
    struct TopicCB*ptopic = homework->Topic;
    if(ptopic == NULL)
        return -2;
    do{
        if(topic == ptopic)
        {
            ret = 0;
            Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
            if(topic->next == topic)
            {
                homework->Topic = NULL;
            }
            else if(topic == homework->Topic)
            {
                homework->Topic = topic->next;
                topic->next->prev = topic->prev;
                topic->prev->next = topic->next;
            }else
            {
                topic->next->prev = topic->prev;
                topic->prev->next = topic->next;
            }
            Lock_MutexPost(ServerInfCb.mutex);
            Free_ServerInfo(topic);
            break;
        }
        ptopic = ptopic->next;
    }while(ptopic != homework->Topic);
    return ret;
}

static int Del_TodayHomeWorkObj(struct classCB*pclass,struct HWorkCB*homework)
{
    int ret = -100;
    struct HWorkCB*phwk = pclass->today;
    if(phwk == NULL)
        return ret;
    do{
        if(phwk == homework)
        {
            ret = 0;
            Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
            if(homework == homework->next)
            {
                pclass->today = NULL;
            }
            else if(pclass->today == homework)
            {
                pclass->today = homework->next;
                homework->next->prev = homework->prev;
                homework->prev->next = homework->next;
            }else
            {
                homework->next->prev = homework->prev;
                homework->prev->next = homework->next;
            }
            homework->next = homework->prev = NULL;

            struct TopicCB*Topic = homework->Topic;
            if(Topic != NULL)
            while(1)
            {
                if(Topic->next==Topic)
                {
                    Del_TopicObj(homework,Topic);
                    break;
                }else
                {
                    Del_TopicObj(homework,Topic);
                }
                Topic = Topic->next;
            }
            Lock_MutexPost(ServerInfCb.mutex);
            Free_ServerInfo(homework->p_details);
            Free_ServerInfo(homework);
            break;
        }
        phwk = phwk->next;
    }while(phwk != pclass->today);

    return ret;
}

static int Del_PastHomeWorkObj(struct classCB*pclass,struct HWorkCB*homework)
{
    struct HWorkCB*phwk = pclass->past;
    int ret = -100;
    if(phwk != NULL)
    do{
        if(phwk == homework)
        {
            ret = 0;
            Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
            if(homework == homework->next)
            {
                pclass->past = NULL;
            }
            else if(pclass->past == homework)
            {
                pclass->past = homework->next;
                homework->next->prev = homework->prev;
                homework->prev->next = homework->next;
            }else
            {
                homework->next->prev = homework->prev;
                homework->prev->next = homework->next;
            }
            homework->next = homework->prev = homework;

            struct TopicCB*Topic = homework->Topic;
            if(Topic != NULL)
            while(1)
            {
                if(Topic->next==Topic)
                {
                    Del_TopicObj(homework,Topic);
                    break;
                }else
                {
                    Del_TopicObj(homework,Topic);
                }
                Topic = Topic->next;
            }
            Lock_MutexPost(ServerInfCb.mutex);
            Free_ServerInfo(homework->p_details);
            Free_ServerInfo(homework);
            break;
        }
        phwk = phwk->next;
    }while(phwk != pclass->past);

    return ret;
}

static int Del_ClassObj(struct classCB*class)
{
    if(class == NULL)
        return -1;
    if(class != Get_ClassOfId(class->classid))
        return -2;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    if(class->next == class)
    {
        ServerInfCb.classhead = NULL;
    }
    else if(class == ServerInfCb.classhead)
    {
        ServerInfCb.classhead = class->next;
        class->next->prev = class->prev;
        class->prev->next = class->next;
    }else
    {
        class->next->prev = class->prev;
        class->prev->next = class->next;
    }
    class->next = class->prev = class;

    struct HWorkCB*phwork = class->past;
    while(class->past!=NULL)
    {
        phwork = phwork->next;
        Del_PastHomeWorkObj(class,phwork->prev);
    }
    phwork = class->today;
    while(class->today != NULL)
    {
        phwork = phwork->next;
        Del_TodayHomeWorkObj(class,phwork->prev);
    }
    Lock_MutexPost(ServerInfCb.mutex);
    Free_ServerInfo(class);
    return 0;
}

bool_t Del_MessagesObj(struct MessagesCB*pmsg)
{
    if( pmsg != Get_MessagesOfID(pmsg->id))
        return false;

    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    if(pmsg->next == pmsg)
    {
        ServerInfCb.masghead = NULL;
    }
    else if(pmsg == ServerInfCb.masghead)
    {
        ServerInfCb.masghead = pmsg->next;
        pmsg->prev->next = pmsg->next;
        pmsg->next->prev = pmsg->prev;
    }else
    {
        pmsg->prev->next = pmsg->next;
        pmsg->next->prev = pmsg->prev;
    }
    Lock_MutexPost(ServerInfCb.mutex);

    Free_ServerInfo(pmsg);
    return true;
}


bool_t Del_MessagesofId(int id)
{
    struct MessagesCB*msg = Get_MessagesOfID(id);
    if(msg == NULL)
        return false;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    if(msg->next == msg)
    {
        ServerInfCb.masghead = NULL;
    }
    else if(msg == ServerInfCb.masghead)
    {
        ServerInfCb.masghead = msg->next;
        msg->prev->next = msg->next;
        msg->next->prev = msg->prev;
    }else
    {
        msg->prev->next = msg->next;
        msg->next->prev = msg->prev;
    }
    Lock_MutexPost(ServerInfCb.mutex);

    Del_MessagesObj(msg);

    return true;
}

static bool_t Del_AllServerInfo(void)
{
    int tmp = 0;
    struct classCB *class;
    struct MessagesCB *msg;
    class = ServerInfCb.classhead;
    while(ServerInfCb.classhead != NULL)
    {
        class = class->next;
        tmp = Del_ClassObj(class->prev);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }

    msg = ServerInfCb.masghead;
    while(ServerInfCb.masghead != NULL)
    {
        msg = msg->next;
        tmp = Del_MessagesObj(msg->prev);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }
    if(ServerInfCb.objnum != 0)
    {
        printf("ServerInfCb.masghead : %d \n\r",ServerInfCb.msgnum);
        return false;
    }
    return true;
}


static bool_t Compare_Topic(struct TopicCB*Topic1,struct TopicCB*Topic2)
{
    if(Topic1 == Topic2)
        return true;

    if((Topic1==NULL)||(Topic2 == NULL))
        return false;

    if((Topic1->score!=Topic2->score)\
        ||(Topic1->id!=Topic2->id)\
       ||(Topic1->mark!=Topic2->mark)\
        ||(Topic1->type!=Topic2->type)\
        ||(strcmp(Topic1->type_name,Topic2->type_name)!=0)\
        ||(strcmp(Topic1->answer,Topic2->answer)!=0)\
        ||(strcmp(Topic1->audio,Topic2->audio)!=0)\
        ||(strcmp(Topic1->text,Topic2->text)!=0)\
        ||(strcmp(Topic1->title,Topic2->title)!=0))
        return false;

    return true;
}

static bool_t Compare_HomeWork(struct HWorkCB*hwork1,struct HWorkCB *hwork2)
{
    if(hwork1 == hwork2)
        return true;
    if((hwork1==NULL)||(hwork2 == NULL))
            return false;
    if((hwork1->score!=hwork2->score)\
            ||(hwork1->status!=hwork2->status)\
            ||(hwork1->workid!=hwork2->workid)\
            ||(strcmp(hwork1->time,hwork2->time)!=0)\
            ||(strcmp(hwork1->title,hwork2->title)!=0)\
            ||(strcmp(hwork1->type,hwork2->type)!=0))
        return false;

    if((hwork1->p_details->ptime != hwork2->p_details->ptime)
        ||(hwork1->p_details->rank != hwork2->p_details->rank)
        ||(hwork1->p_details->asize != hwork2->p_details->asize)
        ||(hwork1->p_details->bsize != hwork2->p_details->bsize)
        ||(hwork1->p_details->csize != hwork2->p_details->csize)
        ||(strcmp(hwork1->p_details->name,hwork2->p_details->name)!=0)
        ||(strcmp(hwork1->p_details->cover,hwork2->p_details->cover)!=0)
        ||(strcmp(hwork1->p_details->Topic,hwork2->p_details->Topic)!=0)
        ||(strcmp(hwork1->p_details->tname,hwork2->p_details->tname)!=0)
        ||(strcmp(hwork1->p_details->ctime,hwork2->p_details->ctime)!=0)
        ||(strcmp(hwork1->p_details->etime,hwork2->p_details->etime)!=0)
        ||(strcmp(hwork1->p_details->ftime,hwork2->p_details->ftime)!=0)
        ||(strcmp(hwork1->p_details->comment,hwork2->p_details->comment)!=0)
        ||(strcmp(hwork1->p_details->audio,hwork2->p_details->audio)!=0))
        return false;
    struct TopicCB*Topic1,*Topic2;
    do{
        Topic1 = hwork1->Topic;
        Topic2 = Get_TopicOfID(hwork2,Topic1->id);
        if(Topic2 == NULL)
            return false;
        if(false == Compare_Topic(Topic1,Topic2))
            return false;
        Topic1 = Topic1->next;
    }while(Topic1 != hwork1->Topic);

    return true;
}

static bool_t Compare_Class(struct classCB *class1,struct classCB *class2)
{
    if(class1 == class2)
        return true;
    if((class1 == NULL)||( class2 == NULL))
        return false;

    if((strcmp(class1->name,class2->name)!=0)\
         ||(class1->classid != class2->classid)\
         ||(class1->PastHomeWOrkNum != class2->PastHomeWOrkNum))
        return false;
    struct HWorkCB *phwork1 = class1->today;
    struct HWorkCB *phwork2;

    if(phwork1 != NULL)
    {
        do{
           phwork2 = Get_TodayHomeWorkOfId(class2,phwork1->workid);
           if(NULL == phwork2)
               return false;
           if(false == Compare_HomeWork(phwork1,phwork2))
               return false;
            phwork1 = phwork1->next;
        }while(class1->today!=phwork1);
    }else
    {
        if(class2->today != NULL)
            return false;
    }

    phwork1 = class1->past;
    if(phwork1 != NULL)
    {
        do{
           phwork2 = Get_PastHomeWorkOfId(class2,phwork1->workid);
           if(NULL == phwork2)
               return false;
           if(false == Compare_HomeWork(phwork1,phwork2))
               return false;
            phwork1 = phwork1->next;
        }while(class1->past!=phwork1);
    }else
    {
        if(class2->past != NULL)
            return false;
    }
    return true;
}


static bool_t Compare_Messages(struct MessagesCB *msg1,struct MessagesCB *msg2)
{
    if((msg1->id != msg2->id)
        ||(msg1->news_type != msg2->news_type)
        ||(msg1->read != msg2->read)
        ||(strcmp(msg1->content,msg2->content)!=0)
        ||(strcmp(msg1->time,msg2->time)!=0))
        return false;
    return true;
}


//通过偏移获取班级
struct classCB* Get_ClassOFCnt(int cnt)
{
    struct classCB*ret = NULL;
    struct classCB*tmp = ServerInfCb.classhead;
    if(tmp==NULL)
        return NULL;


    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    while(cnt)
    {
        tmp = tmp->next;
        if(ServerInfCb.classhead == tmp)
            break;
        cnt--;
    }
    Lock_MutexPost(ServerInfCb.mutex);
    if(cnt == 0)
        ret = tmp;
    return ret;
}
//通过id获取班级
struct classCB* Get_ClassOfId(int id)
{
    struct classCB*ret = NULL;
    struct classCB*tmp = ServerInfCb.classhead;
    if(tmp==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do
    {
        if(tmp->classid == id)
        {
            ret = tmp;
            break;
        }
        tmp = tmp->next;
    }while(ServerInfCb.classhead != tmp);
    Lock_MutexPost(ServerInfCb.mutex);
    return ret;
}

struct classCB* Get_NextClass(struct classCB* pclass)
{
    if(pclass != Get_ClassOfId(pclass->classid))
        return false;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    if(pclass==NULL)
        return NULL;
    if(pclass->next == pclass)
        return NULL;
    if(pclass->next == ServerInfCb.classhead)
        return NULL;
    Lock_MutexPost(ServerInfCb.mutex);
    return pclass->next;
}

//todo 过往作业需要判断数据是否在服务器上，没有存在本地
//通过偏移获取班级下的作业
struct HWorkCB* Get_PastHomeWorkOfCnt(struct classCB*pclass,int cnt)
{
    struct HWorkCB * ret = pclass->past;
    if(ret==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    while(cnt)
    {
        ret = ret->next;
        if(ret ==  pclass->past)
        {
            ret = NULL;
            break;
        }
        cnt--;
    }
    Lock_MutexPost(ServerInfCb.mutex);
    return (cnt == 0) ? ret:NULL;
}

//通过ID位置获取班级下的作业
struct HWorkCB* Get_PastHomeWorkOfId(struct classCB*pclass,int workid)
{
    struct HWorkCB * ret = NULL;
    struct HWorkCB * tmp = pclass->past;
    if((pclass==NULL)||(tmp == NULL))
        return NULL;

    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do{
        if(tmp->workid == workid)
        {
            ret = tmp;
            break;
        }
        tmp = tmp->next;
    }while(tmp != pclass->past);
    Lock_MutexPost(ServerInfCb.mutex);

    return ret;
}

//通过偏移获取班级下的作业
struct HWorkCB* Get_TodayHomeWorkOfCnt(struct classCB*pclass,int cnt)
{
    if(pclass==NULL)
        return NULL;
    struct HWorkCB * ret = pclass->today;
    if(ret==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    while(cnt)
    {
        ret = ret->next;
        if(ret ==  pclass->today)
        {
            break;
        }
        cnt--;
    }
    Lock_MutexPost(ServerInfCb.mutex);
    return (cnt == 0)?ret:NULL;
}
//通过ID位置获取班级下的作业
struct HWorkCB* Get_TodayHomeWorkOfId(struct classCB*pclass,int workid)
{
    struct HWorkCB * ret = NULL;
    struct HWorkCB * tmp = pclass->today;
    if(tmp==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do{
        if(tmp->workid == workid)
        {
            ret = tmp;
            break;
        }
        tmp = tmp->next;
    }while(tmp != pclass->today);
    Lock_MutexPost(ServerInfCb.mutex);

    return ret;
}

//通过位置获取作业下的小题
struct TopicCB* Get_TopicOfCnt(struct HWorkCB*phwork,int cnt)
{
    struct TopicCB * ret = phwork->Topic;
    if(ret==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    while(cnt)
    {
        ret = ret->next;
        if(phwork->Topic == ret)
        {
            break;
        }
        cnt--;
    }
    Lock_MutexPost(ServerInfCb.mutex);
    return (cnt == 0)?ret:NULL;

}

//通过id获取作业下的小题
struct TopicCB* Get_TopicOfID(struct HWorkCB*phwork,int id)
{
    if(phwork==NULL)
        return NULL;

    struct TopicCB * ret = NULL;
    struct TopicCB * tmp = phwork->Topic;
    if(tmp==NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do{
        if(tmp->id == id)
        {
            ret = tmp;
            break;
        }
        tmp = tmp->next;
    }while(tmp != phwork->Topic);
    Lock_MutexPost(ServerInfCb.mutex);

    return ret;

}
//小题数
int Get_HomeWorkTopicNUm(struct HWorkCB*phwork)
{
    int cnt = 0;
    struct TopicCB*Topic = phwork->Topic;
    if(Topic == NULL)
        return cnt;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do{
        cnt++;
        Topic = Topic->next;
    }while(phwork->Topic != Topic);
    Lock_MutexPost(ServerInfCb.mutex);
    return cnt;
}
//标记消息已读
bool_t Read_Messages(struct MessagesCB * msg)
{
    struct MessagesCB *pmsg = Get_MessagesOfID(msg->id);
    if(pmsg != NULL)
    {
        pmsg->read = 1;
    }
    return false;
}
//todo:本地缓存没有从网上获取
struct MessagesCB* Get_MessagesOfCnt(int cnt)
{
    struct MessagesCB*tmp = ServerInfCb.masghead;
    if(tmp == NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    while(cnt)
    {
        tmp = tmp->next;
        if(tmp == ServerInfCb.masghead)
            break;
        cnt--;
    }
    Lock_MutexPost(ServerInfCb.mutex);
    return (cnt == 0)?tmp:NULL;
}

struct MessagesCB* Get_MessagesOfID(int id)
{
    struct MessagesCB*pmsg = NULL;
    struct MessagesCB*tmp = ServerInfCb.masghead;
    if(tmp == NULL)
        return NULL;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    do{

        if(tmp->id == id)
        {
            pmsg = tmp;
            break;
        }
        tmp = tmp->next;
    }while(tmp != ServerInfCb.masghead);
    Lock_MutexPost(ServerInfCb.mutex);
    return pmsg;
}

int Get_MessagesNUm()
{
    return ServerInfCb.msgnum;
}

char Get_ServerInfo_Flag()
{
    return ServerInfCb.fistinit;
}

int Get_HomeWorkAllAudioLink(struct HWorkCB*phwork,char**linkbuf,int bufnum)
{
    int ret = 0;
    int num = 0;
    while(ret<bufnum)
    {
        struct TopicCB*topic = Get_TopicOfCnt(phwork,num++);
        if(topic==NULL)
            break;
        if(strlen(topic->answer)>5)
        {
            linkbuf[ret++] = topic->answer;
        }
        if((strlen(topic->audio)>5)&&(ret<bufnum))
        {
            linkbuf[ret++] = topic->audio;
        }
    }
    return ret;
}

int Makr_Topic(struct TopicCB* topic)
{
    topic->mark = 1;
    return 0;
}

static struct TopicCB* NewTopic_ServerInfo(cJSON* jsontmp)
{
    struct TopicCB*ret = NULL;
    cJSON*  cjsonanswer = NULL;
    if(jsontmp == NULL)
        return NULL;

    cJSON*  cjsonscore = cJSON_GetObjectItem(jsontmp, "score");
    cJSON*  cjsontype_name = cJSON_GetObjectItem(jsontmp, "type_name");
    cJSON*  cjsontmp = cJSON_GetObjectItem(jsontmp, "answer");
    if(cjsontmp) cjsonanswer = cJSON_GetObjectItem(cjsontmp, "audio");
    cJSON*  cjsonid = cJSON_GetObjectItem(jsontmp, "id");
    cJSON*  cjsonaudio = cJSON_GetObjectItem(jsontmp, "audio");
    cJSON*  cjsontext = cJSON_GetObjectItem(jsontmp, "text");
    cJSON*  cjsontype = cJSON_GetObjectItem(jsontmp, "type");
    cJSON*  cjsontitle = cJSON_GetObjectItem(jsontmp, "title");
    cJSON*  cjsonmark = cJSON_GetObjectItem(jsontmp, "mark");

    int mark  = (cjsonmark == NULL)?1:cjsonmark->valueint;
    int score = (cjsonscore == NULL)?0:cjsonscore->valueint;
    char * panswer = (cjsonanswer==NULL)?"":cjsonanswer->valuestring;

    if((cjsonid==NULL)||(cjsontype_name==NULL)||(cjsontitle==NULL)||(cjsonaudio==NULL)
          ||(cjsontext==NULL)||(cjsontype==NULL)||(cjsontitle==NULL))
    return NULL;


    struct TopicCB*content = Malloc_ServerInfo(sizeof(struct TopicCB)\
                              +strlen(cjsontype_name->valuestring)+1\
                              +strlen(cjsonaudio->valuestring)+1\
                              +strlen(cjsontext->valuestring)+1\
                              +strlen(cjsontitle->valuestring)+1\
                              +strlen(panswer)+1);
    if(content==NULL)
    {
        printf("error : Malloc_ServerInfo !! \n\r");
        return ret;
    }

    content->prev = content->next = content;
    content->score = score;
    content->id = cjsonid->valueint;
    content->type = cjsontype->valueint;
    content->mark = mark;

    content->type_name = (char *)(content +1);
    strcpy(content->type_name,cjsontype_name->valuestring);

    content->answer = (char *)(content->type_name+strlen(content->type_name)+1);
    strcpy(content->answer,panswer);

    content->audio = (char *)(content->answer+strlen(content->answer)+1);
    strcpy(content->audio,cjsonaudio->valuestring);

    content->text = (char *)(content->audio+strlen(content->audio)+1);
    strcpy(content->text,cjsontext->valuestring);

    content->title =  (char *)(content->text+strlen(content->text)+1);
    strcpy(content->title,cjsontitle->valuestring);

    return content;
}

static struct HWorkCB *NewHwork_ServerInfo(cJSON* homework)
{
    struct HWorkCB* ret = NULL;
    if(homework==NULL)
        return NULL;

    cJSON * cjsonscore = cJSON_GetObjectItem(homework, "score");
    cJSON * cjsonid = cJSON_GetObjectItem(homework, "id");
    cJSON * cjsontime = cJSON_GetObjectItem(homework, "time");
    cJSON * cjsontitle = cJSON_GetObjectItem(homework, "title");
    cJSON * cjsontype = cJSON_GetObjectItem(homework, "type");
    cJSON * cjsonstatus = cJSON_GetObjectItem(homework, "status");

    if ((cjsonscore==NULL)||(cjsonid==NULL)||(cjsontime==NULL)\
        ||(cjsontitle==NULL)||(cjsontype==NULL))
        return NULL;

    ret = Malloc_ServerInfo(sizeof(struct HWorkCB)+\
                strlen(cjsontime->valuestring)+1+\
                strlen(cjsontitle->valuestring)+1+\
                strlen(cjsontype->valuestring)+1);
    if(ret == NULL)
    {
        printf("error : Malloc_ServerInfo error \n\r");
        return NULL;
    }

    ret->next = ret->prev = ret;
    ret->score = cjsonscore->valueint; //评分
    ret->workid = cjsonid->valueint; //ID
    ret->time = (char *)(ret + 1);//时间
    strcpy(ret->time,cjsontime->valuestring);
    ret->title = (char *)(ret->time + strlen(ret->time)+1); //作业名
    strcpy(ret->title,cjsontitle->valuestring);
    ret->type = (char *)(ret->title + strlen(ret->title)+1);; //作业名
    strcpy(ret->type,cjsontype->valuestring);

    ret->status = cjsonstatus->valueint;//
    ret->p_details = NULL;//作业详情
    ret->Topic = NULL;//作业内容

    return ret;
}

static struct classCB *NewClass_ServerInfo(cJSON* class)
{
    struct classCB *pclass = NULL;
    if(class == NULL)
        return pclass;
    cJSON*  results = cJSON_GetObjectItem(class, "NAME");
    cJSON*  cjsonid = cJSON_GetObjectItem(class, "id");
    if((results == NULL)||(cjsonid == NULL))
        return NULL;

    pclass = Malloc_ServerInfo(sizeof(struct classCB)+strlen(results->valuestring)+1);
    if(pclass == NULL)
    {
        printf("error : Malloc_ServerInfo error \n\r");
        return pclass;
    }
    pclass->classid = cjsonid->valueint;
    pclass->next = pclass;
    pclass->prev = pclass;
    pclass->past = NULL;
    pclass->today = NULL;
    pclass->PastHomeWOrkNum = 0;

    pclass->name = (char *)(pclass +1);
    strcpy(pclass->name,results->valuestring);

    return pclass;
}

static struct MessagesCB *NewMessages_ServerInfo(cJSON* jsontmp)
{
    static struct MessagesCB *pmsg = NULL;
    if(jsontmp == NULL)
        return pmsg;

    cJSON*  cjsonnews_type = cJSON_GetObjectItem(jsontmp,"news_type");
    cJSON*  cjsonid = cJSON_GetObjectItem(jsontmp,"id");
    cJSON*  cjsontime = cJSON_GetObjectItem(jsontmp,"time");
    cJSON*  cjsoncontent = cJSON_GetObjectItem(jsontmp,"content");
    cJSON*  cjsonread = cJSON_GetObjectItem(jsontmp,"read");

    if((cjsonnews_type == NULL)||(cjsonid == NULL)||(cjsontime == NULL)||\
       (cjsoncontent == NULL)||(cjsonread == NULL))
    {
       return NULL;
    }
    pmsg = Malloc_ServerInfo(sizeof(struct MessagesCB)+strlen(cjsontime->valuestring)+1+strlen(cjsoncontent->valuestring)+1);
    if(pmsg==NULL)
        return NULL;

    pmsg->next = pmsg->prev = pmsg;
    pmsg->news_type = cjsonnews_type->valueint;
    pmsg->id = cjsonid->valueint;
    pmsg->read = cjsonread->valueint;

    pmsg->time = (char *)(pmsg + 1);
    strcpy(pmsg->time,cjsontime->valuestring);

    pmsg->content = (char *)(pmsg->time  + strlen(cjsontime->valuestring)+1);
    strcpy(pmsg->content,cjsoncontent->valuestring);

    return pmsg;

}
static int AddNewTopic_ServerInfo( struct HWorkCB* phwork,struct TopicCB*topic)
{
    int ret =- 100;
    if((phwork == NULL)||(topic == NULL))
        return ret;
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    struct TopicCB*ptopic = Get_TopicOfID(phwork,topic->id);
    if(ptopic == NULL)
    {
        ret = 0;
        if(phwork->Topic == NULL)
        {
            topic->next = topic;
            topic->prev = topic;
            phwork->Topic = topic;
        }else
        {
            topic->next = phwork->Topic;
            topic->prev = phwork->Topic->prev;
            phwork->Topic->prev->next = topic;
            phwork->Topic->prev = topic;
        }
    }else
    {
        if(true == Compare_Topic(ptopic,topic))
        {
            ret = 1;
            Free_ServerInfo(topic);
        }else
        {
            ret = 2;
            if(ptopic->next == ptopic)
            {
                topic->next = topic;
                topic->prev = topic;
                phwork->Topic = topic;
            }else if(ptopic == phwork->Topic)
            {
                topic->next = ptopic->next;
                topic->prev = ptopic->prev;
                ptopic->prev->next = topic;
                ptopic->next->prev = topic;
                phwork->Topic = topic;
            }else
            {

                topic->next = ptopic->next;
                topic->prev = ptopic->prev;
                ptopic->prev->next = topic;
                ptopic->next->prev = topic;
            }
            Free_ServerInfo(ptopic);
        }
    }
    Lock_MutexPost(ServerInfCb.mutex);
    return ret;
}

static int Add_NewPastHomework(struct classCB*pclass,struct HWorkCB* hwork)
{
    if((hwork==NULL)||(pclass == NULL))
        return -1;
    if(pclass->past == NULL)
    {
       Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
       hwork->next = hwork;
       hwork->prev = hwork;
       pclass->past = hwork;
       Lock_MutexPost(ServerInfCb.mutex);
       return  0;
    }
    struct HWorkCB*phwork = Get_PastHomeWorkOfId(pclass,hwork->workid);
    if(phwork == NULL)
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        hwork->next = pclass->past;
        hwork->prev = pclass->past->prev;
        pclass->past->prev->next = hwork;
        pclass->past->prev = hwork;
        Lock_MutexPost(ServerInfCb.mutex);
        return 1;
    }

    if(true == Compare_HomeWork(phwork,hwork))
    {
        Del_PastHomeWorkObj(pclass,hwork);
        return 2;
    }
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    hwork->next = phwork;
    hwork->prev = phwork->prev;
    phwork->prev->next = hwork;
    phwork->prev = hwork;
    Lock_MutexPost(ServerInfCb.mutex);
    Del_PastHomeWorkObj(pclass,phwork);
    return 4;
}

static int Add_NewTodayHomework(struct classCB*pclass,struct HWorkCB* hwork)
{
    if((hwork==NULL)||(pclass == NULL))
        return -1;
    if(pclass->today == NULL)
    {
       Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
       hwork->next = hwork;
       hwork->prev = hwork;
       pclass->today = hwork;
       Lock_MutexPost(ServerInfCb.mutex);
       return  0;
    }
    struct HWorkCB*phwork = Get_PastHomeWorkOfId(pclass,hwork->workid);
    if(phwork == NULL)
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        hwork->next = pclass->today;
        hwork->prev = pclass->today->prev;
        pclass->today->prev->next = hwork;
        pclass->today->prev = hwork;
        Lock_MutexPost(ServerInfCb.mutex);
        return 1;
    }

    if(true == Compare_HomeWork(phwork,hwork))
    {
        Del_TodayHomeWorkObj(pclass,hwork);
        return 2;
    }
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    hwork->next = phwork;
    hwork->prev = phwork->prev;
    phwork->prev->next = hwork;
    phwork->prev = hwork;
    Lock_MutexPost(ServerInfCb.mutex);
    Del_TodayHomeWorkObj(pclass,phwork);
    return 4;
}


static int Add_NewClass(struct classCB *class)
{
    int ret = 0;
    if(class == NULL)
        return -100;
    struct classCB *pclass = Get_ClassOfId(class->classid);
    if(pclass == NULL)
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        if(ServerInfCb.classhead == NULL)
        {
            class->next = class;
            class->prev = class;
            ServerInfCb.classhead = class;
        }else
        {
            class->next = ServerInfCb.classhead;
            class->prev = ServerInfCb.classhead->prev;
            ServerInfCb.classhead->prev->next = class;
            ServerInfCb.classhead->prev = class;
        }
        Lock_MutexPost(ServerInfCb.mutex);
        ret = 1;
    }else if(false == Compare_Class(pclass,class))
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        if(pclass->next == pclass)
        {
            class->next = class;
            class->prev = class;
            ServerInfCb.classhead = class;
        }else
        {
            class->next = pclass->next;
            class->prev = pclass->prev;
            pclass->prev->next = class;
            pclass->next->prev = class;
        }
        Lock_MutexPost(ServerInfCb.mutex);
        Del_ClassObj(pclass);
        ret = 2;
    }else
    {
        //班级已经存在并且相同
         Del_ClassObj(class);
         ret = 3;
    }
    return ret;
}

static int Add_NewMessages(struct MessagesCB *msg)
{
    if(msg == NULL)
        return -1;
    struct MessagesCB *msgtmp;
    if(ServerInfCb.masghead==NULL)
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        msg->next = msg;
        msg->prev = msg;
        ServerInfCb.masghead = msg;
        Lock_MutexPost(ServerInfCb.mutex);
        return 1;
    }
    msgtmp = Get_MessagesOfID(msg->id);
    if(NULL == msgtmp)
    {
        Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
        msg->next = ServerInfCb.masghead;
        msg->prev = ServerInfCb.masghead->prev;
        ServerInfCb.masghead->prev->next = msg;
        ServerInfCb.masghead->prev = msg;
        Lock_MutexPost(ServerInfCb.mutex);
        return 2;
    }
    if(true == Compare_Messages(msgtmp,msg))
    {
        Del_MessagesObj(msg);
        return 3;
    }
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    msg->next = msgtmp;
    msg->prev = msgtmp->prev;
    msgtmp->prev->next = msg;
    msgtmp->prev = msg;
    Del_MessagesObj(msgtmp);
    Lock_MutexPost(ServerInfCb.mutex);
    return 4;
}

static struct HWdetailsCB * New_HomeWorkDetails(cJSON*json)
{
    char * NULL_Str = "";
    char *   cjsonname_string
            ,*cjsoncover_string
            ,*cjsoncontent_string
            ,*cjsontname_string
            ,*cjsonctime_string
            ,*cjsonetime_string
            ,*cjsonftime_string
            ,*cjsoncomment_string
            ,*cjsonaudio_string;

    cJSON * cjsonname    = cJSON_GetObjectItem(json, "name");
    cjsonname_string =(cjsonname == NULL) ? NULL_Str:cjsonname->valuestring;
    cJSON * cjsoncover   = cJSON_GetObjectItem(json, "cover");
    cjsoncover_string =(cjsoncover == NULL)? NULL_Str:cjsoncover->valuestring;
    cJSON * cjsoncontent = cJSON_GetObjectItem(json, "content");
    cjsoncontent_string =  (cjsoncontent == NULL)? NULL_Str:cjsoncontent->valuestring;
    cJSON * cjsontname   = cJSON_GetObjectItem(json, "tname");
    cjsontname_string = (cjsontname == NULL) ? NULL_Str:cjsontname->valuestring;
    cJSON * cjsonctime   = cJSON_GetObjectItem(json, "ctime");
    cjsonctime_string = (cjsonctime == NULL)? NULL_Str:cjsonctime->valuestring;
    cJSON * cjsonetime   = cJSON_GetObjectItem(json, "etime");
    cjsonetime_string =(cjsonetime == NULL) ? NULL_Str:cjsonetime->valuestring;
    cJSON * cjsonftime   = cJSON_GetObjectItem(json, "ftime");
    cjsonftime_string = (cjsonftime == NULL) ? NULL_Str:cjsonftime->valuestring;
    cJSON * cjsoncomment = cJSON_GetObjectItem(json, "comment");
    cjsoncomment_string = (cjsoncomment == NULL) ? NULL_Str:cjsoncomment->valuestring;
    cJSON * cjsonaudio   = cJSON_GetObjectItem(json, "audio");
    cjsonaudio_string = (cjsonaudio == NULL)?NULL_Str:cjsonaudio->valuestring;

    cJSON * cjsonptime   = cJSON_GetObjectItem(json, "ptime");
    cJSON * cjsonrank    = cJSON_GetObjectItem(json, "rank");
    cJSON * cjsonasize   = cJSON_GetObjectItem(json, "asize");
    cJSON * cjsonbsize   = cJSON_GetObjectItem(json, "bsize");
    cJSON * cjsoncsize   = cJSON_GetObjectItem(json, "csize");

    struct HWdetailsCB *pdetails = Malloc_ServerInfo(sizeof(struct HWdetailsCB)\
                                   +strlen(cjsonname_string)+1\
                                   +strlen(cjsoncover_string)+1\
                                   +strlen(cjsoncontent_string)+1\
                                   +strlen(cjsontname_string)+1\
                                   +strlen(cjsonctime_string)+1\
                                   +strlen(cjsonetime_string)+1\
                                   +strlen(cjsonftime_string)+1\
                                   +strlen(cjsoncomment_string)+1\
                                   +strlen(cjsonaudio_string)+1);
    if(pdetails==NULL)
    {
       return NULL;
    }

    pdetails->name = (char *)(pdetails +1);//时间;// 9月9日第一单元作业,
    strcpy(pdetails->name,cjsonname_string);

    pdetails->cover = (char *)(pdetails->name+strlen(pdetails->name)+1);// *.png,//背景
    strcpy(pdetails->cover,cjsoncover_string);

    pdetails->Topic =  (char *)(pdetails->cover+strlen(pdetails->cover)+1);// 作业简介,
    strcpy(pdetails->Topic,cjsoncontent_string);

    pdetails->tname =  (char *)(pdetails->Topic+strlen(pdetails->Topic)+1);// 李老师,
    strcpy(pdetails->tname,cjsontname_string);

    pdetails->ctime =  (char *)(pdetails->tname+strlen(pdetails->tname)+1);//发布日期
    strcpy(pdetails->ctime,cjsonctime_string);

    pdetails->etime =  (char *)(pdetails->ctime+strlen(pdetails->ctime)+1);//截止日期
    strcpy(pdetails->etime,cjsonetime_string);

    pdetails->ftime =  (char *)(pdetails->etime+strlen(pdetails->etime)+1);//提交日期
    strcpy(pdetails->ftime,cjsonftime_string);

    pdetails->comment =  (char *)(pdetails->ftime+strlen(pdetails->ftime)+1);//教师评语
    strcpy(pdetails->comment,cjsoncomment_string);

    pdetails->audio =  (char *)(pdetails->comment+strlen(pdetails->comment)+1);// xx.wav,音频评语
    strcpy(pdetails->audio,cjsonaudio_string);

    pdetails->ptime =  (cjsonptime == NULL)? (-1):cjsonptime->valueint;// 30,预计用时
    pdetails->rank  =  (cjsonrank == NULL) ? (-1):cjsonrank->valueint;// 5,班级排名
    pdetails->asize =  (cjsonasize == NULL)? (-1):cjsonasize->valueint;// 2,朗读数目
    pdetails->bsize =  (cjsonbsize == NULL)? (-1):cjsonbsize->valueint;// 3,背诵数目
    pdetails->csize =  (cjsoncsize == NULL)? (-1):cjsoncsize->valueint;// 0 听力数目

    return pdetails;
}

static int Update_HomeWorkDdetails(struct HWorkCB*phw)
{
    int ret = 0;
    if(phw == NULL)
        return -200;
    char *buf = Malloc_ServerInfo(JSON_BUF_DEF_WORK);
    if(buf == NULL)
        return ret;

    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != DevGetHWorkInfo(phw->workid, buf, (JSON_BUF_DEF_WORK)))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);buf = NULL;
    if(json == NULL)
    {
        ret = -1;
        goto Error_End;
    }
    phw->p_details = New_HomeWorkDetails(json);

    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}


static int Update_TopicList(struct HWorkCB*phw)
{
    int ret = 0;
    if(phw == NULL)
        return -200;
    char *buf = Malloc_ServerInfo(JSON_BUF_DEF_WORK);
    if(buf == NULL)
        return ret;
    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != GetTopicList(phw->workid, buf, (JSON_BUF_DEF_WORK)))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        ret = -1;
        goto Error_End;
    }

    int size = cJSON_GetArraySize(json);
    if(size!=0)
    for(int i=0;i<size;i++)
    {
        cJSON*   jsontmp = cJSON_GetArrayItem(json,i);
        cJSON*  jsonquestions = cJSON_GetObjectItem(jsontmp, "questions");
        if(jsonquestions==NULL)
        {
            continue;
        }
        int size2 = cJSON_GetArraySize(jsonquestions);
        if(size2!=0)
        for(int j=0;j<size2;j++)
        {
            cJSON*   JsonTopic = cJSON_GetArrayItem(jsonquestions,j);
            cJSON*  jsonTopic = cJSON_GetObjectItem(JsonTopic, "questions");
            if(jsonTopic == NULL)
                continue;
            int size3 = cJSON_GetArraySize(jsonTopic);
            if(size3!=0)
            for(int k=0;k<size3;k++)
            {
                cJSON*   jsonTopictmp = cJSON_GetArrayItem(jsonTopic,k);
                struct TopicCB*Topic = NewTopic_ServerInfo(jsonTopictmp);
                if(Topic != NULL)
                {
                    int tmp = AddNewTopic_ServerInfo(phw,Topic);
                    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
                }
            }
        }
    }

    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}
//==============================================================================
static int Update_PastHomeWorkList(struct classCB * class)
{
    int ret = 0;
    int tmp;
    if(class == NULL)
        return -100;

    char *buf = Malloc_ServerInfo(JSON_BUF_DEF_WORK);
    if(buf == NULL)
        return -100;
    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != DevGetPastHWorkList(class->classid,1,PAGE_SIZE,buf,JSON_BUF_DEF))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        ret = -1;
        goto Error_End;
    }
    cJSON * cjsonsize = cJSON_GetObjectItem(json, "size");
    cJSON * cjsondata = cJSON_GetObjectItem(json, "data");
    if((cjsondata==NULL)||(cjsonsize == NULL))
    {
        ret = -2;
        goto Error_End;
    }
    class->PastHomeWOrkNum = cjsonsize->valueint;
    int size = cJSON_GetArraySize(cjsondata);
    if(size!=0)
    for(int i=0;i<size;i++)
    {
        cJSON * jsontmp = cJSON_GetArrayItem(cjsondata,i);
        if(jsontmp == NULL)
        {
            ret -= 4;
            continue;
        }
        struct HWorkCB* hwork =  NewHwork_ServerInfo(jsontmp);
        tmp = Update_TopicList(hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Update_HomeWorkDdetails(hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Add_NewPastHomework(class,hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }

    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}

static int Update_TodayHomeWorkList(struct classCB * class)
{
    int ret = 0;
    int tmp;
    if(class == NULL)
        return -100;

    char *buf = Malloc_ServerInfo(JSON_BUF_DEF_WORK);
    if(buf == NULL)
        return -1;
    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != DevGetTodayHWorkList(class->classid, buf, (JSON_BUF_DEF_WORK)))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        ret = -2;
        goto Error_End;
    }

    int size = cJSON_GetArraySize(json);
    if(size!=0)
    for(int i=0;i<size;i++)
    {
        cJSON * jsontmp = cJSON_GetArrayItem(json,i);
        if(jsontmp == NULL)
        {
            ret -=4;
            continue;
        }
        struct HWorkCB* hwork =  NewHwork_ServerInfo(jsontmp);
        if(hwork == NULL)
        {
            ret = -5;
            continue;
        }
        tmp = Update_TopicList(hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Update_HomeWorkDdetails(hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Add_NewTodayHomework(class,hwork);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }
    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}


/*==============================================================================
    功能: 更新班级列表
    参数:
    返回:
 * ============================================================================*/
static int Update_ClassList()
{
    int ret = 0;
    int tmp;

    char *buf = Malloc_ServerInfo(JSON_BUF_DEF);
    if(buf == NULL)
        return -100;
    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != ClassList(buf, JSON_BUF_DEF))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        ret = -1;
        goto Error_End;
    }

    int size = cJSON_GetArraySize(json);
    if(size!=0)
    for(int i=0;i<size;i++)
    {
        cJSON * jsontmp = cJSON_GetArrayItem(json,i);
        if(jsontmp == NULL)
        {
            ret = -3;
            continue;
        }
        struct classCB * class = NewClass_ServerInfo(jsontmp);
        if(class == NULL)
        {
            ret = -4;
            continue;
        }
        tmp = Update_TodayHomeWorkList(class);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Update_PastHomeWorkList(class);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Add_NewClass(class);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }


    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}

int Update_Messages()
{
    int ret = 0;
    int tmp;

    char *buf = Malloc_ServerInfo(JSON_BUF_DEF);
    if(buf == NULL)
        return -100;
    for(int j=0;j< REQUEST_NUM;j++)
    {
        if(0 != DevGetNewsList(1,PAGE_SIZE,buf,JSON_BUF_DEF))
            break;
    }
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        ret = -1;
        goto Error_End;
    }

    int size = cJSON_GetArraySize(json);
    struct MessagesCB *msg = ServerInfCb.masghead;
    while(ServerInfCb.masghead != NULL)
    {
        msg = msg->next;
        tmp = Del_MessagesObj(msg->prev);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }
    if(size!=0)
    for(int i=0;i<size;i++)
    {
        cJSON * jsontmp = cJSON_GetArrayItem(json,i);
        if(jsontmp == NULL)
        {
            ret = -2;
            continue;
        }
        struct MessagesCB * msg = NewMessages_ServerInfo(jsontmp);
        if(msg == NULL)
        {
            ret = -3;
            continue;
        }
        tmp = Add_NewMessages(msg);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }

    if (json) cJSON_Delete(json);
Error_End:
    return ret;

}


int Update_TodayHomeWorkofid(struct classCB*class,int id)
{
    int tmp = 0;
    if(class == NULL)
        return -100;
    struct HWorkCB*hwork = Get_TodayHomeWorkOfId(class,id);
    tmp = Update_TopicList(hwork);
    if(tmp<0) {printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);goto End_update;}
    tmp = Update_HomeWorkDdetails(hwork);
    if(tmp<0) {printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);goto End_update;}
    tmp = Add_NewTodayHomework(class,hwork);
    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
End_update:
    return tmp;
}


int Update_PastHomeWorkofid(struct classCB*class,int id)
{
    int tmp = 0;
    if(class == NULL)
        return -100;
    struct HWorkCB*hwork = Get_PastHomeWorkOfId(class,id);
    tmp = Update_TopicList(hwork);
    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    tmp = Update_HomeWorkDdetails(hwork);
    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    tmp = Add_NewPastHomework(class,hwork);
    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    return tmp;
    return 0;
}

/*==============================================================================
    功能:打印作业下的题目信息
    参数:
    返回:
 * ============================================================================*/
static void printf_TopicCB(struct HWorkCB*hwork)
{
    struct TopicCB* tmp = hwork->Topic;
    int cnt = 0;
    if(tmp)
    do{
        printf("       content: %d \n\r"
               "       score: %d  \n\r"
               "       type_name: %s  \n\r"
               "       answer: %s \n\r"
               "       id :%d \n\r"
               "       mark :%d \n\r"
               "       audio: %s \n\r"
               "       type: %d \n\r"
               "       title: %s \n\r"
                ,cnt++,
                tmp->score,
                tmp->type_name,
                tmp->answer,
                tmp->id ,
                tmp->mark ,
                tmp->audio,
                tmp->type,
                tmp->title);
        tmp = tmp->next;
    }while(tmp != hwork->Topic);

}


/*==============================================================================
    功能: 打印班级下作业信息
    参数:
    返回:
 * ============================================================================*/
static void printf_Pasthomewprk(struct classCB*pclass)
{
    struct HWorkCB*hwkcb = pclass->past;
    struct HWdetailsCB * p_details;
    int cnt = 0;
    if(hwkcb)
    do{
        cnt++;
        printf("    Home Work %d:[score:%d ,workid:%d,time:%s,title:%s,type:%s,status:%d] \n\r"\
                   ,cnt,hwkcb->score,hwkcb->workid,hwkcb->time,hwkcb->title,hwkcb->type,hwkcb->status);
        p_details = hwkcb->p_details;
        if(p_details != NULL)
            printf( "    -------------------\n\r"\
                    "    name : %s\n\r"\
                    "    cover : %s\n\r"\
                    "    content : %s\n\r"\
                    "    tname : %s\n\r"\
                    "    ctime : %s\n\r"\
                    "    etime : %s\n\r"\
                    "    ftime : %s\n\r"\
                    "    ptime :%d\n\r"\
                    "    rank :%d\n\r"\
                    "    comment : %s\n\r"\
                    "    audio : %s \n\r"\
                    "    asize :%d\n\r"\
                    "    bsize :%d\n\r"\
                    "    csize :%d \n\r\n\r",\
                        p_details->name,
                        p_details->cover,
                        p_details->Topic,
                        p_details->tname,
                        p_details->ctime,
                        p_details->etime,
                        p_details->ftime,
                        p_details->ptime,
                        p_details->rank,
                        p_details->comment,
                        p_details->audio,
                        p_details->asize,
                        p_details->bsize,
                        p_details->csize);

        printf_TopicCB(hwkcb);

        hwkcb = hwkcb->next;
    }while(pclass->past != hwkcb);
}
/*==============================================================================
    功能: 打印班级下作业信息
    参数:
    返回:
 * ============================================================================*/
static void printf_Todayhomewprk(struct classCB*pclass)
{
    struct HWorkCB*hwkcb = pclass->today;
    struct HWdetailsCB * p_details;
    int cnt = 0;
    if(hwkcb)
    do{
        cnt++;
        printf("    Home Work %d:[score:%d ,workid:%d,time:%s,title:%s,type:%s,status:%d] \n\r"\
                   ,cnt,hwkcb->score,hwkcb->workid,hwkcb->time,hwkcb->title,hwkcb->type,hwkcb->status);
        p_details = hwkcb->p_details;
        if(p_details != NULL)
            printf( "    -------------------\n\r"\
                    "    name : %s\n\r"\
                    "    cover : %s\n\r"\
                    "    content : %s\n\r"\
                    "    tname : %s\n\r"\
                    "    ctime : %s\n\r"\
                    "    etime : %s\n\r"\
                    "    ftime : %s\n\r"\
                    "    ptime :%d\n\r"\
                    "    rank :%d\n\r"\
                    "    comment : %s\n\r"\
                    "    audio : %s \n\r"\
                    "    asize :%d\n\r"\
                    "    bsize :%d\n\r"\
                    "    csize :%d \n\r\n\r",\
                        p_details->name,
                        p_details->cover,
                        p_details->Topic,
                        p_details->tname,
                        p_details->ctime,
                        p_details->etime,
                        p_details->ftime,
                        p_details->ptime,
                        p_details->rank,
                        p_details->comment,
                        p_details->audio,
                        p_details->asize,
                        p_details->bsize,
                        p_details->csize);

        printf_TopicCB(hwkcb);

        hwkcb = hwkcb->next;
    }while(pclass->today != hwkcb);
}

void printf_Infolist()
{
    struct classCB*pclass = ServerInfCb.classhead;
    int c_cnt = 0;
    printf("[ServerInfCb obj mum :%d ]\n\r",ServerInfCb.objnum);
    Lock_MutexPend(ServerInfCb.mutex,CN_TIMEOUT_FOREVER);
    if(pclass)
    do{

        printf("class %d:[id : %d , name :%s ]\n\r",c_cnt++,pclass->classid,pclass->name);
        printf("    today \n\r");
        printf_Todayhomewprk(pclass);
        printf("    past \n\r");
        printf_Pasthomewprk(pclass);

        pclass = pclass->next;
    }while(pclass != ServerInfCb.classhead);

    struct MessagesCB *msg = ServerInfCb.masghead;
    if(msg)
    printf("=========================Messages====================================\n\r");
    if(msg)
    do{
        printf(
                "       news_type:%d"
                "       id:%d"
                "       time:%s"
                "       content:%s"
                "       read:%d \n\r"
                ,msg->news_type
                ,msg->id
                ,msg->time
                ,msg->content
                ,msg->read);

        msg = msg->next;
    }while(msg != ServerInfCb.masghead);

    Lock_MutexPost(ServerInfCb.mutex);

}

static int Server_InfoInit()
{
    //创建互斥量
    if( ServerInfCb.mutex==NULL)
    {
        ServerInfCb.mutex = Lock_MutexCreate("Server info");
        if(ServerInfCb.mutex == NULL)
            return -1;
    }
    //创建内存池
    return 0;
}

//综合数据包
static int All_ServerInFoUpdate()
{
    int ret = 0;
    int tmp;
    int request_num ;
    int size1,size2,size3;
    if(false == Del_AllServerInfo())
    {
       ret = -1;
    }
    char *buf = Malloc_ServerInfo(JSON_BUF_DEF_ALL);
    if(buf == NULL)
        return -2;
#if 1
    for(request_num=0;request_num< REQUEST_NUM;request_num++)
    {
        tmp = DevGetALLInfo(0, PAGE_SIZE,PAGE_SIZE, buf, JSON_BUF_DEF_ALL);
        printf("info : DevGetALLInfo ret = %d \r\n",tmp);
        if(tmp >=0)
            break;
    }
    if(request_num==REQUEST_NUM)
    {
        ret = -100;
        Free_ServerInfo(buf);
        goto Error_End;
    }
#else
    char * get_strALL();
    char * pp = get_strALL();
    if(strlen(pp) < JSON_BUF_DEF_ALL)
        strcpy(buf,pp);
#endif
    cJSON * json = cJSON_Parse(buf);
    Free_ServerInfo(buf);
    if(json == NULL)
    {
        //printf("\r\n%s\r\n",buf);
        ret = -111;
        goto Error_End;
    }
    cJSON*  cjsonnews = cJSON_GetObjectItem(json, "news");
    cJSON*  cjsonnews_size = cJSON_GetObjectItem(json, "news_size");
    cJSON*  cjsonclass = cJSON_GetObjectItem(json, "classes");

    if(cjsonnews==NULL){ret = -2;goto Error_End;}
    if(cjsonnews_size==NULL){ret = -3;goto Error_End;}
    if(cjsonclass==NULL){ret = -4;goto Error_End;}
    ServerInfCb.msgnum = cjsonnews_size->valueint;

    size1 = cJSON_GetArraySize(cjsonclass);
    if(size1!=0)
    for(int i=0;i<size1;i++)
    {
        cJSON * cjsonclasscnt = cJSON_GetArrayItem(cjsonclass,i);
        if(cjsonclasscnt == NULL)
        {
            ret = -3;
            continue;
        }
        struct classCB * class = NewClass_ServerInfo(cjsonclasscnt);
        if(class == NULL)
        {
            ret  = -4;
            continue;
        }
        // today home work update
        cJSON*  cjsontoday_homework = cJSON_GetObjectItem(cjsonclasscnt, "today_homework");
        if(cjsontoday_homework!=NULL)
        {
            size2 = cJSON_GetArraySize(cjsontoday_homework);
            if(size2!=0)
            for(int j=0;j<size2;j++)
            {
                cJSON * cjsontoday_homeworkcnt = cJSON_GetArrayItem(cjsontoday_homework,j);
                if(cjsontoday_homeworkcnt == NULL)
                {
                    ret = -5;
                    continue;
                }
                struct HWorkCB* hwork =  NewHwork_ServerInfo(cjsontoday_homeworkcnt);
                if(hwork == NULL)
                {
                    ret = -6;
                    continue;
                }
                cJSON*  cjsontopic = cJSON_GetObjectItem(cjsontoday_homeworkcnt, "questions");
                if(cjsontopic != NULL)
                {
                     size3 = cJSON_GetArraySize(cjsontopic);
                     if(size3 != 0)
                     for(int k=0;k<size3;k++)
                     {
                         cJSON*   jsontopiccnt = cJSON_GetArrayItem(cjsontopic,k);
                         if(jsontopiccnt== NULL)
                             continue;
                         cJSON*  cjsontopic_last = cJSON_GetObjectItem(jsontopiccnt, "questions");
                         int size4 = cJSON_GetArraySize(cjsontopic_last);
                         if(size4 != 0)
                         for(int m=0;m<size4;m++)
                         {
                             cJSON*   cjsontopic_lastcnt = cJSON_GetArrayItem(cjsontopic_last,m);
                             if(cjsontopic_lastcnt != NULL)
                             {
                                 struct TopicCB*Topic = NewTopic_ServerInfo(cjsontopic_lastcnt);
                                 tmp = AddNewTopic_ServerInfo(hwork,Topic);
                                 if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
                             }
                         }
                     }
                }
                cJSON*  cjsondetail = cJSON_GetObjectItem(cjsontoday_homeworkcnt, "detail");
                if(cjsondetail == NULL)
                {
                    ret = -8;
                }else{
                    hwork->p_details = New_HomeWorkDetails(cjsondetail);
                }
                tmp = Add_NewTodayHomework(class,hwork);
                if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
            }
        }
        //past home work updata
        cJSON*  cjsonpast_homework = cJSON_GetObjectItem(cjsonclasscnt, "past_homework");
        if(cjsonpast_homework!=NULL)
        {
            size2 = cJSON_GetArraySize(cjsonpast_homework);
            if(size2!=0)
            for(int j=0;j<size2;j++)
            {
                cJSON * cjsonpast_homeworkcnt = cJSON_GetArrayItem(cjsonpast_homework,j);
                if(cjsonpast_homeworkcnt == NULL)
                {
                    ret = -9;
                    continue;
                }
                struct HWorkCB* hwork =  NewHwork_ServerInfo(cjsonpast_homeworkcnt);
                if(hwork == NULL)
                {
                    ret = -10;
                    continue;
                }
                cJSON*  cjsontopic = cJSON_GetObjectItem(cjsonpast_homeworkcnt, "questions");
                if(cjsontopic != NULL)
                {
                     size3 = cJSON_GetArraySize(cjsontopic);
                     if(size3 != 0)
                     for(int k=0;k<size3;k++)
                     {
                         cJSON*   jsontopiccnt = cJSON_GetArrayItem(cjsontopic,k);
                         if(jsontopiccnt== NULL)
                             continue;
                         cJSON*  cjsontopic_last = cJSON_GetObjectItem(jsontopiccnt, "questions");
                         int size4 = cJSON_GetArraySize(cjsontopic_last);
                         if(size4 != 0)
                         for(int m=0;m<size4;m++)
                         {
                             cJSON*   cjsontopic_lastcnt = cJSON_GetArrayItem(cjsontopic_last,m);
                             if(cjsontopic_lastcnt!=NULL)
                             {
                                 struct TopicCB*Topic = NewTopic_ServerInfo(cjsontopic_lastcnt);
                                 tmp = AddNewTopic_ServerInfo(hwork,Topic);
                                 if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
                             }
                         }
                     }
                }
                cJSON*  cjsondetail = cJSON_GetObjectItem(cjsonpast_homeworkcnt, "detail");
                if(cjsondetail == NULL)
                {
                    ret = -12;
                }else{
                    hwork->p_details = New_HomeWorkDetails(cjsondetail);
                }
                tmp = Add_NewPastHomework(class,hwork);
                if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
            }
        }
        tmp = Add_NewClass(class);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }
    //msg update
    size1 = cJSON_GetArraySize(cjsonnews);
    if(size1!=0)
    for(int i=0;i<size1;i++)
    {
        cJSON * cjsonnewscnt = cJSON_GetArrayItem(cjsonnews,i);
        if(cjsonnewscnt == NULL)
        {
            ret = -13;
            continue;
        }
        struct MessagesCB * msg = NewMessages_ServerInfo(cjsonnewscnt);
        if(msg == NULL)
        {
            ret = -14;
            continue;
        }
        tmp = Add_NewMessages(msg);
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    }

    if (json) cJSON_Delete(json);
Error_End:
    return ret;
}


int Update_ServerInfo()
{
    int tmp;
    int ret = 0;
    ServerInfCb.fistinit = 0;
    printf("=============Update all server information Start===========\n\r");
    tmp =Server_InfoInit();
    if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);

    do{
        tmp = All_ServerInFoUpdate();
        if(tmp < 0)
        {
            printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
            Djy_EventDelay(100*mS);
        }
    }while(tmp < 0);
#if 0
    if(tmp<0)
    {
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        Del_AllServerInfo();

        tmp = Update_ClassList();
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        tmp = Update_Messages();
        if(tmp<0) printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
        ret = 1;
    }
#endif
    ServerInfCb.fistinit = 1;
    printf("=============Update all server information end===========\n\r");
    Refresh_FromUpdateServer();
    return ret;
}







