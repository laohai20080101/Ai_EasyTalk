/*
 * text_info.c
 *
 *  Created on: 2019年10月12日
 *      Author: c
 */

#include <string.h>
#include "device_user.h"

#ifdef ANALOG_DATA_DEBUG

char * details ="[{\"difficulty\":3,\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\","
        "\"kind\":\"单词\",\"unitid\":78,\"id\":123,"
        "\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\","
        "\"text\":\"Let's try \nWhat is the weather like today? Listen and write.\nIt's a windy day. \nYes, "
        "it's cold,too. \nWhat's your favorite season,Chen Jie? \nAutumn.\nIt is beautiful here in Beijing.\nLet's talk \nDo you like the music,"
        " children?\nYes. It's very beautiful. \nWhat is it?\nThe Four Seasons. \nToday we'll draw the seasons. \nWhich season do you like best, Mike? "
        "\nWinter. I like snow.\nI like snow,too. \nWhich season do you like best, Wu Yifan? \nSpring. It's pretty. \nYes, it is.\",\"type\":2,\"title\":\"Let's try \","
        "\"ptime\":20}]";


char *contentCB =
"["
"{\"score\":0,\"type_name\":\"背诵\",\"id\":116,\"title\":\"Let'slearn我们来学一学\",\"type\":1},"
"{\"score\":0,\"type_name\":\"背诵\",\"id\":100,\"title\":\"tplopic1\",\"type\":1},"
"{\"score\":0,\"type_name\":\"背诵\",\"id\":122,\"title\":\"TwoMyfavoriteseason\",\"type\":1},"
"{\"score\":0,\"type_name\":\"朗读\",\"id\":123,\"title\":\"Let'stry\",\"type\":2},"
"{\"score\":0,\"type_name\":\"听力\",\"id\":124,\"title\":\"Let'slearn\",\"type\":3},"
"{\"score\":0,\"type_name\":\"背诵\",\"id\":139,\"title\":\"Myfamily\",\"type\":1}"
"]";

char * workList =
"["
"{\"score\": 95,\"id\": 16,\"time\": \"2019-09-28\", \"title\": \"123\",\"type\": \"朗读 背诵\",\"status\": 3},"
"{\"score\": 95,\"id\": 28,\"time\": \"2019-09-28\",\"title\": \"Section a\",\"type\": \"听力 朗读 背诵\",\"status\": 3}"
"]";


char * classList =
"["
"{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\","
"\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":117,\"NAME\":\"新建一班\"},"
"{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\","
"\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":118,\"NAME\":\"新建二班\"},"
"{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\","
"\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":119,\"NAME\":\"新建三班\"},{"
"\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\","
"\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":120,\"NAME\":\"我的班级\"}] ";




const char *classtest =
"[{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\",\"role_name\":\"学生\",\"role\":2,\"size\":2,\"id\":123,\"NAME\":\"福永小学\"},"
"{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\",\"role_name\":\"学生\",\"role\":2,\"size\":3,\"id\":141,\"NAME\":\"创业2班\"},"
"{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\",\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":143,\"NAME\":\"创业1班\"}]";




//==========================test===============================================
int ClassList(char * buf,int len)
{
    char *bufttt = "[{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\",\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":149,\"NAME\":\"声乐1班\"},{\"cover\":\"https://djyos-qinjian.oss-cn-hangzhou.aliyuncs.com/kouyu/imgs/class_avatar.jpg\",\"role_name\":\"学生\",\"role\":2,\"size\":1,\"id\":150,\"NAME\":\"演讲1班\"}]";

//    strcpy(buf,classtest);
    strcpy(buf,bufttt);
    return 0;
}

//内容列表
int DevGetPastHWork(int id,char *buf,int len)
{
    char *p = "";
    switch (id)
    {
        case 132:
            p = "[{\"type_name\":\"背诵\",\"id\":157,\"title\":\"How many\",\"type\":1}]";
            break;
        case 134:
            p = "[{\"type_name\":\"朗读\",\"id\":149,\"title\":\"Let's talk A\",\"type\":2}]";
            break;
        case 135:
            p = "[{\"type_name\":\"朗读\",\"id\":155,\"title\":\" Do you like pears\",\"type\":2}]";
            break;
        case 138:
            p = "[{\"type_name\":\"朗读\",\"id\":138,\"title\":\"Welcome!\",\"type\":2}]";
            break;
        case 139:
            p = "[{\"type_name\":\"背诵\",\"id\":139,\"title\":\"My family\",\"type\":1}]";
            break;
        default:
            p = "[{\"type_name\":\"背诵\",\"id\":139,\"title\":\"My family\",\"type\":1}]";
            break;
    }




    strcpy(buf,p);
    return 0;
}

int DevGetTodayHWork(int id,char *buf,int len)
{
    char *p = "";
    switch (id)
    {

    case 144:
        p = "[{\"type_name\":\"背诵\",\"id\":116,\"title\":\"Let's learn 我们来学一学\",\"type\":1}]";
        break;
    case 145:
        p = "[{\"type_name\":\"朗读\",\"id\":129,\"title\":\"Let's try\",\"type\":2}]";
        break;

//    case 140:
//        p = "[{\"type_name\":\"背诵\",\"id\":139,\"title\":\"My family\",\"type\":1}]";
//        break;
//    case 141:
//        p = "[{\"type_name\":\"朗读\",\"id\":149,\"title\":\"Let's talk A\",\"type\":2}]";
//        break;
        default:
        p = "[{\"type_name\":\"朗读\",\"id\":129,\"title\":\"Let's try\",\"type\":2}]";
            break;
    }

    strcpy(buf,p);
    return 0;
}

//作业列表
int DevGetTodayHWorkList(int cid,char *buf,int len)
{
    char *p = "";
    switch (cid)
    {
    case 149:
       p =  "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":144,\"time\":\"2019-10-17\",\"title\":\"10月17日上午\",\"type\":\"背诵\",\"status\":1}]";
     break;

    case 150:
       p =  "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":145,\"time\":\"2019-10-17\",\"title\":\"演讲稿\",\"type\":\"朗读\",\"status\":1}]";
     break;



//    case 123:
//        p = "[]";
//        break;
//    case 141:
//        p = "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":140,\"time\":\"2019-10-16\",\"title\":\"10月16日十点\",\"type\":\"背诵\",\"status\":2},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":141,\"time\":\"2019-10-16\",\"title\":\"10月16日十一点\",\"type\":\"朗读\",\"status\":2}]";
//        break;
//    case 143:
//        p = "[]";
//        break;
        default:
       p =  "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":145,\"time\":\"2019-10-17\",\"title\":\"演讲稿\",\"type\":\"朗读\",\"status\":1}]";
            break;
    }
    strcpy(buf,p);
    return 0;
}
int DevGetPastHWorkList(int cid,char *buf,int len)
{
    char *p = "";
    switch (cid)
    {
        case 149:
        case 150:
            p = "[]";
            break;

//        case 123:
//            p = "[{\"score\":5,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":110,\"time\":\"2019-10-14\",\"title\":\"10月14日作业\",\"type\":\"背诵\",\"status\":3},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":114,\"time\":\"2019-10-14\",\"title\":\"10月14日作业\",\"type\":\"听力\",\"status\":1},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":119,\"time\":\"2019-10-15\",\"title\":\"英语背诵\",\"type\":\"背诵\",\"status\":2},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":120,\"time\":\"2019-10-15\",\"title\":\"10月15日凌晨\",\"type\":\"背诵\",\"status\":2}]";
//            break;
//        case 141:
//            p = "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":132,\"time\":\"2019-10-15\",\"title\":\"10月15日上午\",\"type\":\"背诵\",\"status\":1},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":134,\"time\":\"2019-10-15\",\"title\":\"10月15日下午\",\"type\":\"朗读\",\"status\":1},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":135,\"time\":\"2019-10-15\",\"title\":\"10月15日四点\",\"type\":\"朗读\",\"status\":1}]";
//
//            break;
//        case 143:
//            p = "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":138,\"time\":\"2019-10-15\",\"title\":\"10月15日七点\",\"type\":\"朗读\",\"status\":1},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":139,\"time\":\"2019-10-15\",\"title\":\"10月15日八点\",\"type\":\"背诵\",\"status\":1}]";
//
//            break;
        default:
            p = "[{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":138,\"time\":\"2019-10-15\",\"title\":\"10月15日七点\",\"type\":\"朗读\",\"status\":1},{\"score\":0,\"stuid\":\"9f3097b75d96419a929741d4aef9b98e\",\"id\":139,\"time\":\"2019-10-15\",\"title\":\"10月15日八点\",\"type\":\"背诵\",\"status\":1}]";

            break;
    }

    strcpy(buf,p);
    return 0;
}
int DevGetTopicQuestion(int qid, char *out_json, int len)
{
    char * p = "";
    switch (qid)
    {
    case 116:
        p = "[{\"difficulty\":3,\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"kind\":\"全文\",\"unitid\":70,\"id\":116,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"Hi, I'm Amy. 你好，我是艾米。\nI'm from the UK. 我是英国人。\nUK 英国\nCanada 加拿大\nUSA 美国\nChina 中国\",\"type\":1,\"title\":\"Let's learn 我们来学一学\",\"ptime\":3}]";
        break;
    case 129:
        p = "[{\"difficulty\":2,\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"kind\":\"单词\",\"unitid\":80,\"id\":129,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"Let's try \nWhich month are they talking about? Listen and tick.\nWhen is the sports meet, Zhang Peng?\nIt's on April 4th.\nIt's next Friday then. Good luck.\nThanks.\nLet's talk\nThere are some special days in April.\nWhat are they?\nApril fool's Day and Easter.\nWhen is April fool's Day?\nIt's on April lst.\nAnd Easter?\nIt's on April 5th this year.\nWow! I love April.\nTalk about the holidays with your partner.\nWhen is China's National Day?\nIt's on October 1st.\nChildren's Day   \nJune 1st\nApril fool's Day\nApril 1st\nNew Year's Day\nJanuary 1st\",\"type\":2,\"title\":\"Let's try\",\"ptime\":20}]";
        break;
        default:
        p = "[{\"difficulty\":2,\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"kind\":\"单词\",\"unitid\":80,\"id\":129,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"Let's try \nWhich month are they talking about? Listen and tick.\nWhen is the sports meet, Zhang Peng?\nIt's on April 4th.\nIt's next Friday then. Good luck.\nThanks.\nLet's talk\nThere are some special days in April.\nWhat are they?\nApril fool's Day and Easter.\nWhen is April fool's Day?\nIt's on April lst.\nAnd Easter?\nIt's on April 5th this year.\nWow! I love April.\nTalk about the holidays with your partner.\nWhen is China's National Day?\nIt's on October 1st.\nChildren's Day   \nJune 1st\nApril fool's Day\nApril 1st\nNew Year's Day\nJanuary 1st\",\"type\":2,\"title\":\"Let's try\",\"ptime\":20}]";
            break;
    }

    strcpy(out_json,p);
    return 0;
}



int DeleteNews(char *news_ids)
{
    return 0;
}

int MarkedNews(int id)
{
    return 0;
}



int SubmitHWork(char *homework_str)
{
    return 0;
}

int DevGetNewsList( char *out_json, int len)
{
    char * p = "[{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":129,\"time\":\"2019-10-1520:02:17\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\",\"read\":\"1\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":128,\"time\":\"2019-10-1520:02:17\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":126,\"time\":\"2019-10-1520:01:18\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":127,\"time\":\"2019-10-1520:01:18\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":124,\"time\":\"2019-10-1520:01:12\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":125,\"time\":\"2019-10-1520:01:12\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":122,\"time\":\"2019-10-1520:00:14\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":123,\"time\":\"2019-10-1520:00:14\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":121,\"time\":\"2019-10-1519:58:15\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":120,\"time\":\"2019-10-1519:58:15\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":118,\"time\":\"2019-10-1519:57:33\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":119,\"time\":\"2019-10-1519:57:33\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":116,\"time\":\"2019-10-1519:15:16\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":117,\"time\":\"2019-10-1519:15:16\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":114,\"time\":\"2019-10-1519:10:45\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":115,\"time\":\"2019-10-1519:10:45\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":113,\"time\":\"2019-10-1517:59:10\",\"content\":\"null在槟榔小学三（二）班布置了作业“测试11”\",\"cid\":53,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":104,\"time\":\"2019-10-1515:41:03\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":103,\"time\":\"2019-10-1513:41:56\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":102,\"time\":\"2019-10-1513:41:56\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":101,\"time\":\"2019-10-1513:41:29\",\"content\":\"王同学在华骏一班布置了作业“title”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":99,\"time\":\"2019-10-1513:39:58\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":100,\"time\":\"2019-10-1513:39:58\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":98,\"time\":\"2019-10-1513:39:34\",\"content\":\"null在槟榔小学三（二）班布置了作业“cehsi”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":96,\"time\":\"2019-10-1512:14:51\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":97,\"time\":\"2019-10-1512:14:51\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":95,\"time\":\"2019-10-1512:14:42\",\"content\":\"null在槟榔小学三（二）班布置了作业“测试10”\",\"cid\":53,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":94,\"time\":\"2019-10-1512:01:28\",\"content\":\"null在槟榔小学三（二）班布置了作业“title”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":93,\"time\":\"2019-10-1512:00:45\",\"content\":\"null在槟榔小学三（二）班布置了作业“title”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":91,\"time\":\"2019-10-1511:53:06\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":92,\"time\":\"2019-10-1511:53:06\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":90,\"time\":\"2019-10-1511:52:31\",\"content\":\"null在槟榔小学三（二）班布置了作业“测试10”\",\"cid\":53,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":89,\"time\":\"2019-10-1511:30:56\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":88,\"time\":\"2019-10-1507:56:22\",\"content\":\"null在槟榔小学三（二）班布置了作业“title”\",\"cid\":53,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":87,\"time\":\"2019-10-1507:49:01\",\"content\":\"null在槟榔小学三（二）班布置了作业“title”\",\"cid\":53,\"status\":\"未读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":42,\"time\":\"2019-10-1416:55:06\",\"content\":\"null在槟榔小学三（二）班布置了作业“自定义作业”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":41,\"time\":\"2019-10-1416:55:06\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":39,\"time\":\"2019-10-1416:53:26\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":40,\"time\":\"2019-10-1416:53:26\",\"content\":\"null在槟榔小学三（二）班布置了作业“自定义作业”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":37,\"time\":\"2019-10-1416:49:42\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":38,\"time\":\"2019-10-1416:49:42\",\"content\":\"null在槟榔小学三（二）班布置了作业“自定义作业”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":null,\"news_type\":3,\"nid\":0,\"id\":36,\"time\":\"2019-10-1416:49:22\",\"content\":\"王伟明在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":34,\"time\":\"2019-10-1416:41:19\",\"content\":\"王同学在华骏一班布置了作业“自定义作业”\",\"cid\":51,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":35,\"time\":\"2019-10-1416:41:19\",\"content\":\"null在槟榔小学三（二）班布置了作业“自定义作业”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":27,\"time\":\"2019-10-1216:33:48\",\"content\":\"null在槟榔小学三（二）班布置了作业“title”\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":25,\"time\":\"2019-10-1209:37:10\",\"content\":\"您今天还有作业尚未批改，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":24,\"time\":\"2019-10-1209:37:10\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":23,\"time\":\"2019-10-1209:35:35\",\"content\":\"你今天的作业还没有完成哦！\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":22,\"time\":\"2019-10-1116:34:02\",\"content\":\"你布置了新的作业\",\"cid\":53,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":3,\"nid\":0,\"id\":21,\"time\":\"2019-10-1116:32:49\",\"content\":\"你布置了新的作业\",\"cid\":5,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":20,\"time\":\"2019-10-1115:41:58\",\"content\":\"老师已批改你的作业，快去看看吧！\",\"cid\":null,\"status\":\"已读\"},"
            "{\"uid\":\"31d64ca61a01482b8b544fa9d9b5511e\",\"news_type\":1,\"nid\":0,\"id\":19,\"time\":\"2019-10-1115:37:43\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":null,\"status\":\"已读\"}]";



    p =
    "[{\"uid\":\"bcb834c90c224526a276117c3e0a0f09\",\"read\":0,\"news_type\":3,\"nid\":0,\"id\":406,\"time\":\"2019-10-24 14:52:05\",\"content\":\"null在声乐1班布置了作业“10月24日作业”\",\"cid\":149,\"status\":\"未读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":394,\"time\":\"2019-10-23 19:34:45\",\"content\":\"老师已批改你的作业，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},{\"uid\":\"bcb834c90c224526a276117c3e0a0f09\",\"read\":0,\"news_type\":3,\"nid\":0,\"id\":373,\"time\":\"2019-10-23 10:04:24\",\"content\":\"null在演讲1班布置了作业“10月23日作业”\",\"cid\":150,\"status\":\"未读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":372,\"time\":\"2019-10-23 09:42:37\",\"content\":\"老师已批改你的作业，快去看看吧！\",\"cid\":0,\"status\":\"已读\"},{\"uid\":\"bcb834c90c224526a276117c3e0a0f09\",\"read\":1,\"news_type\":3,\"nid\":0,\"id\":337,\"time\":\"2019-10-22 11:53:20\",\"content\":\"null在声乐1班布置了作业“10月22日作业”\",\"cid\":149,\"status\":\"已读\"},{\"uid\":\"bcb834c90c224526a276117c3e0a0f09\",\"read\":0,\"news_type\":3,\"nid\":0,\"id\":222,\"time\":\"2019-10-19 10:02:13\",\"content\":\"null在声乐1班布置了作业“10月19日作业”\",\"cid\":149,\"status\":\"未读\"},{\"uid\":\"bcb834c90c224526a276117c3e0a0f09\",\"read\":0,\"news_type\":3,\"nid\":0,\"id\":221,\"time\":\"2019-10-19 10:01:48\",\"content\":\"null在演讲1班布置了作业“10月19日上午”\",\"cid\":150,\"status\":\"未读\"},{\"uid\":null,\"read\":1,\"news_type\":3,\"nid\":0,\"id\":180,\"time\":\"2019-10-18 10:52:52\",\"content\":\"null在声乐1班布置了作业“10月18日上午”\",\"cid\":149,\"status\":\"已读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":176,\"time\":\"2019-10-17 19:14:22\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":174,\"time\":\"2019-10-17 19:05:13\",\"content\":\"恭喜！你的作业已完成并提交。\",\"cid\":0,\"status\":\"已读\"},{\"uid\":null,\"read\":0,\"news_type\":3,\"nid\":0,\"id\":163,\"time\":\"2019-10-17 12:22:24\",\"content\":\"null在演讲1班布置了作业“演讲稿”\",\"cid\":150,\"status\":\"未读\"},{\"uid\":null,\"read\":0,\"news_type\":3,\"nid\":0,\"id\":162,\"time\":\"2019-10-17 12:21:58\",\"content\":\"null在声乐1班布置了作业“10月17日上午”\",\"cid\":149,\"status\":\"未读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":161,\"time\":\"2019-10-17 12:21:29\",\"content\":\"你已成功加入150班\",\"cid\":0,\"status\":\"已读\"},{\"uid\":\"9f3097b75d96419a929741d4aef9b98e\",\"read\":1,\"news_type\":1,\"nid\":0,\"id\":160,\"time\":\"2019-10-17 12:21:19\",\"content\":\"你已成功加入149班\",\"cid\":0,\"status\":\"已读\"}]";


    size_t size = strlen(p);
    if((int)size >= len)
    {
        printf("buf over error \n\r");
    }

    strcpy(out_json,p);
    return 0;
}




int GetTopicList( int id ,char *out_json, int len)
{

//    char *p = "[{\"type_name\":\"背诵\",\"size\":1,\"questions\":"
//            "[{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\","
//            "\"type_name\":\"背诵\",\"kind\":\"句子\",\"id\":1,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\","
//            "\"text\":\"This is the text of book\",\"type\":1,\"title\":\"Section A\"}],\"type\":1},"
//            "{\"type_name\":\" 朗读\",\"size\":2,\"questions\":"
//            "[{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"type_name\":\"朗读\",\"kind\":\"全文\",\"id\":2,"
//            "\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"This is the text of book\","
//            "\"type\":2,\"title\":\"Section B\"},"
//            "{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\""
//            ",\"type_name\":\"朗读\",\"kind\":\"句子\",\"id\":4,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\","
//            "\"text\":\"Hel! Help! It's too high! Froggy wangts tobe a pilot. He wants to fly a plane, but he is afraid of flying.\",\"type\":2,\"title\":\"Froggy's new job\"}],"
//            "\"type\":2},{\"type_name\":\"听力\",\"size\":1,\"questions\":[{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\","
//            "\"type_name\":\"听力\",\"kind\":\"单词\",\"id\":3,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\","
//            "\"text\":\"This is the text of book\",\"type\":3,\"title\":\"Section C\"}],\"type\":3}]";




    char *p =
    "["
        "{"
            "\"type_name\": \"背诵\","
            "\"size\": 1,"
            "\"questions\": ["
            "{"
            "\"image\": \"a.png\","
            "\"score\": 20,"
            "\"type_name\": \"背诵\","
            "\"answer\": \"123\","
            "\"id\": 1,"
            "\"audio\": \"a.mp3\","
            "\"text\": \"This is the text of book\","
            "\"type\": 1,"
            "\"title\": \"Section A\""
            "}"
            "],"
            "\"type\": 1"
        "},"
        "{"
            "\"type_name\": \"朗读\","
            "\"size\": 5,"
            "\"questions\": ["
            "{"
                "\"image\": \"b.png\","
                "\"score\": 20,"
                "\"type_name\": \"朗读\","
                "\"answer\": \"hhah\","
                "\"id\": 2,"
                "\"audio\": \"b.mp3\","
                "\"text\": \"This is the text of book\","
                "\"type\": 2,"
                "\"title\": \"Section B\""
            "},"
            "{"
                "\"image\": \"http://www.171english.cn/shanghaiox/5a/gif/4.jpg\","
                "\"type_name\": \"朗读\","
                "\"id\": 4,"
                "\"audio\": \"*.mp3\","
                "\"text\": \"Hel! Help! It's too high! Froggy wangts tobe a pilot. He wants to fly a plane, but he is afraid of flying.\","
                "\"type\": 2,"
                "\"title\": \"Froggy's new job\""
            "},"
            "{"
                "\"image\": \"http://www.171english.cn/shanghaiox/5a/gif/4.jpg\","
                "\"type_name\": \"朗读\","
                "\"id\": 7,"
                "\"audio\": \"*.mp3\","
                "\"text\": \"Hel! Help! It's too high! Froggy wangts tobe a pilot. He wants to fly a plane, but he is afraid of flying.\","
                "\"type\": 2,"
                "\"title\": \"Froggy's new job\""
            "},"
            "{"
                "\"image\": \"http://www.171english.cn/shanghaiox/5a/gif/4.jpg\","
                "\"type_name\": \"朗读\","
                "\"id\": 8,"
                "\"audio\": \"*.mp3\","
                "\"text\": \"Hel! Help! It's too high! Froggy wangts tobe a pilot. He wants to fly a plane, but he is afraid of flying.\","
                "\"type\": 2,"
                "\"title\": \"Froggy's new job\""
            "},"
            "{"
                "\"image\": \"jpg\","
                "\"type_name\": \"朗读\","
                "\"id\": 9,"
                "\"audio\": \"mp3\","
                "\"text\": \"abcd\","
                "\"type\": 2,"
                "\"title\": \"Section F\""
            "}"
            "],"
            "\"type\": 2"
        "},"
        "{"
            "\"type_name\": \"听力\","
            "\"size\": 2,"
            "\"questions\": ["
            "{"
                "\"image\": \"a.png\","
                "\"score\": null,"
                "\"type_name\": \"听力\","
                "\"answer\": \"55555\","
                "\"id\": 3,"
                "\"audio\": \"c.mp3\","
                "\"text\": \"This is the text of book\","
                "\"type\": 3,"
                "\"title\": \"Section C\""
            "},"
            "{"
                "\"image\": \"a.png\","
                "\"score\": 29,"
                "\"type_name\": \"听力\","
                "\"answer\": \"44444\","
                "\"id\": 3,"
                "\"audio\": \"c.mp3\","
                "\"text\": \"This is the text of book\","
                "\"type\": 3,"
                "\"title\": \"Section C\""
            "}"
            "],"
            "\"type\": 3"
        "}"
    "]";


      p = "["
            "{\"type_name\":\"背诵\",\"size\":1,\"questions\":"
            "["
            "{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"type_name\":\"背诵\",\"kind\":\"句子\",\"id\":1,"
            "\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"This is the text of book\",\"type\":1,\"title\":\"Section A\"}"
            "],\"type\":1},"

            "{\"type_name\":\" 朗读\",\"size\":2,\"questions\":"
            "[{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"type_name\":\"朗读\",\"kind\":\"全文\",\"id\":2,"
            "\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":\"This is the text of book\",\"type\":2,\"title\":\"Section B\"},"
            "{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"type_name\":\"朗读\",\"kind\":\"句子\",\"id\":4,"
            "\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\",\"text\":"
            "\"Hel! Help! It's too high! Froggy wangts tobe a pilot. He wants to fly a plane, but he is afraid of flying.\","
            "\"type\":2,\"title\":\"Froggy's new job\"}],\"type\":2},{\"type_name\":\"听力\",\"size\":1,\"questions\":"
            "["
            "{\"image\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/543fbc4140907efa80eccf33dde276f1.png\",\"type_name\":\"听力\","
            "\"kind\":\"单词\",\"id\":3,\"audio\":\"https://bmob-cdn-26324.bmobpay.com/2019/09/28/9fb70aeb406ace9d80c83f5e03fc8e7a.wav\","
            "\"text\":\"This is the text of book\",\"type\":3,\"title\":\"Section C\"}],\"type\":3}"
            "]";
    int size = (int)strlen(p);
    if(size >= len)
    {
        printf("buf over error \n\r");
    }

    strcpy(out_json,p);
    return 0;
}
int DevGetHWorkInfo(int id ,char *out_json,int len){


    char * p = "{"
    "\"success\": true,"
    "\"code\": \"0000\","
    "\"msg\": \"ok\","
    "\"data\": {"
    "\"id\": 1,"
    "\"name\": \"9月9日第一单元作业\","
    "\"cover\": \"*.png\","
    "\"content\": \"作业简介\","
    "\"tname\": \"李老师\","
    "\"ctime\": \"2019-09-09 18:00:00\","
    "\"etime\": \"2019-09-09 21:00:00\","
    "\"ftime\": \"2019-09-09 21:00:00\","
    "\"ptime\": 30,"
    "\"status\": 3,"
    "\"score\": 80,"
    "\"rank\": 5,"
    "\"comment\": \"这个学生的态度很不错\","
    "\"audio\": \"xxx.wav\","
    "\"asize\": 2,"
    "\"bsize\": 3,"
    "\"csize\": 0"
    "}"
    "}";

    int size = (int)strlen(p);
    if(size >= len)
    {
        printf("buf over error \n\r");
    }

    strcpy(out_json,p);

    return 0;
}
#endif

