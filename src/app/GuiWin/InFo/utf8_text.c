
#include <stddef.h>
#include "../inc/utf8_text.h"
#include "charset.h"

const char * Get_Utf8Text(enum Utf8_Text enumtext)
{
    const char utf8_TeacherComments[] = {0x20,0xe6,0x95,0x99,0xe5,0xb8,0x88,0xe8,0xaf,0x84,0xe8,0xaf,0xad,0xef,0xbc,0x9a,0x00};
    const char utf8_HomeWorkType[] = {0xe4,0xbd,0x9c,0xe4,0xb8,0x9a,0xe7,0xb1,0xbb,0xe5,0x9e,0x8b,0xef,0xbc,0x9a,0x00};
    const char *ret = NULL;
    switch (enumtext)
    {
    case utf8_text_TeacherComments:
        ret =  utf8_TeacherComments;
        break;
    case utf8_text_HomeWorkType:
        ret =  utf8_HomeWorkType;
        break;
        default:
            break;
    }
    return ret;
}


/*==============================================================================
 * 功能：将一行UTF8字符串添加换行符输出
 * 参数：instr输入字符串
 *       outstr 字符串输出缓冲区
 *       outlen 输出字符串最大长度
 *       bytenum 每行字节数
 * 返回：格式化后的行数
 *=========================================================================== */

int Utf8_Typesetting(const char *instr, char *outstr, int outlen, int bytenum)
{
    s32 cnt, num = 0;
    int datacnt = 1;
    int ret = 1;
    struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
    while (*instr != '\0')
    {
        cnt =  mblen_l(instr,6,myCharset);
        datacnt += cnt;
        if (datacnt > outlen)
            return -2;

        for (int i = 0; i < cnt; i++)
        {
            *outstr++ = *instr++;
        }

        num++;
        if (num >= bytenum)
        {
            datacnt += 2;
            if (datacnt > outlen)
                return -2;
            *outstr++ = '\n';
            *outstr++ = '\r';
            num = 0;
            ret ++;
        }
    }
    *outstr++ = '\0';

    return ret;
}

int Utf8_Typesetting_2(const char *instr, char *outstr, int outlen, int bytenum)
{
    int cnt, num = 0;
    int datacnt = 1;
    int ret = 1;

    struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
    while (*instr != '\0')
    {
        cnt =  mblen_l(instr,6,myCharset);
        datacnt += cnt;
        if (datacnt > outlen)
            return -2;

        for (int i = 0; i < cnt; i++)
        {
            *outstr++ = *instr++;
        }

        num++;
        if (num >= bytenum)
        {
            datacnt += 2;
            if (datacnt > outlen)
                return -2;
            *outstr++ = '.';
            *outstr++ = '.';
            *outstr++ = '.';
            num = 0;
            ret ++;
            break;
        }
    }
    *outstr++ = '\0';

    return ret;
}

int Get_Utf8_Typesetting_Page(const char *instr,int bytenum,int line)
{
    int cnt, num = 0;
    int datacnt = 0;
    int ret = 1;
    int page=1;
    struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);

    while (*instr != '\0')
    {
        cnt =  mblen_l(instr,6,myCharset);
        datacnt += cnt;
        if(cnt==1)
            num+=8;
        else if(cnt==3)
            num+=16;

        for (int i = 0; i < cnt; i++)
        {
            instr++;
            if(*instr=='\n')
            {
                num = 0;
                ret ++;
            }
        }

        if (num >= bytenum)
        {
            num = 0;
            ret ++;
        }
    }

    page = ret%line;
    if(page)
        page= ret/line+1;
    else
        page= ret/line;

    return page;
}

int Utf8_Typesetting_3(const char *instr, char *outstr, int bytenum,int offset,int line)
{
    int cnt, num = 0;
    int datacnt = 0;
    int ret = 1;

    instr += offset;

    struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
    while (*instr != '\0')
    {
        cnt =  mblen_l(instr,6,myCharset);
        datacnt += cnt;

        if(cnt==1)
            num+=8;
        else if(cnt==3)
            num+=16;

        for (int i = 0; i < cnt; i++)
        {
            *outstr++ = *instr++;
            if(*instr=='\n')
            {
                num = 0;
                ret ++;
            }
        }

        if (num >= bytenum)
        {
            *outstr++ = '\n';
            *outstr++ = '\r';
            num = 0;
            ret ++;
        }

        if(ret > line)
            break;
    }
    *outstr++ = '\0';

    return datacnt;
}



