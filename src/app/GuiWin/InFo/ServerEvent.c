//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * BmpInfo.c
 *  Created on: 2019年9月20日
 *      Author: czz
 *  功能：命令缓存功能
 */

#include "../inc/GuiInfo.h"
#include "stdio.h"
#include "cJSON.h"
#include "string.h"
#include "stdlib.h"
#include "../inc/ServerInfo.h"
#include "../inc/ServerEvent.h"
#include "device_user.h"
#include "cpu_peri_wifi.h"
#include "ring.h"

static struct {
    struct RingBuf *ring;
    struct SemaphoreLCB *SendCmdSemp;
    struct SemaphoreLCB *DoCmdSemp;
    struct MutexLCB *mutex;
    u8 CmdInBufFlag[cmd_ServerCmdmax];
}serverdata;
static struct StHWorkTopic St_HWorkTopic[20];

int  Get_DeviceSn(char *buf,int buflen);
int record_ctrl(int val, char *oss_path);
int WiFiOpen(unsigned int timeout_ms);
int WiFiClose(unsigned int timeout_ms);
int record_ctrl(int val, char *oss_path);
int OssPlayMedia(char *path, int is_record, int (*event_cb) ());
int OssBreakPlayer();
static int Do_Homework_listenRecording(size_t len)
{
    int is_record = 0;
    int ret = 0;
    para_Homework_listenRecording  para;
    if(len != sizeof(para_Homework_listenRecording))
    {
        Lock_MutexPost(serverdata.mutex);
        ret = -1;
        goto Error_return;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        ret =  -2;
        goto Error_return;
    }
    Lock_MutexPost(serverdata.mutex);


    char buf[200];

    memset(buf,0,sizeof(buf));
    if(para.Topic->answer[0] == '\0')
    {
        char snbuf[50];
        int  Get_DeviceSn(char *buf,int buflen);
        Get_DeviceSn(snbuf,sizeof(snbuf));
        printf("cnbuf :%s \n\r",snbuf);
        sprintf(buf,"/devtemp/%s/tmp_%d_%d_%d.wav",snbuf,para.class->classid,para.homework->workid,para.Topic->id);
        printf("buf : %s \n\r",buf);
        is_record = 1;
    }
    else
    {
        is_record = 0;
        if(strlen(para.Topic->answer)<sizeof(buf))
        {
            strcpy(buf,para.Topic->answer);
        }else
        {
            printf("error : url Too long path \n\r");
        }
    }
    printf("OssPlayMedia :%s   \n\r",buf);
    printf("fun:0x %x   \n\r",(u32)para.fun);

    OssPlayMedia(buf,is_record,para.fun);
    return 0;
Error_return:
    para.fun();
    return ret;
}
static int Do_Homework_listenRecordingStop(size_t len)
{
    para_Homework_listenRecordingStop  para;
    if(len != sizeof(para_Homework_listenRecordingStop))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);
    OssBreakPlayer();
    return 0;
}


static int Do_Details_PlayAudioStart(size_t len)
{
    para_Details_PlayAudioStart para;
    if(len != sizeof(para_Details_PlayAudioStart))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    OssPlayMedia(para.homework->p_details->audio,0,para.fun);
    return 0;
}
static int Do_Details_PlayAudioStop(size_t len)
{
    para_Details_PlayAudioStop para;
    if(len != sizeof(para_Details_PlayAudioStop))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);
    OssBreakPlayer();
    return 0;
}

static int Do_Homework_StartRecording(size_t len)
{
    para_Homework_StartRecording para;
    if(len != sizeof(para_Homework_StartRecording))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    char buf[100];
    char snbuf[50];
    Get_DeviceSn(snbuf,sizeof(snbuf));
    sprintf(buf,"/devtemp/%s/tmp_%d_%d_%d.wav",snbuf,\
            para.class->classid,para.homework->workid,para.Topic->id);

    printf("StartRecording  :%s   \n\r",buf);
    record_ctrl(1,buf);
    return 0;

}

static int Do_Homework_StopRecording(size_t len)
{
    para_Homework_StopRecording para;
    if(len != sizeof(para_Homework_StopRecording))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    char buf[100];
    char snbuf[50];
    Get_DeviceSn(snbuf,(int)sizeof(snbuf));
    sprintf(buf,"/devtemp/%s/tmp_%d_%d_%d.wav",snbuf\
            ,para.class->classid,para.homework->workid,para.Topic->id);
    printf("StopRecording:%s   \n\r",buf);
    record_ctrl(0,buf);

    return 0;
}

static int DO_Homework_PlayAudio(size_t len)
{
    para_Homework_PlayAudio para;
    int ret = 0;
    if(len != sizeof(para_Homework_PlayAudio))
    {
        Lock_MutexPost(serverdata.mutex);
        ret = -1;
        goto Error_return;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        ret = -2;
        goto Error_return;
    }
    Lock_MutexPost(serverdata.mutex);

    printf("播放标准音频 课程ID:%d ,作业id:%d ,题号id:%d \n\r",para.class->classid,para.homework->workid,para.Topic->id);

    printf("Homework PlayAudio :%s \n\r",para.Topic->audio);
    char * ch = strstr(para.Topic->audio,".com");
    if(ch != NULL)
    {
        ch += strlen(".com");
        char buf[512];
        if(strlen(ch) > sizeof(buf))
        {
            ret = -3;
            goto Error_return;
        }

        strcpy(buf,ch);
        printf("Homework PlayAudio buf :%s \n\r",buf);
        printf("fun:0x %x   \n\r",(u32)para.fun);
        OssPlayMedia(buf,0,para.fun);

        return 0;
    }
    ret = -4;
Error_return:
    para.fun();
    return ret;
}

static int DO_Homework_PlayAudioStop(size_t len)
{
    para_Homework_PlayAudioStop para;
    if(len != sizeof(para_Homework_PlayAudioStop))
    {
        return -1;
        Lock_MutexPost(serverdata.mutex);
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);


    char * ch = strstr(para.Topic->audio,".com");
    if(ch != NULL)
    {
        ch += strlen(".com");
        char buf[150];
        if(strlen(ch) > sizeof(buf))
            goto Error_return;

        strcpy(buf,ch);
        printf("Homework Stop PlayAudio buf :%s \n\r",buf);
        OssBreakPlayer();
        return 0;
    }

Error_return:
    return -100;


}
static bool_t Submit_HomeWorkRecord(u32 courseid,u32 workid,u32 cntid,u32 cnt)
{
    if(cnt >= 20)
        return false;
    St_HWorkTopic[cnt].topic_id = cntid;
    //sprintf(St_HWorkTopic[cnt].url,"/test/tmp_%d_%d_%d.wav",course,work,cnt);
    char path[100]={0};
    char snbuf[50];
    int  Get_DeviceSn(char *buf,int buflen);
    Get_DeviceSn(snbuf,sizeof(snbuf));
    sprintf(path,"/devtemp/%s/tmp_%d_%d_%d.wav",snbuf,courseid,workid,cntid);
    sprintf(St_HWorkTopic[cnt].url,"{\"image\": \"\",\"audio\": \"%s\",\"video\": \"\",\"text\": \"\"}", path);
    return true;
}

static int Do_Submit_JobOfid(size_t len)
{
    para_Submit_JobOfid para;
    if(len != sizeof(para_Submit_JobOfid))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    char *buf;
    WiFiOpen(5*1000*mS);
    struct TopicCB* pcontent = NULL;
    buf = malloc(2048*2);
    if(buf == NULL)
        return -3;

    int num = Get_HomeWorkTopicNUm(para.homework);
    int worknum = 0;
    for(int i = 0;i < num; i++)
    {
        pcontent = Get_TopicOfCnt(para.homework,i);
        switch (pcontent->type)
        {
            case 1: //背诵
            case 2: //朗读
            case 4: //朗读
                if(false == Submit_HomeWorkRecord(para.class->classid,para.homework->workid,pcontent->id,worknum))
                {
                    printf("Submit_HomeWorkRecord error \n\r");
                    return -4;
                }
                worknum++;
                break;
            case 3: //听力
                break;
            default:
                break;
        }
    }

    if(0 == gen_json_homework(para.homework->workid, St_HWorkTopic, worknum, buf,2048*2))
    {
        printf("gen_json_homework error \n\r");
        goto Error_return;
    }
    int SubmitHWork(char *homework_str);
    int ret = SubmitHWork(buf);
    printf("Submit JobOfid work %s num:%d ret: %d \n\r" ,para.homework->title,num,ret);
    free(buf);
    WiFiClose(5*1000*mS);
    if(ret < 0)
        return -6;
    Update_ServerInfo();
    return 0;
Error_return:
    WiFiClose(5*1000*mS);
    free(buf);
    return -100;
}
static int Do_Read_MsgNameOfcnt(size_t len)
{
    para_Read_MsgNameOfcnt para;
    if(len != sizeof(para_Read_MsgNameOfcnt))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    WiFiOpen(5*1000*mS);
    int MarkedNews(int id);
    struct MessagesCB * msg = Get_MessagesOfCnt(para.cnt);
    MarkedNews(msg->id);
    Read_Messages(msg);
    WiFiClose(5*1000*mS);
    return 0;
}

static int DO_Del_MsgNameOfBuf(size_t len)
{
    char *buf = malloc(len*12);
    char *idstr =buf+len;

    if(len != Ring_Read(serverdata.ring,(u8*)buf,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);


    WiFiOpen(5*1000*mS);
    char *p = idstr;
    if(idstr == NULL)
    {
        printf("%s %d malloc error !!\n\r",__FILE__,__LINE__);
        WiFiClose(5*1000*mS);
        return false;
    }
    memset(idstr,0,len*11);
    int DeleteNews(char *news_ids);
    struct MessagesCB * msg;
    for(size_t i=0;i< len;i++)
    {
        if(buf[i])
        {
            msg = Get_MessagesOfCnt(i);
            if(msg!=NULL)
            {
                sprintf(p,"%s%d,",idstr,msg->id);
            }
        }
    }
    size_t idstrlen = strlen(idstr);
    idstr[idstrlen-1] = '\0';
    DeleteNews(idstr);
    Update_Messages();

    WiFiClose(5*1000*mS);
    return true;
}

static int Do_Del_MsgNameOfcnt(size_t len)
{
    para_Del_MsgNameOfcnt para;
    if(len != sizeof(para_Del_MsgNameOfcnt))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    char idstr[11];
    WiFiOpen(5*1000*mS);
    int DeleteNews(char *news_ids);
    struct MessagesCB * msg = Get_MessagesOfCnt(para.cnt);
    sprintf(idstr,"%d",msg->id);
    DeleteNews(idstr);
    Update_Messages();
    WiFiClose(5*1000*mS);
    return 0;
}

static int Do_HomeWorkPull(size_t len)
{
    int para;
    if(len != sizeof(int))
    {
        Lock_MutexPost(serverdata.mutex);
        return -1;
    }
    if(len != Ring_Read(serverdata.ring,(u8*)&para,len))
    {
        Lock_MutexPost(serverdata.mutex);
        return -2;
    }
    Lock_MutexPost(serverdata.mutex);

    Update_ServerInfo();
    return 0;
}

static int Server_DoComd(enumServerCmd cmd,size_t len)
{
    bool_t Set_SubmitFlag(bool_t);
    bool_t Refresh_MainWin(void);
    int ret = 0;
    printf("Server_DoComd cmd: %d len : %d \n\r",cmd,len);

    switch (cmd)
    {
        case cmd_Homework_listenRecording:
            ret = Do_Homework_listenRecording(len);
            break;
        case cmd_Homework_listenRecordingStop:
            ret = Do_Homework_listenRecordingStop(len);
            break;

        case cmd_Homework_StartRecording:
            ret = Do_Homework_StartRecording(len);
            break;

        case cmd_Homework_StopRecording:
            ret = Do_Homework_StopRecording(len);
            break;

        case cmd_Homework_PlayAudio:
            ret = DO_Homework_PlayAudio(len);
            break;
        case cmd_Homework_PlayAudioStop:
            ret = DO_Homework_PlayAudioStop(len);
            break;
        case cmd_Details_PlayAudioStart:
            ret = Do_Details_PlayAudioStart(len);
            break;
        case cmd_Details_PlayAudioStop:
            ret = Do_Details_PlayAudioStop(len);
            break;
        case cmd_Submit_JobOfid:
            ret = Do_Submit_JobOfid(len);
            if(ret < 0)
            {
                Set_SubmitFlag(false);
            }
            else
            {
                Set_SubmitFlag(true);
                Djy_EventDelay(500*mS);
                Refresh_MainWin();
            }
            break;

        case cmd_Read_MsgNameOfcnt:
            ret = Do_Read_MsgNameOfcnt(len);
            break;

        case cmd_Del_MsgNameOfBuf:
            ret = DO_Del_MsgNameOfBuf(len);
            break;

        case cmd_Del_MsgNameOfcnt:
            ret = Do_Del_MsgNameOfcnt(len);
            break;
        case cmd_HomeWorkPull:
            ret = Do_HomeWorkPull(len);
            break;
        default:
            ret = -1000;
            break;
    }
    serverdata.CmdInBufFlag[cmd] = false;
    return ret;
}

bool_t Server_SendComd(enumServerCmd cmd,void*para,size_t paralen,u32 timeout)
{
    bool_t ret = false;

    if(serverdata.CmdInBufFlag[cmd] == true)
       return true;
    serverdata.CmdInBufFlag[cmd] = true;
    while(1)
    {
        u32 size = Ring_CheckFree(serverdata.ring);
        if(size < sizeof(enumServerCmd)+paralen+sizeof(paralen))
        {
            if(false ==  Lock_SempPend(serverdata.DoCmdSemp,timeout))
            return false;
        }else
        {
            break;
        }
    }

    if(true == Lock_MutexPend(serverdata.mutex,CN_TIMEOUT_FOREVER))
    {
        printf("Server_SendComd cmd: %d len : %d \n\r",cmd,paralen);

       if(sizeof(enumServerCmd) != Ring_Write(serverdata.ring,(u8*)&cmd,sizeof(enumServerCmd)))
       {
           printf("error : Ring_Write enumServerCmd \n\r");
       }
       if(sizeof(paralen) != Ring_Write(serverdata.ring,(u8*)&paralen,sizeof(paralen)))
       {
           printf("error : Ring_Write sizeof(paralen) \n\r");
       }
       if(paralen != Ring_Write(serverdata.ring,para,paralen))
       {
           printf("error : Ring_Write paralen \n\r");
       }
       Lock_MutexPost(serverdata.mutex);
       Lock_SempPost(serverdata.SendCmdSemp);
       ret = true;
    }
    return ret;
}


static ptu32_t Server_Event(void)
{
    enumServerCmd cmd;
    size_t paralen;
    int  ret;

    while(1)
    {
        Lock_SempPend(serverdata.SendCmdSemp,CN_TIMEOUT_FOREVER);
        if(true == Lock_MutexPend(serverdata.mutex,CN_TIMEOUT_FOREVER))
        {

            if(sizeof(enumServerCmd) != Ring_Read(serverdata.ring,(u8*)&cmd,sizeof(enumServerCmd)))
            {
                printf("error : Ring_Read enumServerCmd \n\r");
            }
            if(sizeof(paralen) != Ring_Read(serverdata.ring,(u8*)&paralen,sizeof(paralen)))
            {
                printf("error : Ring_Read sizeof(paralen) \n\r");
            }
            ret = Server_DoComd(cmd,paralen);
            if(ret < 0)
            {
                printf("error : Server_DoComd: %d ret :%d  !!\r\n",cmd,ret);
            }
            Lock_SempPost(serverdata.DoCmdSemp);
        }
    }
    return 0;
}



int Server_EventInit()
{
    serverdata.SendCmdSemp = Lock_SempCreate(CN_LIMIT_SINT32,0,CN_BLOCK_FIFO,"Send Semp");
    serverdata.DoCmdSemp   = Lock_SempCreate(1,0,CN_BLOCK_FIFO,"DoCmdSemp");
    serverdata.mutex = Lock_MutexCreate("ServerMutex");
    serverdata.ring = Ring_Create(512);
    memset(serverdata.CmdInBufFlag,0,sizeof(serverdata.CmdInBufFlag));
    u16  evtt = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 1,
            Server_Event,NULL, 0x1000, "Server Event");

    if((evtt == CN_EVTT_ID_INVALID) \
        || (serverdata.SendCmdSemp==NULL)\
        || (serverdata.DoCmdSemp==NULL)\
        || (serverdata.ring==NULL)\
        || (serverdata.mutex==NULL))
    {
        printf("error : Server_EventInit Init evtt: %d SendCmdSemp:%d DoCmdSemp:%d ring:%d mutex:%d \n\r"\
                ,(evtt == CN_EVTT_ID_INVALID),(serverdata.SendCmdSemp==NULL),\
                (serverdata.DoCmdSemp==NULL), (serverdata.ring==NULL),(serverdata.mutex==NULL));
    }else{
        Djy_EventPop(evtt, NULL, 0, 0, 0, 0);
    }

    return 0;
}


