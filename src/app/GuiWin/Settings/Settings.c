//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>
//界面元素ID编号
enum SetListsId{
    SetLists_BACKGROUND, //背景
    SetLists_WIFI,       //wifi
    SetLists_TIME,       //时间
    SetLists_POWER,      //电量
//    SetLists_POWER_LOGO,//
    SetLists_BACK,      //返回
//    SetLists_LOGO,      //过往作业
//    SetLists_DividingLine,//分割线
    SetLists_NAME,
    WifiSetting,  //
    HomeWorkPull,
    Device_Binding,
    NetLoading,
//    FactorySet,
    Restart,

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO SetListsCfgTab[ENUM_MAXNUM] =
{
    [SetLists_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "SetLists",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [SetLists_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [SetLists_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [SetLists_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [SetLists_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [SetLists_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

//    [SetLists_LOGO] = {
//        .position = {194-24,20+10,194,20+10+24},
//        .name = "logo",
//        .type = widget_type_picture,
//        .userParam = BMP_SetList_bmp,
//    },
    [SetLists_NAME] = {
        .position = {0,24,240,24+30},
        .name = "设置",
        .type = widget_type_text,
        .userParam = 0,
    },
//    [SetLists_DividingLine] = {
//        .position = {10,65,240-10,65},
//        .name = "DividingLine",
//        .type = widget_type_Line,
//        .userParam = 0x095b81,//划线的颜色
//    },
    [WifiSetting] = {
        .position = {0,66,240,66+34},
        .name = "WiFi连接",
        .type = widget_type_Setting,
        .userParam = BMP_WifiSetLogo_bmp,
    },
    [HomeWorkPull] = {
        .position = {0,118,240,118+34},
        .name = "作业刷新",
        .type = widget_type_Setting,
        .userParam = BMP_CheckFileLogo_bmp,
    },
    [Device_Binding] = {
        .position = {0,170,240,170+34},
        .name = "设备绑定",
        .type = widget_type_Setting,
        .userParam = BMP_Device_BindingLogo_bmp,
    },
    [NetLoading] = {
        .position = {0,222,240,222+34},
        .name = "检查更新",
        .type = widget_type_Setting,
        .userParam = BMP_UpgradeSetLogo_bmp,
    },
//    [FactorySet] = {
//        .position = {0,66+34*4+7,240,66+34*5+7},
//        .name = "出厂设置",
//        .type = widget_type_Setting,
//        .userParam = BMP_FactorySet_bmp,
//    },
    [Restart] = {
        .position = {0,274,240,274+34},
        .name = "重启",
        .type = widget_type_Setting,
        .userParam = BMP_TurnOffLogo_bmp,
    },
};



//按钮控件创建函数
static bool_t  SetListsButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,SetListsCfgTab[SetLists_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        else if(widget_type_Setting == buttoninfo->type)//
        {
           bmp = Get_BmpBuf(buttoninfo->userParam);
           if(bmp != NULL)
           {
               DrawBMP(hdc,16,0,bmp);
    //           Draw_Icon(hdc,0,0,&buttoninfo->position,bmp);
//               Draw_Icon_2(hdc,15,0,25,25,bmp);
           }
           rc.left+=58;
           SetTextColor(hdc,RGB(28,32,42));
           DrawText(hdc,buttoninfo->name,-1,&rc,DT_LEFT|DT_CENTER);
       }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Setting(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, SetListsButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (SetListsCfgTab[i].type)
        {
            case  widget_type_Setting :
                hwndButton = CreateButton(SetListsCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             SetListsCfgTab[i].position.left, SetListsCfgTab[i].position.top,\
                             RectW(&SetListsCfgTab[i].position),RectH(&SetListsCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&SetListsCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
//                     Widget_SetAttr(hwndButton,ENUM_WIDGET_HYALINE_COLOR,CN_COLOR_BLACK);
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(SetListsCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         SetListsCfgTab[i].position.left, SetListsCfgTab[i].position.top,\
                         RectW(&SetListsCfgTab[i].position),RectH(&SetListsCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&SetListsCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Setting(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SetListsCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,SetListsCfgTab[i].userParam);
                        FillRect(hdc,&SetListsCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(SetListsCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,SetListsCfgTab[i].position.left,\
                                SetListsCfgTab[i].position.top,&SetListsCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&SetListsCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    DrawText(hdc,PowerProgressbar.text,-1,&SetListsCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(SetListsCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    DrawText(hdc,SetListsCfgTab[i].name,-1,&SetListsCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}
bool_t  runapp(char *param);
void Set_UpdateWinType(enum WinType state);
static enum WinType HmiNotify_Settings(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 id =LO16(pMsg->Param1);
    u16 event =HI16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case SetLists_BACK:
                NextUi = WIN_Main_WIN;
                break;
            case  WifiSetting     :
                NextUi = WIN_Settings_WifiConnect;
                break;
            case  Restart         :
                runapp(0);
//                NextUi = WIN_Settings_TurnOff;
                break;
            case  NetLoading      :
//                testupdate(0);
                CheckUpdate();
                Set_UpdateWinType(Get_SelectionWinType());
                if(Get_update_flag()==ReadyUpdate)
                {
                    NextUi = WIN_Check_Update;
                }else{
                    NextUi = WIN_Check_None;
                }
                break;
            case  Device_Binding      :
                NextUi = Win_Settings_DeviceBinding;
                break;
//            case  FactorySet      :
//                NextUi = WIN_Settings;
//                break;
            case  HomeWorkPull       :
                Homework_HomeWorkPull(0);
                NextUi = WIN_Main_WIN;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Settings()
{
    return Register_NewWin(WIN_Settings,HmiCreate_Setting,HmiPaint_Setting,HmiNotify_Settings);
}

















