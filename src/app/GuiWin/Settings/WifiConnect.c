//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <app_flash.h>
//控件ID编号
enum WifiConnectId{
    WifiConnect_BACKGROUND, //背景
    WifiConnect_WIFI,       //wifi
    WifiConnect_TIME,       //时间
    WifiConnect_POWER,      //电量
//    WifiConnect_POWER_LOGO,//
    WifiConnect_BACK,      //返回
    WifiConnect_LOGO,      //过往作业
//    WifiConnect_DividingLine,//分割线

    WifiConnect_statue,  //wifi状态
//    WifiConnect_signal,  //wifi信号强度
    WifiConnect_ssid,  //wifi的ssid
//    WifiConnect_passwd,//wifi的pass
    WifiConnect_ip,//wifi的ip
//    WifiConnect_mac,//wifi的mac
//
//    WifiConnect_NextPage, //下一页
    WifiConnect_PreviousPage,//上一页

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO WifiConnectCfgTab[ENUM_MAXNUM] =
{
    [WifiConnect_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "WifiConnect",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [WifiConnect_WIFI] = {
        .position ={4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [WifiConnect_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [WifiConnect_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [WifiConnect_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [WifiConnect_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

    [WifiConnect_LOGO] = {
        .position = {0,24,240,24+30},
        .name = "WiFi信息",
        .type = widget_type_job,
        .userParam = 0,
    },

//    [WifiConnect_DividingLine] = {
//        .position = {10,65,240-10,65},
//        .name = "DividingLine",
//        .type = widget_type_Line,
//        .userParam = 0x095b81,//划线的颜色
//    },

    [WifiConnect_statue] = {
        .position = {15,65+40*0,240,65+40*1},
        .name = "WiFi状态",
        .type = widget_type_text,
        .userParam = 0,
    },
    [WifiConnect_ssid] = {
        .position = {15,65+40*1,240,65+40*2},
        .name = "Ssid",
        .type = widget_type_text,
        .userParam = 1,
    },
//    [WifiConnect_passwd] = {
//        .position = {15,65+40*2,240,65+40*3},
//        .name = "Passwd",
//        .type = widget_type_text,
//        .userParam = 2,
//    },
    [WifiConnect_ip] = {
        .position = {15,65+40*2,240,65+40*3},
        .name = "ip",
        .type = widget_type_text,
        .userParam = 3,
    },
//    [WifiConnect_mac] = {
//        .position = {15,65+40*4,240,65+40*5},
//        .name = "MAC",
//        .type = widget_type_text,
//        .userParam = 4,
//    },
//    [WifiConnect_mac] = {
//        .position = {0,60+40*4,240,60+40*5},
//        .name = "MAC",
//        .type = widget_type_text,
//        .userParam = 4,
//    },
//
//    [WifiConnect_NextPage] = {
//        .position = {120,60+40*5,240,320},
//        .name = "下一页",
//        .type = widget_type_button,
//        .userParam = RGB(47,43,40),
//    },
//
    [WifiConnect_PreviousPage] = {
        .position = {8,270,240-8,320-8},
        .name = "重新配网",
        .type = widget_type_button,
        .userParam = RGB(47,43,40),
    },
};
//struct WifiConnect
//{
//   u32 courseBase;
//   HWND hwnd[2];
//   bool_t overflow;
//};
//static struct WifiConnect Wifi_Connect = {0,{NULL,NULL},false};

static char wifi_text_buf[100];
extern  WIFI_CFG_T  LoadWifi;
extern u8 *getnetmacaddr();
//按钮控件创建函数
static bool_t  WifiConnectButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,WifiConnectCfgTab[WifiConnect_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        else if((0==strcmp(buttoninfo->name,WifiConnectCfgTab[WifiConnect_PreviousPage].name)))
        {
            Draw_Circle_Button(hdc,&rc,21,RGB(247,192,51));
            SetTextColor(hdc,RGB(255,255,255));

            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_WifiConnect(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, WifiConnectButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (WifiConnectCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 CreateButton(WifiConnectCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         WifiConnectCfgTab[i].position.left, WifiConnectCfgTab[i].position.top,\
                         RectW(&WifiConnectCfgTab[i].position),RectH(&WifiConnectCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&WifiConnectCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}
char *DevGetIpAddr();
//绘制消息处函数
static bool_t HmiPaint_WifiConnect(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (WifiConnectCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,WifiConnectCfgTab[i].userParam);
                        FillRect(hdc,&WifiConnectCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(WifiConnectCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,WifiConnectCfgTab[i].position.left,\
                                WifiConnectCfgTab[i].position.top,&WifiConnectCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&WifiConnectCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_text :
                    SetTextColor(hdc,RGB(28,32,42));
                    if(WifiConnectCfgTab[i].userParam == 0)
                    {
                        if(Get_Wifi_Connectedflag() == 1){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"已连接");
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"未连接");
                        }
                    }else if(WifiConnectCfgTab[i].userParam == 1){
                        if(LoadWifi.statue == 0x55){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,LoadWifi.WifiSsid);
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
                        }
//                    }else if(WifiConnectCfgTab[i].userParam == 2){
//                        if(LoadWifi.statue == 0x55){
//                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,LoadWifi.WifiPassWd);
//                        }else{
//                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
//                        }
                    }else if(WifiConnectCfgTab[i].userParam == 3){
                        if(LoadWifi.statue == 0x55){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name, DevGetIpAddr());
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
                        }
                    }
//                    else if(WifiConnectCfgTab[i].userParam == 4){
//                        u8 Wifi_NetMac[6] ;
//                        u8 *mac_buf=Wifi_NetMac;
//                        mac_buf = getnetmacaddr();
//                        sprintf(wifi_text_buf,"%s:%02X-%02X-%02X-%02X-%02X-%02X",WifiConnectCfgTab[i].name,
//                                mac_buf[0], mac_buf[1], mac_buf[2], mac_buf[3], mac_buf[4], mac_buf[5]);
//                    }
                    DrawText(hdc,wifi_text_buf,-1,&WifiConnectCfgTab[i].position,DT_LEFT|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    DrawText(hdc,PowerProgressbar.text,-1,&WifiConnectCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(WifiConnectCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_job :
                    DrawText(hdc,WifiConnectCfgTab[i].name,-1,&WifiConnectCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}
bool_t  runapp(char *param);
static enum WinType HmiNotify_Settings_WifiConnect(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case WifiConnect_BACK:
            NextUi = WIN_Settings;
//          Ui_HomeWorkLists(pMsg);     //建立作业列表界面。
            break;
//        case  WifiConnect_file1      :
//        case  WifiConnect_file2      :
//        case  WifiConnect_file3      :
//        case  WifiConnect_file4      :
//        case  WifiConnect_file5      :
//            NextUi = WIN_Settings;
//            break;
//        case  WifiConnect_NextPage    :
//            NextUi = WIN_Settings_WifiConnect;
//            if(Wifi_Connect.overflow == false)
//            {
//                Wifi_Connect.courseBase+=sizeof(Wifi_Connect.hwnd)/sizeof(HWND);
//                Update_WifiConnectcourse();
//            }
//            break;
            case  WifiConnect_PreviousPage:
            NextUi = WIN_Settings_WifiConnect;
            SwitchToFriendlyReminders(enum_configure_network);
            NextUi = WIN_FriendlyReminders;
//            if(Wifi_Connect.courseBase>5)
//                Wifi_Connect.courseBase-=sizeof(Wifi_Connect.hwnd)/sizeof(HWND);
//            else
//                Wifi_Connect.courseBase=0;
//            Wifi_Connect.overflow = false;
//            Update_WifiConnectcourse();
            break;
            default:
                NextUi = WIN_Settings_WifiConnect;
            break;
        }
    }
    return NextUi;
}

int Register_Settings_WifiConnect()
{
    return Register_NewWin(WIN_Settings_WifiConnect,HmiCreate_WifiConnect,HmiPaint_WifiConnect,HmiNotify_Settings_WifiConnect);
}
