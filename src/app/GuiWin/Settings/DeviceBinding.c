//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"

//界面元素定义
enum DeviceBindingId{
    DeviceBinding_BACKGROUND, //背景
    DeviceBinding_WIFI,       //wifi
    DeviceBinding_TIME,       //时间
    DeviceBinding_POWER,      //电量
//    DeviceBinding_POWER_LOGO,//
    DeviceBinding_BACK,      //返回
    DeviceBinding_NAME,
    DeviceBinding_SN,      //设备SN号
    DeviceBinding_Qrencode,      //二维码
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static struct GUIINFO DeviceBindingCfgTab[ENUM_MAXNUM] =
{
    [DeviceBinding_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "DeviceBinding",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [DeviceBinding_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [DeviceBinding_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [DeviceBinding_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [DeviceBinding_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [DeviceBinding_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

    [DeviceBinding_NAME] = {
        .position = {0,24,240,24+30},
        .name = "设备绑定",
        .type = widget_type_job,
        .userParam = 0,
    },

    [DeviceBinding_SN] = {
        .position = {0,282,240,282+22},
        .name = "设备号",
        .type = widget_type_text,
        .userParam = RGB(178,178,178),
    },

    [DeviceBinding_Qrencode] = {
        .position = {18,66,240-18,66+204},
        .name = "qrencode",
        .type = widget_type_Orencode,
        .userParam = 0,
    },
};



//按钮控件创建函数
static bool_t  DeviceBindingButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,DeviceBindingCfgTab[DeviceBinding_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_DeviceBinding(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, DeviceBindingButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (DeviceBindingCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 CreateButton(DeviceBindingCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         DeviceBindingCfgTab[i].position.left, DeviceBindingCfgTab[i].position.top,\
                         RectW(&DeviceBindingCfgTab[i].position),RectH(&DeviceBindingCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&DeviceBindingCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

/*---------------------------------------------------------------------------
功能： 将从二维码函数库中得到的数据转换为指向存储像素阵列的数组
    输入   :QRcode结构体指针
    输出 :存储像素阵列的数组指针
---------------------------------------------------------------------------*/
static u8 *Data_conversion(QRcode*qrcode,u8 byte)
{
    int i,j,k,n,m,w;
    u8*Data;
    u8*QrcodeData;
    u8 *addr;
    QrcodeData=qrcode->data;
    n=((byte*qrcode->width)+7)/8;   //每行占得字节数
    addr=(u8*)malloc(n*(qrcode->width*byte));
    if(addr == NULL)
        return NULL;
    Data = addr;
    w = (qrcode->width)/8;
    m = (qrcode->width)%8;

    for(k = 0;k < qrcode->width ; k++)
    {
        u32 num = 0;
        for(i=0;i<w;i++)//复制字节整数
        {
            for(j=0;j<8;j++)
            {
                if((*QrcodeData)&0x01)
                {
                    for(u8 cnt =0;cnt<byte;cnt++)
                    {
                        Data[num/8] |= (0x80>>num%8);
                        num++;
                    }
                }
                else
                {
                    for(u8 cnt =0;cnt<byte;cnt++)
                    {
                        Data[num/8] &= ~(0x80>>num%8);
                        num++;
                    }
                }
                QrcodeData++;
            }
        }
        if(m != 0)
        for(j=0;j<m;j++)//复制字节余数部分
        {
            if((*QrcodeData)&0x01)
            {
                for(u8 cnt =0;cnt<byte;cnt++)
                {
                    Data[num/8] |= (0x80>>num%8);
                    num++;
                }
            }
            else
            {
                for(u8 cnt =0;cnt<byte;cnt++)
                {
                    Data[num/8] |= (0x80>>num%8);
                    num++;
                }
            }
            QrcodeData++;
        }
        for(int cnt=1;cnt<byte;cnt++)
        {
            memcpy(Data+cnt*n,Data,n);
        }
        Data += n * byte;
    }
    return addr;
}
/*---------------------------------------------------------------------------
功能：根据输入参数得到二维码信息
    输入   :string: 被编码的数据
        version: 版本号<=40     宽度=(版本号*4+17)
        level: 容错等级L（Low）：7%的字码可被修正
               M（Medium）：15%的字码可被修正
               Q（Quartile）：25%的字码可被修正
               H（High）：30%的字码可被修正
        hint: 编码方式
        casesensitive: 是否区分大小写
    输出 :存储像素阵列的数组指针
---------------------------------------------------------------------------*/
extern QRcode *QRcode_encodeString(const char *string, int version,
                                    QRecLevel level, QRencodeMode hint,
                                    int casesensitive);

//绘制消息处函数
static bool_t HmiPaint_Qrcode(struct WindowMsg *pMsg,char *SNbuf,s32 x,s32 y,u8 byte)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc0;
    QRcode*qrcode; //最后结果
    struct RectBitmap   bitmap;
    struct RopGroup RopCode ={ 0, 0, 0, CN_R2_COPYPEN, 0, 0,0};
    u8 *Data;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
//    GetClientRect(hwnd,&rc0);


    rc0=DeviceBindingCfgTab[DeviceBinding_Qrencode].position;

    SetFillColor(hdc,RGB(247,192,51));
    FillRect(hdc,&rc0);

    SetDrawColor(hdc,CN_COLOR_BLACK);
/* 89x89位宽的二维码参数为
 * 版本号:18
 * 容错等级  (ECC)  数字  (容量)     字母                                汉字          二进制代码
 * L            1,725       1,046       442    586
 * M            1,346       816         345    450
 * Q            948         574         243    322
 * H            746         452         191    250
 * */



    qrcode= QRcode_encodeString(SNbuf,10, QR_ECLEVEL_H, QR_MODE_8,0);
   if(qrcode==NULL)//存储空间不足或数据容量超出规定范围
   {
       QRcode_free(qrcode);
       return false;
   }

    Data=Data_conversion(qrcode,byte);//提取显示数据到Data
    bitmap.bm_bits = Data;
    bitmap.linebytes = ((byte*qrcode->width)+7)/8;
    bitmap.PixelFormat = CN_SYS_PF_GRAY1;
    bitmap.ExColor = CN_COLOR_WHITE;
    bitmap.height=(s32)(qrcode->width*byte);
    bitmap.width=(s32)(qrcode->width*byte);
    printf("bitmap.height = %d,bitmap.width = %d",bitmap.height,bitmap.width);
    DrawBitmap(hdc,x,y,&bitmap,CN_SYS_PF_GRAY1,RopCode);

    EndPaint(hwnd,hdc);
    QRcode_free(qrcode);
    free(Data);
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_DeviceBinding(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    char  SNbuf[30];
    char  buf[100];
    int  Get_DeviceSn(char *buf,int buflen);
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        if(0>Get_DeviceSn(SNbuf,sizeof(SNbuf)))
            SNbuf[0] = '\0';


        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (DeviceBindingCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,DeviceBindingCfgTab[i].userParam);
                        FillRect(hdc,&DeviceBindingCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(DeviceBindingCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,DeviceBindingCfgTab[i].position.left,\
                                DeviceBindingCfgTab[i].position.top,&DeviceBindingCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&DeviceBindingCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    DrawText(hdc,PowerProgressbar.text,-1,&DeviceBindingCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(DeviceBindingCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_Orencode :
                    HmiPaint_Qrcode(pMsg,SNbuf,DeviceBindingCfgTab[i].position.left+16,DeviceBindingCfgTab[i].position.top+16,3);
                    break;
                case  widget_type_text :
                    SetTextColor(hdc,DeviceBindingCfgTab[i].userParam);
                    sprintf(buf,"%s:Geniustalk%s",DeviceBindingCfgTab[i].name,SNbuf+6);
                    DrawText(hdc,buf,-1,&DeviceBindingCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                case  widget_type_job :
                    DrawText(hdc,DeviceBindingCfgTab[i].name,-1,&DeviceBindingCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_DeviceBinding(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  DeviceBinding_BACK        :
                NextUi = WIN_Settings;
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_Settings_DeviceBinding()
{
    return Register_NewWin(Win_Settings_DeviceBinding,HmiCreate_DeviceBinding,HmiPaint_DeviceBinding,HmiNotify_DeviceBinding);

}

