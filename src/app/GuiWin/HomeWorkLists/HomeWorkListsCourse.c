//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "../Homework/homeWork.h"

//控件ID编号
enum HomeWorkListsCId{
    HomeWorkListsC_BACKGROUND, //背景
    HomeWorkListsC_WIFI,       //wifi
    HomeWorkListsC_TIME,       //时间
    HomeWorkListsC_POWER,      //电量

    HomeWorkListsC_BACK,       //返回
    HomeWorkListsC_CourseName,   //课程名

    HomeWorkListsC_work1,  //班级
    HomeWorkListsC_work2,
    HomeWorkListsC_work3,

    HomeWorkListsC_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO HomeWorkListsCCfgTab[HomeWorkListsC_MAXNUM] =
{
    [HomeWorkListsC_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "HomeWorkListsCwork",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },

    [HomeWorkListsC_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [HomeWorkListsC_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [HomeWorkListsC_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

    [HomeWorkListsC_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

    [HomeWorkListsC_CourseName] = {
        .position = {0,24,240,24+30},
        .name = NULL,
        .type = widget_type_text,
        .userParam = 0,
    },

    [HomeWorkListsC_work1] = {
        .position = {8,62,8+224,62+72+4},
        .name = "work1",
        .type = widget_type_work,
        .userParam = 0,
    },
    [HomeWorkListsC_work2] = {
        .position = {8,148,8+224,148+72+4},
        .name = "work2",
        .type = widget_type_work,
        .userParam = 1,
    },
    [HomeWorkListsC_work3] = {
        .position = {8,234,8+224,234+72+4},
        .name = "work3",
        .type = widget_type_work,
        .userParam = 2,
    },

};
struct workCB
{
   u32 workBase;
   HWND hwnd[3];
   u32 workmax;//溢出标志
};
static struct workCB workcb = {0,{NULL,NULL,NULL},0xffffffff};

void Clear_Workcb(void)
{
    workcb.hwnd[0] = NULL;
    workcb.hwnd[1] = NULL;
    workcb.hwnd[2] = NULL;
    workcb.workBase = 0;
    workcb.workmax = 0xffffffff;
}

//按钮控件创建函数
static bool_t  HomeWorkListsCButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
//    struct  workcb work;
    const char * bmp;
    struct HWorkCB* hwork;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        SetTextColor(hdc,RGB(28,32,42));
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,HomeWorkListsCCfgTab[HomeWorkListsC_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
            }
        }
        else if(widget_type_work == buttoninfo->type)//作业窗口
        {
           hwork = Get_PastHomeWorkOfCnt(Get_SelectClass(),workcb.workBase+buttoninfo->userParam);

           if(hwork != NULL)
           {
               SetFillColor(hdc,RGB(255,225,147));
               FillRect(hdc,&rc);

               bmp = Get_BmpBuf(BMP_BookLogoDown_bmp);
               if(bmp != NULL)
               {
                   Draw_Icon_2(hdc,8,19,34,34,bmp);
               }

               rc.left+=58;
//               rc.top+=6;
               rc.top+=26;
               SetTextColor(hdc,RGB(178,178,178));
               DrawText(hdc,"类型:",-1,&rc,DT_LEFT|DT_TOP);
               rc.top-=20;
               struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
               struct Charset* Charsetbak = SetCharset(hdc,myCharset);
               SetTextColor(hdc,RGB(28,32,42));
               DrawText(hdc,hwork->title,-1,&rc,DT_LEFT|DT_TOP);
               rc.top+=20;
               rc.left+=40;
               SetTextColor(hdc,RGB(178,178,178));
               DrawText(hdc,hwork->type,-1,&rc,DT_LEFT|DT_TOP);
               rc.left-=40;
               rc.top+=20;
               DrawText(hdc,hwork->time,-1,&rc,DT_RIGHT|DT_TOP);
               SetCharset(hdc,Charsetbak);

               if(hwork->status ==1){
                   SetTextColor(hdc,RGB(255,79,79));
                   DrawText(hdc,"未完成",-1,&rc,DT_LEFT|DT_TOP);
               }else if(hwork->status ==2){
                   SetTextColor(hdc,RGB(118,80,253));
                   DrawText(hdc,"未批改",-1,&rc,DT_LEFT|DT_TOP);
               }else if(hwork->status ==3){
                   SetTextColor(hdc,RGB(247,192,51));
                   DrawText(hdc,"已批改",-1,&rc,DT_LEFT|DT_TOP);
               }else
               {
                   SetTextColor(hdc,RGB(255,0,0));
                   DrawText(hdc,"状态：未知",-1,&rc,DT_LEFT|DT_TOP);
               }

               rc.top+=15;
               rc.left+=100;
               for(int i=0;i<5;i++)
               {
                   if(i<hwork->score)
                       bmp = Get_BmpBuf(BMP_StarDown_bmp);
                   else
                       bmp = Get_BmpBuf(BMP_StarUp_bmp);

                   if(bmp != NULL)
                   {
                       Draw_Icon_2(hdc,rc.left+i*13,rc.top,11,10,bmp);
                   }
               }
//               printf("hwork status = %d\n\r",hwork->status);
           }else{
               SetFillColor(hdc,RGB(255,255,255));
               FillRect(hdc,&rc);
               SetTextColor(hdc,RGB(28,32,42));
               if(buttoninfo->userParam == 0)
               {
                   if(workcb.workBase)
                   {
                       DrawText(hdc,"这是最后一页，请往下滑！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }else{
                       DrawText(hdc,"该班级尚未过往作业！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }
               }
               if(workcb.workmax == 0xffffffff)
                   workcb.workmax = workcb.workBase+buttoninfo->userParam;
           }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
static bool_t Update_HomeWorkListsCwork(void)
{
    u8 i;
    for(i=0;i<sizeof(workcb.hwnd)/sizeof(HWND);i++)
    {
        if(workcb.hwnd[i] !=NULL)
            PostMessage(workcb.hwnd[i],MSG_PAINT,0,0);

    }
    return true;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_HwlC(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    workcb.workmax = 0xffffffff;

    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, HomeWorkListsCButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<HomeWorkListsC_MAXNUM;i++)
    {
        switch (HomeWorkListsCCfgTab[i].type)
        {
            case  widget_type_work :
                hwndButton = CreateButton(HomeWorkListsCCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             HomeWorkListsCCfgTab[i].position.left, HomeWorkListsCCfgTab[i].position.top,\
                             RectW(&HomeWorkListsCCfgTab[i].position),RectH(&HomeWorkListsCCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&HomeWorkListsCCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
                     workcb.hwnd[HomeWorkListsCCfgTab[i].userParam] = hwndButton;
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(HomeWorkListsCCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         HomeWorkListsCCfgTab[i].position.left, HomeWorkListsCCfgTab[i].position.top,\
                         RectW(&HomeWorkListsCCfgTab[i].position),RectH(&HomeWorkListsCCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&HomeWorkListsCCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_HwlC(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    struct classCB*class;
    RECT rc;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<HomeWorkListsC_MAXNUM;i++)
        {
            switch (HomeWorkListsCCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,HomeWorkListsCCfgTab[i].userParam);
                        FillRect(hdc,&HomeWorkListsCCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(HomeWorkListsCCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,HomeWorkListsCCfgTab[i].position.left,\
                                HomeWorkListsCCfgTab[i].position.top,&HomeWorkListsCCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&HomeWorkListsCCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
                    bmp = Get_BmpBuf(HomeWorkListsCCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    SetTextColor(hdc,RGB(255,255,255));
                    GetClientRect(hwnd,&rc);
                    class = Get_SelectClass();
                    if(class!=NULL)
                    {
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                        DrawText(hdc,class->name,-1,&HomeWorkListsCCfgTab[HomeWorkListsC_CourseName].position,DT_VCENTER|DT_CENTER);
                        SetCharset(hdc,Charsetbak);
                    }
                    break;
                default:    break;
            }
        }
        hdc =BeginPaint(hwnd);
        return true;
    }
    return false;

}

//子控件发来的通知消息
static enum WinType HmiNotify_HWL_Course(struct WindowMsg *pMsg)
{
    #define linkbuf_max   10
    char*linkbuf[linkbuf_max];
    int ret;
    struct HWorkCB* hwork;
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    s16 x,y;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        if(abs(y) > abs(x))
        {
            if(y >0)
            {
                if(workcb.workmax > (workcb.workBase+sizeof(workcb.hwnd)/sizeof(HWND)))
                {
                    workcb.workBase+=sizeof(workcb.hwnd)/sizeof(HWND);
                }
            }else{
                if(workcb.workBase>sizeof(workcb.hwnd)/sizeof(HWND))
                    workcb.workBase-=sizeof(workcb.hwnd)/sizeof(HWND);
                else
                    workcb.workBase=0;
            }
            Update_HomeWorkListsCwork();
        }
    }
    else if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  HomeWorkListsC_BACK        :
                Clear_Workcb();
                NextUi = WIN_HomeWorkLists;
                break;
            case  HomeWorkListsC_work1      :
            case  HomeWorkListsC_work2      :
            case  HomeWorkListsC_work3      :
                if(workcb.workmax > workcb.workBase + HomeWorkListsCCfgTab[id].userParam)
                {
                    hwork = Get_PastHomeWorkOfCnt(Get_SelectClass(),workcb.workBase + HomeWorkListsCCfgTab[id].userParam);
                    if(Set_SelectHmoeWork(hwork))
                    {
                        ret = Get_AudioLinkAddress(linkbuf,linkbuf_max);
                        if(ret != 0)
                        {
                            int AudioCacheAdd(char *arr_url[], int num);
                            AudioCacheAdd(linkbuf, ret);
                        }
                        NextUi = WIN_HWL_Course_Work;
                    }
                }
                break;
            default:
                NextUi = WIN_HWL_Course;
                 break;
        }
    }
    return NextUi;
}


int Register_HWL_Course()
{
    return Register_NewWin(WIN_HWL_Course,HmiCreate_HwlC,HmiPaint_HwlC,HmiNotify_HWL_Course);
}

