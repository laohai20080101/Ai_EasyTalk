/*
 * ServerEvent.h
 *
 *  Created on: 2019年12月12日
 *      Author: c
 */

#ifndef APP_GUIWIN_INC_SERVEREVENT_H_
#define APP_GUIWIN_INC_SERVEREVENT_H_
#include "stddef.h"
#include "ServerInfo.h"
#include "GuiInfo.h"

typedef enum
{
    cmd_Homework_listenRecording,//播放录制的音频
    cmd_Homework_listenRecordingStop,
    cmd_Homework_StartRecording,//开始录音
    cmd_Homework_StopRecording,//停止录音
    cmd_Homework_PlayAudio,//播放录音
    cmd_Homework_PlayAudioStop,//停止播放录音
    cmd_Details_PlayAudioStart,//开始播放作业详情提示音
    cmd_Details_PlayAudioStop,//停止播放作业详情提示音
    cmd_Submit_JobOfid,//提交作业
    cmd_Read_MsgNameOfcnt,//标记消息已读
    cmd_Del_MsgNameOfBuf,//删除消息
    cmd_Del_MsgNameOfcnt,//删除第cnt条消息
    cmd_HomeWorkPull,//更新作业信息
    cmd_ServerCmdmax,  //更新作业信息

}enumServerCmd;


typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    PlayMediaCallback fun;
}para_Details_PlayAudioStart;//开始播放作业详情的提示音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
}para_Details_PlayAudioStop;//停止播放作业详情的提示音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
    PlayMediaCallback fun;
}para_Homework_listenRecording;//播放录制的音频

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
}para_Homework_listenRecordingStop;//停止播放录制的音频

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
}para_Homework_StartRecording;//开始录音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
}para_Homework_StopRecording;//停止录音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
    PlayMediaCallback fun;
}para_Homework_PlayAudio;//播放录音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
    struct TopicCB*Topic;
}para_Homework_PlayAudioStop;//停止播放录音

typedef struct{
    struct classCB*class;
    struct HWorkCB*homework;
}para_Submit_JobOfid;//提交作业

typedef struct{
    u32 cnt;
}para_Read_MsgNameOfcnt;//标记消息已读


typedef struct{
    u32 cnt;
}para_Del_MsgNameOfcnt;//删除第cnt条消息

bool_t Server_SendComd(enumServerCmd cmd,void*para,size_t paralen,u32 timeout);


#endif /* APP_GUIWIN_INC_SERVEREVENT_H_ */
