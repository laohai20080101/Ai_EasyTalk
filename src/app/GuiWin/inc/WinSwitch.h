/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef _GUIWIN_WINSWITCH_H_
#define _GUIWIN_WINSWITCH_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
enum WinType
{
    WIN_Main_WIN = 0,               //主窗口界面
    WIN_HomeWorkLists,              //过往作业科目列表，如语文、数学
    WIN_HWL_Course,                 //过往作业题目列表，例如“10月一日的作业”
    WIN_HWL_Course_Work,            //过往作业界面，详述作业题目要求
    WIN_HWL_Course_Work_Recite,     //做“朗读、背诵”作业的界面，分今日和过往
    WIN_FHW_Course_Work_Recite,
    WIN_HWL_Course_Work_Dictation,  //做“听写”作业的界面，不分今日和过往
    WIN_HWL_Submit,                 //提交作业界面
    WIN_EngLishPlaza,               //口语广场
    WIN_TodayHomeWork,              //今日作业科目列表
    WIN_THW_Course,                 //今日作业题目列表，例如“朗读一只小狐狸”
    WIN_THW_Course_Work,            //今日作业界面，详述作业题目要求
    WIN_Messages,                   //消息界面
    WIN_Messages_delete,            //消息删除界面
    WIN_Messages_Select,            //消息选择
    WIN_Settings,                   //设置界面
    WIN_Check_Update,               //检查更新程序
    WIN_Update_Proram,              //更新程序
    WIN_Check_None,                 //没有最新版本
    WIN_Settings_WifiConnect,       //设置界面——wifi连接
    Win_Settings_DeviceBinding,     //绑定设备二维码
    WIN_Settings_TurnOff,           //关机界面
    WIN_NetLoading,                 //网络加载界面
    WIN_Uploading,                  //正在上传录音界面
    WIN_RecordSubmitFail,           //录音上传失败
    WIN_PowerLower,                 //低电压提示
    WIN_FriendlyReminders,          //温馨提示
	WIN_SubmitStatue,
    WIN_Max,                        //有效界面总数
    WIN_NotChange,                  //不改变窗口
    WIN_Error,                      //状态错误
};
//========================界面内注册接口定义===================================
int Register_Main_WIN();
int Register_HomeWorkLists();
int Register_HWL_Course();
int Register_HWL_Course_Work();
int Register_HWL_Course_Work_Recite();
int Register_FHW_Course_Work_Recite();
int Register_HWL_Course_Work_Dictation();
int Register_HWL_Submit();
int Register_EngLishPlaza();
int Register_TodayHomeWork();
int Register_THW_Course();
int Register_THW_Course_Work();
int Register_Messages();
int Register_MessagesSelect();
int Register_Messages_delete();
int Register_Settings();
int Register_Check_Update();
int Register_Update_Proram();
int Register_Check_None();
int Register_Settings_WifiConnect();
int Register_Settings_TurnOff();
int Register_Settings_DeviceBinding();
int Register_NetLoading();
int Register_Uploading();
int Register_RecordSubmitFail();
int Register_PowerLower();
int Register_FriendlyReminders();
int Register_SubmitStatue();

int Init_WinSwitch();
enum WinType Get_SelectionWinType();
bool_t Refresh_SwitchWIn(HWND hwnd);

typedef bool_t (*T_HmiCreate)(struct WindowMsg *pMsg);//界面控件创建
typedef bool_t (*T_HmiPaint)(struct WindowMsg *pMsg);//界面绘制
typedef enum WinType (*T_DoMsg)(struct WindowMsg *pMsg);//界面消息响应

struct GuiWinCb
{
    T_HmiCreate HmiCreate;//界面控件创建
    T_HmiPaint HmiPaint;//界面绘制
    T_DoMsg DoMsg;//界面消息响应
};
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg);
bool_t HmiRefresh(struct WindowMsg *pMsg);
bool_t HmiNotify_EasyTalk(struct WindowMsg *pMsg);
void Set_ShutOff_Statue(u8 statue);
enum WinType Get_LastWinType();
void Set_LastWinType(enum WinType state);
enum WinType Get_NextWinType();
void Set_NextWinType(enum WinType state);
enum WinType Get_UpdateWinType();
void Set_UpdateWinType(enum WinType state);

#define MSG_REFRESH_UI     (MSG_WM_USER + 0x0001)  //
#define MSG_KEY_CMD        (MSG_WM_USER + 0x0002)  //
#define CN_RECREAT_WIDGET       0
#define CN_ONLY_RESHOW          1
bool_t Refresh_GuiWin();
int Update_WinType(enum WinType type);
bool_t Set_CheckUpdateWin(void);
bool_t Refresh_FromUpdateServer();
bool_t Refresh_SubmitStatueWin(void);
void Draw_Icon(HDC hdc,s32 x,s32 y,const RECT *rc,const char *bmp);
void Draw_Icon_2(HDC hdc,s32 x,s32 y,s32 width,s32 height,const char *bmp);
void Draw_Circle_Button(HDC hdc,RECT *rc,s32 r,u32 color);
bool_t Refresh_UpdateServerInfo(void);
bool_t Check_IsPlayOrRecord();
//=======================按键通知类型===========================================
enum Keycmd{
    enum_key_Short_press,
    enum_key_Long_press,
    enum_key_error,
};

//======================设置警告类型============================================
enum enumFriendlyReminders{
    enum_Insufficient_buffer,//70% （网络快走丢了，快去找找哪里网络好。），按钮（好的）
    enum_buf_overflow,       //100% （作业跟着网路走丢了，要去找个网络好的地方重新做一下这题吧。），按钮（好的）
    enum_warning_Submit_homework,//（网络快走丢了,快去找个网络好的地方重新提交吧。），按钮（好的）
    enum_configure_network, //（设备将要自行重启，重启后请拿出手机，打开wifi连接至设备热点（Geniustalk+编码）以进行网络设置。），按钮（好的）
    enum_Recording_upload_failed, //内容（第四题的录音走丢了，去看看是不是忘记了。），按钮（好的
    enumFriendlyReminders_max,
    enumFriendlyReminders_null,
};

bool_t SwitchToFriendlyReminders(enum enumFriendlyReminders Tips);


#ifdef __cplusplus
}
#endif

#endif /* _GUIWIN_WINSWITCH_H_ */
