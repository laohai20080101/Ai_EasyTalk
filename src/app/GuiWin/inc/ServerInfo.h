/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef _SERVER_INFO_H_
#define _SERVER_INFO_H_
#include "stddef.h"

#ifdef __cplusplus
extern "C" {
#endif


//typedef  unsigned int u32;
//#define true   1
//#define false  0
//typedef char bool_t;

#define REQUEST_NUM  (3)

#define PAGE_SIZE    30
#define JSON_BUF_DEF 20480

#define JSON_BUF_DEF_CLASS   20480
#define JSON_BUF_DEF_MSG     20480
#define JSON_BUF_DEF_ALL     204800
#define JSON_BUF_DEF_WORK    20480


//题目
struct TopicCB
{
    struct TopicCB *next,*prev;
    int score;
    char *type_name;
    char *answer;
    int id ;
    char *audio;
    char *text;
    int type;
    char *title;
    int mark;
};

//作业详情
struct HWdetailsCB
{
    char * name;// 9月9日第一单元作业,
    char *cover;// *.png,//背景
    char *Topic;// 作业简介,
    char *tname;// 李老师,
    char *ctime;//发布日期
    char *etime;//截止日期
    char *ftime;//提交日期
    int ptime;// 30,预计用时
    int rank;// 5,班级排名
    char *comment;//教师评语
    char *audio;// xx.wav,音频评语
    int asize;// 2,朗读数目
    int bsize;// 3,背诵数目
    int csize;// 0 听力数目
};

//作业
struct HWorkCB
{
    struct HWorkCB * next,*prev;
    int score; //评分
    int workid ; //ID
    char * time;//时间
    char *title; //作业名
    char *type; //类型名
    int status; //1未完成 ，2未点评，3已点评
    struct HWdetailsCB * p_details;

    struct TopicCB*Topic;//作业内容
};

//班级列表控制块
struct classCB{
    struct classCB *next,*prev;
    char * name;
    int classid;
    int PastHomeWOrkNum;
    struct HWorkCB *past;//过往家庭作业列表
    struct HWorkCB *today;//今日家庭作业列表
};
struct MessagesCB{
    struct MessagesCB*next,*prev;
    int news_type;//": 3,
    int id;//": 42,
    char *time;//": "2019-10-14 16:55:06",
    char *content;//": "null在槟榔小学三（二）班布置了作业“自定义作业”",
    int read;//是否已读"status": "已读"
};


bool_t Del_MessagesObj(struct MessagesCB*pmsg);
bool_t Del_MessagesofId(int id);
struct classCB* Get_ClassOFCnt(int cnt);
struct classCB* Get_ClassOfId(int id);
struct classCB* Get_NextClass(struct classCB* pclass);
struct HWorkCB* Get_PastHomeWorkOfCnt(struct classCB*pclass,int cnt);
struct HWorkCB* Get_PastHomeWorkOfId(struct classCB*pclass,int workid);

struct HWorkCB* Get_TodayHomeWorkOfCnt(struct classCB*pclass,int cnt);
struct HWorkCB* Get_TodayHomeWorkOfId(struct classCB*pclass,int workid);

int Makr_Topic(struct TopicCB* topic);//标记作业已完成
struct TopicCB* Get_TopicOfCnt(struct HWorkCB*phwork,int cnt);
struct TopicCB* Get_TopicOfID(struct HWorkCB*phwork,int id);
int Get_HomeWorkTopicNUm(struct HWorkCB*phwork);
int Update_TodayHomeWorkofid(struct classCB*class,int id);
int Update_PastHomeWorkofid(struct classCB*class,int id);

int Get_MessagesNUm();
int Update_Messages();
bool_t Read_Messages(struct MessagesCB * msg);
struct MessagesCB* Get_MessagesOfCnt(int cnt);
struct MessagesCB* Get_MessagesOfID(int id);
struct MessagesCB* Get_NextMessages(struct MessagesCB*msg);
int Update_ServerInfo(void);
void printf_Infolist();
int Get_HomeWorkAllAudioLink(struct HWorkCB*phwork,char**linkbuf,int bufnum);
char Get_ServerInfo_Flag();


#ifdef __cplusplus
}
#endif

#endif /* _SERVER_INFO_H_ */
