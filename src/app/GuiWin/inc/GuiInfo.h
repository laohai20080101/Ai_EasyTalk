/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef IBOOT_GUI_INFO_H_
#define IBOOT_GUI_INFO_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "widget/gdd_progressbar.h"
#ifndef GUI_INFO_FILE_C_
//extern PROGRESSBAR_DATA PowerProgressbar;
//extern HWND Powerhwnd;
#endif
#include "../Homework/homeWork.h"
#include "ServerInfo.h"
#include "utf8_text.h"
//界面元素枚举
enum widgettype
{
    type_background,
    widget_type_course,
    widget_type_Setting,
    widget_type_button,
    widget_type_msg,
    widget_type_picture,
    widget_type_text,
    widget_type_job,
    widget_type_time,
    widget_type_Line,
    widget_type_Power,
    widget_type_work,
    widget_type_Orencode,
    type_HomeWork_background,//作业背景
    type_MainLogo_background,//主界面logo背景
};

//界面元素信息
struct GUIINFO
{
    RECT position;//坐标，大小信息
    const char* name;
    enum widgettype type;
    ptu32_t userParam;
} ;

//=================Bmp枚举=======================
enum Bmptype
{
    BMP_Close_bmp,
    BMP_ArrowLeft_bmp,
    BMP_ArrowRight_bmp,
//    BMP_MainInterface_bmp,       //开机界面
    BMP_TodayHomeLogo_bmp,
    BMP_HomeWorkLogo_bmp,
    BMP_EngLishPlazaLogo_bmp,
    BMP_MessagesLogo_bmp,
    BMP_SetLogo_bmp,
    BMP_NetLoading_bmp,          //网络加载界面
    BMP_Background_bmp,          //背景
    BMP_HomeWorkLists_bmp,       //过往作业
    BMP_EngLishPlaza_bmp,        //口语广场
    BMP_TodayHomeWork_bmp,       //今日作业
    BMP_Messages_bmp,            //消息
    BMP_Settings_bmp,            //设置
    BMP_PowerLogo_bmp,          //冲电标志
    BMP_WIFI,                   //wifi
    BMP_courseLogo_bmp,         //课程
    BMP_GoBack_bmp,//返回
    BMP_Record_bmp,//录音
//    BMP_Record_2_bmp,//录音
    BMP_RecordDown_bmp,//录音按下
    BMP_PlayPause_bmp,//播放暂停
    BMP_PlayPause_2_bmp,//没有录音播放
    BMP_PlayPauseStop_bmp,
    BMP_PlayPauseBig_bmp, //大一点的播放暂停
    BMP_PlayPauseBigStop_bmp, //大一点的播放暂停
    BMP_Volume_bmp,//原音
    BMP_VolumeStop_bmp,//原音暂停
//    BMP_HomeWorkListsLog_bmp,
    BMP_BookLogoDown_bmp,
    BMP_UpgradeSetLogo_bmp,
    BMP_Device_BindingLogo_bmp,
    BMP_CheckFileLogo_bmp,
//    BMP_EngLishPlazaList_bmp,
//    BMP_FactorySet_bmp,
//    BMP_MessageList_bmp,
//    BMP_SetList_bmp,
    BMP_StarDown_bmp,
    BMP_StarUp_bmp,
//    BMP_TodayHomeworList_bmp,
    BMP_TurnOffLogo_bmp,
    BMP_WifiSetLogo_bmp,
    BMP_Round_bmp,
    BMP_RoundDot_bmp,
    BMP_Submitting_bmp,
    BMP_SubmitSuccess_bmp,
    BMP_SubmitFail_bmp,
    BMP_VoicePlay_bmp,
    BMP_WorkingHard_bmp,
    BMP_VoiceStop_bmp,
    BMP_MAXNNUM, //图标数量
    Bmp_NULL,
};




const char * Get_BmpBuf(enum Bmptype type);

struct WinTime{
  u8 hour;
  u8 minute;
};
bool_t Get_Wifi_Connectedflag(void);
bool_t Wifi_Connectedflag(u8 flag);

bool_t Win_SetTime(struct WinTime time);
char * Win_GetTime();

bool_t Win_UpdatePower(u8 power);
//u32 Win_GetPower();
char current_power(void);

//获取班级信息通过班级编号
const char *Get_TodayHomeWorkCourseNameOfcnt(u32 cnt);
const char *Get_HomeWorkListsCourseNameOfcnt(u32 cnt);

struct  workcb
{
    const char * work;//作业
    const char * type;//类型
    const char * time;//时间
    const char * state;//完成状态
    int complete;      //提交状态
    u32 is_read;      //是否已经查看。1:已阅读；0:未阅读
    u32 score;         //评分
    u32 num;          //这项作业中的小题数
};
struct  workcb Get_HomeWorkListsCourseWorkOfcnt(u32 Course,u32 work,int *error);
struct contentCB*  Get_HomeWorkListsCnt(u32 class ,u32 work,u32 cnt,int *error);
struct  workcb Get_TodayHomeWorkCourseWorkOfcnt(u32 work,int *error);
struct contentCB*  Get_TodayHomeWorkCnt(u32 class ,u32 work,u32 cnt,int *error);

struct HWorkCB*  Get_TodayHomeWorkMoreInfo(u32 class ,u32 work);
struct HWorkCB*  Get_HomeWorkListsMoreInfo(u32 class ,u32 work);

struct  MsgCb
{
    const char * msg;//消息内容
    const char * time;//时间
    bool_t newmsg;   //是否为新消息
};
typedef int (*PlayMediaCallback)();
bool_t Read_MsgNameOfcnt(u32 cnt);
bool_t Del_MsgNameOfBuf(u8 *buf,int len);
struct  MsgCb Get_MsgNameOfcnt(u32 cnt,int *error);
bool_t Del_MsgNameOfcnt(u32 cnt);

const char *Get_FileNameOfcnt(u32 cnt);
bool_t Submit_JobOfid();
bool_t Strat_PlayDetailsAudio(PlayMediaCallback fun);
bool_t Stop_PlayDetailsAudio();
bool_t Homework_PlayAudio(struct TopicCB*Topic,PlayMediaCallback fun);
bool_t Homework_PlayAudioStop(struct TopicCB*Topic);
bool_t Homework_StartRecording(struct TopicCB*Topic);
bool_t Homework_StopRecording(struct TopicCB*Topic);
bool_t Homework_listenRecording(struct TopicCB*Topic,PlayMediaCallback fun);
bool_t Homework_listenRecordingStop(struct TopicCB*Topic);
bool_t Homework_HomeWorkPull(int para);
//做作业时

int Get_AudioLinkAddress(char**linkbuf,int bufnum);

bool_t Set_MainWinPower(u32 Percentage);
int  Set_BrightStatus(bool_t brighton);
int Set_Backlight(u32 light);
int Set_VolumeSize(u32 *Volum);
int Set_ShoutDown();
void MainInterface(void);
bool_t Get_PowerFlag(void);
void Set_PowerFlag(bool_t statue);

int Uncompress_MainInterface(void);
int UncompressPicture_to_PSRAM(void);

#ifdef __cplusplus
}
#endif

#endif /* IBOOT_GUI_INFO_H_ */
