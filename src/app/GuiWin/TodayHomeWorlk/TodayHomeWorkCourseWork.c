//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "../inc/ServerInfo.h"

//界面元素ID编号
enum TodayCourseWorkId{
    TodayCourseWork_BACKGROUND, //背景
    TodayCourseWork_WIFI,       //wifi
    TodayCourseWork_TIME,       //时间
    TodayCourseWork_POWER,      //电量
    TodayCourseWork_BACK,       //返回

    TodayCourseWork_CourseWorkName,   //课程中的作业名

    TodayCourseWork_workinfo, //作业信息

    TodayCourseWork_voice, //语音评价
    TodayCourseWork_show, //查看作业

    TodayCourseWork_MAXNUM,//总数量
};
//==================================config======================================
static struct GUIINFO TodayCourseWorkCfgTab[TodayCourseWork_MAXNUM] =
{
    [TodayCourseWork_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "TodayCourseWorkwork",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },

    [TodayCourseWork_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [TodayCourseWork_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [TodayCourseWork_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

    [TodayCourseWork_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

    [TodayCourseWork_CourseWorkName] = {
        .position = {0,24,240,24+30},
        .name = NULL,
        .type = widget_type_job,
        .userParam = 0,
    },

    [TodayCourseWork_workinfo] = {
        .position = {8,54,240-8,260},
        .name = NULL,
        .type = widget_type_text,
        .userParam = 0,
    },
    [TodayCourseWork_voice] = {
            .position = {72,230,72+96,230+34},
            .name = "语音评价",
            .type = widget_type_work,
            .userParam = BMP_VoicePlay_bmp,
    },
    [TodayCourseWork_show] = {
        .position = {8,270,240-8,320-8},
        .name = "开始作答",
        .type = widget_type_button,
        .userParam = RGB(47,43,40),
    },

};

static int Play_voice=0;
static HWND Hwnd_Voice;
//按钮控件创建函数
static bool_t  TodayCourseWorkButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    struct HWorkCB*   hwdcb;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,TodayCourseWorkCfgTab[TodayCourseWork_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        else if (0==strcmp(buttoninfo->name,TodayCourseWorkCfgTab[TodayCourseWork_show].name))//查看作业
        {
            Draw_Circle_Button(hdc,&rc,21,RGB(247,192,51));
            SetTextColor(hdc,RGB(255,255,255));

            struct HWorkCB*hwork = Get_SelectHmoeWork();
            if(hwork)
            {
                if(hwork->status == 1)
                    DrawText(hdc,"开始作答",-1,&rc,DT_VCENTER|DT_CENTER);
                else
                    DrawText(hdc,"查看作业",-1,&rc,DT_VCENTER|DT_CENTER);

            }else
            {
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }
        }
        else if (0==strcmp(buttoninfo->name,TodayCourseWorkCfgTab[TodayCourseWork_voice].name))//查看作业
        {
            hwdcb =  Get_SelectHmoeWork();
            if(hwdcb->status == 3)//已完成
            {
                if(Play_voice == 0)
                    bmp = Get_BmpBuf(buttoninfo->userParam);
                else
                    bmp = Get_BmpBuf(BMP_VoiceStop_bmp);
                if(bmp != NULL)
                {
                    DrawBMP(hdc,0,0,bmp);
         //           Draw_Icon(hdc,0,0,&buttoninfo->position,bmp);
     //               Draw_Icon_2(hdc,15,0,25,25,bmp);
                }
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_ThwCW(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
//    HWND hwndButton;
    struct HWorkCB*   hwdcb;

    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, TodayCourseWorkButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<TodayCourseWork_MAXNUM;i++)
    {
        switch (TodayCourseWorkCfgTab[i].type)
        {
            case  widget_type_work :
                {
                    hwdcb =  Get_SelectHmoeWork();
                    if(hwdcb->status == 3)//已完成
                    {
                        Hwnd_Voice =CreateButton(TodayCourseWorkCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                TodayCourseWorkCfgTab[i].position.left, TodayCourseWorkCfgTab[i].position.top,\
                                     RectW(&TodayCourseWorkCfgTab[i].position),RectH(&TodayCourseWorkCfgTab[i].position), //按钮位置和大小
                                     hwnd,i,(ptu32_t)&TodayCourseWorkCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(TodayCourseWorkCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         TodayCourseWorkCfgTab[i].position.left, TodayCourseWorkCfgTab[i].position.top,\
                         RectW(&TodayCourseWorkCfgTab[i].position),RectH(&TodayCourseWorkCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&TodayCourseWorkCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_ThwCW(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    RECT rc;
    struct HWorkCB*  hwdcb;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<TodayCourseWork_MAXNUM;i++)
        {
            switch (TodayCourseWorkCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,TodayCourseWorkCfgTab[i].userParam);
                        FillRect(hdc,&TodayCourseWorkCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(TodayCourseWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,TodayCourseWorkCfgTab[i].position.left,\
                                TodayCourseWorkCfgTab[i].position.top,&TodayCourseWorkCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&TodayCourseWorkCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
                    bmp = Get_BmpBuf(TodayCourseWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;

                case  widget_type_text :
                    hwdcb =  Get_SelectHmoeWork();
                    if(hwdcb != NULL)
                    {
                        char *txtbuf = malloc(800);
                        if(txtbuf == NULL)
                        {
                            printf("malloc error \n\r");
                            return false;
                        }
                        SetRect(&rc,TodayCourseWorkCfgTab[i].position.left,TodayCourseWorkCfgTab[i].position.top,\
                                RectW(&TodayCourseWorkCfgTab[i].position),RectH(&TodayCourseWorkCfgTab[i].position));
                        OffsetRect(&rc,0, 8);
                        SetTextColor(hdc,RGB(178,178,178));
                        DrawText(hdc,"作业要求：",-1,&rc,DT_TOP|DT_LEFT);
                        OffsetRect(&rc,0, 26);
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                        //作业要求
                        int ret = Utf8_Typesetting(hwdcb->p_details->Topic,txtbuf,800,13);
                        SetTextColor(hdc,RGB(28,32,42));
                        DrawText(hdc,txtbuf,-1,&rc,DT_TOP|DT_LEFT);
                        //作业类型
                        OffsetRect(&rc,0,22*ret);
                        strcpy(txtbuf,Get_Utf8Text(utf8_text_HomeWorkType));
                        strcat(txtbuf, hwdcb->type);
                        SetTextColor(hdc,RGB(178,178,178));
                        DrawText(hdc,txtbuf,-1,&rc,DT_TOP|DT_LEFT);

                        //显示时间
                        OffsetRect(&rc,0,20);
                        SetTextColor(hdc,RGB(178,178,178));
                        DrawText(hdc,hwdcb->time,-1,&rc,DT_RIGHT|DT_TOP);

                        for(int i=0;i<5;i++)
                        {
                            if(i<hwdcb->score)
                                bmp = Get_BmpBuf(BMP_StarDown_bmp);
                            else
                                bmp = Get_BmpBuf(BMP_StarUp_bmp);

                            if(bmp != NULL)
                            {
                                Draw_Icon_2(hdc,170+i*13,rc.top+20,11,10,bmp);
                            }
                        }

                        if(hwdcb->status == 3)//已完成
                        {
                            OffsetRect(&rc,0, 10+20);
                            strcpy(txtbuf, Get_Utf8Text(utf8_text_TeacherComments));
                            DrawText(hdc,txtbuf,-1,&rc,DT_TOP|DT_LEFT);
                            OffsetRect(&rc,0, 22);
                            // 教师评语
                            ret = Utf8_Typesetting(hwdcb->p_details->comment,txtbuf,800,13);
                            SetTextColor(hdc,RGB(28,32,42));
                            DrawText(hdc,txtbuf,-1,&rc,DT_TOP|DT_LEFT);
                        }
                        SetCharset(hdc,Charsetbak);
                        free(txtbuf);
                    }
                    break;
                case  widget_type_job :
                    hwdcb =  Get_SelectHmoeWork();
                    if(hwdcb)
                    {
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                        DrawText(hdc,hwdcb->title,-1,&TodayCourseWorkCfgTab[i].position,DT_VCENTER|DT_CENTER);
                        SetCharset(hdc,Charsetbak);
                    }
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static int Play_Endcb()
{
    printf("play end \r\n");
    Play_voice = 0;

    PostMessage(Hwnd_Voice,MSG_PAINT,0,0);
    //刷新界面
    return 0;
}

//子控件发来的通知消息
static enum WinType HmiNotify_THW_Course_Work(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    struct TopicCB*topic;
    struct HWorkCB*   hwdcb;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  TodayCourseWork_BACK        :
                NextUi = WIN_THW_Course;
                break;
            case  TodayCourseWork_show  :
                if(false == Set_SelectTopicNum(0))
                    printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,1);
                topic = Get_TopicOfCnt(Get_SelectHmoeWork(),0);
                if(topic == NULL)
                    printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,1);

                struct HWorkCB*hwork = Get_SelectHmoeWork();
                if(hwork)
                {
                    if(hwork->status == 1)
                        NextUi = topic2WinType(topic);
                    else
                        NextUi = topic1WinType(topic);
                }
                if(WIN_NotChange==NextUi)
                    printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,1);
                break;
            case  TodayCourseWork_voice  :
                {
                    hwdcb =  Get_SelectHmoeWork();
                    if(hwdcb->status == 3)//已完成
                    {
                        printf("-----------------\r\n");
                        if(Play_voice == 0)
                        {
                            Play_voice = 1;
                            Strat_PlayDetailsAudio(Play_Endcb);
                            PostMessage(Hwnd_Voice,MSG_PAINT,0,0);
                        }else if(Play_voice == 1)
                        {
                            Play_voice = 0;
                            Stop_PlayDetailsAudio();
                        }
                    }
                }
                break;

            default:
            break;
        }
    }
    if((NextUi != WIN_NotChange) && (Play_voice == 1))
    {
        Play_voice = 0;
        Stop_PlayDetailsAudio();
    }
    return NextUi;
}

int Register_THW_Course_Work()
{
    return Register_NewWin(WIN_THW_Course_Work,HmiCreate_ThwCW,HmiPaint_ThwCW,HmiNotify_THW_Course_Work);
}




