//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
//界面元素ID编号
enum THWwidgeId{
    TodayHomeWork_BACKGROUND, //背景
    TodayHomeWork_WIFI,       //wifi
    TodayHomeWork_TIME,       //时间
    TodayHomeWork_POWER,      //电量
    TodayHomeWork_BACK,      //返回
    TodayHomeWork_NAME,
    TodayHomeWork_course1,  //班级
    TodayHomeWork_course2,
    TodayHomeWork_course3,
    TodayHomeWork_course4,
    ENUM_MAXNUM,//总数量
} Exit_FlagOfTHwl;

//==================================config======================================
static const  struct GUIINFO TodayHomeWorkCfgTab[ENUM_MAXNUM] =
{
    [TodayHomeWork_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "TodayHomeWork",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },

    [TodayHomeWork_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [TodayHomeWork_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },

    [TodayHomeWork_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

    [TodayHomeWork_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },
    [TodayHomeWork_NAME] = {
        .position = {0,24,240,24+30},
        .name = "当前作业",
        .type = widget_type_text,
        .userParam = 0,
    },

    [TodayHomeWork_course1] = {
        .position = {8,62,8+224,62+54},
        .name = "course1",
        .type = widget_type_course,
        .userParam = 0,
    },
    [TodayHomeWork_course2] = {
        .position = {8,126,8+224,126+54},
        .name = "course2",
        .type = widget_type_course,
        .userParam = 1,
    },
    [TodayHomeWork_course3] = {
        .position = {8,190,8+224,190+54},
        .name = "course3",
        .type = widget_type_course,
        .userParam = 2,
    },
    [TodayHomeWork_course4] = {
        .position = {8,254,8+224,254+54},
        .name = "course4",
        .type = widget_type_course,
        .userParam = 3,
    },
};

struct courseCB_Today
{
   u32 courseBase;
   HWND hwnd[4];
   u32 coursemax;
}coursecb_today = {0,{NULL,NULL,NULL,NULL},0xffffffff};

void Clear_Coursecb_Today(void)
{
    coursecb_today.hwnd[0] = NULL;
    coursecb_today.hwnd[1] = NULL;
    coursecb_today.hwnd[2] = NULL;
    coursecb_today.hwnd[3] = NULL;
    coursecb_today.courseBase = 0;
    coursecb_today.coursemax = 0xffffffff;
}

//按钮控件创建函数
static bool_t  TodayHomeWorkButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        SetTextColor(hdc,RGB(28,32,42));
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,TodayHomeWorkCfgTab[TodayHomeWork_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
            }
        }
        else if(widget_type_course == buttoninfo->type)//课程窗口
        {
           struct classCB* course = Get_ClassOFCnt(coursecb_today.courseBase+buttoninfo->userParam);
           if(course!=NULL)
           {
               SetFillColor(hdc,RGB(255,225,147));
               FillRect(hdc,&rc);

               bmp = Get_BmpBuf(BMP_courseLogo_bmp);
               if(bmp != NULL)
               {
                   Draw_Icon_2(hdc,8,10,34,34,bmp);
               }
               bmp = Get_BmpBuf(BMP_ArrowRight_bmp);
               if(bmp != NULL)
               {
                   Draw_Icon_2(hdc,200,18,12,18,bmp);
               }
               rc.left+=57;
               struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
               struct Charset* Charsetbak = SetCharset(hdc,myCharset);
               DrawText(hdc,course->name,-1,&rc,DT_LEFT|DT_CENTER);
               SetCharset(hdc,Charsetbak);
           }else
           {
               SetFillColor(hdc,RGB(255,255,255));
               FillRect(hdc,&rc);
               SetTextColor(hdc,RGB(28,32,42));
               if(buttoninfo->userParam == 0)
               {
                   if(coursecb_today.courseBase)
                   {
                       DrawText(hdc,"这是最后一页，请往下滑！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }else{
                       DrawText(hdc,"尚未加入班级！",-1,&rc,DT_CENTER|DT_VCENTER);
                   }
               }
               if(coursecb_today.coursemax == 0xffffffff)
                   coursecb_today.coursemax = coursecb_today.courseBase+buttoninfo->userParam;
           }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
static bool_t Update_TodayHomeWorkCourse(void)
{
    u8 i;
    for(i=0;i<sizeof(coursecb_today.hwnd)/sizeof(HWND);i++)
    {
        if(coursecb_today.hwnd[i] !=NULL)
            PostMessage(coursecb_today.hwnd[i],MSG_PAINT,0,0);

    }
    return true;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Thw(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;

    coursecb_today.coursemax = 0xffffffff;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, TodayHomeWorkButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (TodayHomeWorkCfgTab[i].type)
        {
            case  widget_type_course :
                hwndButton = CreateButton(TodayHomeWorkCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             TodayHomeWorkCfgTab[i].position.left, TodayHomeWorkCfgTab[i].position.top,\
                             RectW(&TodayHomeWorkCfgTab[i].position),RectH(&TodayHomeWorkCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&TodayHomeWorkCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
                     coursecb_today.hwnd[TodayHomeWorkCfgTab[i].userParam] = hwndButton;
                }
                break;
            case  widget_type_button :
                {
                 CreateButton(TodayHomeWorkCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         TodayHomeWorkCfgTab[i].position.left, TodayHomeWorkCfgTab[i].position.top,\
                         RectW(&TodayHomeWorkCfgTab[i].position),RectH(&TodayHomeWorkCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&TodayHomeWorkCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Thw(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (TodayHomeWorkCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,TodayHomeWorkCfgTab[i].userParam);
                        FillRect(hdc,&TodayHomeWorkCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(TodayHomeWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,TodayHomeWorkCfgTab[i].position.left,\
                                TodayHomeWorkCfgTab[i].position.top,&TodayHomeWorkCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&TodayHomeWorkCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
                    bmp = Get_BmpBuf(TodayHomeWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    DrawText(hdc,TodayHomeWorkCfgTab[i].name,-1,&TodayHomeWorkCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}

//子控件发来的通知消息
static enum WinType HmiNotify_TodayHomeWork(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    s16 x,y;
    struct classCB* tmp;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        if(abs(y) > abs(x))
        {
            if(y >0){
                //下一页
                tmp = Get_ClassOFCnt(coursecb_today.courseBase+sizeof(coursecb_today.hwnd)/sizeof(HWND));
                if(tmp != NULL)
                {
                    coursecb_today.courseBase+=sizeof(coursecb_today.hwnd)/sizeof(HWND);
                    Update_TodayHomeWorkCourse();
                }
            }
            else{
                //上一页
                 if(coursecb_today.courseBase>5)
                     coursecb_today.courseBase-=5;
                 else
                     coursecb_today.courseBase=0;
                 Update_TodayHomeWorkCourse();
            }
        }
    }else if(event==MSG_BTN_UP){
        switch (id)
        {
            case  TodayHomeWork_BACK        :
                Clear_Coursecb_Today();
                NextUi = WIN_Main_WIN;
                break;
            case  TodayHomeWork_course1      :
            case  TodayHomeWork_course2      :
            case  TodayHomeWork_course3      :
            case  TodayHomeWork_course4      :
                if(coursecb_today.coursemax > (coursecb_today.courseBase + TodayHomeWorkCfgTab[id].userParam))
                {
                    tmp = Get_ClassOFCnt(coursecb_today.courseBase + TodayHomeWorkCfgTab[id].userParam);
                    Set_TodayHomeWorkType(true);
                    if((tmp !=NULL)&&(Set_SelectClass(tmp)))
                    {
                        NextUi = WIN_THW_Course;
                    }
                }
                break;
            default:
                break;
        }
    }
    return NextUi;
}
int Register_TodayHomeWork()
{
    return Register_NewWin(WIN_TodayHomeWork,HmiCreate_Thw,HmiPaint_Thw,HmiNotify_TodayHomeWork);
}


