//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      英语广场界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"

//界面元素ID编号
enum EngLishPlazaId{
    EngLishPlaza_BACKGROUND, //背景
    EngLishPlaza_WIFI,       //wifi
    EngLishPlaza_TIME,       //时间
    EngLishPlaza_POWER,      //电量
//    EngLishPlaza_POWER_LOGO,//
    EngLishPlaza_BACK,      //返回
//    EngLishPlaza_LOGO,      //
    EngLishPlaza_NAME,
    EngLishPlaza_PICTURE,
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO EngLishPlazaCfgTab[ENUM_MAXNUM] =
{
    [EngLishPlaza_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "MainWin",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },
    [EngLishPlaza_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [EngLishPlaza_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = 0,
    },

    [EngLishPlaza_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },

//    [EngLishPlaza_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },

    [EngLishPlaza_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },

//    [EngLishPlaza_LOGO] = {
//        .position = {160-24,20+10,160,20+10+24},
//        .name = "logo",
//        .type = widget_type_picture,
//        .userParam = BMP_EngLishPlazaList_bmp,
//    },

    [EngLishPlaza_NAME] = {
        .position = {0,24,240,24+30},
        .name = "广场",
        .type = widget_type_text,
        .userParam = 0,
    },

    [EngLishPlaza_PICTURE] = {
        .position = {42,62,42+157,62+250},
        .name = "努力开发中",
        .type = widget_type_picture,
        .userParam = BMP_WorkingHard_bmp,
    },
};



//按钮控件创建函数
static bool_t  EngLishPlazaButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
//    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
//        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,EngLishPlazaCfgTab[EngLishPlaza_BACK].name))//返回
        {
//            rc.bottom-=8;
//            SetFillColor(hdc,RGB(247,192,51));
//            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
//                DrawBMP(hdc,8,6,bmp);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Setting(struct WindowMsg *pMsg)
{
    HDC  hdc;
    RECT rc;
    const char * bmp ;
    HWND hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        //消息处理函数表
        static const struct MsgProcTable s_gMsgTablebutton[] =
        {
                {MSG_PAINT, EngLishPlazaButtonPaint},
        };
        static struct MsgTableLink  s_gDemoMsgLinkBUtton;

        s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
        s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

        GetClientRect(hwnd,&rc);
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            if(EngLishPlazaCfgTab[i].type == type_background)
            {
                bmp = Get_BmpBuf(EngLishPlazaCfgTab[i].userParam);
                if(bmp != NULL)
                {
                    DrawBMP(hdc,EngLishPlazaCfgTab[i].position.left,\
                            EngLishPlazaCfgTab[i].position.top,bmp);
                }
                break;
            }
        }
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (EngLishPlazaCfgTab[i].type)
            {
                case  widget_type_button :
                    {
                     CreateButton(EngLishPlazaCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             EngLishPlazaCfgTab[i].position.left, EngLishPlazaCfgTab[i].position.top,\
                             RectW(&EngLishPlazaCfgTab[i].position),RectH(&EngLishPlazaCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&EngLishPlazaCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                    break;
                default:    break;
            }
        }

        return true;
        }
        return false;
}

//绘制消息处函数
static bool_t HmiPaint_Setting(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (EngLishPlazaCfgTab[i].type)
            {
                case  type_background :
                    bmp = Get_BmpBuf(EngLishPlazaCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        DrawBMP(hdc,EngLishPlazaCfgTab[i].position.left,\
                                EngLishPlazaCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(EngLishPlazaCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,EngLishPlazaCfgTab[i].position.left,\
                                EngLishPlazaCfgTab[i].position.top,&EngLishPlazaCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&EngLishPlazaCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
//                    PowerProgressbar.Pos = Win_GetPower();
//                    sprintf(PowerProgressbar.text,"%d",PowerProgressbar.Pos);
//                    DrawText(hdc,PowerProgressbar.text,-1,&EngLishPlazaCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    //    SendMessage(Powerhwnd, MSG_ProcessBar_SETDATA, (u32)&PowerProgressbar, 0);

                    bmp = Get_BmpBuf(EngLishPlazaCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    SetTextColor(hdc,RGB(255,255,255));
                    DrawText(hdc,EngLishPlazaCfgTab[i].name,-1,&EngLishPlazaCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}

static enum WinType HmiNotify_EngLishPlaza(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 id =LO16(pMsg->Param1);
    u16 event =HI16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case EngLishPlaza_BACK:
                NextUi = WIN_Main_WIN;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_EngLishPlaza()
{
    return Register_NewWin(WIN_EngLishPlaza,HmiCreate_Setting,HmiPaint_Setting,HmiNotify_EngLishPlaza);
}
