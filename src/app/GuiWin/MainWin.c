//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "app_flash.h"
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include <upgrade.h>


extern void SetChargeFlag(enum ChargeFlag flag);
extern bool_t IsCharge(void);

#define CN_SETTING_VOLUME           0       //当前设置音量
#define CN_SETTING_BRIGHT           1       //当前设置亮度
#define CN_TIMER_CLOSE_BRIGHT       10      //关闭背光定时
#define CN_TIMER_POWERDOWN          20      //关闭电源定时
#define CN_TIMER_PAUSE_PLAY         21      //播放暂停
#define CN_TIMER_COMEBACK           22      //录音
#define CN_TIMER_POWERING           23      //电源一分钟定时
#define CN_TIMER_STOPSHUTDOWN       24      //一分钟检查是否在播放音频，或者在录音

#define CN_TIMER_CTRL               30      //音量、亮度、电源开关控制
#define CN_TIMER_CHARGE_FLAG        40      //检测是否充电标志
#define CN_VOLUMEBAR                10      //也没什么含义

HWND tg_pMainWinHwn,tg_VolumeBar;
struct WinTimer *tg_pCtrlTimer,*tg_pCloseBrightTimer,*tg_pPowerDownTimer,*tg_pPoweringTimer,*tg_pChargeFlagTimer;

struct WinTimer *tg_pCtrlStopShutdown;
u32 u32g_Volume = 80;                   //当前音量值
u32 u32g_Bright = 50;                   //当前亮度值
u32 u32g_Setting = CN_SETTING_VOLUME;   //"+"、"-"按钮的功能，选择调音量或者背光亮度

static enum WinType eng_LastWindow = WIN_Main_WIN;
static enum WinType eng_NexttWindow = WIN_Main_WIN;
static enum WinType eng_UpdateWindow = WIN_Main_WIN;
static u8 ShutOff_Statue = 0;
void Set_ShutOff_Statue(u8 statue)
{
    ShutOff_Statue = statue;
}
enum WinType Get_LastWinType()
{
    return eng_LastWindow;
}

void Set_LastWinType(enum WinType state)
{
    eng_LastWindow = state;
}

enum WinType Get_NextWinType()
{
    return eng_NexttWindow;
}

void Set_NextWinType(enum WinType state)
{
    eng_NexttWindow = state;
}

enum WinType Get_UpdateWinType()
{
    return eng_UpdateWindow;
}

void Set_UpdateWinType(enum WinType state)
{
    eng_UpdateWindow = state;
}

PROGRESSBAR_DATA tg_SettingProgressbar = //音量进度条
{
    .Flag    =PBF_SHOWTEXT|PBF_ORG_LEFT,
    .Range   =100,
    .Pos     = 50,
    .FGColor = CN_COLOR_GREEN,
    .BGColor = CN_COLOR_WHITE,
    .TextColor =CN_COLOR_WHITE,
    .DrawTextFlag =DT_VCENTER|DT_CENTER,
    .text = NULL,
};

//按键手动控制定时器的状态机
enum CtrlTimerMachine
{
    CtrlNullOp,
    CtrlUpVolume,
    CtrlDownVolume,
    CtrlUpBright,
    CtrlDownBright,
    CtrlPowerDown,
    CtrlUpdateApp,
    CtrlSubmitFail,
    CtrlWaitExit,
};
enum CtrlTimerMachine en_StatueMachine;
u32 u32g_BrightStatus = 1;       //表示背光是亮的，开机后确实是亮的


enum widgeId{
    ENUM_BACKGROUND, //背景
    ENUM_WIFI,       //wifi
//    ENUM_POWER_LOGO,//
    ENUM_TIME,       //时间
    ENUM_POWER,      //电量
//    ENUM_ARROW_LEFT_PICTURE,
//    ENUM_ARROW_RIGHT_PICTURE,
    ENUM_MAIN_LIST_UI,
//==============================================================================
    ENUM_mini_ui1,
    ENUM_mini_ui2,
    ENUM_mini_ui3,
    ENUM_mini_ui4,
    ENUM_mini_ui5,
//==============================================================================
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MainWinCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "MainWin",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },

    [ENUM_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [ENUM_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
//    [ENUM_POWER_LOGO] = {
//        .position = {212,4,212+24,4+13},
//        .name = "plogo",
//        .type = widget_type_picture,
//        .userParam = BMP_PowerLogo_bmp,
//    },
    [ENUM_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
//    [ENUM_ARROW_LEFT_PICTURE] = {
//        .position = {16,120,16+12,120+18},
//        .name = "ENUM_ARROW_LEFT_PICTURE",
//        .type = type_MainLogo_background,
//        .userParam = BMP_ArrowLeft_bmp,
//    },
//    [ENUM_ARROW_RIGHT_PICTURE] = {
//        .position = {212,120,212+12,120+18},
//        .name = "ENUM_ARROW_RIGHT_PICTURE",
//        .type = type_MainLogo_background,
//        .userParam = BMP_ArrowRight_bmp,
//    },
    [ENUM_MAIN_LIST_UI] = {
        .position ={28,32,28+184,32+220},
        .name = "ENUM_MAIN_LIST_UI",
        .type = widget_type_button,
        .userParam = ENUM_MAIN_LIST_UI,
    },
    [ENUM_mini_ui1] = {
        .position = {9+42*0,268,9+42*1,268+42},
        .name = "ENUM_mini_ui1",
        .type = widget_type_button,
        .userParam = ENUM_mini_ui1,
    },
    [ENUM_mini_ui2] = {
        .position = {9+42*1+3*1,268,9+42*2+3*1,268+42},
        .name = "ENUM_mini_ui2",
        .type = widget_type_button,
        .userParam = ENUM_mini_ui2,
    },
    [ENUM_mini_ui3] = {
        .position = {9+42*2+3*2,268-10,9+42*3+3*2,268+42-10},
        .name = "ENUM_mini_ui3",
        .type = widget_type_button,
        .userParam = ENUM_mini_ui3,
    },
    [ENUM_mini_ui4] = {
        .position = {9+42*3+3*3,268,9+42*4+3*3,268+42},
        .name = "ENUM_mini_ui4",
        .type = widget_type_button,
        .userParam = ENUM_mini_ui4,
    },
    [ENUM_mini_ui5] = {
        .position = {9+42*4+3*4,268,9+42*5+3*4,268+42},
        .name = "ENUM_SET_LOGO",
        .type = widget_type_button,
        .userParam = ENUM_mini_ui5,
    },

};
//extern  PROGRESSBAR_DATA PowerProgressbar;


enum enumui
{
     enumui_TodayHomeWork,
     enumui_HomeWorkLists,
     enumui_EngLishPlaza,
     enumui_Messages,
     enumui_Settings,
     enumui_Max,
};
struct UTypeCB
{
    enum enumui  Selection;
    enum Bmptype UIInfotab[enumui_Max][enumui_Max];

} PageParameters = {
    enumui_TodayHomeWork,
    {{BMP_MessagesLogo_bmp, BMP_SetLogo_bmp,BMP_TodayHomeLogo_bmp, BMP_HomeWorkLogo_bmp, BMP_EngLishPlazaLogo_bmp },
     {BMP_SetLogo_bmp,BMP_TodayHomeLogo_bmp, BMP_HomeWorkLogo_bmp, BMP_EngLishPlazaLogo_bmp, BMP_MessagesLogo_bmp },
     {BMP_TodayHomeLogo_bmp, BMP_HomeWorkLogo_bmp, BMP_EngLishPlazaLogo_bmp, BMP_MessagesLogo_bmp, BMP_SetLogo_bmp},
     {BMP_HomeWorkLogo_bmp, BMP_EngLishPlazaLogo_bmp, BMP_MessagesLogo_bmp, BMP_SetLogo_bmp, BMP_TodayHomeLogo_bmp},
     {BMP_EngLishPlazaLogo_bmp, BMP_MessagesLogo_bmp, BMP_SetLogo_bmp, BMP_TodayHomeLogo_bmp, BMP_HomeWorkLogo_bmp}}
};

static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);
    hdc =BeginPaint(hwnd);
    GetClientRect(hwnd,&rc);

    enum Bmptype type = Bmp_NULL;

    bmp = Get_BmpBuf(BMP_Background_bmp);
    if(bmp != NULL)
    {
        DrawBMP(hdc,-buttoninfo->position.left,-buttoninfo->position.top,bmp);
    }
    switch (buttoninfo->userParam)
    {

        case ENUM_MAIN_LIST_UI:
            {
                switch (PageParameters.Selection)
                {
                    case enumui_TodayHomeWork:
                        type = BMP_TodayHomeWork_bmp;
                        break;
                    case enumui_HomeWorkLists:
                        type = BMP_HomeWorkLists_bmp;
                        break;
                    case enumui_EngLishPlaza:
                        type = BMP_EngLishPlaza_bmp;
                        break;
                    case  enumui_Messages:
                        type = BMP_Messages_bmp;
                        break;
                    case enumui_Settings:
                        type = BMP_Settings_bmp;
                        break;
                    default: break;
                }
            } break;
        case ENUM_mini_ui1:
            type = PageParameters.UIInfotab[PageParameters.Selection][0];
            break;
        case ENUM_mini_ui2:
            type = PageParameters.UIInfotab[PageParameters.Selection][1];
            break;
        case ENUM_mini_ui3:
            type = PageParameters.UIInfotab[PageParameters.Selection][2];
            break;
        case ENUM_mini_ui4:
            type = PageParameters.UIInfotab[PageParameters.Selection][3];
            break;
        case ENUM_mini_ui5:
            type = PageParameters.UIInfotab[PageParameters.Selection][4];
        break;
        default:                               break;
    }

    bmp = Get_BmpBuf(type);
    if(bmp != NULL)
    {
        Draw_Icon(hdc,0,0,&buttoninfo->position,bmp);
    }
    EndPaint(hwnd,hdc);
    return true;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_MainWin(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;

//    Mainlistcb.Firstmenu = true;
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,BmpButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (MainWinCfgTab[i].type)
        {
            case  widget_type_button :
            {

                HWND tmpHwnd =  CreateButton(MainWinCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                 MainWinCfgTab[i].position.left, MainWinCfgTab[i].position.top,\
                                 RectW(&MainWinCfgTab[i].position),RectH(&MainWinCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&MainWinCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(tmpHwnd == NULL)
                {
                    printf("\n\r------CreateButton Mainhwnd error  !!\n\r");
#if(DEBUG == 1)
                    void heap();
                    void event();
//                  bool_t init_jtag(char *param);
                    heap();
                    event();
//                  init_jtag(NULL);
#endif
                }
            }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_MainWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);

    //主界面背景
    bmp = Get_BmpBuf(MainWinCfgTab[ENUM_BACKGROUND].userParam);
    if(bmp != NULL)
    {
        DrawBMP(hdc,MainWinCfgTab[ENUM_BACKGROUND].position.left,\
                MainWinCfgTab[ENUM_BACKGROUND].position.top,bmp);
    }
    //左右箭头 logo

    //Wifi 状态
    bmp = Get_BmpBuf(MainWinCfgTab[ENUM_WIFI].userParam);
    if(bmp != NULL)
    {
        Draw_Icon(hdc,MainWinCfgTab[ENUM_WIFI].position.left,\
                MainWinCfgTab[ENUM_WIFI].position.top,&MainWinCfgTab[ENUM_WIFI].position,bmp);
    }

    //时间
    char* time =  Win_GetTime();
    DrawText(hdc,time,-1,&MainWinCfgTab[ENUM_TIME].position,DT_CENTER|DT_VCENTER);

    //电量

    bmp = Get_BmpBuf(MainWinCfgTab[ENUM_POWER].userParam);
    if(bmp != NULL)
    {
        Draw_Icon_2(hdc,212,4,24,13,bmp);
    }
    EndPaint(hwnd,hdc);
    return true;

}
static  enum WinType Do_MsgBtnMove(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    s16 x= LO16(pMsg->Param2);
    s16 y= HI16(pMsg->Param2);

    if(abs(y) < abs(x))
    {
        if(x < 0)
        {
            if(PageParameters.Selection >= enumui_Max-1)
            {
                PageParameters.Selection = 0;
            }
            else
            {
                PageParameters.Selection++;
            }
        }
        else
        {
            if(PageParameters.Selection == 0)
            {
                PageParameters.Selection = enumui_Max-1;
            }
            else
            {
                PageParameters.Selection--;
            }
        }
        NextUi = WIN_Main_WIN;
    }
    return NextUi;
}
static  enum WinType Do_MsgBtnUp(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    char flag = Get_ServerInfo_Flag();
    int minicnt = -100 ;
    u16 id =LO16(pMsg->Param1);
    switch(id)
    {
        case ENUM_MAIN_LIST_UI:
            switch (PageParameters.Selection)
            {
                case enumui_TodayHomeWork:
                    NextUi = WIN_TodayHomeWork;
                    break;
                case enumui_HomeWorkLists:
                    NextUi = WIN_HomeWorkLists;
                    break;
                case enumui_EngLishPlaza:
                    NextUi = WIN_EngLishPlaza;
                    break;
                case  enumui_Messages:
                    NextUi = WIN_Messages;
                    break;
                case enumui_Settings:
                    NextUi = WIN_Settings;
                    break;
                default: break;
            }
            break;
        case ENUM_mini_ui1:
            minicnt = enumui_Max -2;
            break;
        case ENUM_mini_ui2:
            minicnt = enumui_Max -1;
            break;
        case ENUM_mini_ui3:
            minicnt = enumui_Max;
            break;
        case ENUM_mini_ui4:
            minicnt = enumui_Max + 1;
            break;
        case ENUM_mini_ui5:
            minicnt =  enumui_Max + 2;
        break;
        default: break;
    }

    if(minicnt != -100)
    {
        PageParameters.Selection = (PageParameters.Selection + minicnt)%enumui_Max;
        switch (PageParameters.Selection)
        {
            case enumui_TodayHomeWork:
                NextUi = WIN_TodayHomeWork;
                break;
            case enumui_HomeWorkLists:
                NextUi = WIN_HomeWorkLists;
                break;
            case enumui_EngLishPlaza:
                NextUi = WIN_EngLishPlaza;
                break;
            case  enumui_Messages:
                NextUi = WIN_Messages;
                break;
            case enumui_Settings:
                NextUi = WIN_Settings;
                break;
            default: break;
        }
    }

    if(!flag)
    {
        switch (NextUi)
        {
            case WIN_TodayHomeWork:
            case WIN_HomeWorkLists:
            case  WIN_Messages:
                Set_LastWinType(Get_SelectionWinType());
                Set_NextWinType(NextUi);
                NextUi = WIN_NetLoading;
                break;
            default: break;
        }
    }
    return NextUi;
}
//  UpdateDisplay(0);
static enum WinType HmiNotify_MainWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);

    switch (event) {
    case MSG_BTN_PEN_MOVE:
        NextUi = Do_MsgBtnMove(pMsg);
        break;
    case MSG_BTN_UP:
        NextUi = Do_MsgBtnUp(pMsg);
        break;
    default:   break;
    }
    return NextUi;
}




struct keycb{
    enum keystatus {
        Initial_state,
        key_Down,
        key_up,
    }status;
    struct WinTimer*  Timerhd;
};
static struct keycb PAUSE_PLAY_CB = {Initial_state,NULL};
static struct keycb COMEBACK_CB = {Initial_state,NULL};

static bool_t HmiKeyDown(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u16 KeyValue;
    hwnd =pMsg->hwnd;
    KeyValue = pMsg->Param1;
    HDC hdc =BeginPaint(hwnd);
    GDD_ResetTimer(tg_pCloseBrightTimer,30000);
    GDD_ResetTimer(tg_pPowerDownTimer,600000);
    if(KeyValue == POWER_KEY)
    {
        en_StatueMachine = CtrlPowerDown;
        GDD_ResetTimer(tg_pCtrlTimer,3000);
    }else{
        if(KeyValue == VOL_UP_KEY)
        {
            GDD_ResetTimer(tg_pCtrlTimer,500);
            if(u32g_Volume == 0)
            {
                if(GetSpeakerState() == Speaker_off)
                    OpenSpeaker();  //开喇叭
            }

            if(u32g_Setting == CN_SETTING_VOLUME)
            {
                if(u32g_Volume <= 90)
                    u32g_Volume +=10;
                tg_SettingProgressbar.Pos = u32g_Volume;
                if(en_StatueMachine == CtrlWaitExit)
                {
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                else
                {
                tg_VolumeBar = CreateProgressBar("音量",WS_CHILD|PBS_HOR|WS_VISIBLE | WS_UNFILL,
                                                20,150,200,20,tg_pMainWinHwn,CN_VOLUMEBAR,
                                                (ptu32_t)&tg_SettingProgressbar,NULL);
                }
                en_StatueMachine = CtrlUpVolume;
            }
            else
            {
                if(u32g_Bright <= 90)
                    u32g_Bright +=10;
                tg_SettingProgressbar.Pos = u32g_Bright;
                en_StatueMachine = CtrlUpBright;
                //TODO：在setting界面创建进度条
            }
            djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
        }
        else if(KeyValue == VOL_DOWN_KEY)
        {
            GDD_ResetTimer(tg_pCtrlTimer,500);
            if(u32g_Setting == CN_SETTING_VOLUME)
            {
                if(u32g_Volume >= 10)
                    u32g_Volume -=10;
                tg_SettingProgressbar.Pos = u32g_Volume;
                if(en_StatueMachine == CtrlWaitExit)
                {
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                else
                {
                tg_VolumeBar = CreateProgressBar("音量",WS_CHILD|PBS_HOR|WS_VISIBLE | WS_UNFILL,
                                                20,150,200,20,tg_pMainWinHwn,CN_VOLUMEBAR,
                                                (ptu32_t)&tg_SettingProgressbar,NULL);
                }
                en_StatueMachine = CtrlDownVolume;
            }
            else
            {
                if(u32g_Bright >= 10)
                    u32g_Bright -=10;
                tg_SettingProgressbar.Pos = u32g_Bright;
                en_StatueMachine = CtrlDownBright;
                //TODO：在setting界面创建控制条
            }
            if(u32g_Volume < 10)
            {
                if(GetSpeakerState() == Speaker_on)
                    CloseSpeaker();    //关喇叭
                u32g_Volume = 0;
            }
            djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
        }
        else if((KeyValue == PAUSE_PLAY_KEY)&&(PAUSE_PLAY_CB.status == Initial_state))
        {
            PAUSE_PLAY_CB.status = key_Down;
            GDD_ResetTimer(PAUSE_PLAY_CB.Timerhd,3000);
            GDD_StartTimer(PAUSE_PLAY_CB.Timerhd);
        }
        else if((KeyValue == COMEBACK_KEY) && (COMEBACK_CB.status == Initial_state))
        {
           COMEBACK_CB.status = key_Down;
           GDD_ResetTimer(COMEBACK_CB.Timerhd,3000);
           GDD_StartTimer(COMEBACK_CB.Timerhd);
        }
//      else if(KeyValue == HEADSET_STATE)
//        {
//            if(GetSpeakerState() == Speaker_on)
//            {
//                CloseSpeaker();    //连接耳机、关喇叭
//            }
//        }
    }

    EndPaint(hwnd,hdc);
    return true;
}

static bool_t Key_MsgDistribution(struct WindowMsg *pMsg,enum Keycmd cmd,enum EasyKeyValue keyid)
{
    struct WindowMsg Msg;

    Msg.hwnd   = pMsg->hwnd;
    Msg.Code   = MSG_NOTIFY;
    Msg.Param1 = (MSG_KEY_CHANGE<<16|keyid);
    Msg.Param2 = cmd;
    Msg.ExData = NULL;

    return  HmiNotify_EasyTalk(&Msg);
}

bool_t Set_BrightStatusOpenScreen()
{
    if(u32g_BrightStatus == 0)
    {
        u32g_BrightStatus = 1;
        OpenScreen();
    }


    return true;
}

static bool_t HmiKeyUp(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u16 KeyValue;
    hwnd = pMsg->hwnd;
    HDC hdc =BeginPaint(hwnd);
    KeyValue = pMsg->Param1;
    if(KeyValue == POWER_KEY)
    {
        GDD_StopTimer(tg_pCtrlTimer);
        if(u32g_BrightStatus == 1)
        {
            GDD_StopTimer(tg_pCloseBrightTimer);
            if(!ShutOff_Statue)
            {
                u32g_BrightStatus = 0;
//                CloseBackLight();
                CloseScreen();
            }
        }
        else
        {
            u32g_BrightStatus = 1;
//            OpenBackLight();
            OpenScreen();
        }
    }else{
        if(KeyValue == VOL_UP_KEY)
        {
            en_StatueMachine = CtrlWaitExit;
        }
        else if(KeyValue == VOL_DOWN_KEY)
        {
            en_StatueMachine = CtrlWaitExit;
        }
        else if((KeyValue == PAUSE_PLAY_KEY)&&(PAUSE_PLAY_CB.status==key_Down))
        {
            GDD_StopTimer(PAUSE_PLAY_CB.Timerhd);
            Key_MsgDistribution(pMsg,enum_key_Short_press,PAUSE_PLAY_KEY);
            PAUSE_PLAY_CB.status = Initial_state;
        }
        else if((KeyValue == COMEBACK_KEY)&&(COMEBACK_CB.status == key_Down))
        {
            GDD_StopTimer(COMEBACK_CB.Timerhd);
            Key_MsgDistribution(pMsg,enum_key_Short_press,COMEBACK_KEY);
            COMEBACK_CB.status = Initial_state;
        }
//      else if(KeyValue == HEADSET_STATE)
//        {
//            if(GetSpeakerState() == Speaker_off)
//            {
//                OpenSpeaker();    //断开耳机、开喇叭
//            }
//        }
    }
    EndPaint(hwnd,hdc);
    return true;
}

void Set_Time_Ctrl()
{
    en_StatueMachine = CtrlUpdateApp;
    GDD_ResetTimer(tg_pCtrlTimer,1000);
}

void SubmitFail_Time_Ctrl()
{
    en_StatueMachine = CtrlSubmitFail;
    GDD_ResetTimer(tg_pCtrlTimer,2000);
}

void Record_SubmitHomeFail()
{
    DestroyAllChild(tg_pMainWinHwn);
    PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, WIN_RecordSubmitFail, 0);
}

void Power_Lower()
{
    eng_LastWindow = Get_SelectionWinType();
    DestroyAllChild(tg_pMainWinHwn);
    PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, WIN_PowerLower, 0);
}

static bool_t HmiTimer(struct WindowMsg *pMsg)
{
    HWND hwnd;
    struct WinTimer *pTmr;
    u16 TmrId;
    hwnd=pMsg->hwnd;
    TmrId=pMsg->Param1;
    pTmr=GDD_FindTimer(hwnd,TmrId);
    static bool_t last_charge_flag = false, now_charge_flag;

    if(TmrId == CN_TIMER_CTRL)
    {
        HDC hdc =BeginPaint(hwnd);
        switch(en_StatueMachine)
        {
            //TODO：在以下各case内，加上调音量或亮度代码
            case CtrlUpVolume:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Volume == 0)
                {
                    if(GetSpeakerState() == Speaker_off)
                        OpenSpeaker(); //开喇叭
                }

                if(u32g_Volume <= 90)
                {
                    u32g_Volume += 10;
                    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                break;
            case CtrlDownVolume:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Volume >= 10)
                {
                    u32g_Volume -= 10;
                    if(u32g_Volume < 10)
                    {
                        if(GetSpeakerState() == Speaker_on)
                            CloseSpeaker(); //关喇叭
                        u32g_Volume = 0;
                    }
                    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                break;
            case CtrlUpBright:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Bright <= 90)
                {
                    u32g_Bright +=10;
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Bright,0);
                }
                break;
            case CtrlDownBright:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Bright >= 10)
                {
                    u32g_Bright -=10;
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Bright,0);
                }
                break;
            case CtrlWaitExit:
                en_StatueMachine = CtrlNullOp;
                GDD_StopTimer(pTmr);
                DestroyAllChild(hwnd);
                PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
                break;
            case CtrlPowerDown:
                printf(">ShutOff\r\n");
                ShutOff_Statue = 1;
                eng_LastWindow = Get_SelectionWinType();
                DestroyAllChild(hwnd);      //删除当前控件。
                PostMessage(hwnd, MSG_REFRESH_UI, WIN_Settings_TurnOff, 0);
                break;
            case CtrlUpdateApp:
                GDD_ResetTimer(pTmr,1000);
                DestroyAllChild(hwnd);
                PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
                break;
            case CtrlSubmitFail:
                GDD_StopTimer(pTmr);
                DestroyAllChild(hwnd);
                PostMessage(hwnd, MSG_REFRESH_UI, Get_LastWinType(), 0);
                break;
            default:
                break;
        }
        EndPaint(hwnd,hdc);
    }
    else if(TmrId == CN_TIMER_CLOSE_BRIGHT)     //表示定时关背光定时器到了。
    {
        if(Get_update_flag()!=UpgradInProgress)
        {
            printf(">CloseScreen\r\n");
            GDD_StopTimer(pTmr);
            u32g_BrightStatus = 0;
            CloseScreen();
        }
    }
    else if(TmrId == CN_TIMER_POWERDOWN)        //表示定时关机定时器到了。
    {
        if(Get_update_flag()!=UpgradInProgress)
        {
            GDD_StopTimer(pTmr);
            Set_ShoutDown();
        }
    }else if(TmrId == CN_TIMER_POWERING)        //表示电源一分钟定时器到了。
    {
//        printf("+++++CN_TIMER_POWERING+++++");
        GDD_ResetTimer(pTmr,60000);
//        if(IsCharge())
//        {
//            GDD_ResetTimer(tg_pPowerDownTimer,300000);
//        }
    }else if(TmrId == CN_TIMER_STOPSHUTDOWN)
    {
        GDD_ResetTimer(pTmr,1000);
        if(true == Check_IsPlayOrRecord())
        {
//            printf("IsPlayOrRecord Reset PowerDownTimer\r\n");
            GDD_ResetTimer(tg_pPowerDownTimer,600000);
        }
        int WarningCheck();
        WarningCheck();

    }else if(TmrId == CN_TIMER_PAUSE_PLAY)
    {
        GDD_StopTimer(PAUSE_PLAY_CB.Timerhd);
        Key_MsgDistribution(pMsg,enum_key_Long_press,PAUSE_PLAY_KEY);
        PAUSE_PLAY_CB.status = Initial_state;
    }else if(TmrId == CN_TIMER_COMEBACK)
    {
        GDD_StopTimer(COMEBACK_CB.Timerhd);
        Key_MsgDistribution(pMsg,enum_key_Long_press,COMEBACK_KEY);
        COMEBACK_CB.status = Initial_state;
    }else if(TmrId == CN_TIMER_CHARGE_FLAG)
    {
        if(usb_is_plug_in() != 0)
            SetChargeFlag(OnCharge);
        else
            SetChargeFlag(NoCharge);
        now_charge_flag = IsCharge();
        if(now_charge_flag != last_charge_flag)
        {
            last_charge_flag = now_charge_flag;
            set_cell_power();
            DestroyAllChild(hwnd);      //删除当前控件。
            PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
        }

    }
    return true;
}

static bool_t HmiMove_MainWin(struct WindowMsg *pMsg)
{
    struct WindowMsg Msg;

    Msg.hwnd   = pMsg->hwnd;
    Msg.Code   = MSG_NOTIFY;
    Msg.Param1 = (MSG_BTN_PEN_MOVE<<16|ENUM_MAIN_LIST_UI);
    Msg.Param2 = pMsg->Param1;
    Msg.ExData = NULL;

    HmiNotify_EasyTalk(&Msg);
    return true;
}
bool_t Refresh_GuiWin()
{
    return Refresh_SwitchWIn(tg_pMainWinHwn);
}
void Main_GuiWin(void)
{
    Init_WinSwitch();
    int Server_EventInit();
    Server_EventInit();
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTable[] =
    {
        {MSG_CREATE,HmiCreate_MainWin},         //主窗口创建消息
        {MSG_PAINT,HmiPaint_MainWin},           //绘制消息
        {MSG_NOTIFY,HmiNotify_EasyTalk},        //子控件发来的通知消息
        {MSG_REFRESH_UI,HmiRefresh},            //刷新窗口消息
        {MSG_KEY_DOWN,HmiKeyDown},              //按键按下消息
        {MSG_KEY_UP,HmiKeyUp},                  //按键按下消息
        {MSG_TIMER,HmiTimer},                   //定时器消息响应函数
        {MSG_TOUCH_MOVE,HmiMove_MainWin},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLink;

    s_gBmpDemoMsgLink.MsgNum = sizeof(s_gBmpMsgTable) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLink.myTable = (struct MsgProcTable *)&s_gBmpMsgTable;
    tg_pMainWinHwn = GDD_CreateGuiApp((char*)MainWinCfgTab[ENUM_BACKGROUND].name,
                                &s_gBmpDemoMsgLink, 0x1000, CN_WINBUF_PARENT,0);
    tg_pCtrlTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CTRL,100);
    tg_pCloseBrightTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CLOSE_BRIGHT,30000);
    tg_pPowerDownTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_POWERDOWN,600000);
    tg_pPoweringTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_POWERING,60000);
    tg_pCtrlStopShutdown = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_STOPSHUTDOWN,1000);
    tg_pChargeFlagTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CHARGE_FLAG,1000);
    PAUSE_PLAY_CB.Timerhd = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_PAUSE_PLAY,3000);
    COMEBACK_CB.Timerhd = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_COMEBACK,3000);
    GDD_ResetTimer(tg_pCloseBrightTimer,30000);
    GDD_ResetTimer(tg_pPowerDownTimer,600000);
    GDD_ResetTimer(tg_pChargeFlagTimer,1000);
    GDD_ResetTimer(tg_pCtrlStopShutdown,1000);
}

int Register_Main_WIN()
{
    return Register_NewWin(WIN_Main_WIN,HmiCreate_MainWin,HmiPaint_MainWin,HmiNotify_MainWin);
}





