//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <app_flash.h>
#include "homeWork.h"
//控件ID编号
enum SubmitStatueId{
    SubmitStatue_BACK,      //返回
    SubmitStatue_BACKGROUND, //背景
    SubmitStatue_PICTURE, //
//    SubmitStatue_Confirm,//确认关机
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO SubmitStatueCfgTab[ENUM_MAXNUM] =
{
    [SubmitStatue_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "SubmitStatue",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },

//    [SubmitStatue_BACK] = {
//        .position ={190,8,190+42,8+42},
//        .name = "关闭",
//        .type = widget_type_button,
//        .userParam = BMP_Close_bmp,
//    },

    [SubmitStatue_PICTURE] = {
        .position ={38,33,38+154,33+248},
        .name = "loading",
        .type = widget_type_picture,
        .userParam = BMP_Submitting_bmp,
    },

};

#if 0
//按钮控件创建函数
static bool_t  SubmitStatueButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,SubmitStatueCfgTab[SubmitStatue_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,9,9,24,24,bmp);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
#endif
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static bool_t HmiCreate_SubmitStatue(struct WindowMsg *pMsg)
{
#if 0
    RECT rc;
    HDC  hdc;
    const char * bmp ;
    HWND hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        //消息处理函数表
        static const struct MsgProcTable s_gMsgTablebutton[] =
        {
                {MSG_PAINT, SubmitStatueButtonPaint},
        };
        static struct MsgTableLink  s_gDemoMsgLinkBUtton;

        s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
        s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

        GetClientRect(hwnd,&rc);
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            if(SubmitStatueCfgTab[i].type == type_background)
            {
                bmp = Get_BmpBuf(SubmitStatueCfgTab[i].userParam);
                if(bmp != NULL)
                {
                    DrawBMP(hdc,SubmitStatueCfgTab[i].position.left,\
                            SubmitStatueCfgTab[i].position.top,bmp);
                }
                break;
            }
        }
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SubmitStatueCfgTab[i].type)
            {
                case  widget_type_button :
                    {
                     CreateButton(SubmitStatueCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             SubmitStatueCfgTab[i].position.left, SubmitStatueCfgTab[i].position.top,\
                             RectW(&SubmitStatueCfgTab[i].position),RectH(&SubmitStatueCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&SubmitStatueCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
#endif
    return false;
}
#pragma GCC diagnostic pop

//绘制消息处函数
static bool_t HmiPaint_SubmitStatue(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;

//    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SubmitStatueCfgTab[i].type)
            {
                case  type_background :
                        bmp = Get_BmpBuf(SubmitStatueCfgTab[i].userParam);
                        if(bmp != NULL)
                        {
                            DrawBMP(hdc,SubmitStatueCfgTab[i].position.left,\
                                    SubmitStatueCfgTab[i].position.top,bmp);
                        }
                        break;
                case  widget_type_picture :
                    if(Get_SubmitFlag() == 2)
                        bmp = Get_BmpBuf(BMP_SubmitSuccess_bmp);
                    else if(Get_SubmitFlag() == 3)
                        bmp = Get_BmpBuf(BMP_SubmitFail_bmp);
                    else
                        bmp = Get_BmpBuf(SubmitStatueCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,SubmitStatueCfgTab[i].position.left,\
                                SubmitStatueCfgTab[i].position.top,&SubmitStatueCfgTab[i].position,bmp);
                    }
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static enum WinType HmiNotify_SubmitStatue(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
//    u16 event,id;
//    event =HI16(pMsg->Param1);
//    id =LO16(pMsg->Param1);
//    if(event==MSG_BTN_UP)
//    {
//        switch (id)
//        {
//            case SubmitStatue_BACK:
//                NextUi = Get_LastWinType();
//                break;
//            default:
//                NextUi = WIN_SubmitStatue;
//                break;
//        }
//    }
    return NextUi;
}
#pragma GCC diagnostic pop
int Register_SubmitStatue()
{
    return Register_NewWin(WIN_SubmitStatue,HmiCreate_SubmitStatue,HmiPaint_SubmitStatue,HmiNotify_SubmitStatue);
}
