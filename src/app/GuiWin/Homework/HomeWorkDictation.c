//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "homeWork.h"
#include "board.h"

enum DictationId{
    Dictation_BACKGROUND, //背景
    Dictation_WIFI,       //wifi
    Dictation_TIME,       //时间
    Dictation_POWER,      //电量
    Dictation_BACK,      //返回
    Dictation_LOGO,      //开始练习
    Dictation_work_num,      //作业题计数
    Dictation_Jobtype,      //作业类型
    Dictation_Jobcontent,      //作业内容
    Dictation_answer,      //答案
    Dictation_play, //播放录音结果
    Dictation_NextPageOrComplete, //下一页
    Dictation_PreviousPage,//上一页
    DOHOMEWORK_MAXNUM,//总数量
};
//==================================config======================================

static const struct GUIINFO DictationCfgTab[DOHOMEWORK_MAXNUM] =
{
    [Dictation_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "Dictation",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [Dictation_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [Dictation_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [Dictation_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [Dictation_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },
    [Dictation_LOGO] = {
        .position = {0,24,240,24+30},
        .name = "开始作答",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Dictation_work_num] = {
        .position = {212,238-5,240-8,238+22-5},
        .name = "work_num",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Dictation_Jobtype] = {//作业类型
        .position = {8,62,8+64,62+30},
        .name = "basemap",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Dictation_Jobcontent] = {//作业内容
        .position = {8,100,240-8,170},
        .name = "basemap",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Dictation_answer] = {
        .position ={75,211,75+90,211+36},
        .name = "显示原文",
        .type = widget_type_button,
        .userParam = RGB(247,192,51),
    },
    [Dictation_play] = {
        .position = {8,260-5,8+224,260+42+20-5},
        .name = "原音",
        .type = widget_type_button,
        .userParam = BMP_PlayPauseBig_bmp,
    },
    [Dictation_NextPageOrComplete] = {
        .position = {174,62,174+58,62+30},
        .name = "下一题",
        .type = widget_type_button,
        .userParam =  RGB(247,192,51),
    },
    [Dictation_PreviousPage] = {
        .position = {112,62,112+58,62+30},
        .name = "上一题",
        .type = widget_type_button,
        .userParam =  RGB(216,216,216),
    },
};
struct TextCB{
    int SelectCnt;
    int TextTotalPage;
    int TextCurrentPage;
    int TextOffset;
    int TextOffsetBuf[10];
};
static struct TextCB TextDetialCB = {0,0,0,0,{0}};
void media_stop();
void media_play();

enum keycmd{
    Cmd_PlayAudio_down,       //播放原音
    Cmd_Re_PlayAudio,
};
enum DICTATION_STATE{
    DICTATION_NULL,  //空闲
    DICTATION_PlayAudio,//播放标准音频
    DICTATION_PlayAudio_stop,//播放标准音频
};
struct DictationCb{
   HWND Hwnd_Play;
   HWND Hwnd_complete;
   enum DICTATION_STATE state;
   bool_t Answer_flag;
};
static struct DictationCb DictationCB = {NULL,NULL,DICTATION_NULL,true};
//按钮控件创建函数
static bool_t  DictationButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,DictationCfgTab[Dictation_NextPageOrComplete].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            if(Get_SelectTopicNum()+1 == Get_HomeWorkTopicNUm(Get_SelectHmoeWork()))
                DrawText(hdc,"完成",-1,&rc,DT_VCENTER|DT_CENTER);
            else
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);

        }else if(0==strcmp(buttoninfo->name,DictationCfgTab[Dictation_PreviousPage].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }else if(0==strcmp(buttoninfo->name,DictationCfgTab[Dictation_answer].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,18,buttoninfo->userParam);
            if(DictationCB.Answer_flag==true){
                DrawText(hdc,"隐藏原文",-1,&rc,DT_VCENTER|DT_CENTER);
            }else{
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }
        }else if(0==strcmp(buttoninfo->name,DictationCfgTab[Dictation_play].name))
        {

            SetFillColor(hdc,RGB(255,255,255));
            FillRect(hdc,&rc);

            SetTextColor(hdc,RGB(178,178,178));
            rc.top+=42;
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            if(DictationCB.state == DICTATION_PlayAudio)
                bmp = Get_BmpBuf(BMP_PlayPauseBigStop_bmp);
            else
                bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                DrawBMP(hdc,0,0,bmp);
            }
        }
        else
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_HwlCWD(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;

    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, DictationButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<DOHOMEWORK_MAXNUM;i++)
    {

        switch (DictationCfgTab[i].type)
        {
            case  widget_type_button :
                {
                    hwndButton = CreateButton(DictationCfgTab[i].name, WS_CHILD | BS_NORMAL | WS_UNFILL,    //按钮风格
                         DictationCfgTab[i].position.left, DictationCfgTab[i].position.top,\
                         RectW(&DictationCfgTab[i].position),RectH(&DictationCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&DictationCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    if(hwndButton != NULL)
                    {
                        if(Dictation_play == i)
                            DictationCB.Hwnd_Play = hwndButton;
                        if(Dictation_NextPageOrComplete == i)
                            DictationCB.Hwnd_complete = hwndButton;
                    }
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_HwlCWD(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    struct TopicCB*topic;
    static char work_num_buf[16];
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<DOHOMEWORK_MAXNUM;i++)
        {
            switch (DictationCfgTab[i].type)
            {
                case  type_background :
                    SetFillColor(hdc,DictationCfgTab[i].userParam);
                    FillRect(hdc,&DictationCfgTab[i].position);
                    break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(DictationCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,DictationCfgTab[i].position.left,\
                                DictationCfgTab[i].position.top,&DictationCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&DictationCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
                    bmp = Get_BmpBuf(DictationCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    if(i==Dictation_LOGO)
                    {
                        DrawText(hdc,DictationCfgTab[i].name,-1,&DictationCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    }else if(i==Dictation_work_num)
                    {
                        SetTextColor(hdc,RGB(178,178,178));
                        sprintf(work_num_buf,"%d/%d",Get_SelectTopicNum()+1,Get_HomeWorkTopicNUm(Get_SelectHmoeWork()));
                        DrawText(hdc,work_num_buf,-1,&DictationCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    }
                    else if(i==Dictation_Jobtype)
                    {
                        SetTextColor(hdc,RGB(178,178,178));
                        topic =  Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                        if(topic != NULL)
                        {
                            struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                            struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                            DrawText(hdc,topic->type_name,-1,&DictationCfgTab[i].position,DT_LEFT|DT_VCENTER);
                            SetCharset(hdc,Charsetbak);
                        }
                    }
                    else if(i==Dictation_Jobcontent)
                    {
                        if(DictationCB.Answer_flag==true)
                        {
                            SetTextColor(hdc,RGB(255,188,0));
                            topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                            if(topic != NULL)
                            {
                                struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                                struct Charset* Charsetbak = SetCharset(hdc,myCharset);

                                char *txtbuf = malloc(1024);
                                if(txtbuf == NULL)
                                {
                                    printf("malloc error \n\r");
                                    return false;
                                }

                                if(TextDetialCB.SelectCnt != (Get_SelectTopicNum()+1)){
                                    TextDetialCB.SelectCnt = Get_SelectTopicNum()+1;
                                    TextDetialCB.TextTotalPage=Get_Utf8_Typesetting_Page(topic->text,224,5);
                                    //printf("TextDetialCB.TextTotalPage = %d\r\n",TextDetialCB.TextTotalPage);
                                    TextDetialCB.TextCurrentPage = 1;
                                    TextDetialCB.TextOffset = 0;
                                }
                                TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-1] = Utf8_Typesetting_3(topic->text,txtbuf,224,TextDetialCB.TextOffset,5);
                                //printf("+++++++ TextOffset = %d\r\n",TextDetialCB.TextOffset);
                                DrawText(hdc,txtbuf,-1,&DictationCfgTab[i].position,DT_LEFT|DT_TOP);
                                SetCharset(hdc,Charsetbak);
                                free(txtbuf);
                            }
                        }else{
                            SetFillColor(hdc,RGB(255,255,255));
                            FillRect(hdc,&DictationCfgTab[i].position);
                        }
                    }
                    break;
                default:    break;
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}
static int PlayMediaclloback()
{
    if( WIN_HWL_Course_Work_Dictation !=  Get_SelectionWinType())
        return 0;
    //printf("information DictationCB.state %d \n\r",DictationCB.state);
    switch (DictationCB.state) {
        case DICTATION_NULL:
        {
        }break;
        case DICTATION_PlayAudio:
        {
            DictationCB.state = DICTATION_NULL;
        }break;
        case DICTATION_PlayAudio_stop:
        {
            printf("error: PlayAudio stop clloback \r\n");
        }break;
        default:            break;
    }
    if(DictationCB.Hwnd_Play != NULL)
        PostMessage(DictationCB.Hwnd_Play,MSG_PAINT,0,0);
    return 0;
}
#if 0
#include "shell.h"
static bool_t debug(char *param)
{
    (void) param;
    int print_Wininfo();
    printf("information DictationCB.state %d \n\r",DictationCB.state);
    return true;
}

ADD_TO_ROUTINE_SHELL(debug,debug,"重新初始化 :COMMAND:init_jtag+enter");
#endif
static enum WinType Do_KeyCmd(struct TopicCB*topic,enum keycmd cmd)
{
    enum WinType NextUi = WIN_NotChange;
    //printf("information DictationCB.state%d  cmd %d \n\r",DictationCB.state,cmd);
    switch (DictationCB.state) {
        case DICTATION_NULL:
            switch (cmd) {
            case Cmd_PlayAudio_down:
                Homework_PlayAudio(topic,PlayMediaclloback);
                DictationCB.state = DICTATION_PlayAudio;
                break;
            case Cmd_Re_PlayAudio:
                Homework_PlayAudio(topic,PlayMediaclloback);
                DictationCB.state = DICTATION_PlayAudio;
                break;
            default:   break;
            }break;
        case DICTATION_PlayAudio:
            switch (cmd) {
            case Cmd_PlayAudio_down:
                media_stop();
                DictationCB.state = DICTATION_PlayAudio_stop;
                break;
            case Cmd_Re_PlayAudio:
                Homework_PlayAudioStop(topic);
                Homework_PlayAudio(topic,PlayMediaclloback);
                DictationCB.state = DICTATION_PlayAudio;
                break;
            default:   break;
            }break;
        case DICTATION_PlayAudio_stop:
            switch (cmd) {
            case Cmd_PlayAudio_down:
                media_play();
                DictationCB.state = DICTATION_PlayAudio;
                break;
            case Cmd_Re_PlayAudio:
                media_play();
                Homework_PlayAudio(topic,PlayMediaclloback);
                DictationCB.state = DICTATION_PlayAudio;
                break;
            default:   break;
            }break;
        default:    break;
    }

    if(DictationCB.Hwnd_Play != NULL)
        PostMessage(DictationCB.Hwnd_Play,MSG_PAINT,0,0);

    return NextUi;
}

//子控件发来的通知消息
static enum WinType HmiNotify_HWL_Course_Work_Dictation(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    s16 x,y;

    struct TopicCB*topic;
    struct TopicCB*Topic;
    struct HWorkCB* hwcb;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        //printf("x = %d  y = %d\r\n",x,y);
        if(DictationCB.Answer_flag == true)
        {
            if(abs(y) > abs(x))
            {
                if(y >0)
                {
                    //printf("yyyyy\r\n");
                    if(TextDetialCB.TextTotalPage > TextDetialCB.TextCurrentPage)
                    {
                        TextDetialCB.TextOffset+=TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-1];
                        TextDetialCB.TextCurrentPage ++;
                    }
                }else{
                    //printf("xxxxx\r\n");
                    if(TextDetialCB.TextCurrentPage > 1)
                    {
                        TextDetialCB.TextOffset-=TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-2];
                        TextDetialCB.TextCurrentPage --;
                    }else{
                        TextDetialCB.TextOffset = 0;
                    }
                }

                NextUi = WIN_HWL_Course_Work_Dictation;
            }
        }
    }else{
        topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
        if(event==MSG_BTN_UP)
        {
            switch (id)
            {
                case  Dictation_BACK        :
                    if(Is_TodayHomeWorkType()==false)
                        NextUi = WIN_HWL_Course_Work;
                    else
                        NextUi = WIN_THW_Course_Work;
                    break;
                case Dictation_answer      :
                    if(DictationCB.Answer_flag==true)
                        DictationCB.Answer_flag=false;
                    else
                        DictationCB.Answer_flag=true;
                    NextUi = WIN_HWL_Course_Work_Dictation;
                    break;
                case Dictation_play      :
                    NextUi = Do_KeyCmd(topic,Cmd_PlayAudio_down);
                    break;
                case Dictation_NextPageOrComplete    :
                    Topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum()+1);
                    if(Topic != NULL)
                    {
                        if(Set_SelectTopicNum(Get_SelectTopicNum()+1))
                            NextUi = topic2WinType(Topic);
                    }else
                    {
                        hwcb = Get_SelectHmoeWork();
                        if(hwcb->status != 1)
                        {
                            if(Is_TodayHomeWorkType()==false)
                                NextUi = WIN_HWL_Course_Work;
                            else
                                NextUi = WIN_THW_Course_Work;
                        }
                        else
                        {
                            if(Is_TodayHomeWorkType()==false)
                                NextUi = WIN_HWL_Course_Work;
                            else{
                                NextUi = WIN_HWL_Submit;
                                Clean_SubmitJobFlag();
                            }
                        }

    //                    NextUi = WIN_HWL_Submit;
                    }
                    break;
                case Dictation_PreviousPage:
                    if(Get_SelectTopicNum() > 0)
                    {
                        if(true == Set_SelectTopicNum(Get_SelectTopicNum()-1))
                        {
                            Topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                            NextUi = topic2WinType(Topic);
                        }
                    }
                    break;
                default:
                    NextUi = WIN_HWL_Course_Work_Dictation;
                    break;
            }
        }else if(event==MSG_KEY_CHANGE)
        {
            enum Keycmd cmd = (enum Keycmd)pMsg->Param2;
            switch (id) {
            case PAUSE_PLAY_KEY:
                if(cmd == enum_key_Short_press)
                {
                    NextUi = Do_KeyCmd(topic,Cmd_PlayAudio_down);
                }
                else if(cmd == enum_key_Long_press)
                {
                    NextUi = Do_KeyCmd(topic,Cmd_Re_PlayAudio);
                }
                break;
            case COMEBACK_KEY:
                if(cmd == enum_key_Short_press){}
                else if(cmd == enum_key_Long_press){}
                break;
            default:    break;
            }
        }
        if(NextUi != WIN_NotChange)
        {
            switch (DictationCB.state) {
                case DICTATION_NULL:  break;
                case DICTATION_PlayAudio:
                    Homework_PlayAudioStop(topic);
                    break;
                case DICTATION_PlayAudio_stop:
                    Homework_PlayAudioStop(topic);
                    media_play();
                    break;
                default:    break;
            }
            TextDetialCB.SelectCnt = 0;
            DictationCB.state = DICTATION_NULL;
            //printf("DictationCB.state :%d \n\r",DictationCB.state);
            media_play(); //todo: 容错
        }
    }
    return NextUi;
}
bool_t HWD_IsPlayOrRecord()
{
    bool_t ret = true;
    switch (DictationCB.state) {
        case DICTATION_NULL:
        case DICTATION_PlayAudio_stop:
            ret = false;
            break;
        default:    break;
    }
    return ret;
}

int Register_HWL_Course_Work_Dictation()
{
    return Register_NewWin(WIN_HWL_Course_Work_Dictation,HmiCreate_HwlCWD,HmiPaint_HwlCWD,HmiNotify_HWL_Course_Work_Dictation);
}



