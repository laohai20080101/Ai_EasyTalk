//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */
#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "../inc/ServerInfo.h"
#include "homeWork.h"


static bool_t __IS_PastHomeWork;
static struct classCB*__SelectClass;
static struct HWorkCB*__SelectHwork;
static int __SelectTopicNum;

bool_t Set_TodayHomeWorkType(bool_t type)
{
    __IS_PastHomeWork = type;
    return true;
}
bool_t Is_TodayHomeWorkType()
{
    return __IS_PastHomeWork;
}

bool_t Set_SelectClass(struct classCB*class)
{
    if(class == NULL)
        return false;
    if(NULL == Get_ClassOfId(class->classid))
        return false;
    __SelectClass = class;
    return true;
}

struct classCB*Get_SelectClass()
{
    struct classCB*tmp;

    if(NULL == Get_ClassOfId(__SelectClass->classid))
    {
        printf("SelectClass Error: file:%s , line:%d \n\r",__FILE__,__LINE__);
        tmp = Get_ClassOFCnt(0);
    }else
    {
        tmp = __SelectClass;
    }
    return tmp;
}

bool_t Set_SelectHmoeWork(struct HWorkCB*hwork)
{
    struct HWorkCB*phwork;
    bool_t ret = false;
    if(hwork == NULL)
        return false;
    if(Is_TodayHomeWorkType())
    {
        phwork = Get_TodayHomeWorkOfId(__SelectClass,hwork->workid);
        if(phwork)
        {
            __SelectHwork = phwork;
            ret = true;
        }
    }
    else
    {
        phwork = Get_PastHomeWorkOfId(__SelectClass,hwork->workid);
        if(phwork)
        {
            __SelectHwork = phwork;
            ret = true;
        }
    }

    return ret;
}
struct HWorkCB* Get_SelectHmoeWork()
{
    return __SelectHwork;
}

bool_t Set_SelectTopicNum(int num)
{
    if(NULL == Get_TopicOfCnt(__SelectHwork,num))
    {
        if(Is_TodayHomeWorkType())
            printf("TodayHomeWork:%s , homework :%s topic is NULL \n\r",__SelectClass->name,__SelectHwork->title);
        else
            printf("PastHomeWork:%s , homework :%s topic is NULL \n\r",__SelectClass->name,__SelectHwork->title);
        return false;
    }
    __SelectTopicNum = num;
    return true;
}

int Get_SelectTopicNum()
{
    return __SelectTopicNum;
}

enum WinType topic2WinType( struct TopicCB*topic)
{
    enum WinType NextUi = WIN_NotChange;
    if(topic == NULL)
        return NextUi;
    if((topic->type == 1)||(topic->type == 2)||(topic->type == 4))
    {
        NextUi = WIN_HWL_Course_Work_Recite;
    }else if(topic->type == 3)
    {
        NextUi = WIN_HWL_Course_Work_Dictation;
    }
    return NextUi;
}

enum WinType topic1WinType( struct TopicCB*topic)
{
    enum WinType NextUi = WIN_NotChange;
    if(topic == NULL)
        return NextUi;
    if((topic->type == 1)||(topic->type == 2)||(topic->type == 4))
    {
        NextUi = WIN_FHW_Course_Work_Recite;
    }else if(topic->type == 3)
    {
        NextUi = WIN_HWL_Course_Work_Dictation;
    }
    return NextUi;
}

#if 0
//=============================================================================
enum WinType Ui_CourseWork_show(u32 course,u32 work,u32 cnt,bool_t lists)
{
    enum WinType NextUi = WIN_NotChange;
    int error;
    struct TopicCB* pcontent;
    HomeWorkLists = lists;
    if(lists)
    {
        dowork = Get_HomeWorkListsCourseWorkOfcnt(course, work,&error);
        pcontent = Get_HomeWorkListsCnt(course , work, cnt,&error);
    }
    else
    {
        dowork = Get_TodayHomeWorkCourseWorkOfcnt(course, work,&error);

        pcontent = Get_TodayHomeWorkCnt(course , work, cnt,&error);
    }

    if(!error)
    {
        HomeWorkCB.course = course;
        HomeWorkCB.work   = work;
        HomeWorkCB.cnt    = cnt;
        HomeWorkCB.num    = dowork.num;
        HomeWorkCB.complete    = dowork.complete;

        switch (pcontent->type)
        {
            case 1:  //背诵
                HomeWorkCB.Wtype =   worktype_recite;
                break;
            case 2: //朗读
                HomeWorkCB.Wtype =   worktype_Readingaloud;
                break;
            case 3: //听力
                HomeWorkCB.Wtype =   worktype_dictation;
                break;
            case 4: //自定义
                HomeWorkCB.Wtype =   worktype_Custom;
                break;
            default:
                HomeWorkCB.Wtype =   worktype_error;//错误
                break;
        }

    }else
    {
        HomeWorkCB.Wtype = worktype_error;
    }
    switch (HomeWorkCB.Wtype)
    {
        case worktype_Readingaloud:     //朗读和背诵是一致的
        case worktype_recite:
        case worktype_Custom:
            NextUi = WIN_HWL_Course_Work_Recite;
            break;
        case worktype_dictation:
            NextUi = WIN_HWL_Course_Work_Dictation;
            break;
        default:
            NextUi = WIN_NotChange;
            break;
    }
    return NextUi;
}
#endif

