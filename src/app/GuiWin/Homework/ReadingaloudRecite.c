//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "homeWork.h"
#include "board.h"
//extern fun
void media_stop();
void media_play();
//

extern struct WinTimer *tg_pPowerDownTimer;

static enum RECITE_STATE  NullState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  NullState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  NullState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  NullState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);

static enum RECITE_STATE  RecordState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  RecordState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  RecordState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  RecordState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);


static enum RECITE_STATE  PlayRecordState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecordState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecordState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecordState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);


static enum RECITE_STATE  PlayRecord_stopState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);

static enum RECITE_STATE  PlayAudioState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudioState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudioState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudioState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);

static enum RECITE_STATE  PlayAudio_stopState_DoCmd_PlayRecord_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_record_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_PlayAudio_down(struct TopicCB*Topic);
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_Re_PlayAudio(struct TopicCB*Topic);

enum Do_HomeWorkId{
    Do_HomeWork_BACKGROUND, //背景
    Do_HomeWork_WIFI,       //wifi
    Do_HomeWork_TIME,       //时间
    Do_HomeWork_POWER,      //电量
    Do_HomeWork_BACK,      //返回
    Do_HomeWork_LOGO,      //开始练习
    Do_HomeWork_work_num,      //作业题计数
    Do_HomeWork_Jobtype,      //作业类型
    Do_HomeWork_Jobcontent,      //作业内容
    Do_HomeWork_answer,      //答案
    Do_HomeWork_PlayRecord,      //按键喇叭
    Do_HomeWork_record,      //开始录音
    Do_HomeWork_PlayAudio, //播放录音结果
    Do_HomeWork_NextPageOrComplete, //下一页
    Do_HomeWork_PreviousPage,//上一页

    DOHOMEWORK_MAXNUM,//总数量
};
//==================================config======================================

static const struct GUIINFO Do_HomeWorkCfgTab[DOHOMEWORK_MAXNUM] =
{
    [Do_HomeWork_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "Do_HomeWork",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [Do_HomeWork_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [Do_HomeWork_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [Do_HomeWork_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [Do_HomeWork_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },
    [Do_HomeWork_LOGO] = {
        .position = {0,24,240,24+30},
        .name = "开始作答  ",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Do_HomeWork_work_num] = {
        .position = {212,238-5,240-8,238+22-5},
        .name = "work_num",
        .type = widget_type_text,
        .userParam = 0,
    },

    [Do_HomeWork_Jobtype] = {//作业类型
        .position = {8,62,8+64,62+30},
        .name = "basemap",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Do_HomeWork_Jobcontent] = {//作业内容
        .position = {8,100,240-8,210},
        .name = "basemap",
        .type = widget_type_text,
        .userParam = 0,
    },
    [Do_HomeWork_answer] = {
        .position ={75,211,75+90,211+36},
        .name = "显示原文",
        .type = widget_type_button,
        .userParam = RGB(247,192,51),
    },
    [Do_HomeWork_PlayAudio] = {
        .position ={174,260-5,174+58,260+42+20-5},
        .name = "原音",
        .type = widget_type_button,
        .userParam = BMP_Volume_bmp,
    },
    [Do_HomeWork_PlayRecord] = {
        .position = {8,260-5,8+58,260+42+20-5},
        .name = "播放",
        .type = widget_type_button,
        .userParam = BMP_PlayPause_bmp,
    },
    [Do_HomeWork_record] = {
        .position =  {74,260-5,74+92,260+42+20-5},
        .name = "录音",
        .type = widget_type_button,
        .userParam = BMP_Record_bmp,
    },
    [Do_HomeWork_NextPageOrComplete] = {
        .position = {174,62,174+58,62+30},
        .name = "下一题",
        .type = widget_type_button,
        .userParam =  RGB(247,192,51),
    },
    [Do_HomeWork_PreviousPage] = {
        .position = {112,62,112+58,62+30},
        .name = "上一题",
        .type = widget_type_button,
        .userParam =  RGB(216,216,216),
    },
};
enum RECITE_STATE{
   ReadingaloudRecite_NULL,  //空闲
   ReadingaloudRecite_record,//录音
   ReadingaloudRecite_PlayRecord,//播放录音
   ReadingaloudRecite_PlayRecord_stop,//播放录音暂停
   ReadingaloudRecite_PlayAudio,//播放标准音频
   ReadingaloudRecite_PlayAudio_stop,//播放标准音频暂停
   ReadingaloudRecite_max,
};
struct ReciteCB{
    HWND Hwnd_record;
    HWND Hwnd_volume;
    HWND Hwnd_play_pause;
    bool_t Answer_flag;
    enum RECITE_STATE Recite_state;
};
struct TextCB{
    int SelectCnt;
    int TextTotalPage;
    int TextCurrentPage;
    int TextOffset;
    int TextOffsetBuf[10];
};
static struct ReciteCB RReciteCB = {NULL,NULL,NULL,true,ReadingaloudRecite_NULL};
static struct TextCB TextDetialCB = {0,0,0,0,{0}};
enum SwitchCmd{
    SwitchCmd_PlayRecord_down,      //播放录音
    SwitchCmd_record_down,          //录音
    SwitchCmd_PlayAudio_down,       //播放原音
    SwitchCmd_Re_PlayAudio,
    SwitchCmd_max,
};
typedef enum RECITE_STATE (*StateSwitchCmd)(struct TopicCB*Topic);
static const StateSwitchCmd Statefuntab[ReadingaloudRecite_max][SwitchCmd_max] = {
    [ReadingaloudRecite_NULL] = {
        [SwitchCmd_PlayRecord_down] = NullState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = NullState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = NullState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = NullState_DoCmd_Re_PlayAudio,
    },
    [ReadingaloudRecite_record] = {
        [SwitchCmd_PlayRecord_down] = RecordState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = RecordState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = RecordState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = RecordState_DoCmd_Re_PlayAudio,
    },
    [ReadingaloudRecite_PlayRecord] = {
        [SwitchCmd_PlayRecord_down] = PlayRecordState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = PlayRecordState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = PlayRecordState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = PlayRecordState_DoCmd_Re_PlayAudio,
    },
    [ReadingaloudRecite_PlayRecord_stop] = {
        [SwitchCmd_PlayRecord_down] = PlayRecord_stopState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = PlayRecord_stopState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = PlayRecord_stopState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = PlayRecord_stopState_DoCmd_Re_PlayAudio,
    },
    [ReadingaloudRecite_PlayAudio] = {
        [SwitchCmd_PlayRecord_down] = PlayAudioState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = PlayAudioState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = PlayAudioState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = PlayAudioState_DoCmd_Re_PlayAudio,
    },
    [ReadingaloudRecite_PlayAudio_stop] = {
        [SwitchCmd_PlayRecord_down] = PlayAudio_stopState_DoCmd_PlayRecord_down,
        [SwitchCmd_record_down]     = PlayAudio_stopState_DoCmd_record_down,
        [SwitchCmd_PlayAudio_down]  = PlayAudio_stopState_DoCmd_PlayAudio_down,
        [SwitchCmd_Re_PlayAudio]    = PlayAudio_stopState_DoCmd_Re_PlayAudio,
    },
};

static int PlayMediaclloback()
{
    printf("info: Play Media end %d \n\r",RReciteCB.Recite_state);
    if( WIN_HWL_Course_Work_Recite !=  Get_SelectionWinType())
        return 0;

    switch (RReciteCB.Recite_state) {
    case ReadingaloudRecite_PlayAudio:
    case ReadingaloudRecite_PlayRecord_stop:
    case ReadingaloudRecite_NULL:
        RReciteCB.Recite_state = ReadingaloudRecite_NULL;
        if(RReciteCB.Hwnd_volume != NULL)
            PostMessage(RReciteCB.Hwnd_volume,MSG_PAINT,0,0);
        if(RReciteCB.Hwnd_record != NULL)
            PostMessage(RReciteCB.Hwnd_record,MSG_PAINT,0,0);
        if(RReciteCB.Hwnd_play_pause != NULL)
            PostMessage(RReciteCB.Hwnd_play_pause,MSG_PAINT,0,0);
        break;
        default:   break;
    }
    return 0;
}
int Recordclloback (int state)
{
    printf("info: Recordclloback  %d \n\r",state);

    if(state == 0)
        return -1;

    printf("info: Record error  %d \n\r",RReciteCB.Recite_state);
    if(WIN_HWL_Course_Work_Recite !=  Get_SelectionWinType())
        return 0;
    switch (RReciteCB.Recite_state) {
        case ReadingaloudRecite_PlayRecord_stop:
            media_play();
        case ReadingaloudRecite_record:
        case ReadingaloudRecite_NULL:
            RReciteCB.Recite_state = ReadingaloudRecite_NULL;
            if(RReciteCB.Hwnd_volume != NULL)
                PostMessage(RReciteCB.Hwnd_volume,MSG_PAINT,0,0);
            if(RReciteCB.Hwnd_record != NULL)
                PostMessage(RReciteCB.Hwnd_record,MSG_PAINT,0,0);
            if(RReciteCB.Hwnd_play_pause != NULL)
                PostMessage(RReciteCB.Hwnd_play_pause,MSG_PAINT,0,0);
            break;
        default:
            break;
    }
    Record_SubmitHomeFail();
    return 0;

}



static int listenRecordclloback()
{
    printf("info:listen Record end %d \n\r",RReciteCB.Recite_state);
    if(WIN_HWL_Course_Work_Recite !=  Get_SelectionWinType())
        return 0;
    switch (RReciteCB.Recite_state) {
        case ReadingaloudRecite_PlayRecord_stop:
            media_play();
        case ReadingaloudRecite_PlayRecord:
        case ReadingaloudRecite_NULL:
            RReciteCB.Recite_state = ReadingaloudRecite_NULL;
            if(RReciteCB.Hwnd_volume != NULL)
                PostMessage(RReciteCB.Hwnd_volume,MSG_PAINT,0,0);
            if(RReciteCB.Hwnd_record != NULL)
                PostMessage(RReciteCB.Hwnd_record,MSG_PAINT,0,0);
            if(RReciteCB.Hwnd_play_pause != NULL)
                PostMessage(RReciteCB.Hwnd_play_pause,MSG_PAINT,0,0);
            break;
        default:
            break;
    }
    return 0;
}
static enum RECITE_STATE  NullState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    Homework_listenRecording(Topic,listenRecordclloback);
    return ReadingaloudRecite_PlayRecord;
}
static enum RECITE_STATE  NullState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_StartRecording(Topic);
    return ReadingaloudRecite_record;
}
static enum RECITE_STATE  NullState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}
static enum RECITE_STATE  NullState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}

static enum RECITE_STATE  RecordState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    Homework_StopRecording(Topic);
    Homework_listenRecording(Topic,listenRecordclloback);
    return ReadingaloudRecite_PlayRecord;
}
static enum RECITE_STATE  RecordState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_StopRecording(Topic);
    return ReadingaloudRecite_NULL;
}
static enum RECITE_STATE  RecordState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    Homework_StopRecording(Topic);
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}
static enum RECITE_STATE  RecordState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_StopRecording(Topic);
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}


static enum RECITE_STATE  PlayRecordState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    (void)Topic;
    media_stop();
    return ReadingaloudRecite_PlayRecord_stop;
}
static enum RECITE_STATE  PlayRecordState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    Homework_StartRecording(Topic);
    return ReadingaloudRecite_record;
}
static enum RECITE_STATE  PlayRecordState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}
static enum RECITE_STATE  PlayRecordState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}


static enum RECITE_STATE  PlayRecord_stopState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    (void)Topic;
    media_play();
    return ReadingaloudRecite_PlayRecord;
}
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    media_play();
    Homework_StartRecording(Topic);
    return ReadingaloudRecite_record;
}
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    media_play();
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}
static enum RECITE_STATE  PlayRecord_stopState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_listenRecordingStop(Topic);
    media_play();
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}

static enum RECITE_STATE  PlayAudioState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    Homework_PlayAudioStop(Topic);
    Homework_listenRecording(Topic,listenRecordclloback);
    return ReadingaloudRecite_PlayRecord;
}
static enum RECITE_STATE  PlayAudioState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_PlayAudioStop(Topic);
    Homework_StartRecording(Topic);
    return ReadingaloudRecite_record;
}
static enum RECITE_STATE  PlayAudioState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    (void)Topic;
    media_stop();
    return ReadingaloudRecite_PlayAudio_stop;
}
static enum RECITE_STATE  PlayAudioState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}

static enum RECITE_STATE  PlayAudio_stopState_DoCmd_PlayRecord_down(struct TopicCB*Topic)
{
    Homework_PlayAudioStop(Topic);
    media_play();
    Homework_listenRecording(Topic,listenRecordclloback);
    return ReadingaloudRecite_PlayRecord;
}
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_record_down(struct TopicCB*Topic)
{
    Homework_PlayAudioStop(Topic);
    media_play();
    Homework_StartRecording(Topic);
    return ReadingaloudRecite_record;
}
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_PlayAudio_down(struct TopicCB*Topic)
{
    (void)Topic;
    media_play();
    return ReadingaloudRecite_PlayAudio;
}
static enum RECITE_STATE  PlayAudio_stopState_DoCmd_Re_PlayAudio(struct TopicCB*Topic)
{
    Homework_PlayAudioStop(Topic);
    media_play();
    Homework_PlayAudio(Topic,PlayMediaclloback);
    return ReadingaloudRecite_PlayAudio;
}

//int GetRecordPlayStatue()
//{
//    int ret;
//
//    struct classCB* class = Get_SelectClass();
//    struct HWorkCB* homework = Get_SelectHmoeWork();
//    struct TopicCB* Topic = Get_TopicOfCnt(homework,Get_SelectTopicNum());
//    char buf[100];
//    char snbuf[50];
//
//    Get_DeviceSn(snbuf,(int)sizeof(snbuf));
//    sprintf(buf,"/devtemp/%s/tmp_%d_%d_%d.wav",snbuf\
//            ,class->classid,homework->workid,Topic->id);
//
//    ret = OssGetFileInfo("bucket-kouyutong", buf, 5000);
//
//    return ret;
//}

//按钮控件创建函数
static bool_t  Do_HomeWorkButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;

    const char * bmp;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_NextPageOrComplete].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            if(Get_SelectTopicNum()+1==Get_HomeWorkTopicNUm(Get_SelectHmoeWork()))
                DrawText(hdc,"完成",-1,&rc,DT_VCENTER|DT_CENTER);
            else
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }
        else if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_PreviousPage].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }
        else if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_answer].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,18,buttoninfo->userParam);
            if(RReciteCB.Answer_flag==true){
                DrawText(hdc,"隐藏原文",-1,&rc,DT_VCENTER|DT_CENTER);
            }else{
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }
        }
        else if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_record].name))
        {
            SetFillColor(hdc,RGB(255,255,255));
            FillRect(hdc,&rc);

            SetTextColor(hdc,RGB(178,178,178));
            rc.top+=42;
//            struct HWorkCB*hwork = Get_SelectHmoeWork();
//            if(hwork!=NULL)
//            {
//                if(hwork->status != 1){
//                    bmp = Get_BmpBuf(BMP_Record_2_bmp);
//                }else{
                    if(RReciteCB.Recite_state==ReadingaloudRecite_record){
                        bmp = Get_BmpBuf(BMP_RecordDown_bmp);
                        DrawText(hdc,"完成",-1,&rc,DT_VCENTER|DT_CENTER);
                    }else{
                        bmp = Get_BmpBuf(BMP_Record_bmp);
                        DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
                    }
//                }
//            }
            if(bmp != NULL)
            {
                DrawBMP(hdc,0,0,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_PlayRecord].name))
        {
            SetFillColor(hdc,RGB(255,255,255));
            FillRect(hdc,&rc);
            SetTextColor(hdc,RGB(178,178,178));
            rc.top+=42;

//            ret =GetRecordPlayStatue();
            if(1)
            {
                if(RReciteCB.Recite_state==ReadingaloudRecite_PlayRecord){
                    bmp = Get_BmpBuf(BMP_VolumeStop_bmp);
                    DrawText(hdc,"暂停",-1,&rc,DT_VCENTER|DT_CENTER);
                }else{
                    DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
                    bmp = Get_BmpBuf(buttoninfo->userParam);
                }
            }else{
                    bmp = Get_BmpBuf(BMP_PlayPause_2_bmp);
                    DrawText(hdc,"无播放",-1,&rc,DT_VCENTER|DT_CENTER);
            }
            if(bmp != NULL)
            {
                DrawBMP(hdc,0,0,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,Do_HomeWorkCfgTab[Do_HomeWork_PlayAudio].name))
        {
            SetFillColor(hdc,RGB(255,255,255));
            FillRect(hdc,&rc);
            SetTextColor(hdc,RGB(178,178,178));
            rc.top+=42;
            if(RReciteCB.Recite_state==ReadingaloudRecite_PlayAudio){
                bmp = Get_BmpBuf(BMP_PlayPauseStop_bmp);
                DrawText(hdc,"暂停",-1,&rc,DT_VCENTER|DT_CENTER);
            }else{
                DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
                bmp = Get_BmpBuf(buttoninfo->userParam);
            }
            if(bmp != NULL)
            {
                DrawBMP(hdc,0,0,bmp);
            }
        }
        else
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_HwlCWR(struct WindowMsg *pMsg)
{
    RECT rc0;

    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, Do_HomeWorkButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;
    GetClientRect(hwnd,&rc0);
    for(int i=0;i<DOHOMEWORK_MAXNUM;i++)
    {
        switch (Do_HomeWorkCfgTab[i].type)
        {
            case  widget_type_button :
                {
                    hwndButton = CreateButton(Do_HomeWorkCfgTab[i].name, WS_CHILD | BS_NORMAL | WS_UNFILL,    //按钮风格
                         Do_HomeWorkCfgTab[i].position.left, Do_HomeWorkCfgTab[i].position.top,\
                         RectW(&Do_HomeWorkCfgTab[i].position),RectH(&Do_HomeWorkCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&Do_HomeWorkCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    if(hwndButton != NULL)
                    {
                        if(Do_HomeWork_record == i)
                            RReciteCB.Hwnd_record = hwndButton;
                        else if(Do_HomeWork_PlayRecord == i)
                            RReciteCB.Hwnd_volume = hwndButton;
                        else if(Do_HomeWork_PlayAudio == i)
                            RReciteCB.Hwnd_play_pause = hwndButton;
                    }
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_HwlCWR(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    struct TopicCB* topic;
    char* time;
    static char work_num_buf[16];
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<DOHOMEWORK_MAXNUM;i++)
        {
            switch (Do_HomeWorkCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,Do_HomeWorkCfgTab[i].userParam);
                        FillRect(hdc,&Do_HomeWorkCfgTab[i].position);
                        break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(Do_HomeWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,Do_HomeWorkCfgTab[i].position.left,\
                                Do_HomeWorkCfgTab[i].position.top,&Do_HomeWorkCfgTab[i].position,bmp);
                    }
                    break;

                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&Do_HomeWorkCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :
                    bmp = Get_BmpBuf(Do_HomeWorkCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_text :
                    if(i==Do_HomeWork_LOGO){
                        struct HWorkCB*hwork = Get_SelectHmoeWork();
                        if(hwork!=NULL)
                        {
                            if(hwork->status != 1){
                                DrawText(hdc,"已完成的作业",-1,&Do_HomeWorkCfgTab[i].position,DT_CENTER|DT_VCENTER);
                            }else{
                                DrawText(hdc,Do_HomeWorkCfgTab[i].name,-1,&Do_HomeWorkCfgTab[i].position,DT_CENTER|DT_VCENTER);
                            }
                        }
                    }else if(i==Do_HomeWork_work_num){
                        SetTextColor(hdc,RGB(178,178,178));
                        sprintf(work_num_buf,"%d/%d",Get_SelectTopicNum()+1,Get_HomeWorkTopicNUm(Get_SelectHmoeWork()));
                        DrawText(hdc,work_num_buf,-1,&Do_HomeWorkCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                    }
                    else if(i==Do_HomeWork_Jobtype)
                    {
                        SetTextColor(hdc,RGB(178,178,178));
                        topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                        if(topic != NULL)
                        {
                            struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                            struct Charset* Charsetbak = SetCharset(hdc,myCharset);
                            DrawText(hdc,topic->type_name,-1,&Do_HomeWorkCfgTab[i].position,DT_LEFT|DT_VCENTER);
                            SetCharset(hdc,Charsetbak);
                        }
                    }
                    else if(i==Do_HomeWork_Jobcontent)
                    {
                        if(RReciteCB.Answer_flag==true)
                        {
                            SetTextColor(hdc,RGB(255,188,0));
                            topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                            if(topic != NULL)
                            {
                                struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                                struct Charset* Charsetbak = SetCharset(hdc,myCharset);

                                char *txtbuf = malloc(1024);
                                if(txtbuf == NULL)
                                {
                                    printf("malloc error \n\r");
                                    return false;
                                }

                                if(TextDetialCB.SelectCnt != (Get_SelectTopicNum()+1)){
                                    TextDetialCB.SelectCnt = Get_SelectTopicNum()+1;
                                    TextDetialCB.TextTotalPage=Get_Utf8_Typesetting_Page(topic->text,224,5);
//                                    printf("TextDetialCB.TextTotalPage = %d\r\n",TextDetialCB.TextTotalPage);
                                    TextDetialCB.TextCurrentPage = 1;
                                    TextDetialCB.TextOffset = 0;
                                }
                                TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-1] = Utf8_Typesetting_3(topic->text,txtbuf,224,TextDetialCB.TextOffset,5);
//                                printf("+++++++ TextOffset = %d\r\n",TextDetialCB.TextOffset);
                                DrawText(hdc,txtbuf,-1,&Do_HomeWorkCfgTab[i].position,DT_LEFT|DT_TOP);
                                SetCharset(hdc,Charsetbak);
                                free(txtbuf);
                            }
                        }else{
                            SetFillColor(hdc,RGB(255,255,255));
                            FillRect(hdc,&Do_HomeWorkCfgTab[i].position);
                        }
                    }

                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType ReciteStateSwitch(struct TopicCB*Topic,enum SwitchCmd cmd)
{
    enum WinType NextUi = WIN_NotChange;
    enum RECITE_STATE State = ReadingaloudRecite_NULL;

    if((RReciteCB.Recite_state<ReadingaloudRecite_max)&&(cmd<SwitchCmd_max))
    {
        State  = Statefuntab[RReciteCB.Recite_state][cmd](Topic);
    }else
    {
        printf("error : State  RRecite \n\r");
    }
    RReciteCB.Recite_state = State;

    if(RReciteCB.Hwnd_volume != NULL)
        PostMessage(RReciteCB.Hwnd_volume,MSG_PAINT,0,0);
    if(RReciteCB.Hwnd_record != NULL)
        PostMessage(RReciteCB.Hwnd_record,MSG_PAINT,0,0);
    if(RReciteCB.Hwnd_play_pause != NULL)
        PostMessage(RReciteCB.Hwnd_play_pause,MSG_PAINT,0,0);
    return NextUi;
}


int Ui_StopRecord()
{

    if(RReciteCB.Recite_state == ReadingaloudRecite_record)
    {
        RReciteCB.Recite_state = ReadingaloudRecite_NULL;
        if(RReciteCB.Hwnd_volume != NULL)
            PostMessage(RReciteCB.Hwnd_volume,MSG_PAINT,0,0);
        if(RReciteCB.Hwnd_record != NULL)
            PostMessage(RReciteCB.Hwnd_record,MSG_PAINT,0,0);
        if(RReciteCB.Hwnd_play_pause != NULL)
            PostMessage(RReciteCB.Hwnd_play_pause,MSG_PAINT,0,0);
    }
    return 0;
}


//==============================================================================
static  enum WinType Do_KeyChangeMsg(struct WindowMsg *pMsg,struct TopicCB*Topic)
{
    enum WinType NextUi = WIN_NotChange;
    u16 id =LO16(pMsg->Param1);
    enum Keycmd cmd = (enum Keycmd)pMsg->Param2;
    switch (id) {
    case PAUSE_PLAY_KEY:
        if(cmd == enum_key_Short_press)
        {
            printf("info:PAUSE_PLAY_KEY enum_key_Short_press \r\n");
            ReciteStateSwitch(Topic,SwitchCmd_PlayAudio_down);//原音的播放暂停
        }
        else if(cmd == enum_key_Long_press){
            printf("info:PAUSE_PLAY_KEY enum_key_Long_press \r\n");
            ReciteStateSwitch(Topic,SwitchCmd_Re_PlayAudio);//重新播放
        }
        break;
    case COMEBACK_KEY:
        if(cmd == enum_key_Short_press){
            printf("info:COMEBACK_KEY enum_key_Short_press \r\n");
            if(MediaIsUploading() && RReciteCB.Recite_state==ReadingaloudRecite_NULL){
                NextUi = WIN_Uploading;
            }else{
//                struct HWorkCB* phwork = Get_SelectHmoeWork();
//                if(phwork->status == 1){
                    GDD_StopTimer(tg_pPowerDownTimer);
                    NextUi = ReciteStateSwitch(Topic,SwitchCmd_record_down);
//                }
            }
        }
        else if(cmd == enum_key_Long_press){
            printf("info:COMEBACK_KEY enum_key_Long_press \r\n");
            ReciteStateSwitch(Topic,SwitchCmd_PlayRecord_down);//播放自己的录音
        }
        break;
    default:    break;
    }

    return NextUi;
}


static  enum WinType Do_BtnUpMsg(struct WindowMsg *pMsg,struct TopicCB*Topic,int *endflag)
{
    enum WinType NextUi = WIN_NotChange;
    u16 id =LO16(pMsg->Param1);
    struct HWorkCB*  phwork;
    struct TopicCB*topic;
    switch (id)
    {
        case  Do_HomeWork_BACK        :
            RReciteCB.Answer_flag = true;
            if(Is_TodayHomeWorkType()==false)
                NextUi = WIN_HWL_Course_Work;
            else
                NextUi = WIN_THW_Course_Work;
            break;
        case Do_HomeWork_answer      :
            if(RReciteCB.Answer_flag==true)
                RReciteCB.Answer_flag=false;
            else
                RReciteCB.Answer_flag=true;
            NextUi = WIN_HWL_Course_Work_Recite;
            *endflag = 1;
            break;
        case Do_HomeWork_NextPageOrComplete    :
            RReciteCB.Answer_flag = true;
            phwork = Get_SelectHmoeWork();
            Topic = Get_TopicOfCnt(phwork,Get_SelectTopicNum()+1);
            if(Topic != NULL)
            {
                if(Set_SelectTopicNum(Get_SelectTopicNum()+1))
                NextUi = topic2WinType(Topic);
            }else{
//                if(phwork->status != 1){
//                    if(Is_TodayHomeWorkType()==false)
//                        NextUi = WIN_HWL_Course_Work;
//                    else
//                        NextUi = WIN_THW_Course_Work;
//                }
//                else{
                    NextUi = WIN_HWL_Submit;
                    Clean_SubmitJobFlag();
//                }
            }
            break;
        case Do_HomeWork_PreviousPage:
            RReciteCB.Answer_flag = true;
            topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
            if(Get_SelectTopicNum() > 0)
            {
                if(true == Set_SelectTopicNum(Get_SelectTopicNum()-1))
                    NextUi = topic2WinType(topic);
            }
            break;
        case Do_HomeWork_PlayRecord :
            NextUi = ReciteStateSwitch(Topic,SwitchCmd_PlayRecord_down);
            break;
        case Do_HomeWork_record     :
            if(MediaIsUploading() && RReciteCB.Recite_state==ReadingaloudRecite_NULL){
                NextUi = WIN_Uploading;
            }else{
//                phwork = Get_SelectHmoeWork();
//                if(phwork->status == 1){
                    GDD_StopTimer(tg_pPowerDownTimer);
                    NextUi = ReciteStateSwitch(Topic,SwitchCmd_record_down);
//                }
            }
            break;
        case Do_HomeWork_PlayAudio  :
            NextUi = ReciteStateSwitch(Topic,SwitchCmd_PlayAudio_down);
            break;
        default:  break;
    }
    return NextUi;
}

//子控件发来的通知消息
static enum WinType HmiNotify_HWL_Course_Work_Recite(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    int endflag = 0;
    s16 x,y;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        //printf("x = %d  y = %d\r\n",x,y);
        if(RReciteCB.Answer_flag == true)
        {
            if(abs(y) > abs(x))
            {
                if(y >0)
                {
                    //printf("yyyyy\r\n");
                    if(TextDetialCB.TextTotalPage > TextDetialCB.TextCurrentPage)
                    {
                        TextDetialCB.TextOffset+=TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-1];
                        TextDetialCB.TextCurrentPage ++;
                    }
                }else{
                    //printf("xxxxx\r\n");
                    if(TextDetialCB.TextCurrentPage > 1)
                    {
                        TextDetialCB.TextOffset-=TextDetialCB.TextOffsetBuf[TextDetialCB.TextCurrentPage-2];
                        TextDetialCB.TextCurrentPage --;
                    }else{
                        TextDetialCB.TextOffset = 0;
                    }
                }

                NextUi = WIN_HWL_Course_Work_Recite;
            }
        }
    }else{
        struct TopicCB*Topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());

        if(event==MSG_BTN_UP){
            NextUi = Do_BtnUpMsg(pMsg,Topic,&endflag);
        }else if(event==MSG_KEY_CHANGE){
            NextUi = Do_KeyChangeMsg(pMsg,Topic);
        }

        if(((NextUi!= WIN_NotChange)&&(NextUi!=WIN_HWL_Course_Work_Recite)&&(endflag == 0))
                ||(((id == Do_HomeWork_NextPageOrComplete )||(id == Do_HomeWork_PreviousPage))&&(event == MSG_BTN_UP)))
        {
            switch (RReciteCB.Recite_state)
            {
                case ReadingaloudRecite_NULL:
                    break;
                case ReadingaloudRecite_record:
                    Homework_StopRecording(Topic);
                    break;
                case ReadingaloudRecite_PlayRecord:
                    Homework_listenRecordingStop(Topic);
                    break;
                case ReadingaloudRecite_PlayRecord_stop:
                    Homework_listenRecordingStop(Topic);
                    media_play();
                    break;
                case ReadingaloudRecite_PlayAudio:
                    Homework_PlayAudioStop(Topic);
                    break;
                case ReadingaloudRecite_PlayAudio_stop:
                    Homework_PlayAudioStop(Topic);
                    media_play();
                    break;
                default:             break;
            }
            TextDetialCB.SelectCnt = 0;
            RReciteCB.Recite_state = ReadingaloudRecite_NULL;
            //============容错======
            media_play();
            //=======================
        }
    }
    return NextUi;
}


bool_t CWR_IsPlayOrRecord()
{
    switch (RReciteCB.Recite_state)
    {
        case ReadingaloudRecite_NULL:
        case ReadingaloudRecite_PlayRecord_stop:
        case ReadingaloudRecite_PlayAudio_stop:
            return false;
        default:             break;
    }
    return true;
}


int Register_HWL_Course_Work_Recite()
{
    return Register_NewWin(WIN_HWL_Course_Work_Recite,HmiCreate_HwlCWR,HmiPaint_HwlCWR,HmiNotify_HWL_Course_Work_Recite);
}



