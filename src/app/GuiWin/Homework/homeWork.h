/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef _HOME_WORK_H_
#define _HOME_WORK_H_
#include "stddef.h"
#include "../inc/ServerInfo.h"
#ifdef __cplusplus
extern "C" {
#endif

enum HomeWorktype {
    worktype_dictation,//听写
    worktype_recite,//背诵
    worktype_Readingaloud,//朗读
    worktype_Custom,//自定义
    worktype_error,//其他
};

struct HomeWorkCB{
    u32 course; //课程编号
    u32 work;   //作业编号
    u32 cnt;    //作业小题号
    u32 num;    //作业总数
    int complete;//提交状态
    enum HomeWorktype Wtype;
};

extern struct HomeWorkCB HomeWorkCB;


enum SubmitHomeWorkReturn{
    return_Redo,
    return_Back,
    return_Submit,
    return_error,
};

bool_t Set_TodayHomeWorkType(bool_t type);
bool_t Is_TodayHomeWorkType();
bool_t Set_SelectClass(struct classCB*class);
struct classCB*Get_SelectClass();
bool_t Set_SelectHmoeWork(struct HWorkCB*hwork);
struct HWorkCB* Get_SelectHmoeWork();
bool_t Set_SelectTopicNum(int num);
int Get_SelectTopicNum();
bool_t  IS_TodayHomeWorkType();
enum WinType topic2WinType( struct TopicCB*topic);
enum WinType topic1WinType( struct TopicCB*topic);




bool_t Set_SubmitFlag(bool_t falg);
bool_t Clean_SubmitJobFlag();
int Get_SubmitFlag();

bool_t Get_HomeWorkListsFlag();
enum WinType Ui_CourseWork_show(u32 course,u32 work,u32 cnt,bool_t lists);
#ifdef __cplusplus
}
#endif

#endif /* _HOME_WORK_H_ */
