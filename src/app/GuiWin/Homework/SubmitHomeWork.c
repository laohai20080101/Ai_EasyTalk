//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "homeWork.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "string.h"

enum SubmitJobId{
    SubmitJob_BACKGROUND, //背景
    SubmitJob_WIFI,       //wifi
    SubmitJob_TIME,       //时间
    SubmitJob_POWER,      //电量
    SubmitJob_BACK,      //返回
    SubmitJob_LOGO,      //提交作业logo
    SubmitJob_Submit,
    SubmitJob_Redo,
//    SubmitJob_flag,     //提交错误显示提示
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const struct GUIINFO SubmitJobCfgTab[ENUM_MAXNUM] =
{
    [SubmitJob_BACKGROUND] = {
        .position = {0,0,240,54},
        .name = "SubmitJob",
        .type = type_background,
        .userParam = RGB(247,192,51),
    },
    [SubmitJob_WIFI] = {
        .position = {4,3,15+4,15+3},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },

    [SubmitJob_TIME] = {
        .position = {105,4,105+30,17+4},
        .name = "time",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [SubmitJob_POWER] = {
        .position = {183,4,210,17+4},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [SubmitJob_BACK] = {
        .position ={0,18,44,62},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_ArrowLeft_bmp,
    },
    [SubmitJob_LOGO] = {
        .position = {0,24,240,24+30},
        .name = "提交作业",
        .type = widget_type_job,
        .userParam = 0,
    },

    [SubmitJob_Submit] = {
        .position = {8,114,8+224,114+42},
        .name = "提交",
        .type = widget_type_button,
        .userParam = RGB(247,192,51),
    },
    [SubmitJob_Redo] = {
        .position = {8,164,8+224,164+42},
        .name = "返回",
        .type = widget_type_button,
        .userParam = RGB(216,216,216),
    },
//    [SubmitJob_flag] = {
//        .position = {8,164+50,8+224,164+(42*2)},
//        .name = "SubmitJob_flag",
//        .type = widget_type_button,
//        .userParam = 0,
//    },
};
enum SubmitJobflag{
    HomeWork_NotSubmitted,
    HomeWork_SubmissionInProgress,
    HomeWork_Submissionfailed,
    HomeWork_SubmissionSuccessfully,
};
static enum SubmitJobflag SubmitJob_boolflag;
//static HWND  SubmitJob_Hwnd = NULL;

//按钮控件创建函数
static bool_t  SubmitJobButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,SubmitJobCfgTab[SubmitJob_BACK].name))//返回
        {
            rc.bottom-=8;
            SetFillColor(hdc,RGB(247,192,51));
            FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,8,12,12,18,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,SubmitJobCfgTab[SubmitJob_Submit].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,21,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }
        else if(0==strcmp(buttoninfo->name,SubmitJobCfgTab[SubmitJob_Redo].name))
        {
            SetTextColor(hdc,RGB(255,255,255));
            Draw_Circle_Button(hdc,&rc,21,buttoninfo->userParam);
            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }
//        else if(0==strcmp(buttoninfo->name,SubmitJobCfgTab[SubmitJob_flag].name))
//        {
//            SetFillColor(hdc,RGB(255,255,255));
//            FillRect(hdc,&rc);
//            switch (SubmitJob_boolflag) {
//                case HomeWork_NotSubmitted:
//                    SetTextColor(hdc,RGB(120,120,120));
//                    DrawText(hdc,"作业未提交",-1,&rc,DT_VCENTER|DT_CENTER);
//                    break;
//                case HomeWork_SubmissionInProgress:
//                    SetTextColor(hdc,RGB(0,255,0));
//                    DrawText(hdc,"作业提交中",-1,&rc,DT_VCENTER|DT_CENTER);
//                    break;
//                case HomeWork_Submissionfailed:
//                    SetTextColor(hdc,RGB(255,0,0));
//                    DrawText(hdc,"作业提交失败",-1,&rc,DT_VCENTER|DT_CENTER);
//                    break;
//                case HomeWork_SubmissionSuccessfully:
//                    SetTextColor(hdc,RGB(0,0,255));
//                    DrawText(hdc,"作业提交完成",-1,&rc,DT_VCENTER|DT_CENTER);
//                    break;
//                default:   break;
//            }
//        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Submit(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND tmphwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, SubmitJobButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (SubmitJobCfgTab[i].type)
        {
            case  widget_type_button :
            {
                tmphwnd = CreateButton(SubmitJobCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         SubmitJobCfgTab[i].position.left, SubmitJobCfgTab[i].position.top,\
                         RectW(&SubmitJobCfgTab[i].position),RectH(&SubmitJobCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&SubmitJobCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
//                if((i == SubmitJob_flag)&&(tmphwnd != NULL))
//                {
//                    SubmitJob_Hwnd = tmphwnd;
//                }
            }
            break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Submit(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SubmitJobCfgTab[i].type)
            {
                case  type_background :
                        SetFillColor(hdc,SubmitJobCfgTab[i].userParam);
                        FillRect(hdc,&SubmitJobCfgTab[i].position);
                    break;

                case  widget_type_picture :
                    bmp = Get_BmpBuf(SubmitJobCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        Draw_Icon(hdc,SubmitJobCfgTab[i].position.left,\
                                SubmitJobCfgTab[i].position.top,&SubmitJobCfgTab[i].position,bmp);
                    }
                    break;
                case  widget_type_time :
                    time =  Win_GetTime();
                    DrawText(hdc,time,-1,&SubmitJobCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;

                case  widget_type_Power :

                    bmp = Get_BmpBuf(SubmitJobCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                //        Draw_Icon(hdc,rc.left,rc.top,&rc,bmp);
                        Draw_Icon_2(hdc,212,4,24,13,bmp);
                    }
                    break;
                case  widget_type_job :
                    DrawText(hdc,SubmitJobCfgTab[i].name,-1,&SubmitJobCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}

//子控件发来的通知消息
static enum WinType HmiNotify_HWL_Submit(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    struct TopicCB*topic;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case SubmitJob_BACK        :
            case SubmitJob_Redo        :
                Set_SelectTopicNum(0);
                topic = Get_TopicOfCnt(Get_SelectHmoeWork(),Get_SelectTopicNum());
                NextUi =  topic2WinType(topic);
                break;
            case SubmitJob_Submit:
                Set_LastWinType(WIN_HWL_Submit);
                SubmitJob_boolflag = HomeWork_SubmissionInProgress;
                Refresh_SubmitStatueWin();
//                if(SubmitJob_Hwnd != NULL)
//                    PostMessage(SubmitJob_Hwnd,MSG_PAINT,0,0);
                Submit_JobOfid();
                break;
            default:
            break;
        }
    }
    return NextUi;
}

bool_t Set_SubmitFlag(bool_t flag)
{
    if(WIN_SubmitStatue != Get_SelectionWinType())
        return false;
    if(flag == false){
        SubmitFail_Time_Ctrl();
        SubmitJob_boolflag = HomeWork_Submissionfailed;
    }else
        SubmitJob_boolflag = HomeWork_SubmissionSuccessfully;

    Refresh_SubmitStatueWin();
//    if(SubmitJob_Hwnd != NULL)
//        PostMessage(SubmitJob_Hwnd,MSG_PAINT,0,0);

    return true;
}

int Get_SubmitFlag()
{
    int ret;

    if(SubmitJob_boolflag == HomeWork_SubmissionInProgress)
        ret =1;
    else if(SubmitJob_boolflag == HomeWork_SubmissionSuccessfully)
        ret =2;
    else if(SubmitJob_boolflag == HomeWork_Submissionfailed)
        ret =3;

    return ret;
}


bool_t Clean_SubmitJobFlag()
{
    SubmitJob_boolflag = HomeWork_NotSubmitted;
    return true;
}

int Register_HWL_Submit()
{
    return Register_NewWin(WIN_HWL_Submit,HmiCreate_Submit,HmiPaint_Submit,HmiNotify_HWL_Submit);
}

