/*
 * WarningCheck.c
 *
 *  Created on: 2020��3��16��
 *      Author: c
 */
#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"

int RecCacheGetSizeInfo(int *free_size, int *total_size);

int WarningCheck()
{
    int free_size;
    int total_size;
    int Percentile;
    RecCacheGetSizeInfo(&free_size, &total_size);
    Percentile = ((total_size-free_size)*100)/total_size ;
    enum enumFriendlyReminders Get_SelectionFriendly();
    enum enumFriendlyReminders status =  Get_SelectionFriendly();
    if((Percentile >= 70)&&(status != enum_Insufficient_buffer))
    {
        printf("free_size: %d  total_size:%d   %d \r\n",free_size,total_size,(((total_size-free_size)*100)/total_size));
        SwitchToFriendlyReminders(enum_Insufficient_buffer);
    }
    return 0;
}





























