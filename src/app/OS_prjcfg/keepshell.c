/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

void keep_shell(void)
{

    volatile void *keep;
    (void)keep;

    extern void *djysh_arp;
    keep = (void *)djysh_arp;

    extern void *djysh_startmsg;
    keep = (void *)djysh_startmsg;

    extern void *djysh_linemem;
    keep = (void *)djysh_linemem;

    extern void *djysh_blackboxi;
    keep = (void *)djysh_blackboxi;

    extern void *djysh_dhcpdclient;
    keep = (void *)djysh_dhcpdclient;

    extern void *djysh_cp;
    keep = (void *)djysh_cp;

    extern void *djysh_heap;
    keep = (void *)djysh_heap;

    extern void *djysh_evtt;
    keep = (void *)djysh_evtt;

    extern void *djysh_iapmode;
    keep = (void *)djysh_iapmode;

    extern void *djysh_resetshell;
    keep = (void *)djysh_resetshell;

    extern void *djysh_rout;
    keep = (void *)djysh_rout;

    extern void *djysh_help;
    keep = (void *)djysh_help;

    extern void *djysh_time;
    keep = (void *)djysh_time;

    extern void *djysh_tcpipver;
    keep = (void *)djysh_tcpipver;

    extern void *djysh_wdtshow;
    keep = (void *)djysh_wdtshow;

}