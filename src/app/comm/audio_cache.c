#if 1
#include "mongoose.h"

void *psram_malloc (unsigned int size);
int OssDownloadRange(char *bucket, char *path, int pos_start, int pos_end, void *fdo, int timeout_ms);
void *GetAudioCacheAddr();
unsigned int GetAudioCacheSize();
char *GetOssBucketName();

#define AUDIO_CACHE_LOCK()         do { if (pCacheMgr->sem_cache) Lock_SempPend(pCacheMgr->sem_cache, 0xFFFFFFFF); } while (0);
#define AUDIO_CACHE_UNLOCK()       do { if (pCacheMgr->sem_cache) Lock_SempPost(pCacheMgr->sem_cache); } while (0);

typedef struct StAudioCache {
    char url[512];
    int pos_readed;
    int pos_writed;
    int pos_total;
    char *pmalloc;
    int malloc_len;
    int flag; // 0: init; 1: need delete; 2: download ok
    unsigned int timestamp;
}StAudioCache;

typedef struct StCacheMgr {
    struct StAudioCache *pgAudioCache;
    struct SemaphoreLCB* sem_cache;
    int max_num;
    int index_doing;
    int stop;
}StCacheMgr;

extern int login_ok;

struct StCacheMgr gCacheMgr;

struct HeapCB *share_mem_heap=0;

void *ShareMemMalloc (unsigned int size)
{
    if (share_mem_heap==0){
        share_mem_heap =M_FindHeap("SHARE_MEM");
        if(share_mem_heap==NULL){
            printf("M_FindHeapd  ERROR!\r\n");
            return NULL;
        }
    }
    return M_MallocHeap(size,share_mem_heap,0);
}

void ShareMemFree(void *p)
{
    return M_FreeHeap(p, share_mem_heap);
}

void *ShareMemReAlloc (void *p, unsigned int size)
{
    void *rp = 0;
    if (size == 0) {
        if (p) ShareMemFree(p);
        return 0;
    }
    if (p == 0 && size == 0) return 0;
    if (p == 0 && size > 0) {
        rp = ShareMemMalloc(size);
        if (rp==0) {
          printf("ShareMemReAlloc, rp = NULL, size=%d!\r\n", size);
        }
        return rp;
    }

    rp = ShareMemMalloc(size);
    if (rp) {
        memcpy(rp, p, size);
    }
    return rp;
}


int GetShareMemMallocMaxSize()
{
    int size = 0;
    int right = 0;
    int left = 0;
    char *p = 0;

    unsigned int total = GetAudioCacheSize();//GetUpdateBufSize();
    //二分查找
    right = total+1;
    size = left + (right - left) / 2;
    while (left < right && left != size) {
        p = ShareMemMalloc(size);
        if (p) {
            ShareMemFree(p);
            left = size;
        }
        else {
            right = size;
        }
        size = left + (right - left) / 2;
    }
    return size;
}

void *AudioCacheGet(char *url, int *rd_wrted, int *rd_total)
{
    int i = 0;
//    int ret = 0;
    char *p = 0;
    struct StCacheMgr *pCacheMgr = &gCacheMgr;
    struct StAudioCache *pCache = 0;
    if (url==0 || rd_wrted==0 || rd_total==0) return 0;
    if (pCacheMgr->pgAudioCache==0) return 0;
    AUDIO_CACHE_LOCK();
    pCache = pCacheMgr->pgAudioCache;
    for (i = 0; pCache && i < pCacheMgr->max_num; i++) {
        if (strcmp(pCache[i].url, url) == 0) {
            *rd_wrted = pCache[i].pos_writed;
            *rd_total = pCache[i].pos_total;
            p = pCache[i].pmalloc;
            break;
        }
    }
    AUDIO_CACHE_UNLOCK();
    return p;
}

char *GetHttpPath(char *url)
{
    char *p = 0;
    if (url == 0) return 0;
    if (url[0] == '/') return url;
    if ((p = strstr(url, "http://")) == url) {
        p += strlen("http://");
        p = strstr(p, "/");
        if (p) {
            return p;
        }
    }
    if ((p = strstr(url, "https://")) == url) {
        p += strlen("https://");
        p = strstr(p, "/");
        if (p) {
            return p;
        }
    }
    return 0;
}
int WiFiOpen(unsigned int timeout_ms);
int WiFiClose(unsigned int timeout_ms);
int AudioCacheAdd(char *arr_url[], int num)
{
    int i = 0;
    int j = 0;

    if (arr_url==NULL || num <= 0) return 0;

    //match the item and delete no matched.
    struct StCacheMgr *pCacheMgr = &gCacheMgr;

    struct StAudioCache *pNewCache = 0;
    struct StAudioCache *pOldCache = 0;

    //stop download thread firstly

    //empty, add all num to Cache
    pNewCache = malloc(num * sizeof(struct StAudioCache));
    if (pNewCache==0) return -1;

    AUDIO_CACHE_LOCK();
    memset(pNewCache, 0, num * sizeof(struct StAudioCache));

    if (pCacheMgr->pgAudioCache) {
        pOldCache = pCacheMgr->pgAudioCache;
        for (i = 0; i < num; i++) {
            if (GetHttpPath(arr_url[i])==0) continue;
            for (j = 0; j < pCacheMgr->max_num; j++) {
                if (strcmp(GetHttpPath(arr_url[i]), pOldCache[j].url)==0) {
                    memcpy(&pNewCache[i], &pOldCache[j], sizeof(struct StAudioCache));
                    pNewCache[i].flag = 1;
                    pOldCache[j].flag = 1;
                    break;
                }
            }
            if (j >= pCacheMgr->max_num) {
                strcpy(pNewCache[i].url, GetHttpPath(arr_url[i]));
            }
        }
        pCacheMgr->max_num = num;
    }
    else {
        for (i = 0; i < num; i++) {
            if (GetHttpPath(arr_url[i])==0) continue;
            strcpy(pNewCache[i].url, GetHttpPath(arr_url[i]));
        }
        pCacheMgr->max_num = num;
    }
#if 0
    printf("==AudioCacheAdd==\r\n");
    for (i = 0; i < pCacheMgr->max_num; i++) {
        printf("[%d] ", i);
        printf("writed:%d ", pNewCache[i].pos_writed);
        printf("total:%d ", pNewCache[i].pos_total);
        printf("url=%s\r\n", pNewCache[i].url);
    }
#endif
    //free older and set new array
    if (pCacheMgr->pgAudioCache) {
        //free sub-item
        pOldCache = pCacheMgr->pgAudioCache;
        for (i = 0; i < pCacheMgr->max_num; i++) {
            if (pOldCache[i].flag == 0) {
                if (pOldCache[i].pmalloc) ShareMemFree(pOldCache[i].pmalloc);
                pOldCache[i].pmalloc = 0;
            }
        }
        free(pCacheMgr->pgAudioCache);
        pCacheMgr->pgAudioCache = 0;
    }

    pCacheMgr->pgAudioCache = pNewCache;
    pCacheMgr->stop  = 0;
    AUDIO_CACHE_UNLOCK();
    //start download thread

    return 1;
}


int write_buffer(unsigned char *data, int len, int total, int timeout)
{
    //printf("===len=%d, total=%d===!\r\n", len, total);
    int ret = 0;
    void *pNew = 0;
    int max = 0;
    struct StCacheMgr *pCacheMgr = &gCacheMgr;
    int index = 0;
    int left_size = 0;
    struct StAudioCache *pCache = 0;
    AUDIO_CACHE_LOCK();
    index = pCacheMgr->index_doing;
    if (index < pCacheMgr->max_num)  {
        pCache = pCacheMgr->pgAudioCache;
        if (pCache) {
            if (pCache[index].pos_total < total) { //第一次下载，获取的都是音频文件的总大小
                pCache[index].pos_total = total;

                pNew = ShareMemReAlloc (pCache[index].pmalloc, pCache[index].pos_total);
                if (pNew) {
                    max = pCache[index].pos_total;
                }
                else {
                    max = GetShareMemMallocMaxSize();
                    //至少10K数据作音频缓冲，否者不做。
                    if (max >= 10*1024 && max > pCache[index].malloc_len) {
                        pNew = ShareMemReAlloc (pCache[index].pmalloc, max);
                    }
                }
                if (pNew) {
                    pCache[index].pmalloc = pNew;
                    pCache[index].malloc_len = max;
                }
                left_size = pCache[index].malloc_len - pCache[index].pos_writed;
                if (left_size > len) {
                    //pCache[index].pos_writed += len;
                    ret = len;
                }
                else {
                    //pCache[index].pos_writed += left_size;
                    ret = left_size;
                }
                //printf("[url:%s, download begin, writed=%d, total:%d]\r\n", pCache[index].url, pCache[index].pos_writed, pCache[index].pos_total);
            }
            else {//后续下载，可能需要动态realloc缓冲大小
                left_size = pCache[index].malloc_len - pCache[index].pos_writed;
                if (left_size > len) {
                    //pCache[index].pos_writed += len;
                    ret = len;
                }
                else {
                    //pCache[index].pos_writed += left_size;
                    ret = left_size;
                }
            }
        }
    }
    if (pCache[index].pos_writed == pCache[index].pos_total) {
        //printf("[url:%s, download ended, writed=%d, total:%d]\r\n", pCache[index].url, pCache[index].pos_writed, pCache[index].pos_total);
    }
    if (ret > 0) {//拷贝数据。
        memcpy(&pCache[index].pmalloc[pCache[index].pos_writed], data, ret);
        pCache[index].pos_writed += ret;
    }
    AUDIO_CACHE_UNLOCK();
    return ret;
}


//"/devtemp/DJYOS0015/tmp_220_404_116.wav"
int is_wifi_connected();
ptu32_t EventProcDownload(void)
{
    int ret = 0;
    int cnt = 0;
    int j = 0;
    struct StAudioCache *pCache = 0;
    struct StCacheMgr *pCacheMgr = &gCacheMgr;
    int num = 0;
    int i = 0;
    while (1) {
        if (!is_wifi_connected()/*!login_ok*/) {
            Djy_EventDelay(1000*1000);
            cnt = 0;
            continue;
        }
        if (pCacheMgr->stop == 1) {
            cnt = 0;
            Djy_EventDelay(1000*1000);
            continue;
        }

        if (WiFiOpen(3000)<0) {
            WiFiClose(100);
            Djy_EventDelay(1000*1000);
            continue;
        }
        AUDIO_CACHE_LOCK();
        num = pCacheMgr->max_num;
        pCache = pCacheMgr->pgAudioCache;
        for (i = 0; i< num; i++) {
            num = pCacheMgr->max_num;
            pCache = pCacheMgr->pgAudioCache;
            if (pCache[i].url[0]==0) continue;
            if (pCache && num > 0) {
                pCacheMgr->index_doing = i;
                if (pCache[i].pos_total == 0 || pCache[i].pos_writed < pCache[i].malloc_len) {
                    AUDIO_CACHE_UNLOCK();
                    ret = OssDownloadRange(GetOssBucketName(), pCache[i].url, pCache[i].pos_writed, -1, write_buffer, 5000);
                    AUDIO_CACHE_LOCK();
                }
            }
        }

        j = 0;
        num = pCacheMgr->max_num;
        pCache = pCacheMgr->pgAudioCache;
        for (i = 0; i< num; i++) {
            if (pCache && pCache[i].pos_writed > 0 && pCache[i].pos_writed == pCache[i].malloc_len) {//下载数据和malloc的一致，不尝试下载。
                j++;
            }
            if (pCache && pCache[i].pos_total > 0 && pCache[i].pmalloc==0) {//有下载过，不过没有内存分配，不尝试再下载。
                j++;
            }
        }
        if (j == num) {
            // all download ok! set flag;
            printf("<<< info: ALL Audio download done >>>\r\n");
            pCacheMgr->stop = 1;
            WiFiClose(1000);
        }

#if 0
        printf("==info: download audio information ==\r\n");
        num = pCacheMgr->max_num;
        pCache = pCacheMgr->pgAudioCache;
        for (i = 0; i< num; i++) {
            if (pCache) {
                if (pCache[i].url[0]==0) continue;
                printf("[%d] ", i);
                printf("total:%d ", pCache[i].pos_total);
                printf("malloc:0x%x, len=%d ", pCache[i].pmalloc, pCache[i].malloc_len);
                printf("writed:%d ", pCache[i].pos_writed);
                printf("url=%s\r\n", pCache[i].url);
            }
        }
#endif
        AUDIO_CACHE_UNLOCK();

        if (cnt++ > 30) {//如果网络断开，不刷新数据，直接停止。
            pCacheMgr->stop = 1;
            WiFiClose(1000);
        }

        Djy_EventDelay(1000*1000);
    }
}


bool_t heapadded = false;
extern u32 xff0c;
int InitAudioCache()
{
    OssMgrInit();
#if 1
    if(Heap_Add(GetAudioCacheAddr(), GetAudioCacheSize(), 64, 0, true, "SHARE_MEM") == 0)
    {
        printf("SHARE_MEM add heap fail.\r\n");
        return 0;
    }
    xff0c = *(u32*)0x37fff0c;
    heapadded = true;       //lst test
#endif
    struct StCacheMgr *pCacheMgr = &gCacheMgr;

    pCacheMgr->sem_cache = semp_init(1, 1, 0);
    if(!pCacheMgr->sem_cache) {
        printf("error: pCacheMgr->sem_cache failed!\r\n");
        return -1;
    }
    pCacheMgr->stop = 1;

    return 0;
}



int FreeAudioCache()
{
    int i = 0;
    struct StAudioCache *pCache = 0;
    struct StCacheMgr *pCacheMgr = &gCacheMgr;
    AUDIO_CACHE_LOCK();
    if (pCacheMgr->pgAudioCache) {
        //free sub-item
        pCache = pCacheMgr->pgAudioCache;
        for (i = 0; i < pCacheMgr->max_num; i++) {
            if (pCache[i].pmalloc) ShareMemFree(pCache[i].pmalloc);
            pCache[i].pmalloc = 0;
        }
        free(pCacheMgr->pgAudioCache);
        pCacheMgr->pgAudioCache = 0;
    }
    AUDIO_CACHE_UNLOCK();
    return 0;
}

bool_t oss_download_event_init(void)
{
    u16 evtt_download;
//    char *stack = psram_malloc(0x2000);
//    if (stack==0) {
//        printf("error: predownload_stack==NULL!\r\n");
//        return 0;
//    }
    InitAudioCache();

    evtt_download = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 0, EventProcDownload, NULL, 0x1000, "download_event");
    if(evtt_download != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_download, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\nevtt_download Event Start Fail!\r\n");
    }

    return true;
}





int WavPlayData(unsigned char *buf, int len, unsigned int timeout);

int audio_cache_test ()
{
    int i = 0;
    int mark = 0;
    char *p = 0;
    int sz_wrted = 0;
    int sz_total = 0;

    OssMgrInit();
    const char * arr_url[] = {
            "/devtemp/DJYOS0015/tmp_220_383_116.wav",
            "/devtemp/DJYOS0015/tmp_220_383_139.wav",
            "/devtemp/DJYOS0015/tmp_220_385_139.wav",
           // "/devtemp/DJYOS0015/tmp_220_391_149.wav",
            "/devtemp/DJYOS0015/tmp_220_404_116.wav",
            "/devtemp/DJYOS0015/tmp_220_407_116.wav",
            "/devtemp/DJYOS0015/tmp_220_414_138.wav",
    };
    oss_download_event_init();
//    AudioCacheAdd(arr_url, sizeof(arr_url)/sizeof(arr_url[0])/2);
//    Djy_EventDelay(20*1000*1000);
    AudioCacheAdd(arr_url, sizeof(arr_url)/sizeof(arr_url[0]));
    Djy_EventDelay(20*1000*1000);
    printf("----------play wav test------------\r\n");
    for (i=0; i<sizeof(arr_url)/sizeof(arr_url[0]); i++){
        p = AudioCacheGet(arr_url[i], &sz_wrted, &sz_total);
        if (p) {
            printf("-- start play audio [p=0x%x,len=%d], total=%d, url=%s!\r\n", p, sz_wrted, sz_total, arr_url[i]);
            WavPlayData(p, sz_wrted, 5000);
            printf("-- ended play audio [p=0x%x,len=%d], total=%d, url=%s!\r\n", p, sz_wrted, sz_total, arr_url[i]);
            Djy_EventDelay(5*1000*1000);
        }
    }

    return 0;
}


#endif
