#ifndef __RECORDER_CACHE_H__
#define __RECORDER_CACHE_H__

enum {
	ERC_WRITE_FULL = 0, //MUST BE 0
	ERC_READ_EMPTY = -100,
	ERC_CREATE_ITEM = -101,
	ERC_PARAMRAM = -102,
	ERC_NO_NEW_ITEM = -103,
	ERC_NET_STOP = -104,
};

enum {
	E_APPEND_HEAD = 0,
	E_APPEND_TAIL,
};

enum {
	ERS_NO_TAIL = 0, //部分的，没尾巴（先播放，然后网络下载）
	ERS_COMPLETE, //完整的（直接播放，不用任何下载）
	ERS_NO_HEAD, //部分的，没头（不能播放，直接从网络下载）
};

int RecCacheInit(int max_nums);

int RecCacheDeInit();

// return : 添加新记录失败，需要等待读空一些数据才能再添加新项目
int RecCacheAppendTail(char *url, unsigned char *data, int len, int flag);

//设置最后一个录音结束标志
int RecCacheSetItemDone();

int RecCacheReadHead(char *out_url, int *out_id, int ulen, unsigned char *out_data, int dlen, int *pstatus);

int RecCacheCheckReadStatus(char *url);

//删除指定的url缓冲。url格式例如/devtemp/4HH1HU20030001R/tmp_295_839_116.wav
int RecCacheDelItem(char *url);


//item, 记录数组的大小，默认是10
int RecCacheReset(int items);

//return:
// ret == -1: find none, 如果缓冲音频头部被破坏也返回-1
// ret == 0: 完整的音频，不用网络下载
// ret == 1: 部分音频，可以播放前半段，后半段从网络下载
// ppdata, plen : 环型缓冲区尾巴
// ppdata1, plen1: 环形缓冲区头部， 这个有可能没头部，所以可以为0
int RecCacheFindCache(char *url, unsigned char **ppdata, unsigned int *plen,
        unsigned char **ppdata1, unsigned int *plen1);

/* 网上播放了一段录音，然后录音还没上传，部分还在本地缓冲区， 从本地缓冲区获取数据 */
/* ret = 0: 正常返回 */
/* ret < 0: 错误返回 */
int RecCacheFindCacheTail(char *url, int pos, unsigned char **ppdata, unsigned int *plen,
        unsigned char **ppdata1, unsigned int *plen1);

#endif

