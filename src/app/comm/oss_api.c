#include "mongoose.h"
#include "cJSON.h"
#include "comm_api.h"
#include "recorder_cache.h"

extern void *psram_malloc (unsigned int size);
#ifndef free
#define free(x) M_Free(x)
#endif


#define hex_dump
typedef struct StAliyunOssMgr {
    char OssKeyId[100];
    char OssKeySecret[100];
    char Host[100];
    unsigned short port;
    // http put and responds
    struct mg_mgr mgr;
    struct mg_connection *nc;
    int status;
}StAliyunOssMgr;

struct tm *oss_gmtime(const time_t *time, int time_zone);

struct StAliyunOssMgr gOssMgr;

#define LOCAL_SERVER_TEST 0

int media_is_stop();

int OssMgrInit()
{
    static int flag = 0;
    if (flag == 0) {
        flag = 1;
//        char key[] = "Osp3xo3WZ9OjhOqRmktsT8PVlXLwdx";
//        char keyid[] = "LTAI4Fd45yCCfJTbEMUbfdBG";
        char key[] = "BcAwgk3c3Tp9YouEZWPcgrj5ReHZi2";
        char keyid[] = "LTAI4FxcdNDoNc3TE1v4YivZ";
    #if LOCAL_SERVER_TEST
        char host[] = "192.168.137.1";
    #else
        //char host[] = "djyos-qinjian.oss-cn-hangzhou.aliyuncs.com";
        char host[] = "bucket-kouyutong.oss-cn-hangzhou.aliyuncs.com";
    #endif
        struct StAliyunOssMgr *pOssMgr = &gOssMgr;
        memset(pOssMgr, 0, sizeof(struct StAliyunOssMgr));
        strcpy(pOssMgr->OssKeyId, keyid);
        strcpy(pOssMgr->OssKeySecret, key);
        strcpy(pOssMgr->Host, host);
        pOssMgr->port = 80;
    }
    return 0;
}


#define BUCKET_NAME "bucket-kouyutong"

char *GetOssBucketName()
{
    return BUCKET_NAME;
}

static void cb_http_timestamp_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    char *pnew = 0;
    struct http_message *hm = (struct http_message *) ev_data;
    cJSON* cjson = 0;
    if (nc->user_data == 0) {
        printf("error: cb_http_timestamp_handler, nc->user_data is NULL!\r\n");
        return;
    }
    unsigned int *timestamp = (unsigned int*)nc->user_data;

    if (ev == MG_EV_HTTP_REPLY)
    {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        //hex_dump("STAT_SBC_GETTIME(message):", hm->message.p, hm->message.len);
        pnew = malloc(hm->body.len+1);
        if (pnew==0) goto END_FUN;
        memcpy(pnew, hm->body.p, hm->body.len);
        pnew[hm->body.len] = 0;
        //hex_dump("STAT_SBC_GETTIME(body):", hm->body.p, hm->body.len);
        cjson = cJSON_Parse(pnew);
        if (cjson)
        {
            cJSON* json_time = cJSON_GetObjectItem(cjson, "timestamp");
            if (json_time)
            {
                *timestamp=json_time->valueint;
            }
        }
    }

END_FUN:
    if (pnew) free(pnew);
    if (cjson) {
        cJSON_Delete(cjson);
    }
}

static unsigned int gRtcTickSecOffset = 0;
static unsigned int gRtcTickSecTimestamp = 0;

int ClearTimeStamp()
{
    gRtcTickSecOffset = 0;
    gRtcTickSecTimestamp = 0;
    return 0;
}

int NetGetTimeStamp( unsigned int  *ptimestamp, int timeout_ms)
{
    int ret = 0;
    unsigned int timestamp = 0;
    unsigned int timemark = 0;
    struct mg_connection *nc;
    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);
    timestamp = 0;
    nc = mg_connect_http(&mgr, cb_http_timestamp_handler, "http://api2.bmob.cn/1/timestamp",
        "X-Bmob-Application-Id: 5504f1c66ce052a8a0b488a13997d3c3\r\n"
        "X-Bmob-REST-API-Key: 429942853b8d37b7aae8b36924fc0258\r\n"
        "User-Agent: Mongoose/6.15\r\n",
        0);
    if (nc) {
        nc->user_data = &timestamp;
    }
    else
    {
        mg_mgr_free(&mgr);
        return -1;
    }
    printf("info: wait timestamp!\r\n");

    timemark = DjyGetSysTime()/1000;
    while (timestamp == 0) {
        if (DjyGetSysTime()/1000-timemark > timeout_ms) {
            printf("timestamp: timeout break!\r\n");
            break;
        }
        mg_mgr_poll(&mgr, 500);
        Djy_EventDelay(10*1000);
    }
    if (timestamp) {
        *ptimestamp = timestamp;
        ret = 0;
    }
    else {
        *ptimestamp = 0;
        ret = -1;
    }
    mg_mgr_free(&mgr);
    return ret;
}

char *arr_ntp_server[] = {
        "ntp.ntsc.ac.cn",
        "cn.pool.ntp.org",
        "us.pool.ntp.org",
};
int get_ntp_tiemstamp(u32 *ptimestamp, char *ntp_domain, int timeout_ms);

int ntp_timestamp(u32 *ptimestamp, int timeout_ms)
{
    unsigned int timestamp = 0;
    static int index = 0;
    index++;
    index = index % (sizeof(arr_ntp_server)/sizeof(arr_ntp_server[0]));
    int status = get_ntp_tiemstamp(&timestamp, arr_ntp_server[index], 5000);
    printf("info: get_ntp_tiemstamp, status:%d, index:%d, ntp_server:%s, timestamp=%d!\r\n", status, index, arr_ntp_server[index], timestamp);
    if (status >= 0) {
        *ptimestamp = timestamp;
    }
    return status;
}
int  GetTimeStamp(unsigned int *timestamp_out, int timeout_ms)
{
    unsigned int cur_tick_sec  = 0;
    unsigned int timestamp = 0;
    int status = 0;
    if (gRtcTickSecOffset == 0 && gRtcTickSecTimestamp == 0) {
        //status = NetGetTimeStamp(&timestamp, timeout_ms);
        status = ntp_timestamp(&timestamp, timeout_ms);
        if (status >= 0 && timestamp) {
            *timestamp_out = timestamp;
            gRtcTickSecOffset = DjyGetSysTime()/1000000;
            gRtcTickSecTimestamp = timestamp;
        }
        printf("gRtcTickSecOffset = %d, gRtcTickSecTimestamp = %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp);
    }
    else {
        cur_tick_sec = DjyGetSysTime()/1000000;
        *timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
        //printf("Tick Offset: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, *timestamp_out);
    }
    return 0;
}

int GetTimeHourMinute(int *hour, int *min)
{
    unsigned int timestamp_out = 0;
    unsigned int cur_tick_sec  = 0;
    struct tm *ptm = 0;
    cur_tick_sec = DjyGetSysTime()/1000000;
    timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
    time_t t = timestamp_out;
    ptm = oss_gmtime(&t, 8);
    *hour = ptm->tm_hour;
    *min = ptm->tm_min;
    //printf("---->hour=%d, min=%d!\r\n", *hour, *min);
    //strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", oss_gmtime(&t, 0));
    //printf("GetTimeHourMinute: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, timestamp_out);
    return 0;
}

static void cb_http_upload_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct http_message *hm = (struct http_message *) ev_data;
    int *status = (int*)nc->user_data;

    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            *status = -1;
            break;
        }
#if 1
        int opt = 0;
        opt = 1024*6;
        if(0 != setsockopt(nc->sock, SOL_SOCKET ,SO_SNDBUF,&opt, 4))
        {
            printf("error: cb_http_upload_handler set client sndbuf failed!\r\n");
        }
#endif
        break;
    case MG_EV_HTTP_REPLY:
        {   //hex_dump("cb_http_upload_handler:", hm->message.p, hm->message.len);
            switch (hm->resp_code) {
            case 200:
                nc->flags |= MG_F_CLOSE_IMMEDIATELY;
                if (0 == memcmp(hm->message.p, "HTTP/1.1 200 OK", strlen("HTTP/1.1 200 OK"))) {
                    *status = 1;
                }
                else {
                    *status = -1;
                }
                break;
            case 302:
                printf("http 302 not supported!\r\n");
                *status = -302;
                break;
            case 401:
                *status = -401;
                break;
            default:
                *status = -1;
                break;
            }
            break;
        }
    case MG_EV_CLOSE:
        pOssMgr->nc = 0;
        printf ("http_upload close!\r\n");
        break;
    default:
        break;
    }
}

int GenOssSignatureURL(const char *key, const char *VERB, char *DATE, char *ResourcePath, char *sign_out, int sign_len)
{
    int ret = -1;
    int min = 0;
    unsigned char sha1_out[20] = "";
    char *data = 0;
    unsigned char *dst = 0;

    data = malloc(1024);
    if (data == 0) goto FUN_RET;
    memset(data, 0, 1024);

    dst = malloc(256);
    if (dst == 0) goto FUN_RET;
    memset(dst, 0, 256);

    strcat(data, VERB);
    strcat(data, "\n");
    strcat(data, "\n");
    strcat(data, "\n");
    strcat(data, DATE);
    strcat(data, "\n");
    strcat(data, ResourcePath);

    memset(sha1_out, 0, sizeof(sha1_out));

    cs_hmac_sha1((const unsigned char *)key, strlen(key),(const unsigned char *)data, strlen(data), sha1_out);

    cs_base64_encode(sha1_out, sizeof(sha1_out), (char*)dst);
    //printf("%s\r\n", dst);

    struct mg_str mg_url = mg_url_encode(mg_mk_str((const char*)dst));
    min = mg_url.len + 1;
    min = min < sign_len ? min : sign_len;
    memcpy(sign_out, mg_url.p, min);
    sign_out[min] = 0;
    ret = 0;

FUN_RET:
    if (data) {
        free(data);
        data = 0;
    }
    if (dst) {
        free(dst);
        dst = 0;
    }
    return ret;
}
int GenOssSignature(char *key, char *VERB, char* ContentType, char *DATE, char *ResourcePath, char *sign_out, int sign_len)
{
    int ret = -1;
    int min = 0;
    unsigned char sha1_out[20] = "";
    //char key[] = "Osp3xo3WZ9OjhOqRmktsT8PVlXLwdx";
    unsigned char *dst = 0;
    char *data = 0;
    char *con_type = "";

    if (ContentType) {
        con_type = ContentType;
    }

    data = malloc(1024);
    if (data == 0) goto FUN_RET;
    memset(data, 0, 1024);

    dst = malloc(256);
    if (dst == 0) goto FUN_RET;
    memset(dst, 0, 256);

    strcat(data, VERB);
    strcat(data, "\n");
    strcat(data, "\n");
    strcat(data, con_type); strcat(data, "\n");
    strcat(data, DATE);
    strcat(data, "\n");
    strcat(data, ResourcePath);

    memset(sha1_out, 0, sizeof(sha1_out));
    cs_hmac_sha1((const unsigned char *)key, strlen(key),(const unsigned char *)data, strlen(data), sha1_out);

    cs_base64_encode(sha1_out, sizeof(sha1_out),(char*)dst);
    //printf("%s\r\n", dst);

    min = strlen((char*)dst);
    min = min < sign_len ? min : sign_len;
    memcpy(sign_out, dst, min);
    sign_out[min] = 0;
    ret = 0;

FUN_RET:
    if (data) {
        free(data);
        data = 0;
    }
    if (dst) {
        free(dst);
        dst = 0;
    }
    return ret;
}

int GenUrlForDownload(const char *bucket, char *source, unsigned int expires,const char *keyid, char *signature, char *url_out, int len)
{
    int ret = -1;
    char *url_temp = 0;
    int min = 0;

    url_temp = malloc(2048);
    if (url_temp==0) goto FUN_RET;
    memset(url_temp, 0, 2048);
    min = 2048-1;

    sprintf(url_temp, "http://%s%s?Expires=%d&OSSAccessKeyId=%s&Signature=%s",
        bucket, source, expires, keyid, signature);
    //printf("%s", url_temp);
    min = min < len ? min : len;
    memcpy(url_out, url_temp, min);
    ret = 0;

FUN_RET:
    if (url_temp) {
        free(url_temp);
        url_temp = 0;
    }
    return ret;
}

int GetTempOssUrl(char *path, unsigned int timestamp, unsigned int exp_secs, char *out, int len)
{
    int ret = 0;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;

//    const char key[] = "Osp3xo3WZ9OjhOqRmktsT8PVlXLwdx";
//    const char key_idx[] = "LTAI4Fd45yCCfJTbEMUbfdBG";
    const char *key = pOssMgr->OssKeySecret;
    const char *key_idx = pOssMgr->OssKeyId;
    const char *host = pOssMgr->Host;
    char sig[200] = "";
    char timestamp_string[30] = "";
    char path_tmp[256] = "";
    sprintf(timestamp_string, "%d", timestamp + exp_secs);
    sprintf(path_tmp, "/%s%s", GetOssBucketName(), path);
    ret = GenOssSignatureURL(key, "GET", timestamp_string, path_tmp, sig, sizeof(sig));
    if (ret >= 0) {
        ret = GenUrlForDownload(host/*"djyos-qinjian.oss-cn-hangzhou.aliyuncs.com"*/, path, timestamp + exp_secs, key_idx, sig, out, len);
    }
    return ret;
}

int add_suffix(char *path, char *suffix, char *out, int len)
{
    int i = 0;
    int mark = -1;
    int total = 0;
    int min = 0;
    while (path[i]) {
        if (path[i] == '/' || path[i] == '\\') mark = -1;
        if (path[i] == '.') mark = i;
        i++;
    }
    total = i;
    total += strlen(suffix) + 1;
    char *new = malloc(total);
    if (!new) return -1;
    memset(new, 0, total);
    if (mark == -1) {
        strcat(new, path);
        strcat(new, suffix);
    }
    else {
        memcpy(new, path, mark);
        memcpy(new + mark, suffix, strlen(suffix));
        memcpy(new + mark + strlen(suffix), path + mark, strlen(path) - mark);
    }
    printf("path: %s!\r\n", new);
    min = len-1;
    min = min < total ? min : total;
    memcpy(out, new, min);
    if (new) free(new);
    return min;
}

struct tm *oss_gmtime(const time_t *time, int time_zone)
{
    static struct  tm result;
    s64 temp_time;
    if(time == NULL)
    {
        extern s64 __Rtc_Time(s64 *rtctime);
        temp_time = __Rtc_Time(NULL);
        temp_time += ((s64)8*3600);
    }
    else
    {
        temp_time = *time + ((s64)time_zone*3600);
    }
    return Tm_LocalTime_r(&temp_time, &result);
}
char *mg_strftime_patch(char *base)
{
    //char base[100] = "Sun, 29 Sep 2019 7:5:33 GMT";
    int hour, mini, sec;
    char zone[20] = { 0 };
    char buf[50] = { 0 };
    char *p = strstr(base, ":");
    while (p > base && *p != ' ') {
        p--;
    }
    if (*p == ' ') p++;
    sscanf(p, "%d:%d:%d %s", &hour, &mini, &sec, zone);
    sprintf(buf, "%02d:%02d:%02d %s", hour, mini, sec, zone);
    strcpy(p, buf);
    printf("%s", base);
    return base;
}

char *GTM_TIME(unsigned int timestamp, char *buf, int len)
{
    char GMT[100] = {0};
    time_t t = timestamp;
    strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", oss_gmtime(&t, 0));
    //mg_strftime_patch(GMT);
    int min = strlen(GMT)+1;
    min = min < len ? min : len;
    memcpy(buf, GMT, min);
    return buf;
}
//"%Y%m%d%H%M%S"
int FormatTime(char *outbuf, int len, char *fmt)
{
    int ret = 0;
    unsigned int timestamp = 0;
    if (outbuf==0) return -1;
    ret = GetTimeStamp(&timestamp, 5000);
    if (ret < 0) return -1;
    time_t t = timestamp;
    if (fmt==0) {
        strftime(outbuf, len, "%Y%m%d%H%M%S", oss_gmtime(&t, 8));
    }
    else {
        strftime(outbuf, len, fmt, oss_gmtime(&t, 8));
    }
    return 0;
}

//bucket = "djyos-qinjian"
//path = "/test/eee"
int OssOpen(char *bucket, char *path, int timeout_ms)
{
    int ret = -1;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    //char timestamp_string[100] = { 0 };
    //char sign[200] = { 0 };
    //char temp[1024] = { 0 };
    //char suffix[100] = { 0 };
    char *temp = 0;
    char *sign = 0;

    if (!is_wifi_connected()) return -1;

    sign = malloc(200);
    if (sign==0) goto FUN_RET;
    memset(sign, 0, 200);

    temp = malloc(1024);
    if (temp==0) goto FUN_RET;
    memset(temp, 0, 1024);


    GetTimeStamp(&timestamp, timeout_ms);

    printf("info: OssOpen->GetTimeStamp=%d!\r\n", timestamp);

#if 0
    time_t t = timestamp+3600*8;
    struct tm ttt;
    memset(&ttt, 0, sizeof(struct tm));
    struct tm *pt = Tm_LocalTime_x(&t, &ttt);
    strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", pt);
    memset(suffix, 0, sizeof(suffix));
    strftime(suffix, sizeof(suffix), "@%Y%m%d%H%M%S", pt);
    add_suffix(path, suffix, suffix, sizeof(suffix));
    path = &suffix[0];
#else
//    time_t t = timestamp;
    GTM_TIME(timestamp, GMT, sizeof(GMT));
#endif

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s", bucket, path);
    GenOssSignature(pOssMgr->OssKeySecret, "PUT", 0, GMT, temp, sign, 200);

    if (pOssMgr->nc) {
        mg_mgr_free(&pOssMgr->mgr);
        memset(&pOssMgr->mgr, 0, sizeof(struct mg_mgr));
        pOssMgr->nc = 0;
        pOssMgr->status = 0;
    };
    mg_mgr_init(&pOssMgr->mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    pOssMgr->nc = mg_connect(&pOssMgr->mgr, temp, cb_http_upload_handler);
    printf("======pOssMgr->nc=0x%x=========\r\n", (unsigned int)pOssMgr->nc);

    pOssMgr->status = 0;
    if (pOssMgr->nc) {
        pOssMgr->nc->user_data = &pOssMgr->status;
    }
    mg_set_protocol_http_websocket(pOssMgr->nc);
    memset(temp, 0, 1024);
    sprintf(temp,
        "PUT %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : */*\r\n"
        "Connection: keep-alive\r\n"
        "date: %s\r\n"
        "authorization: OSS %s:%s\r\n"
        "Transfer-Encoding: chunked\r\n\r\n", path, pOssMgr->Host, GMT, pOssMgr->OssKeyId, sign);
    mg_printf(pOssMgr->nc, "%s", temp);
    ret = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    if (sign) {
        free(sign);
        sign = 0;
    }
    return ret;
}

int OssWrite(char *data, int len, int timeout_ms)
{
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    if (pOssMgr->nc == 0) {
        return -5;
    }
    mg_send_http_chunk(pOssMgr->nc,data, len);

    //mg_mgr_poll(&pOssMgr->mgr, timeout_ms);
    unsigned int timemark = DjyGetSysTime()/1000;
    while (pOssMgr->nc && pOssMgr->nc->send_mbuf.len > 1024)
    {
        mg_mgr_poll(&pOssMgr->mgr, 500);
        if (DjyGetSysTime()/1000-timemark > timeout_ms) {
            printf("OssWrite: timeout break!\r\n");
            return -2;
        }
        //printf(".");
    }
    if (pOssMgr->status < 0 || pOssMgr->nc == 0) {
        printf("error: OssWrite, pOssMgr->status=%d.\r\n", pOssMgr->status);
        return -1;
    }
    return len;
}

int OssRead(unsigned char *data, int len, int timeout_ms)
{
    (void)data;
    (void)len;
    (void)timeout_ms;
//    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    return 0;
}

int OssSendEndFlag()
{
    int ret = 0;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    if (pOssMgr->nc == 0) {
        ret = -5;
        goto END_FUN;
    }
    mg_send_http_chunk(pOssMgr->nc, "", 0);
    unsigned int timemark = DjyGetSysTime();
    while (pOssMgr->nc && pOssMgr->status == 0) {
        mg_mgr_poll(&pOssMgr->mgr, 500);
        Djy_EventDelay(10*1000);
        if (DjyGetSysTime()-timemark > 5000*1000) {
            printf("OssSendEndFlag: timeout break!\r\n");
            ret = -2;
            break;
        }
    }
    if (pOssMgr->status == 0) {
        printf("error: timeout!!!\r\n");
        ret = -2;
    }
    if (pOssMgr->status < 0) {
        printf("error: http respond error!\r\n");
        ret = pOssMgr->status;
    }
    if (pOssMgr->status > 0) {
        printf("info: successfull !\r\n");
        ret = 0;
    }
END_FUN:

    return ret;
}

int OssClose()
{
    int ret = 0;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    mg_mgr_free(&pOssMgr->mgr);
    memset(&pOssMgr->mgr, 0, sizeof(struct mg_mgr));
    pOssMgr->nc = 0;
    pOssMgr->status = 0;
    return ret;
}

#if 0
int sniprintf(char *buf, size_t buf_size, const char *fmt, ...)
{
    int result;
    va_list ap;
    va_start(ap, fmt);
    result = c_vsnprintf(buf, buf_size, fmt, ap);
    va_end(ap);
    return result;
}
#else
int sniprintf(char *buf, size_t buf_size, const char *fmt, ...)
{
    int result;
    va_list ap;
    va_start(ap, fmt);
    char* pnew = (char*)malloc(strlen(fmt) + 1);
    if (pnew == 0) return 0;
    strcpy(pnew, fmt);
    char *p = pnew;
    while (*p) {
        if (*p == '%' && *p != 0 && *(p + 1) == '.') {
            p++;
            *p = '0';
        }
        p++;
    }
    result = c_vsnprintf(buf, buf_size, pnew, ap);
    va_end(ap);
    if (pnew) free(pnew);
    return result;
}
#endif
int siscanf(const char *ibuf, const char *fmt, ...)
{
    va_list ap;
    int ret;
    va_start(ap, fmt);
    ret = vsscanf(ibuf, fmt, ap);
    va_end(ap);
    return(ret);
}
typedef int (*FUN_EVENT_NOTIFY) ();
typedef int (*FUN_NET_PROC)(unsigned char *data, int len, int total, int timeout);
typedef struct StDlFileData {
    int media_type;//0: mp3, 1: wav
    volatile int is_break;
    volatile int status;
    volatile int is_start;
    volatile unsigned int timemark;
    volatile unsigned int timeout;
    volatile unsigned int body_size;
    volatile unsigned int mark_pos;
    FUN_NET_PROC fun_net_do;
    FUN_EVENT_NOTIFY notify;
    volatile int is_record;//如果播放是的自己录音的数据，设置为1； 如果设置为2，则会下载网上录音的时候同时保存到录音缓冲区； 0是播放原音。
    char url[512];
}StDlFileData;

struct StDlFileData gDlFileData;

int WavPlayData(unsigned char *buf, int len, unsigned int timeout);
int Mp3PlayData(unsigned char *buf, int len, unsigned int timeout);
int RecordCacheWrite(char *data, int len);
int play_data(unsigned char *data, int len, s32 total_len,int timeout)
{
    int ret = 0;
    int cached_cnts = 0;
    int mark = 0;
    unsigned int timemark = 0;
    struct StDlFileData *pUserData = &gDlFileData;

    timemark = DjyGetSysTime()/1000;
    mark = 0;
    while (1) {
        if (pUserData->is_break) break;
        switch(pUserData->media_type) {
            case 0://mp3
                ret = Mp3PlayData(&data[mark], len-mark, timeout);//设置了非阻塞模式
                break;
            case 1://wav
                ret = WavPlayData(&data[mark], len-mark, timeout);//设置了非阻塞模式
                break;
            default:
                break;
        }
        if (ret > 0) {
            mark += ret;
            timemark = DjyGetSysTime()/1000;
        }
        else {
            Djy_EventDelay(20*1000);
        }
        if (mark >= len) {
            break;
        }
        if (media_is_stop()) { //播放暂停，需要一直等待
            timemark = DjyGetSysTime()/1000;
        }
        if ((unsigned int)(DjyGetSysTime()/1000) - timemark > timeout) {
            break;
        }
    }
    return mark;
}
void WavDataEndPlay ();
void Mp3DataEndPlay ();
static void cb_http_download_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    (void)ev_data;
    struct StDlFileData *pUserData = nc->user_data;
    struct mbuf *io = &nc->recv_mbuf;
    int ret = 0;
    s32 abc;
    switch (ev) {
    case MG_EV_CONNECT:
        {
            if (*(s32*)ev_data != -1)
            {
                pUserData->is_start = 1;
				int opt = 10 * 1024;
				if (0 != setsockopt(nc->sock, SOL_SOCKET, SO_RCVBUF, &opt, 4))
				{
					printf("Client: setsockopt failed!\n\r");
					nc->flags |= MG_F_CLOSE_IMMEDIATELY;
					pUserData->status = -1;
				}
				if (pUserData->is_break) {
					nc->flags |= MG_F_CLOSE_IMMEDIATELY;
					pUserData->status = -1;
				}
                pUserData->body_size = 0;
                pUserData->mark_pos = 0;
            }
        }
        break;
    case MG_EV_RECV:
        {
            //printf("=== read: %d ===!\r\n", io->len);

            if (pUserData->is_start == 1) {
                pUserData->body_size = 0;
                pUserData->mark_pos = 0;
                pUserData->is_start = 0;
                if (0 == memcmp(io->buf, "HTTP/1.1 200", strlen("HTTP/1.1 200"))||
                    0 == memcmp(io->buf, "HTTP/1.1 206", strlen("HTTP/1.1 206"))) {

                    printf("info: http download recv header ok!\r\n");
                    char *p = 0;
                    if ((p = strstr(io->buf, "\r\n\r\n"))) {
                        p = p + 4; //p pto data
                        //memset(&shm, 0, sizeof(struct http_message));
                        //mg_http_parse_headers(io->buf, p, p-io->buf, &shm);
                        //printf("shm->body.len=%d!\r\n", shm.body.len);
                        //pUserData->body_size = shm.body.len;
                        char *plen = (char*)c_strnstr(io->buf, "Content-Length:", io->len);
                        if (!mg_ncasecmp(plen, "Content-Length:", 15)) {
                            pUserData->body_size = atoi(plen+15);
                            printf("media body size: %d!\r\n", pUserData->body_size);
                        }
                        //ret = play_data(p, io->len - (p - io->buf), 1000);
                        if (pUserData->fun_net_do) {
                            ret = pUserData->fun_net_do((unsigned char*)p, io->len - (p - io->buf), pUserData->body_size, 1000);
                        }
                        if (pUserData->body_size > 0 && io->len >= (size_t)(p - io->buf)) {
                            pUserData->mark_pos += io->len - (p - io->buf);
                        }
                        mbuf_remove(io, io->len);
                        pUserData->timemark = DjyGetSysTime() / 1000;
                    }
                    pUserData->status = 0;
                }
                else {
                    if (0 == memcmp(io->buf, "HTTP/1.1 404", strlen("HTTP/1.1 404"))) {
                        pUserData->status = -404;
                    }
                    else {
                        pUserData->status = -1;
                    }
                }
            }
            else {
                //ret = play_data(io->buf, io->len, 1000);
                if (pUserData->fun_net_do) {
                    ret = pUserData->fun_net_do((unsigned char*)io->buf, io->len, pUserData->body_size, 1000);
                }
                if (io->len > 0 && ret > 0) {
                    pUserData->mark_pos += io->len;
                    mbuf_remove(io, ret);
                }
                pUserData->timemark = DjyGetSysTime() / 1000;
            }
            //mbuf_remove(io, io->len);
            if (pUserData->is_break) {
                printf("info: is_break by man, close now!\r\n");
                nc->flags |= MG_F_CLOSE_IMMEDIATELY;
                pUserData->status = -1;
            }
            if (pUserData->body_size > 0 && pUserData->mark_pos >= pUserData->body_size) {
                //printf("info: body_size full(%d,%d), close now!\r\n", pUserData->mark_pos, pUserData->body_size);
                nc->flags |= MG_F_CLOSE_IMMEDIATELY;
                printf("media download done: %d!\r\n", pUserData->mark_pos);
                pUserData->status = 1;
            }
        }
        break;
    case MG_EV_CLOSE:
        {
            if (pUserData->status == 0){
                pUserData->status = -1;
            }
            //pUserData->body_size = 0;
            //pUserData->mark_pos = 0;

        }
        break;
    }
}


/*
 * type: 0->播放原音频， 非0->播放录音
 * ret == 0: 完整音频，不用再下载
 * ret > 0： 部分音频，返回网络下载的范围的起始地址。
 * ret < 0: Cache没任何音频数据，重新下载。
 */
int AudioCachePlay(char *path, int type)
{
    int ret = 0;
    int sz_wrted = 0;
    int sz_total = 0;
    char *p = 0;
    unsigned char *pAddr = 0;
    int length = 0;
    unsigned char *pAddr1 = 0; //记录是环形缓冲区，反转时用到
    int length1 = 0;

    if (type == 0) { /* 预先下载的音频数据 */
        p = AudioCacheGet(path, &sz_wrted, &sz_total);
        if (p && sz_wrted > 0 && sz_wrted == sz_total) {//完整的音频在Cache里，直接返回0
            play_data(p, sz_wrted, 0, 5000);
            ret = 0;
        }
        else if (p && sz_wrted > 0 && sz_wrted < sz_total) {//部分的音频在Cache里，直接返回继续下载范围的起始地址
            play_data(p, sz_wrted, 0, 5000);
            ret = sz_wrted;
        }
        else {//没音频数据，直接网络重头下载并播放。
            ret = -1;
        }
    }
    else {/* 录音缓冲区查找, 预下载音频没包含录音临时地址  */
        printf("info: find in recorder cache!\r\n");
        //RecCachePrintInfo();
        ret = RecCacheFindCache(path, &pAddr, &length, &pAddr1, &length1);
        switch (ret) {
        case 0: //完整的音频，不用网络下载
            printf("info: recoder cache mached complete!\r\n");
            if (length > 0 && pAddr) {
                play_data(pAddr, length, 0, 5000);
                ret = 0;
            }
            if (length1 > 0 && pAddr1) {//环型缓冲反转情况
                play_data(pAddr1, length1, 0, 5000);
                ret = 0;
            }
            break;
#if 0   //不会到这里
        case 1: //部分音频，可以播放前半段，后半段从网络下载
            printf("info: recoder cache mached party!\r\n");
            if (length > 0 && pAddr){
                play_data(pAddr, length, 0, 5000);
                ret = length;
            }
            if (length1 > 0 && pAddr1) {//环型缓冲反转情况
                play_data(pAddr1, length1, 0, 5000);
                ret = length+length1;
            }
            break;
#endif
        case -1://find none, 如果缓冲音频头部被破坏也返回-1
        default:
            printf("info: recoder cache mached none!\r\n");
            ret = -1;
            break;
        }

    }

    return ret;
}

//nNetCnts: 获取网络下载了多少数据
int OssDownloadAndPlay(char *bucket, char *path, int pos_start, int pos_end, int *nNetCnts, int timeout_ms)
{
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct StDlFileData *pUserData = &gDlFileData;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    char *temp = 0;
    char *sign = 0;
    unsigned int time_val = 0;
    int ret = -1;

    printf ("OssDownloadAndPlay: %s!\r\n", path);

    if (!is_wifi_connected()) {
        printf("error: wifi break out!\r\n");
        return -1;
    }

    temp = malloc(1024);
    if (temp == 0) goto FUN_RET;
    memset(temp, 0, 1024);

    sign = malloc(200);
    if (sign == 0) goto FUN_RET;
    memset(sign, 0, 200);

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s", bucket, path);
    GenOssSignature(pOssMgr->OssKeySecret, "GET", 0, GMT, temp, sign, 200);

    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    nc = mg_connect(&mgr, temp, cb_http_download_handler);
    pUserData->status = 0;
    pUserData->fun_net_do = play_data;
    if (nc==0) goto MGR_FREE;

    nc->user_data = pUserData;

    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);

    if (pos_end <= 0) {
        sprintf(temp,
            "GET %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "User-Agent: Mongoose/6.15\r\n"
            "Accept : */*\r\n"
            "Range: bytes=%d-\r\n"
            "Connection: keep-alive\r\n"
            "date: %s\r\n"
            "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, pos_start, GMT, pOssMgr->OssKeyId, sign);
    }
    else {
        sprintf(temp,
            "GET %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "User-Agent: Mongoose/6.15\r\n"
            "Accept : */*\r\n"
            "Range: bytes=%d-%d\r\n"
            "Connection: keep-alive\r\n"
            "date: %s\r\n"
            "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, pos_start, pos_end, GMT, pOssMgr->OssKeyId, sign);
    }
#if 0
    sprintf(temp,
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : */*\r\n"
        "Connection: keep-alive\r\n"
        "date: %s\r\n"
        "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, GMT, pOssMgr->OssKeyId, sign);
#endif
    mg_printf(nc, "%s", temp);
    pUserData->timemark = DjyGetSysTime()/1000;
    pUserData->timeout = timeout_ms;
    while (pUserData->status == 0)
    {
        mg_mgr_poll(&mgr, 500);
        time_val = DjyGetSysTime()/1000 - pUserData->timemark;
        if (time_val > pUserData->timeout) {
            printf("==info: OssDownloadAndPlay break!==\r\n");
            break;
        }
        if (media_is_stop()) {
            pUserData->timemark = DjyGetSysTime()/1000;
        }
    }

MGR_FREE:
    mg_mgr_free(&mgr);
    ret = pUserData->status;
    //pUserData->url[0] = 0;

    if (ret != -404 && pUserData->mark_pos > 0 && nNetCnts) {//ret == -404, not found
        *nNetCnts = pUserData->mark_pos;
        if (pUserData->mark_pos == pUserData->body_size) {
            //下载完成
            ret = 0;
            printf("----------------1--pos=%d, body_size=%d------------------\r\n", pUserData->mark_pos, pUserData->body_size);
        }
        else {
            //下载部分
            printf("warning: close by peer!\r\n");
            printf("----------------2--pos=%d, body_size=%d-------------------\r\n", pUserData->mark_pos, pUserData->body_size);
            ret = -2;
        }
    }
    pUserData->body_size = 0;
    pUserData->mark_pos = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    if (sign) {
        free(sign);
        sign = 0;
    }
    return ret;
}

int WebDownloadAndPlay(char *host, int port, char *path, int timeout_ms)
{
//    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct StDlFileData *pUserData = &gDlFileData;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;
    char *temp = 0;
    int ret = -1;
    unsigned int time_val = 0;

    printf ("WebDownloadAndPlay: %s!\r\n", path);

    if (!is_wifi_connected()){
        pUserData->url[0] = 0;
        return -1;
    }

    temp = malloc(1024);
    if (temp == 0) goto FUN_RET;
    memset(temp, 0, 1024);

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", host, port);
    nc = mg_connect(&mgr, temp, cb_http_download_handler);
    pUserData->status = 0;
    pUserData->fun_net_do = play_data;
    if (nc == 0)  goto MGR_FREE;

    nc->user_data = pUserData;

    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    sprintf(temp,
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : */*\r\n"
        "Connection: keep-alive\r\n"
        "date: %s\r\n\r\n", path, host, GMT);
    mg_printf(nc, "%s", temp);
    pUserData->timemark = DjyGetSysTime()/1000;
    pUserData->timeout = 300000;
    while (pUserData->status == 0)
    {
        mg_mgr_poll(&mgr, 500);
        time_val = DjyGetSysTime()/1000 - pUserData->timemark;
        if (time_val > pUserData->timeout) {
            printf("==info: WebDownloadAndPlay break!==\r\n");
            pUserData->status = 0;
            break;
        }
        if (media_is_stop()) {
            pUserData->timemark = DjyGetSysTime()/1000;
        }
        Djy_EventDelay(10*1000);
    }
    ret = pUserData->status;

MGR_FREE:
    mg_mgr_free(&mgr);
    pUserData->url[0] = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    return ret;
}

//void *fdo: pointer of function: typedef int (*FUN_NET_PROC)(unsigned char *data, int len, int total, int timeout);
int OssDownload(char *bucket, char *path, void *fdo, int timeout_ms)
{
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct StDlFileData userData;
    struct StDlFileData *pUserData = 0;
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    char *temp = 0;
    char *sign = 0;
    int ret = -1;
    unsigned int time_val = 0;

    printf ("oss download firmware: %s!\r\n", path);

    if (!is_wifi_connected()) {
        printf("error: wifi break out!\r\n");
        return -1;
    }

    temp = malloc(1024);
    if (temp == 0) goto FUN_RET;
    memset(temp, 0, 1024);

    sign = malloc(200);
    if (sign == 0) goto FUN_RET;
    memset(sign, 0, 200);

    memset(&userData, 0, sizeof(struct StDlFileData));
    pUserData = &userData;
    pUserData->fun_net_do = fdo;

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s", bucket, path);
    GenOssSignature(pOssMgr->OssKeySecret, "GET", 0, GMT, temp, sign, 200);

    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    nc = mg_connect(&mgr, temp, cb_http_download_handler);
    pUserData->status = 0;

    if (nc == 0) goto MGR_FREE;

    nc->user_data = pUserData;

    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    sprintf(temp,
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : */*\r\n"
        "Connection: keep-alive\r\n"
        "date: %s\r\n"
        "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, GMT, pOssMgr->OssKeyId, sign);
    mg_printf(nc, "%s", temp);
    pUserData->timemark = DjyGetSysTime()/1000;
    pUserData->timeout = 10000;
    while (pUserData->status == 0)
    {
        mg_mgr_poll(&mgr, 500);
        time_val = DjyGetSysTime()/1000 - pUserData->timemark;
        if (time_val > pUserData->timeout) {
            printf("==info: OssDownload break!==\r\n");
            break;
        }
        if (media_is_stop()) {
            pUserData->timemark = DjyGetSysTime()/1000;
        }
    }
    ret = 0;

MGR_FREE:
    mg_mgr_free(&mgr);
    pUserData->url[0] = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    if (sign) {
        free(sign);
        sign = 0;
    }

    return ret;
}

int WebDownload(char *host, int port, char *path, void *fdo, int timeout_ms)
{
//    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct StDlFileData userData;
    struct StDlFileData *pUserData = 0;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;
    char *temp = 0;
    int ret = -1;
    unsigned int time_val = 0;

    printf ("WebDownload: %s!\r\n", path);

    if (!is_wifi_connected()){
        pUserData->url[0] = 0;
        return -1;
    }

    temp = malloc(1024);
    if (temp == 0) goto FUN_RET;
    memset(temp, 0, 1024);

    memset(&userData, 0, sizeof(struct StDlFileData));
    pUserData = &userData;
    pUserData->fun_net_do = fdo;

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", host, port);
    nc = mg_connect(&mgr, temp, cb_http_download_handler);
    pUserData->status = 0;
    if (nc == 0)  goto MGR_FREE;

    nc->user_data = pUserData;

    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    sprintf(temp,
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : */*\r\n"
        "Connection: keep-alive\r\n"
        "date: %s\r\n\r\n", path, host, GMT);
    mg_printf(nc, "%s", temp);
    pUserData->timemark = DjyGetSysTime()/1000;
    pUserData->timeout = 20000;
    while (pUserData->status == 0)
    {
        mg_mgr_poll(&mgr, 500);
        time_val = DjyGetSysTime()/1000 - pUserData->timemark;
        if (time_val > pUserData->timeout) {
            printf("==info: WebDownloadAndPlay break!==\r\n");
            pUserData->status = 0;
            break;
        }
        if (media_is_stop()) {
            pUserData->timemark = DjyGetSysTime()/1000;
        }
        Djy_EventDelay(10*1000);
    }
    ret = pUserData->status;

MGR_FREE:
    mg_mgr_free(&mgr);
    pUserData->url[0] = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    return ret;
}

//void *fdo: pointer of function: typedef int (*FUN_NET_PROC)(unsigned char *data, int len, int total, int timeout);
int OssDownloadRange(char *bucket, char *path, int pos_start, int pos_end, void *fdo, int timeout_ms)
{
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    struct StDlFileData userData;
    struct StDlFileData *pUserData = 0;
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    char *temp = 0;
    char *sign = 0;
    int ret = -1;
    unsigned int time_val = 0;

    //printf("oss download firmware: %s!\r\n", path);

    if (!is_wifi_connected()) {
        printf("error: wifi break out!\r\n");
        return -1;
    }

    temp = malloc(1024);
    if (temp == 0) goto FUN_RET;
    memset(temp, 0, 1024);

    sign = malloc(200);
    if (sign == 0) goto FUN_RET;
    memset(sign, 0, 200);

    memset(&userData, 0, sizeof(struct StDlFileData));
    pUserData = &userData;
    pUserData->fun_net_do = fdo;

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s", bucket, path);
    GenOssSignature(pOssMgr->OssKeySecret, "GET", 0, GMT, temp, sign, 200);

    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    nc = mg_connect(&mgr, temp, cb_http_download_handler);
    pUserData->status = 0;

    if (nc == 0) goto MGR_FREE;

    nc->user_data = pUserData;

    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    if (pos_end <= 0) {
        sprintf(temp,
            "GET %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "User-Agent: Mongoose/6.15\r\n"
            "Accept : */*\r\n"
            "Range: bytes=%d-\r\n"
            "Connection: keep-alive\r\n"
            "date: %s\r\n"
            "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, pos_start, GMT, pOssMgr->OssKeyId, sign);
    }
    else {
        sprintf(temp,
            "GET %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "User-Agent: Mongoose/6.15\r\n"
            "Accept : */*\r\n"
            "Range: bytes=%d-%d\r\n"
            "Connection: keep-alive\r\n"
            "date: %s\r\n"
            "authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, pos_start, pos_end, GMT, pOssMgr->OssKeyId, sign);
    }
    mg_printf(nc, "%s", temp);
    pUserData->timemark = DjyGetSysTime() / 1000;
    pUserData->timeout = 300000;
    while (pUserData->status == 0)
    {
        mg_mgr_poll(&mgr, 500);
        time_val = DjyGetSysTime() / 1000 - pUserData->timemark;
        if (time_val > pUserData->timeout) {
            printf("==info: OssDownloadRange break!==\r\n");
            break;
        }
        if (media_is_stop()) {
            pUserData->timemark = DjyGetSysTime() / 1000;
        }
    }
    ret = 0;

MGR_FREE:
    mg_mgr_free(&mgr);
    pUserData->url[0] = 0;

FUN_RET:
    if (temp) {
        free(temp);
        temp = 0;
    }
    if (sign) {
        free(sign);
        sign = 0;
    }

    return ret;
}

int WavIsBusy();
int Mp3IsBusy();
int OssPlayerIsBusy()
{
    struct StDlFileData *pUserData = &gDlFileData;

    //int dloading = pUserData->url[0] != 0;
    //if (dloading) return 1;
    if (pUserData->media_type == 0) {//mp3
        return Mp3IsBusy();
    }
    if (pUserData->media_type == 1) {//wav
        return WavIsBusy();
    }
    return 0;
}

int mp3_stream_reset();
int wav_stream_reset();
void AudioDevReset();
int OssBreakPlayer()
{
    struct StDlFileData *pUserData = &gDlFileData;
    if (pUserData->url[0]==0) {
        pUserData->is_break = 0;
        pUserData->is_start = 0;
        pUserData->status = 0;
        pUserData->timemark = 0;
        pUserData->is_break = 0;
        pUserData->timeout = 0;

        mp3_stream_reset();
        wav_stream_reset();
        AudioDevReset();
    }
    else {
        pUserData->is_break = 1;
        pUserData->timeout = 0;

        mp3_stream_reset();
        wav_stream_reset();
        AudioDevReset();

        unsigned int timemark = DjyGetSysTime()/1000;
        while (pUserData->url[0]!=0) {
            Djy_EventDelay(300*1000);
            if (DjyGetSysTime()/1000 - timemark > 10000) {
                printf("info: OssBreakPlayer timeout(10s) break!\r\n");
                break;
            }
        }
        pUserData->url[0] = 0;
        pUserData->is_break = 0;
        pUserData->is_start = 0;
        pUserData->status = 0;
        pUserData->timemark = 0;
        pUserData->is_break = 0;
        pUserData->timeout = 0;
    }
    return 0;
}
void SetMp3DoneMsgCB(int (*event_cb) ());
void SetWavDoneMsgCB(int (*event_cb) ());

/*
 *  path: http地址
 *  is_record: 是播放自己的录音的音频还是播放原音，这里导致下载过程中是否保存多一份数据到本地有关。
 *  event_cb： 回调事件，这里是音频播放完成标志。
 */
int OssPlayMedia(char *path, int is_record, int (*event_cb) ())
{
    struct StDlFileData *pUserData = &gDlFileData;
    OssBreakPlayer();

    path = GetHttpPath(path);

    printf("OssPlayMedia: is_record=%d, path=%s!\r\n", is_record, path);

    if (path==0) return -1;

    if (strstr(path, ".mp3")) {
        //mp3_stream_reset();//clear buffer of the pre-mp3
        pUserData->media_type = 0;
        pUserData->is_record = is_record;
        SetMp3DoneMsgCB(event_cb);
        pUserData->notify = event_cb;

    }
    else if (strstr(path, ".wav")){
        //wav_stream_reset();
        pUserData->media_type = 1;
        pUserData->is_record = is_record;
        SetWavDoneMsgCB(event_cb);
        pUserData->notify = event_cb;
    }
    else {
        //mp3_stream_reset();
        pUserData->is_record = is_record;
        pUserData->media_type = 0;
    }
    strncpy(pUserData->url, path, sizeof(pUserData->url));   //todo:此过程被打断？

    return 0;
}

typedef struct StOssFileInfo {
    int size;
    int append_pos;
}StOssFileInfo;

typedef struct StOssUserData {
    int status;
    union {
        int append_pos;
        struct StOssFileInfo finfo;
    };
}StOssUserData;

volatile int gflag_oss_break = 0;

void oss_append_break()
{
    gflag_oss_break = 1;
}


static void cb_http_oss_fileinfo_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StOssUserData *pData = (struct StOssUserData*)nc->user_data;
    struct mg_str *s;
    char buf[100] = "";
//    int int_append_pos = 0;

    if (pData == 0) {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        return;
    }

    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            pData->status = -1;
        }
        break;
    case MG_EV_HTTP_CHUNK:
    {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        if (pData->status == 0) {
            if (hm->resp_code == 200) {
                pData->status = hm->resp_code;
            }
            else {
                pData->status = 0 - hm->resp_code;
            }

            if ((s = mg_get_http_header(hm, "x-oss-next-append-position")) != NULL) {
                memcpy(buf, s->p, s->len);
                memcpy(buf + s->len, "", 1);
                pData->finfo.append_pos = atoi(buf);
            }
            if ((s = mg_get_http_header(hm, "Content-Length")) != NULL) {
                memcpy(buf, s->p, s->len);
                memcpy(buf + s->len, "", 1);
                pData->finfo.size = atoi(buf);
            }
            if ((s = mg_get_http_header(hm, "Content-Type")) != NULL) {
                memcpy(buf, s->p, s->len);
                memcpy(buf + s->len, "", 1);
                printf("Content-Type: %s\r\n", buf);
            }
            if ((s = mg_get_http_header(hm, "Last-Modified")) != NULL) {
                memcpy(buf, s->p, s->len);
                memcpy(buf + s->len, "", 1);
                printf("Last-Modified: %s\r\n", buf);
            }
        }
        break;
    }

    case MG_EV_CLOSE:
        break;
    default:
        break;
    }
}


int OssGetFileInfo(char *bucket, char *path, struct StOssFileInfo *finfo, int timeout_ms)
{
    struct mg_mgr mgr;
    struct mg_connection *nc = 0;
    struct StOssUserData user_data;

    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    char GMT[60] = { 0 };
     unsigned int timestamp = 0;
     char *temp = 0;
     char *sign = 0;
     int ret = 0;

     if (!is_wifi_connected()) return -1;


     temp = malloc(1024);
     if (temp == 0) goto ERROR_RET;
     memset(temp, 0, 1024);

     sign = malloc(200);
     if (sign == 0) goto ERROR_RET;
     memset(sign, 0, 200);

     GetTimeStamp(&timestamp, timeout_ms);
     GTM_TIME(timestamp, GMT, sizeof(GMT));

     memset(temp, 0, 1024);
     sprintf(temp, "/%s%s", bucket, path);

    GenOssSignature(pOssMgr->OssKeySecret, "HEAD", 0, GMT, temp, sign, 200);

    memset(&mgr, 0, sizeof(struct mg_mgr));
    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    nc = mg_connect(&mgr, temp, cb_http_oss_fileinfo_handler);

    memset(&user_data, 0, sizeof(struct StOssUserData));
    if (nc == 0) goto ERROR_RET;

    nc->user_data = &user_data;
    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    sprintf(temp,
        "HEAD %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Date: %s\r\n"
        "Authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, GMT, pOssMgr->OssKeyId, sign);
    mg_printf(nc, "%s", temp);

    unsigned int timemark = DjyGetSysTime() / 1000;
    while (user_data.status == 0) {
        if (DjyGetSysTime()/1000-timemark > timeout_ms) {
            printf("%s: timeout break!\r\n", __FUNCTION__);
            break;
        }
        mg_mgr_poll(&mgr, 500);
        Djy_EventDelay(10 * 1000);
    }
    if (user_data.status == 200) {
        printf("success: oss file exist ok, file_size=%d!\r\n", user_data.finfo.size);
        if (finfo) {
            memcpy(finfo, &user_data.finfo, sizeof(struct StOssFileInfo));
        }
    }
    ret = user_data.status;

ERROR_RET:
    mg_mgr_free(&mgr);
    if (temp) free(temp);
    if (sign) free(sign);
    return ret;
}

static void cb_http_oss_delete_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StOssUserData *pData = (struct StOssUserData*)nc->user_data;
    struct mg_str *s;
    char str_append_pos[20] = "";
    int int_append_pos = 0;

    if (gflag_oss_break == 1) {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        gflag_oss_break = 0;
        pData->status = -100;
        return;
    }

    if (pData == 0) {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        return;
    }

    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            pData->status = -1;
        }
        break;
    case MG_EV_HTTP_REPLY:
    {
        switch (hm->resp_code) {
        case 200:
        case 204:
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pData->status = 204;
            break;
        case 302:
            pData->status = -302;
            break;
        case 401:
            pData->status = -401;
            break;
        case 409:
            pData->status = -409;
//            if ((s = mg_get_http_header(hm, "x-oss-next-append-position")) != NULL) {
//                memcpy(str_append_pos, s->p, s->len);
//                memcpy(str_append_pos + s->len, "", 1);
//                int_append_pos = atoi(str_append_pos);
//                if (int_append_pos > 0 && int_append_pos < 0xEFFFFFFF) {
//                    pData->append_pos = int_append_pos;
//                }
//                printf("=========>%d<--------\r\n", int_append_pos);
//            }
            break;
        default:
            pData->status = -1;
            break;
        }
        break;
    }
    case MG_EV_CLOSE:
        break;
    default:
        break;
    }
}


int OssDeleteFile(char *bucket, char *path, int timeout_ms)
{
    struct mg_mgr mgr;
    struct mg_connection *nc = 0;
    struct StOssUserData user_data;

    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    char *temp = 0;
    char *sign = 0;
    int ret = 0;

    if (!is_wifi_connected()) return -1;


    temp = malloc(1024);
    if (temp == 0) goto ERROR_RET;
    memset(temp, 0, 1024);

    sign = malloc(200);
    if (sign == 0) goto ERROR_RET;
    memset(sign, 0, 200);

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s", bucket, path);
    GenOssSignature(pOssMgr->OssKeySecret, "DELETE", 0, GMT, temp, sign, 200);

    memset(&mgr, 0, sizeof(struct mg_mgr));
    mg_mgr_init(&mgr, NULL);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);
    nc = mg_connect(&mgr, temp, cb_http_oss_delete_handler);

    memset(&user_data, 0, sizeof(struct StOssUserData));
    if (nc == 0) goto ERROR_RET;

    nc->user_data = &user_data;
    mg_set_protocol_http_websocket(nc);

    memset(temp, 0, 1024);
    sprintf(temp,
        "DELETE %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Accept : *\/*\r\n"
        "Connection: keep-alive\r\n"
        "Date: %s\r\n"
        "Authorization: OSS %s:%s\r\n\r\n", path, pOssMgr->Host, GMT, pOssMgr->OssKeyId, sign);
    mg_printf(nc, "%s", temp);

    unsigned int timemark = DjyGetSysTime() / 1000;
    while (user_data.status == 0) {
        if (DjyGetSysTime()/1000-timemark > timeout_ms) {
            printf("%s: timeout break!\r\n", __FUNCTION__);
            break;
        }
        if (gflag_oss_break == 1) {
            gflag_oss_break = 0;
            break;
        }
        mg_mgr_poll(&mgr, 500);
        Djy_EventDelay(10 * 1000);
    }
    if (user_data.status == 204) {
        printf("success: %s, delete file ok, file_pos=%d!\r\n", __FUNCTION__, user_data.append_pos);
    }
//    if (user_data.status == -409) {
//        printf("warning: %s file exist, req_pos=%d, file_pos=%d!\r\n", __FUNCTION__, user_data.append_pos);
//    }
    ret = user_data.status;

ERROR_RET:
    mg_mgr_free(&mgr);
    if (temp) free(temp);
    if (sign) free(sign);
    return ret;
}



static void cb_http_oss_append_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StOssUserData *pData = (struct StOssUserData*)nc->user_data;
    struct mg_str *s;
    char str_append_pos[20] = "";
    int int_append_pos = 0;

    if (gflag_oss_break == 1) {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        gflag_oss_break = 0;
        pData->status = -100;
        return;
    }

    if (pData == 0) {
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        pData->status = -101;
        return;
    }

    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data == -1) {
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pData->status = -1;
            break;
        }
        int opt = 0;
        opt = 1024 * 8;
        if (0 != setsockopt(nc->sock, SOL_SOCKET, SO_SNDBUF, &opt, 4))
        {
            printf("error: cb_http_oss_append_handler set client sndbuf failed!\r\n");
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pData->status = -1;
        }
        break;
    case MG_EV_HTTP_REPLY:
    {
        switch (hm->resp_code) {
        case 200:
            //nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pData->status = 200;
            if ((s = mg_get_http_header(hm, "x-oss-next-append-position")) != NULL) {
                memcpy(str_append_pos, s->p, s->len);
                memcpy(str_append_pos + s->len, "", 1);
                int_append_pos = atoi(str_append_pos);
                if (int_append_pos > 0 && int_append_pos < 0xEFFFFFFF) {
                    pData->append_pos = int_append_pos;
                }
            }
            break;
        case 302:
            pData->status = -302;
            break;
        case 401:
            pData->status = -401;
            break;
        case 409:
            pData->status = -409;
            if ((s = mg_get_http_header(hm, "x-oss-next-append-position")) != NULL) {
                memcpy(str_append_pos, s->p, s->len);
                memcpy(str_append_pos + s->len, "", 1);
                int_append_pos = atoi(str_append_pos);
                if (int_append_pos > 0 && int_append_pos < 0xEFFFFFFF) {
                    pData->append_pos = int_append_pos;
                }
                printf("warning: 409, line(%s, %d, next append pos=%d)\r\n", __FUNCTION__, __LINE__, int_append_pos);
            }
            break;
        default:
            pData->status = -1;
            break;
        }
        break;
    }
    case MG_EV_CLOSE:
        printf("INFO: MG_EV_CLOSE!\r\n");
        pData->status = -300;
        break;
    default:
        break;
    }
}


int IsOssSocketInit = 0;
static int OssSocketDeInit(struct mg_mgr *pmgr)
{
    int ret = 0;

    if (IsOssSocketInit == 0) return 1;
    mg_mgr_free(pmgr);
    IsOssSocketInit = 0;
    return ret;
}
//is_reconnect == 1: 强制关掉连接重新连接
static struct mg_mgr *OssSocketInit(char *bucket, char *path, struct StOssUserData *pud, char *host, int port, int is_reconnect)
{
    int ret = -1;
    static struct mg_mgr mgr;
    static struct mg_connection *nc = 0;
    char temp[100];

    if (is_reconnect == 1 && IsOssSocketInit == 1) {
        OssSocketDeInit(&mgr);
    }
    if (IsOssSocketInit == 1) return &mgr;

    mg_mgr_init(&mgr, NULL);
    memset(temp, 0, sizeof(temp));
    sprintf(temp, "%s:%d", host, port);
    nc = mg_connect(&mgr, temp, cb_http_oss_append_handler);

    if (nc && pud) {
        nc->user_data = pud;
    }
    mg_set_protocol_http_websocket(nc);
    mgr.user_data = nc;

    IsOssSocketInit = 1;

    return &mgr;
}


int OssAppendFile(char *bucket, char *path, int pos, int *post_pos, unsigned char *data, int len, int timeout_ms)
{
    struct mg_connection *nc = 0;
    struct StOssUserData user_data;
    struct mg_mgr *pmgmgr = 0;
    struct StAliyunOssMgr *pOssMgr = &gOssMgr;
    char GMT[60] = { 0 };
    unsigned int timestamp = 0;
    char *temp = 0;
    char *sign = 0;
    int ret = 0;
    int is_reconnect = 0;
    if (!is_wifi_connected()) return -1;


    memset(&user_data, 0, sizeof(struct StOssUserData));

    if (pos == 0) {//第一次创建文件，写入位置为0
        is_reconnect = 1;
    }
    else {
        is_reconnect = 0;
    }

    pmgmgr = OssSocketInit(bucket, path, &user_data, pOssMgr->Host, pOssMgr->port, is_reconnect);

    nc = pmgmgr->user_data;

    temp = malloc(1024);
    if (temp == 0) goto ERROR_RET;
    memset(temp, 0, 1024);

    sign = malloc(200);
    if (sign == 0) goto ERROR_RET;
    memset(sign, 0, 200);

    GetTimeStamp(&timestamp, timeout_ms);
    GTM_TIME(timestamp, GMT, sizeof(GMT));

    memset(temp, 0, 1024);
    sprintf(temp, "/%s%s?append&position=%d", bucket, path, pos);
    GenOssSignature(pOssMgr->OssKeySecret, "POST", "audio/wav", GMT, temp, sign, 200);

    memset(temp, 0, 1024);
    sprintf(temp, "%s:%d", pOssMgr->Host, pOssMgr->port);

    memset(temp, 0, 1024);
    sprintf(temp,
        "POST %s?append&position=%d HTTP/1.1\r\n"
        "Host: %s\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Content-Length: %d\r\n"
        "Content-Type: audio/wav\r\n"  /*text/plain*/
        "Accept : *\/*\r\n"
        "Connection: keep-alive\r\n"
        "Date: %s\r\n"
        "Authorization: OSS %s:%s\r\n\r\n", path, pos, pOssMgr->Host, len, GMT, pOssMgr->OssKeyId, sign);
    mg_printf(nc, "%s", temp);

    mg_send(nc, data, len);

    unsigned int timemark = DjyGetSysTime() / 1000;
    while (user_data.status == 0) {
        if (DjyGetSysTime() / 1000 - timemark > timeout_ms) {
            printf("%s: timeout break!\r\n", __FUNCTION__);
            break;
        }
        if (gflag_oss_break == 1) {
            user_data.status = -100;
            gflag_oss_break = 0;
            break;
        }
        mg_mgr_poll(pmgmgr, 500);
        Djy_EventDelay(10 * 1000);
    }

    if (user_data.status == -300) { //close event by peer
        OssSocketDeInit(pmgmgr);
    }
    else if (user_data.status == 0) { //timeout
        OssSocketDeInit(pmgmgr);
    }
    else if (user_data.status == 200) {
        //printf("success: append file ok, file_pos=%d!\r\n", user_data.append_pos);
        if (post_pos) *post_pos = user_data.append_pos;
    }
    else if (user_data.status == -409) {
        printf("warning: file exist, req_pos=%d, file_pos=%d!\r\n", pos, user_data.append_pos);
        if (post_pos) *post_pos = user_data.append_pos;
    }
    else {
        OssSocketDeInit(pmgmgr);
        printf("ERROR: user_data.status=%d!\r\n", user_data.status);
    }
    ret = user_data.status;

ERROR_RET:
    if (temp) free(temp);
    if (sign) free(sign);

    return ret;
}


//flag:
//flag==0, delete file, then append data from position 0; flag!=0: append data to the file last;
//return:
// ret > 0: next position to append
// ret = 0: timeout
// ret < 0: error code
int oss_append_next(char *path, unsigned char *data, int len, int flag, int pos, int timeout_ms)
{
    int ret = 0;
    int status = 0;
    char *bucket = GetOssBucketName();
    int mark = 0;
    int append_pos = -1;
    if (flag == 0) {
        printf("info: OssDeleteFile, url=%s\r\n", path);
        ret = OssDeleteFile(bucket, path, timeout_ms);
        if (ret < 0) return ret; //delete file error
        pos = 0; //清除文件后，从0开始追加文件
    }
    unsigned int timemark = DjyGetSysTime() / 1000;

    do {
        append_pos = -1;
        ret = OssAppendFile(bucket, path, pos, &append_pos, &data[mark], len - mark, timeout_ms);
        //printf("info: %s->OssAppendFile, ret=%d, pos=%d, append_pos=%d, len=%d!\r\n", __FUNCTION__, ret, pos, append_pos, len);
        if (ret > 0 && append_pos > 0) {
            status = append_pos;
            break;
        }
        if (ret < 0 && append_pos >= 0) {
            //这里有种情况，当服务器已经回应了，这边超时，服务器其实已经收到了这个包，这是应该判断
            if (pos <= append_pos && pos + len >= append_pos) {
                printf("==Info: OssAppendFile, ret=%d, pos=%d, len=%d, mark=%d, append_pos=%d!==\r\n", ret, pos, len, mark, append_pos);
                mark = append_pos - pos;
                pos = append_pos;
                if (mark == len) {
                    status = append_pos;
                    printf("==Warning: OssAppendFile, ret=%d, pos=%d, len=%d, mark=%d, append_pos=%d!==\r\n", ret, pos, len, mark, append_pos);
                    break;
                }
            }
            else {
                printf("==ERROR: OssAppendFile, ret=%d, pos=%d, len=%d, mark=%d, append_pos=%d!==\r\n", ret, pos, len, mark, append_pos);
            }

        }
        if ((int)(DjyGetSysTime() / 1000 - timemark) > timeout_ms) {
            status = 0;
            printf("%s: timeout break!\r\n", __FUNCTION__);
            break;
        }
        if (ret <= 0 && append_pos < 0) {
            Djy_EventDelay(500 * 1000);
        }
    } while (1);
    return status;
}


int OssPlay(struct StDlFileData *pUserData, int pos, int *download_pos)
{

    int times = 6;
    int mark = pos;
    int nNetCnts = 0;
    int ret = 0;
    int timeout = 10*1000;

    if (pUserData==0) return -1;

    while (times--) {
        if (pUserData->is_break) {
            if (pUserData->notify) {
                pUserData->notify();
                pUserData->notify = 0;
            }
            break;
        }
        nNetCnts = -1;
        ret = OssDownloadAndPlay(GetOssBucketName(), pUserData->url, mark, -1, &nNetCnts, timeout);
        if (ret == 0) {
            printf("info: OssDownloadAndPlay, nNetCnts=%d!\r\n", nNetCnts);
            if (nNetCnts >= 0) { //正常退出记录录音最新位置，剩下部分可能在本地缓冲区去取。
                mark += nNetCnts;
                if (download_pos) *download_pos = mark;
            }
            break;
        }
        else if (ret == -2) {//下载部分被断开，继续从下载点下载
            if (nNetCnts >= 0) {
                mark += nNetCnts;
                printf("info: %s->OssDownloadAndPlay, mark=%d, nNetCnts=%d!\r\n", __FUNCTION__, mark, nNetCnts);
            }
            timeout += 10*1000;
        }
        else if (ret == -404 && pUserData->notify) {// HTTP: 404 Not Found
            printf("warning: download failed, but need notify the key status!\r\n");
            pUserData->notify();
            pUserData->notify = 0;
            if (download_pos) *download_pos = -1;
            break;
        }
    }
    return 0;
}
/*
 * 返回1：可以找到尾部缓冲数据
 * 返回-1：错误。
 */
int RecordCachePlayTail(char *url, int play_max)
{
    int ret = -1;
    unsigned char *pAddr = 0;
    int length = 0;
    unsigned char *pAddr1 = 0; //记录是环形缓冲区，反转时用到
    int length1 = 0;
    printf("info: RecordCachePlayTail, url=%s, play_max=%d!\r\n", url, play_max);
    if (play_max > 0) {
        pAddr = 0;
        pAddr1 = 0;
        length = 0;
        length1 = 0;
        ret = RecCacheFindCacheTail(url, play_max, &pAddr, &length, &pAddr1, &length1);
        if (ret == 0) {
            printf("info:  RecordCachePlayTail->RecCacheFindCacheTail get ok, size=%d!\r\n", length+length1);
            if (length > 0 && pAddr) {
                play_data(pAddr, length, 0, 5000);
                ret = 1;
            }
            if (length1 > 0 && pAddr1) {//环型缓冲反转情况
                play_data(pAddr1, length1, 0, 5000);
                ret = 1;
            }
        }
    }
    return ret;
}

int OssAndCahcePlay(struct StDlFileData *pUserData, int pos)
{
    int ret = 0;
    int mark = 0;
    int play_max = 0;
    int trys = 0;

    trys = 1;
    mark = pos;
    play_max = 0;
    while (trys--) {
        OssPlay(pUserData, mark, &play_max);
        printf("info: OssPlay, mark=%d, play_max=%d!\r\n", mark, play_max);
        if (play_max < 0) break;
        if (pUserData->is_break) break;
        if (play_max < mark) {
            printf("info: OssAndCahcePlay, play_max=%d, mark=%d!\r\n", play_max, mark);
            break;
        }
        mark = play_max;
        printf("info: RecordCachePlayTail!\r\n");
        ret = RecordCachePlayTail(pUserData->url, play_max);
        if (ret == 1) break;//播放完成
    }
    return 0;
}


ptu32_t oss_EventMain(void)
{
    unsigned int timemark = 0;

    int ret = 0;
    int net_status = 0;
    int nNetCnts = 0;
    int times = 0;
    struct StDlFileData *pUserData = &gDlFileData;
    OssMgrInit();
    struct StOssFileInfo finfo;

    while (1)
    {
        if (pUserData->url[0])
        {
            wav_stream_reset();
            mp3_stream_reset();
            Djy_EventDelay(20*1000);

            ret = AudioCachePlay(pUserData->url, pUserData->is_record);//先播放缓冲里的数据。
            printf("info: AudioCachePlay, url=%s, ret=%d, is_record=%d!\r\n", pUserData->url, ret, pUserData->is_record);
            if (ret > 0)
            {
                printf("info: audio Cache matched party, play first and then download!\r\n");
                if (pUserData->is_record) //如果是录音，不过这里不存在没结束的录音，录音必须结束才能播放。
                {
                    //不用做，暂不支持边上传边播放的情况。
                }
                else {//不是录音数据，则播放原音数据。
                    OssPlay(pUserData, ret, 0);
                }
            }
            else if (ret < 0) //没在缓冲区，从头下载。
            {
                printf("audio Cache matched None, download and play, is_record=%d!\r\n", pUserData->is_record);

                if (pUserData->is_record) {
                    printf("info: OssAndCahcePlay!\r\n");
                    OssAndCahcePlay(pUserData, 0);
                } else {
                    OssPlay(pUserData, 0, 0);
                }
            }
            else
            {   //Do Nothing
                printf("audio Cache matched Whole One, doing nothing!\r\n");
            }
            //音频全部在Cache, 不用下载，直接播放。
            //发送结束标志，暂停播放器。
            printf("Player Send Over Flag: media_type=%d!\r\n", pUserData->media_type);
            if (pUserData->media_type==1) {//wav
                WavDataEndPlay ();
            }
            else if (pUserData->media_type==0) { //mp3
                Mp3DataEndPlay ();
            }
            do {
                if (pUserData->url[0]==0) break;
                if (pUserData->is_break) break;
                Djy_EventDelay(300*1000);
            } while (OssPlayerIsBusy());
            pUserData->url[0] = 0;
        }
        Djy_EventDelay(10*1000);
    }
}
bool_t oss_player_event_init(void)
{
    u16 evtt_oss;
//  void *oss_stack=psram_malloc (0x4000);
    evtt_oss = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_REAL+2, 0, 0, oss_EventMain, /*oss_stack*/NULL, 0x1000/*0x1500*/, "oss_event");
    if(evtt_oss != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_oss, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_oss Event Start Fail!\r\n");
    }

    return true;
}


int OssTest()
{
    int ret = 0;
    OssMgrInit();

//    OssPlayMedia("/test/1.mp3", 0, 0);
//    do {
//        Djy_EventDelay(3000*1000);
//    } while (OssPlayerIsBusy());
//
//    OssPlayMedia("/test/8.mp3", 0, 0);
//    do {
//        Djy_EventDelay(3000*1000);
//    } while (OssPlayerIsBusy());
//
//    ret = OssDownloadAndPlay(GetOssBucketName(), "/test/1.mp3", 0, -1, 5000);

    //ret = WebDownloadAndPlay("bmob-cdn-26324.bmobpay.com", 80, pUserData->url, 5000);

    //ret = OssDownload(GetOssBucketName(), "/test/Ai_EasyTalk_App_Debug.bin", update_bin, 5000);

    //ret = WebDownload("djyos-qinjian.oss-cn-hangzhou.aliyuncs.com", 80, "/test/Ai_EasyTalk_App_Debug.bin", update_bin, 5000);

//      ret = OssGetFileInfo("bucket-kouyutong", "/devtemp/DJYOS0015/xxx_abc.txt", 5000);
//
//      ret = OssDeleteFile("bucket-kouyutong", "/devtemp/DJYOS0015/xxx_abc.txt", 5000);

//    int pos = 0;
//    int next_pos = 0;
//    int is_head = 0;
//    int cnts = 5;
//    while (cnts--) {
//        next_pos  =  oss_append_next("/devtemp/DJYOS0015/xxx_abc.txt", "my name is vincent\r\n", strlen("my name is vincent\r\n"), is_head, pos, 5000);
//        if (next_pos > 0) {
//            pos = next_pos;
//            is_head = 1;
//        }
//    }
#if 0
    ret = OssOpen(GetOssBucketName(), "/test/abc.txt", 5000);
    printf("info: OssOpen, ret=%d!\r\n", ret);
    int n = 100;
    while (n--) {
        ret = OssWrite("this is test!\r\n", strlen("this is test!\r\n"), 100);
    }
    ret = OssClose();
    printf("info: OssClose, ret=%d!\r\n", ret);
#endif
    return 0;
}
