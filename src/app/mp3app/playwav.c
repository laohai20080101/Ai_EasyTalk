#include "playmp3.h"

#include "mad.h"
#include "file.h"
#include "LoopBytes.h"

#include "typedef.h"
#include "audio_pub.h"
#include "LoopRec.h"
#include "recorder.h"

#include "../mem/mem_mgr.h"
#include <board.h>

extern void *psram_malloc (unsigned int size);

#define debug printf
#define assert(x) if (!(x)) {printf("assert error: %s!\r\n", __FUNCTION__); while (1) {};}

int InitLoopBytesBuffer();
struct StLoopBytesMgr *GetMgrOfLoopMgr();

#define WAV_CHANGE_2_CHANNEL

enum {
    WAV_PLAY_START = 0, //设置为0， 有数据， 开始播放
    WAV_PLAY_STOP1, //设置为1，标志启动刹车，还在刹车中;
    WAV_PLAY_STOP2, //设置为2，立即退出播放，清空数据;
    WAV_PLAY_STOP3, //设置为3，完全结束, 在空转。
};

typedef struct StWavStream {
    volatile int end_flag;
    int nReaded;
    int nPos;
    int (*getdata)(unsigned char *buf, int len, int timeout);
}StWavStream;

static struct StWavStream gWavStream;

void WavDataEndPlay ()
{
    struct StWavStream *pMgr = &gWavStream;
    if(GetSpeakerState() == Speaker_on)
        CloseSpeaker();
    pMgr->end_flag = WAV_PLAY_STOP1;
    if(GetSpeakerState() == Speaker_off)
        OpenSpeaker();
}

void WavDataEndPlayByMan ()
{
    struct StWavStream *pMgr = &gWavStream;
    if(GetSpeakerState() == Speaker_on)
        CloseSpeaker();
    pMgr->end_flag = WAV_PLAY_STOP2;
    if(GetSpeakerState() == Speaker_off)
        OpenSpeaker();
}


void WavDataStartPlay ()
{
    struct StWavStream *pMgr = &gWavStream;
    pMgr->end_flag = WAV_PLAY_START;
}


int WavEndDataFalgGet()
{
    struct StWavStream *pMgr = &gWavStream;
    return pMgr->end_flag;
}


int WavIsBusy()
{
    struct StWavStream *pMgr = &gWavStream;
    return pMgr->end_flag==WAV_PLAY_START ||
           pMgr->end_flag==WAV_PLAY_STOP1 ||
           pMgr->end_flag==WAV_PLAY_STOP2;
}

int WavPlayData(unsigned char *buf, int len, unsigned int timeout)
{
    int opt = 0;
    int ret = 0;
    if (len<=0) return -1;
    opt = 1; //More than one byte will notify to the mp3 play process.
    if (GetMgrOfLoopMgr()) {
        WavDataStartPlay ();
        LoopBytesMgrCtrl(GetMgrOfLoopMgr(), OPT_LOOPBYTES_SET_THRESHOLDADD, &opt, 4);
        opt = 1;// pull_mode == 1 , get it instantly
        LoopBytesMgrCtrl(GetMgrOfLoopMgr(), OPT_LOOPBYTES_SET_PULL_MODE, &opt, 4);
        ret = LoopBytesMgrPushPend(GetMgrOfLoopMgr(), buf, len, timeout, 0);

    }
    return ret;
}

CBFUN_PLAYER_DONE gWavNotifyCB = 0;
void SetWavDoneMsgCB(int (*event_cb) ())
{
    gWavNotifyCB = event_cb;
}

extern uint32_t media_data_send(char *buf, uint32_t len);
void SetAudioOptions(unsigned int channnel, unsigned int samplerate);
ptu32_t decode_wav(void)
{
    struct StWavStream *pMgr = &gWavStream;
    int ret = 0;
    int channels = 1;
    int flag_notify = 0;
    __WaveHeader wav_header;
    memset(&wav_header, 0, sizeof(__WaveHeader));
    __WaveHeader *wavhead = &wav_header;

    int length = 1*1024;
    unsigned char *pbuf = psram_malloc(length);
    if (pbuf == 0) {
        printf("error: decode_wav, my_malloc failed!\r\n");
        return 0;
    }
    unsigned char *pPlay = psram_malloc(length*2);
    if (pPlay == 0) {
        printf("error: decode_wav, my_malloc failed!\r\n");
        return 0;
    }
    while (GetMgrOfLoopMgr()) {
        if (WavEndDataFalgGet()) {
            Djy_EventDelay(50*1000);
            continue;
        }
        //get wav header firstly
        ret = LoopBytesMgrPullPend(GetMgrOfLoopMgr(), pbuf,  sizeof(__WaveHeader), 500, 0);//500ms
        if (ret <= 0) {
            goto END_WAV;
        }
        memset(&wav_header, 0, sizeof(__WaveHeader));
        if (ret == sizeof(__WaveHeader)) {
            memcpy(&wav_header, pbuf, ret);
            wavhead = &wav_header;
#if 0
            printf("==sizeof: %d==!\r\n", sizeof(__WaveHeader));
            printf("wavhead->riff.ChunkID = 0X%X!\r\n", wavhead->riff.ChunkID);   //"RIFF"
            printf("wavhead->riff.ChunkSize = %d!\r\n", wavhead->riff.ChunkSize);          //还未确定,最后需要计算
            printf("wavhead->riff.Format = 0X%X!\r\n", wavhead->riff.Format);    //"WAVE"
            printf("wavhead->fmt.ChunkID = 0X%X!\r\n", wavhead->fmt.ChunkID);    //"fmt "
            printf("wavhead->fmt.ChunkSize = %d!\r\n", wavhead->fmt.ChunkSize);          //大小为16个字节
            printf("wavhead->fmt.AudioFormat = 0X%X!\r\n", wavhead->fmt.AudioFormat);      //0X01,表示PCM;0X01,表示IMA ADPCM
            printf("wavhead->fmt.NumOfChannels = %d!\r\n", wavhead->fmt.NumOfChannels);        //單声道
            printf("wavhead->fmt.SampleRate = %d!\r\n", wavhead->fmt.SampleRate);//设置采样速率
            printf("wavhead->fmt.ByteRate = %d!\r\n", wavhead->fmt.ByteRate);//字节速率=采样率*通道数*(ADC位数/8)
            printf("wavhead->fmt.BlockAlign = %d!\r\n", wavhead->fmt.BlockAlign);          //块大小=通道数*(ADC位数/8)
            printf("wavhead->fmt.BitsPerSample = %d!\r\n", wavhead->fmt.BitsPerSample);      //16位PCM
            printf("wavhead->data.ChunkID = 0X%X!\r\n", wavhead->data.ChunkID);   //"data"
            printf("wavhead->data.ChunkSize = 0X%X!\r\n", wavhead->data.ChunkSize);          //数据大小,还需要计算
#endif
            if (wavhead->riff.ChunkID != 0X46464952 ||
                wavhead->riff.Format != 0X45564157 ||
                wavhead->fmt.ChunkID != 0X20746D66||
                wavhead->fmt.AudioFormat != 0x01 )
            {
                printf("warning: wav header wrong!\r\n");
                Djy_EventDelay(50*1000);
                goto END_WAV;
            }
        }
        else {
            Djy_EventDelay(50*1000);
            goto END_WAV;
        }
        //get body of the wav data
        s64 timemark = DjyGetSysTime();
        //int exit_flag = 0;
        while (1) {
            if (pMgr->end_flag == WAV_PLAY_STOP2) break;//立即退出
            ret = LoopBytesMgrPullPend(GetMgrOfLoopMgr(), pbuf,  length, 500, 0);//100ms
            if (ret <= 0) {
                if (pMgr->end_flag == WAV_PLAY_STOP2) {//立即退出
                    printf("warning: decode_wav, event end_flag=2, man break!\r\n");
                    goto END_WAV;
                }
                if (pMgr->end_flag == WAV_PLAY_STOP1) {//已经设置停止播放标志，且数据已经为空，则退出。
                    printf("warning: decode_wav, event end_flag=1, normal break!\r\n");
                    goto END_WAV;
                }
                if (DjyGetSysTime() - timemark > 600000*1000) {//超时退出，没设置任何停止事件。
                    printf("warning: decode_wav, event timeout, error break!\r\n");
                    goto END_WAV;
                }
                Djy_EventDelay(10*1000);
                continue;
            }

            char *pMedia = 0;
#ifdef WAV_CHANGE_2_CHANNEL
            if (wavhead->fmt.NumOfChannels == 1) {
                if (ret%2==1) { //获取偶数
                    int ret_tmp = LoopBytesMgrPullPend(GetMgrOfLoopMgr(), &pbuf[ret],  1, 5000, 0);//100ms
                    if (ret_tmp==1) {
                        ret += 1;
                    }
                }
                unsigned short *pSrc = (unsigned short *)pbuf;
                unsigned short *pDst =  (unsigned short *)pPlay;
                channels = 2;
                int i = 0;
                for (i=0; i<ret/2; i++) {
                    pDst[i*2] = pSrc[i];
                    pDst[i*2+1] = pSrc[i];
                }
                ret = ret * 2;
                pMedia = (char *)pPlay;
            }
            else  {
                pMedia = (char*)pbuf;
                channels = wavhead->fmt.NumOfChannels;
            }
#else
            pMedia = pbuf;
            channels = wavhead->fmt.NumOfChannels;
#endif
            int writed = 0;
            int total = ret;
            SetAudioOptions(channels, wavhead->fmt.SampleRate);
            while (1) {
                if (pMgr->end_flag == WAV_PLAY_STOP2) break;//立即退出
                ret = media_data_send(pMedia+writed, total-writed);
                if (ret > 0) {
                    writed += ret;
                }
                if (writed < total) {
                    Djy_EventDelay(10*1000);
                }
                else {
                    break;
                }
            }
            timemark = DjyGetSysTime();
        }//ended while (1) {
END_WAV:
        //LoopBytesMgrReset(GetMgrOfLoopMgr());
        while (media_is_stop()){
            Djy_EventDelay(100*1000);
        }
        pMgr->end_flag = WAV_PLAY_STOP3;//完全结束。
        if (gWavNotifyCB) {
            printf("info: Wav Notify Callback!\r\n");
            gWavNotifyCB();
        }
        if(GetSpeakerState() == Speaker_on)
            CloseSpeaker();
    }
    return ret;
}

int wav_play_from_file(char *path)
{
    int ret = 0;
    int length = 1024;
    FILE *f = fopen(path, "rb");
    if (f == 0) {
        printf("error: wav_play_from_file, path=%s!\r\n", path);
        return -1;
    }
    fseek(f, 0L, SEEK_END);
    int file_size = ftell(f);
    fseek(f, 0L, SEEK_SET);
    printf("info: file size: %d!\r\n", file_size);

    unsigned char *p = (unsigned char*)malloc(length);
    if (p == 0) goto ERROR_RET;

    int n = 0;
    int pos = 0;
    int offset = 0;
    __WaveHeader wav_header;
    memset(&wav_header, 0, sizeof(__WaveHeader));

    void AudioDevReset();
    int mp3_stream_reset();
    int wav_stream_reset();
    AudioDevReset();
    mp3_stream_reset();
    wav_stream_reset();

    __WaveHeader *wavhead = &wav_header;
    if (length >= (int)sizeof(__WaveHeader)) {
        n = fread(p, 1, sizeof(__WaveHeader), f);

        memcpy(&wav_header, p, n);
        wavhead = &wav_header;

        offset += n;

        printf("==sizeof: %d==!\r\n", sizeof(__WaveHeader));
        printf("wavhead->riff.ChunkID = 0X%X!\r\n", wavhead->riff.ChunkID);   //"RIFF"
        printf("wavhead->riff.ChunkSize = %d!\r\n", wavhead->riff.ChunkSize);          //还未确定,最后需要计算
        printf("wavhead->riff.Format = 0X%X!\r\n", wavhead->riff.Format);    //"WAVE"
        printf("wavhead->fmt.ChunkID = 0X%X!\r\n", wavhead->fmt.ChunkID);    //"fmt "
        printf("wavhead->fmt.ChunkSize = %d!\r\n", wavhead->fmt.ChunkSize);          //大小为16个字节
        printf("wavhead->fmt.AudioFormat = 0X%X!\r\n", wavhead->fmt.AudioFormat);      //0X01,表示PCM;0X01,表示IMA ADPCM
        printf("wavhead->fmt.NumOfChannels = %d!\r\n", wavhead->fmt.NumOfChannels);        //單声道
        printf("wavhead->fmt.SampleRate = %d!\r\n", wavhead->fmt.SampleRate);//设置采样速率
        printf("wavhead->fmt.ByteRate = %d!\r\n", wavhead->fmt.ByteRate);//字节速率=采样率*通道数*(ADC位数/8)
        printf("wavhead->fmt.BlockAlign = %d!\r\n", wavhead->fmt.BlockAlign);          //块大小=通道数*(ADC位数/8)
        printf("wavhead->fmt.BitsPerSample = %d!\r\n", wavhead->fmt.BitsPerSample);      //16位PCM
        printf("wavhead->data.ChunkID = 0X%X!\r\n", wavhead->data.ChunkID);   //"data"
        printf("wavhead->data.ChunkSize = 0X%X!\r\n", wavhead->data.ChunkSize);          //数据大小,还需要计算
        SetAudioOptions(wavhead->fmt.NumOfChannels, wavhead->fmt.SampleRate);
        ret = WavPlayData(p, n, 5000);
    }

    while (1) {
        if (offset >= file_size) break;
        n = fread(p, 1, length, f);

        if (n > 0) {
            offset += n;
            pos = 0;
            while (1) {
                if (pos >= n) break;
                ret = WavPlayData(p + pos, n - pos, 5000);
                if (ret > 0) {
                    pos += ret;
                }
                else {
                    printf("wav_play_from_file->WavPlayData, ret=%d!\r\n", ret);
                    Djy_EventDelay(300 * 1000);
                }
            }
        }
        else {
            printf("wav_play_from_file->fread, n=%d!\r\n", n);
            Djy_EventDelay(300 * 1000);
        }
    }

ERROR_RET:
    WavDataEndPlay ();
    if (f) fclose(f);
    if (p) free(p);
    return 0;
}

int wav_stream_reset()
{
    int cnt = 0;
    struct StWavStream *pMgr = &gWavStream;
    if (pMgr->end_flag != WAV_PLAY_STOP3) {
        pMgr->end_flag = WAV_PLAY_STOP2;
        while (pMgr->end_flag != WAV_PLAY_STOP3){
            Djy_EventDelay(100 * 1000);
            if (cnt++ > 10) break;
        }
    }
    if (GetMgrOfLoopMgr()) {
        LoopBytesMgrReset(GetMgrOfLoopMgr());
    }
    return 0;
}

int media_init();

int wav_module_init()
{
    struct StWavStream *pMgr = &gWavStream;
    memset(pMgr, 0, sizeof(struct StWavStream));
    pMgr->end_flag = WAV_PLAY_STOP3; //开始就禁止播放。

    media_init();

    djy_audio_dac_open(AUDIO_DMA_BUFF_SIZE, 1, audio_sample_rate_44100);

    int volume = 80;
    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME, &volume);

    InitLoopBytesBuffer();
    u16 task_playwav = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_REAL, 0, 1, decode_wav, 0, 0X800, "playwav");
    if(task_playwav != CN_EVTT_ID_INVALID)
    {
        Djy_EventPop(task_playwav,NULL,0,0,0,0);
    }
    return 0;
}


