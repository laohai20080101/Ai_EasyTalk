#include "mp3Common.h"
#include "stdio.h"
#include "playmp3.h"
#include "LoopBytes.h"
#include "httpc.h"


int ipdumphex(const unsigned char *buf, int size, int flag)
{
    int i;
    if (flag)
        printf("\r\n===<<< len:%d <<<==\r\n", size);
    else
        printf("\r\n===>>> len:%d >>>==\r\n", size);
    for(i=0; i<size; i++) {
        if (i >= 54) break;
        printf("%02x,%s", buf[i], (i+1)%16?((i+1)%8?"":" "):"\r\n");
    }
    return 0;
}

/*******************************************************************************
* Function Implemantation
*******************************************************************************/
/*
    MCLK:26MHz, delay(1): about 25us
                delay(10):about 125us
                delay(100):about 850us
 */
//void delay(int num)
//{
//    volatile int i,j;
//
//    for(i = 0; i < num; i ++)
//    {
//        for(j = 0; j < 100; j ++)
//            ;
//    }
//}


int ProgressBar(int passed, int total, int speed, char *url)
{
#if 0
    static unsigned int last_time = 0;
    int loopmax = 0;
    int looptotals = 0;
    int played = 0;
    int n = 0;
    char bar[101] = { 0 };
    char breaken[10]="        ";
    if (total<=0) return -1;

    if (GET_TIME_MS()-last_time < 200 && passed != total) {
        return -1;
    }
    loopmax = LoopBytesMgrTotals();
    looptotals = LoopBytesMgrMax();
    last_time = GET_TIME_MS();
    memset(bar, '.', sizeof(bar));
    bar[100] = 0;
    n = passed*100/total;
    if (passed == total) n = 100;
    memset(bar, '#', n);
    if (looptotals < passed) {
        played = passed - looptotals;
        played = played*100/total;
        memset(bar, '=', played);
    }
    bar[100] = 0;
    if (passed == total) {
        printf("[%-100s][%d%%][%dKB/S][%d/%d][%s][LOOP:%d,%d][brate:%d]%s\r\n", bar, n, speed, passed, total, url, loopmax, looptotals, Mp3Framebitrate(), breaken);
    }
    else {
        printf("[%-100s][%d%%][%dKB/S][%d/%d][%s][LOOP:%d,%d][brate:%d]%s\r", bar, n, speed, passed, total, url, loopmax, looptotals, Mp3Framebitrate(), breaken);
    }
    fflush(stdout);
#else
    (void) passed;
    (void) total;
    (void) speed;
    (void) url;
#endif
    return 0;
}

int dumphex16(const unsigned char *buf, int size)
{
    int i;
    for(i=0; i<size; i++) {
        printf("%02x %s", buf[i], (i+1)%16?" ":"\r\n");
    }
    return 0;
}

int idle_info_task()
{
#if 0
    struct StHttpDlInfo *pHttpInfo = GetHttpDlInfo();
    static unsigned int last_time = 0;
    static int end_flash = 0;
    int speed = 0;
    int loopmax = 0;
    int looptotals = 0;
    int played = 0;
    int n = 0;
    char bar[101] = { 0 };
    char breaken[10]="        ";
    while (1) {
        //bk_wlan_dtim_rf_ps_mode_enable();
        //bk_wlan_dtim_rf_ps_timer_start();
        if (pHttpInfo->mp3_size <= 0) {
            Djy_EventDelay(1000*1000);
            continue;
        }
        loopmax = LoopBytesMgrTotals();
        looptotals = LoopBytesMgrMax();
        last_time = GET_TIME_MS();
        memset(bar, '.', sizeof(bar));
        bar[100] = 0;
        n = pHttpInfo->mp3_pos*100/pHttpInfo->mp3_size;
        if (pHttpInfo->mp3_pos == pHttpInfo->mp3_size) n = 100;
        memset(bar, '#', n);
        if (looptotals < pHttpInfo->mp3_pos) {
            played = pHttpInfo->mp3_pos - looptotals;
            played = played*100/pHttpInfo->mp3_size;
            memset(bar, '=', played);
        }
        bar[100] = 0;
        speed = (int)(pHttpInfo->mp3_pos/(GET_TIME_MS()-pHttpInfo->begin))*0.98;

        if (pHttpInfo->mp3_pos == pHttpInfo->mp3_size) {
            if (end_flash == 0)
                printf("[%-100s][%d%%][%dKB/S][%d/%d][%s][LOOP:%d/%d][brate:%d]%s\r\n", bar, n, speed, pHttpInfo->mp3_pos, pHttpInfo->mp3_size, pHttpInfo->url, loopmax, looptotals, Mp3Framebitrate(), breaken);
            if (pHttpInfo->mp3_size == pHttpInfo->mp3_pos) {
                end_flash = 1;
            }
        }
        else {
            end_flash = 0;
            printf("[%-100s][%d%%][%dKB/S][%d/%d][%s][LOOP:%d/%d][brate:%d]%s\r", bar, n, speed, pHttpInfo->mp3_pos, pHttpInfo->mp3_size, pHttpInfo->url, loopmax, looptotals, Mp3Framebitrate(), breaken);
        }
        fflush(stdout);
        Djy_EventDelay(500*1000);
    }
#endif
    return 0;
}


