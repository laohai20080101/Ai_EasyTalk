#ifndef __HTTPC_H__
#define __HTTPC_H__


typedef struct StHttpDlInfo{
    volatile int mp3_size;
    volatile int mp3_pos;
    volatile unsigned int begin;
    volatile int speed;
    char url[512];
}StHttpDlInfo;


struct StHttpDlInfo *GetHttpDlInfo();

#endif
