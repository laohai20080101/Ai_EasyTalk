#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include <dbug.h>
#include "app_flash.h"
#include <project_config.h>


WIFI_CFG_T  LoadWifi;
WIFI_CFG_T  LoadUser;
char *NLP_SN_BUF;
#if 0
uint8 FlashIsInit(void)
{
    uint8 ucRet;
    uint8 ucLen;
    uint32 uiPageBuf[FLASH_PAGE_SIZE / sizeof(uint32)];
    uint8* ucPageBuf = (uint8*)uiPageBuf;

    memset(ucPageBuf, 0, FLASH_PAGE_SIZE);
    ucLen = strlen("gxj");

    djy_flash_read(MACHINE_STRING_ADDR,uiPageBuf,ucLen);
    printf("FlashIsInit ,ucLen=%d %s",ucLen,ucPageBuf);
    ucRet = strcmp((char*)ucPageBuf, "gxj");
    printf("ucRet = %d \r\n",ucRet);
    if(ucRet)
    {
        memset(ucPageBuf, 0, FLASH_PAGE_SIZE);
        strcpy((char*)ucPageBuf,"gxj");

        djy_flash_erase(MACHINE_STRING_ADDR);
        djy_flash_write(MACHINE_STRING_ADDR, ucPageBuf,ucLen);
        printf("Flash_write ,ucLen=%d %s\r\n ",ucLen,ucPageBuf);

        WifiSave();
        return 0;
    }else{
        //WifiLoad();
        printf("Flash_read\r\n ");
    }

    return 1;
}
#endif
//int wifi_mac_flash_write(unsigned char *mac, int len)
//{
//    if((mac == NULL)|| (len) > FLASH_SECTOR_SIZE)
//        return false;
//
//    if(SetNameValueFS(CFG_WIFI_MAC_FILE_NAME, (char *)mac, len))
//        return true;
//    else
//        return false;
//}
//
//bool_t wifi_mac_flash_read(unsigned char *mac, int len)
//{
//    if(mac != NULL)
//    {
//        if(GetNameValueFS(CFG_WIFI_MAC_FILE_NAME, (char *)mac, len))
//            return true;
//    }
//    return false;
//}


void WifiSave(char *ssid,char *key)
{
    uint8 ucLen;
    WIFI_CFG_T  WifiCfg;
    uint32 uiPageBuf[FLASH_PAGE_SIZE / sizeof(uint32)];
    uint8* ucPageBuf = (uint8*)uiPageBuf;

    if((strlen(ssid) > 32) || (strlen(key) > 64))
    {
        error_printf("WIFI_write"," wifi account or password is too long\r\n.");
    }
    else
    {
        ucLen = sizeof(WIFI_CFG_T);
        memset(ucPageBuf, 0, FLASH_PAGE_SIZE);

        memset(&WifiCfg, 0, sizeof(WifiCfg));
        WifiCfg.statue = 0x55;
        memcpy((uint8*)(WifiCfg.WifiSsid),ssid,strlen(ssid)+1);
        memcpy((uint8*)(WifiCfg.WifiPassWd),key,strlen(key)+1);

        memcpy((uint8*)ucPageBuf, (uint8*)(&WifiCfg), ucLen);

        SetNameValueFS(CFG_WIFI_FILE_NAME, (char *)ucPageBuf, ucLen);
    }
}
void WifiErase( )
{
    RmNameValueFS(CFG_WIFI_FILE_NAME);
}
uint8 WifiLoad(void)
{
    uint32 uiPageBuf[FLASH_PAGE_SIZE / sizeof(uint32)];
    uint8* ucPageBuf = (uint8*)uiPageBuf;
    uint8 ucLen;
    ucLen = sizeof(WIFI_CFG_T);
    GetNameValueFS(CFG_WIFI_FILE_NAME, (char *)ucPageBuf, ucLen);

    memcpy((uint8*)(&LoadWifi), ucPageBuf, ucLen);
    printf("LoadWifi.statue =  %d\r\n ",LoadWifi.statue);
    if(LoadWifi.statue == 0x55)
    {
        printf("WIFI_Read ,ssid= %s,key = %s\r\n ",LoadWifi.WifiSsid,LoadWifi.WifiPassWd);
        return 1;
    }else
        return 0;
}

void UserSave(char *ssid,char *key)
{
    uint8 ucLen;
    WIFI_CFG_T  UserCfg;

    uint32 uiPageBuf[FLASH_PAGE_SIZE / sizeof(uint32)];
    uint8* ucPageBuf = (uint8*)uiPageBuf;

    if((strlen(ssid) > 32) || (strlen(key) > 64))
    {
        error_printf("User_write"," wifi account or password is too long\r\n.");
    }
    else
    {
        ucLen = sizeof(WIFI_CFG_T);
        memset(ucPageBuf, 0, FLASH_PAGE_SIZE);

        UserCfg.statue = 0x55;
        memcpy((uint8*)(UserCfg.WifiSsid),ssid,strlen(ssid)+1);
        memcpy((uint8*)(UserCfg.WifiPassWd),key,strlen(key)+1);

        memcpy((uint8*)ucPageBuf, (uint8*)(&UserCfg), ucLen);
        SetNameValueFS(CFG_USER_WIFI_FILE_NAME, (char *)ucPageBuf, ucLen);
    }
}

uint8 UserLoad(void)
{
    uint32 uiPageBuf[FLASH_PAGE_SIZE / sizeof(uint32)];
    uint8* ucPageBuf = (uint8*)uiPageBuf;
    uint8 ucLen;

    ucLen = sizeof(WIFI_CFG_T);
    GetNameValueFS(CFG_USER_WIFI_FILE_NAME, (char *)ucPageBuf, ucLen);
    memcpy((uint8*)(&LoadUser), ucPageBuf, ucLen);

    if(LoadUser.statue == 0x55)
    {
        printf("user_Read ,user_ssid= %s,user_key = %s\r\n ",LoadUser.WifiSsid,LoadUser.WifiPassWd);
        return 1;
    }else
        return 0;
}


bool_t Sn_InfoSave(char *info ,u32 len)
{
    if((info == NULL)|| (len) > FLASH_SECTOR_SIZE)
        return false;

    if(SetNameValueFS(CFG_SN_INFO_FILE_NAME, info, len))
        return true;
    else
        return false;
}
bool_t Sn_InfoLoad(char *buf ,u32 maxlen)
{
    u32 len;
    if((maxlen == 0) || (buf == NULL))
        return false;
    len = maxlen > FLASH_SECTOR_SIZE ? FLASH_SECTOR_SIZE :maxlen;
    GetNameValueFS(CFG_SN_INFO_FILE_NAME, buf, len);
    if(strlen(buf) < len)
    {
        return true;
    }
    return false;
}

bool_t Sn_InfoErase( )
{
    if(RmNameValueFS(CFG_SN_INFO_FILE_NAME))
        return true;
    else
        return false;
}

bool_t Sn_init()
{
    static char SN_BUF[64];
    NLP_SN_BUF = SN_BUF;
    char *sn = "zhongqin-djyos-AIPJ-YYRC-%x%x%x%x%x%x";
    u8 *getnetmacaddr();

    u8 * macaddr = getnetmacaddr();
    if(false == Sn_InfoLoad(SN_BUF,sizeof(SN_BUF)))
    {
        //用%2x打印实际上只出来一位
        sprintf(SN_BUF,sn,  (macaddr[3]>>4)&0x0f,\
                            (macaddr[3]>>0)&0x0f,\
                            (macaddr[4]>>4)&0x0f,\
                            (macaddr[4]>>0)&0x0f,\
                            (macaddr[5]>>4)&0x0f,\
                            (macaddr[5]>>0)&0x0f);
        Sn_InfoSave(SN_BUF,sizeof(SN_BUF));
    }
    return true;
}

bool_t Nlp_InfoErase( )
{
    if(RmNameValueFS(CFG_NLP_INFO_FILE_NAME))
        return true;
    else
        return false;
}

bool_t Nlp_InfoSave(char *info ,u32 len)
{
    if((info == NULL)|| (len) > FLASH_SECTOR_SIZE)
        return false;

    if(SetNameValueFS(CFG_NLP_INFO_FILE_NAME, info, len))
        return true;
    else
        return false;
}

bool_t Nlp_InfoLoad(char *buf ,u32 maxlen)
{
    if((maxlen == 0) || (buf == NULL))
        return false;
    maxlen = maxlen > FLASH_SECTOR_SIZE ? FLASH_SECTOR_SIZE :maxlen;

    GetNameValueFS(CFG_NLP_INFO_FILE_NAME, buf, maxlen);
    for(u32 i = 0;i<30;i++)
    {
        if((u8)buf[i] != 0xff)
            return true;
    }
    return false;
}

bool_t DhcpIp_InfoErase( )
{
    if(RmNameValueFS(CFG_DHCPIP_INFO_FILE_NAME))
        return true;
    else
        return false;
}

bool_t DhcpIp_InfoSave(char *info ,u32 len)
{
    if((info == NULL)|| (len) > FLASH_SECTOR_SIZE)
        return false;

    if(SetNameValueFS(CFG_DHCPIP_INFO_FILE_NAME, info, len))
        return true;
    else
        return false;
}

bool_t DhcpIp_InfoLoad(unsigned int *ip)
{
    if(ip == NULL)
        return false;
    if (!GetNameValueFS(CFG_DHCPIP_INFO_FILE_NAME, (char *)ip, 4))
        return false;
    return true;
}

#include "shell.h"
bool_t sh_setsn(char *param)
{
    return  Sn_InfoSave(param,strlen(param));
}
ADD_TO_ROUTINE_SHELL(snset,sh_setsn,"Rewrite SN for example: snst djysn1");
