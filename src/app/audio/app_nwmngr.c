#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "app_flash.h"
#include "app_nwmngr.h"
//#include  "VoicePrompt.h"
//#include "app_led.h"
#include <sys/dhcp.h>
#include "../comm/comm_api.h"
u16 gNwMngrEvtt = CN_EVTT_ID_INVALID; // nwmngr事件类型

extern WIFI_CFG_T LoadWifi;
int airkissEnterProcess(void);
int WeChat=0;

static int airkissRequested = 0; // 是否请求微信配网
static NwmngrStatus linkStatus = NWMNGR_STATUS_UNLINK; // 网络连接状态

// 返回值： 1<<0表示需要初始化DHCP客户端；1<<1表示需要初始化DHCP服务端
//u32 Get_DhcpInitflag(void)
//{
//    if (!WifiLoad())
//    {
//#if (1 == SET_WIFI_DEFAULT_SSID_PW)
//        strcpy(LoadWifi.WifiSsid, "P20");
//        strcpy(LoadWifi.WifiPassWd, "12345678");
//#endif
//    }
//
//    return (1<<0);
//}

void nwmngrRequestConnect(NwmngrReqType type)
{
    switch(type)
    {
    case NWMNGR_REQ_TYPE_AUTO:
    {
        linkStatus = NWMNGR_STATUS_UNLINK;
        break;
    };
    case NWMNGR_REQ_TYPE_AIRKISS:
    {
        linkStatus = NWMNGR_STATUS_UNLINK;
        airkissRequested = 1;
        break;
    };
    default:
    {

    }
    }
}

NwmngrStatus nwmngrGetLinkStatus(void)
{
    return linkStatus;
}

// 连接到AP
// DHCP获得IP地址
// 是否启动微信配网
// 改变网络状态
ptu32_t nwmngr_event_main(void)
{
    int ret = -1;

    printf("[INFO] nwmngr_event_main start.\r\n");

    while (1)
    {
        if (NWMNGR_STATUS_UNLINK == linkStatus)
        {
            // 判断用户要求进行微信配网，那么进入微信配网功能获得SSID和密码
            if (airkissRequested)
            {
                int akLinkStatus = -1;

                airkissRequested = 0;
                linkStatus = NWMNGR_STATUS_CONNECTING;
                printf("[INFO] start Airkiss.\r\n");
                DjyWifi_StaDisConnect();
//                VoicePrompt(wechatconnecting);
//                LedSetState(LED_PLAYING_STATE);
               // Djy_EventDelay(1000*mS);
                akLinkStatus = airkissEnterProcess();
                printf("[INFO] airkissEnterProcess return:%d\r\n", akLinkStatus);
                WeChat=1;

                linkStatus = NWMNGR_STATUS_UNLINK;

            }
            else
            {
                linkStatus = NWMNGR_STATUS_CONNECTING;
                // 执行联网动作
                printf("[DEBUG] ConnectTo... %s , %s\r\n", LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
                DjyWifi_StaAdvancedConnect(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
//                LedSetState(LED_PLAYING_STATE);
//                if(!WeChat)
//                VoicePrompt(wifi_connecting);
                ret = PendDhcpDone(20*1000*1000);
                printf("info: PendDhcpDone, ret=%d!\r\n", ret);
                if (1 == ret)
                {
                    DjyWifi_StaConnectDone();
                    if(WeChat)
                    {
//                        VoicePrompt(wifi_connect_sucess);
//                        LedSetState(LED_STANDBY_STATE);
                        WeChat=0;
                    }
                    linkStatus = NWMNGR_STATUS_LINKED;
                }
                else
                {
//                    LedSetState(LED_STANDBY_STATE);
//                    VoicePrompt(wifi_connect_error);
                    linkStatus = NWMNGR_STATUS_UNLINK;
                }
            }
        }
        else if (NWMNGR_STATUS_CONNECTING == linkStatus&&WeChat==0)
        {
            if(mhdr_get_station_status()==MSG_GOT_IP)
            {
//                VoicePrompt(wifi_connect_sucess);
                linkStatus=NWMNGR_STATUS_LINKED;
//                LedSetState(LED_STANDBY_STATE);
            }
        }
        else if (NWMNGR_STATUS_LINKED == linkStatus&&WeChat==0)
        {
            // TODO 自动检测网络是否断开
            if(mhdr_get_station_status()!=MSG_GOT_IP)
            {
//                VoicePrompt(networkdisconnection);
                linkStatus=NWMNGR_STATUS_CONNECTING;
//                LedSetState(LED_STANDBY_STATE);
            }
            static s64 lastGetSbcTimeTs = 0;
            if ((0 == lastGetSbcTimeTs) || (DjyGetSysTime()/1000-lastGetSbcTimeTs >= 60*60*1000))
            {
//                getsbctime(NULL);
                lastGetSbcTimeTs = DjyGetSysTime()/1000;
            }
        }
        Djy_EventDelay(10*mS);
    }

    return 0;
}

void nwmngrEventInit(void)
{
    gNwMngrEvtt = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0,
            nwmngr_event_main, NULL, 0x1000, "nwmngr function");

    if (CN_EVTT_ID_INVALID != gNwMngrEvtt)
    {
        Djy_EventPop(gNwMngrEvtt, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-nwmngr_event_main error\r\n");
    }
}

