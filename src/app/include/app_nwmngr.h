#ifndef __APP_NWMNGR_H__
#define __APP_NWMNGR_H__

// 网络连接状态 定义
typedef enum {
    NWMNGR_STATUS_UNLINK = 0,       // 未连接
    NWMNGR_STATUS_CONNECTING = 1,   // 正在连接
    NWMNGR_STATUS_LINKED = 2,       // 连接并获得了IP地址
} NwmngrStatus;

// 网络请求类型 定义
typedef enum {
    NWMNGR_REQ_TYPE_AUTO = 1,       // 自动连接
    NWMNGR_REQ_TYPE_AIRKISS = 2,   // 开始进行微信配网
} NwmngrReqType;

// 请求网络连接
extern void nwmngrRequestConnect(NwmngrReqType type);

// 获得网络连接状态
// 返回值参考 NWMNGR_STATUS_XXX 定义
extern NwmngrStatus nwmngrGetLinkStatus(void);
extern void nwmngrEventInit(void);

#endif
