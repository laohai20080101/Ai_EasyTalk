
#if 0
/*
* file:	mqttsn.h
* author:	vincent.cws2008@gmail.com
* history:
* @2019-04-29: initial - circle_buf
*
*/

#ifndef __LOOP_OBJ_H__
#define __LOOP_OBJ_H__
#ifdef   __cplusplus
extern "C" {
#endif

#include "stdlib.h"

enum {
    LOOP_MODE_CUTOVER = 0,
    LOOP_MODE_BLOCKED,
    LOOP_MODE_MOVESTEP,
};


typedef struct StLoopObjMgr {

	int total_cnts;
	int size_type;
	int used_cnts;
	int mode; //mode==0: full will over write;;
				//mode==1: full will stop, and push return failed
				//mode==2: full will pull one and push one,keep the latest full data.
	int idx_head;
	int idx_tail;
	unsigned char arrRec[0];
}StLoopObjMgr;

typedef void(*FunPrint)(void *data);

struct StLoopObjMgr* CreateLoopObjMgr(int sizeOfType, int cntsOfArr, int mode);
void DeleteLoopObjMgr(struct StLoopObjMgr *pMgr);
int PushLoopObjTail(struct StLoopObjMgr* pMgr, void *pRecObj);
int PullLoopObjHead(struct StLoopObjMgr* pMgr, void *pRecObj);
int PrintLoopObj(struct StLoopObjMgr* pMgr, FunPrint f);
int test();

#ifdef   __cplusplus
}
#endif
#endif


#endif
