#ifndef __LOOP_REC_H__
#define __LOOP_REC_H__
#ifdef   __cplusplus
extern "C" {
#endif

#include "stdlib.h"


//#include "LoopObj.h"

//extern struct StLoopObjMgr;

#define ARRAY_REC_MAX 100

typedef struct StFrmRec {
    int frm_length;
    int frm_duration;//ms
}StFrmRec;

typedef struct StLoopRecMgr {
    volatile int flag_init;
    volatile int total_len;
    volatile int total_dur;
    struct StLoopObjMgr *pLoopObjMgr;
}StLoopRecMgr;

int LoopRecMgrInit();
int LoopRecMgrPushTail(int frm_len, int frm_dur);
int LoopRecMgrPullHead(int *frm_len, int *frm_dur);
int LoopRecMgrDeInit();

int GetFrmRecTotalLens();
int GetFrmRecTotalDurs();


#ifdef   __cplusplus
}
#endif

#endif
