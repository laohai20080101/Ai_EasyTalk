/****************************************************
 *  Automatically-generated file. Do not edit!  *
 ****************************************************/


#include "project_config.h"
#include "djyos.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include <djyfs/filesystems.h>
#include <Iboot_info.h>


extern ptu32_t djy_main(void);


const char CN_MANUFACTURER_NAME[] = PRODUCT_MANUFACTURER_NAME;
const struct ProductInfo Djy_Product_Info __attribute__ ((section(".DjyProductInfo"))) =
{
    .VersionNumber[0] = PRODUCT_VERSION_LARGE,
    .VersionNumber[1] = PRODUCT_VERSION_MEDIUM,
    .VersionNumber[2] = PRODUCT_VERSION_SMALL,
#if(CN_PTR_BITS < 64)
    .ManufacturerNameAddr      = (u32)(&CN_MANUFACTURER_NAME),
    .ManufacturerNamereserved32    = 0xffffffff,
#else
    .ManufacturerNameAddr      = (u64)(&CN_MANUFACTURER_NAME),
#endif
    .ProductClassify = PRODUCT_PRODUCT_CLASSIFY,
    .ProductType = PRODUCT_PRODUCT_MODEL,




    .TypeCode = PRODUCT_PRODUCT_MODEL_CODE,




    .ProductionTime = {0xff,0xff,0xff,0xff},
    .ProductionNumber = {0xff,0xff,0xff,0xff,0xff},
    .reserved8 = 0,
    .BoardType = PRODUCT_BOARD_TYPE,
    .CPU_Type = PRODUCT_CPU_TYPE,
    .Reserved ={
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
            },
};


ptu32_t __djy_main(void)
{
    extern ptu32_t ModuleInstall_st7796s(const char *DisplayName,const char* HeapName);
    ModuleInstall_st7796s(CFG_ST7796S_DISPLAY_NAME,CFG_ST7796S_HEAP_NAME);


    extern void ModuleInstall_Gdd_AND_Desktop(void);
    ModuleInstall_Gdd_AND_Desktop();


    extern bool_t ModuleInstall_Touch(void);
    ModuleInstall_Touch();    //初始化人机界面输入模块



    extern bool_t ModuleInstall_KeyBoard(void);
    ModuleInstall_KeyBoard();


    extern bool_t ModuleInstall_KeyBoardHal(const char *dev_name);
    ModuleInstall_KeyBoardHal(CFG_KEYBOARD_NAME);
    #if(CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT == true)
    extern bool_t GDD_AddInputDev(const char *InputDevName);
    GDD_AddInputDev(CFG_KEYBOARD_NAME);
    #endif


    extern bool_t ModuleInstall_FT6236(void);
    ModuleInstall_FT6236( );
    #if(CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT == true)
    extern bool_t GDD_AddInputDev(const char *InputDevName);
    GDD_AddInputDev(CFG_FT6236_TOUCH_NAME);
    #endif


    extern void ModuleInstall_BrdWdt(void);
    ModuleInstall_BrdWdt();


    djy_main();


    return 0;
}


void Sys_ModuleInit(void)
{
    uint16_t evtt_main;


    extern void Stdio_KnlInOutInit(char * StdioIn, char *StdioOut);
    Stdio_KnlInOutInit(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME);
    extern s32 ModuleInstall_Shell(ptu32_t para);
    ModuleInstall_Shell(0);


    //----------------------------early----------------------------//
    extern void ModuleInstall_BlackBox(void);
    ModuleInstall_BlackBox( );


    extern s32 ModuleInstall_dev(void);
    ModuleInstall_dev();    // 安装设备文件系统；



    extern bool_t ModuleInstall_MsgQ(void);
    ModuleInstall_MsgQ ( );


    //----------------------------medium----------------------------//
    extern bool_t ModuleInstall_HmiIn(void);
    ModuleInstall_HmiIn();      //初始化人机界面输入模块



    extern bool_t ModuleInstall_GK(void);
    ModuleInstall_GK();


    extern bool_t ModuleInstall_Multiplex(void);
    ModuleInstall_Multiplex ();


    extern bool_t ModuleInstall_Wdt(void);
    ModuleInstall_Wdt();


    extern s32 ModuleInstall_XIP_IBOOT_FS(u32 opt, void *data);
    ModuleInstall_XIP_IBOOT_FS(0,NULL);


    extern s32 ModuleInstall_FAT(const char *dir, u32 opt, void *data);
    ModuleInstall_FAT(CFG_FAT_MOUNT_POINT, CFG_FAT_MS_INSTALLUSE, CFG_FAT_MEDIA_KIND);


    extern void ModuleInstall_ADC(void);
    ModuleInstall_ADC( );


    extern int ModuleInstall_SDCARD(void);
    ModuleInstall_SDCARD();


    extern void ModuleInstall_random(void);
    ModuleInstall_random();


    extern s32 ModuleInstall_EFS(const char *target, u32 opt, u32 config);
    ModuleInstall_EFS(CFG_EFS_MOUNT_POINT, CFG_EFS_INSTALL_OPTION, 0);


    extern bool_t ModuleInstall_Font(void);
    ModuleInstall_Font ( );


    #if(CFG_OS_TINY == flase)
    extern s32 kernel_command(void);
    kernel_command();
    #endif


    extern int ModuleInstall_QSPI_PSRAM(void);
    ModuleInstall_QSPI_PSRAM();


    extern ptu32_t ModuleInstall_UART(ptu32_t SerialNo);
    #if CFG_UART1_ENABLE ==1
    ModuleInstall_UART(CN_UART1);
    #endif
    #if CFG_UART2_ENABLE ==1
    ModuleInstall_UART(CN_UART2);
    #endif


    extern bool_t ModuleInstall_TcpIp(void);
    ModuleInstall_TcpIp( );


    //----------------------------later----------------------------//
    extern bool_t ModuleInstall_DjyBus(void);
    ModuleInstall_DjyBus ( );


    extern bool_t ModuleInstall_SPIBus(void);
    ModuleInstall_SPIBus();


    extern bool_t ModuleInstall_IICBus(void);
    ModuleInstall_IICBus ( );


    bool_t ModuleInstall_init_ioiic(const char * busname);
    ModuleInstall_init_ioiic(CFG_IO_IIC_BUS_NAME);


    extern void ModuleInstall_LowPower (void);
    ModuleInstall_LowPower();


    extern ptu32_t ModuleInstall_Charset(ptu32_t para);
    ModuleInstall_Charset(0);
    extern void ModuleInstall_CharsetNls(const char * DefaultCharset);
    ModuleInstall_CharsetNls("C");


    extern bool_t ModuleInstall_CharsetGb2312(void);
    ModuleInstall_CharsetGb2312 ( );


    extern void ModuleInstall_FontGB2312(void);
    ModuleInstall_FontGB2312();


    extern bool_t ModuleInstall_CharsetUtf8(void);
    ModuleInstall_CharsetUtf8 ( );


    extern int ModuleInstall_Flash(void);
    ModuleInstall_Flash();


    extern bool_t ModuleInstall_EmbFlashInstallEfs(const char *TargetFs, s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_EmbFlashInstallEfs(CFG_EMBFLASH_EFS_MOUNT_NAME, CFG_EMBFLASH_EFS_PART_START,
    CFG_EMBFLASH_EFS_PART_END, CFG_EMBFLASH_EFS_PART_FORMAT);


    extern bool_t ModuleInstall_FlashInstallXIP(const char *TargetFs,s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_FlashInstallXIP(CFG_EFLASH_XIPFSMOUNT_NAME,CFG_EFLASH_XIP_PART_START,
    CFG_EFLASH_XIP_PART_END, CFG_EFLASH_XIP_PART_FORMAT);


    #if !defined (CFG_RUNMODE_BAREAPP)
    extern bool_t ModuleInstall_XIP(void);
    ModuleInstall_XIP( );
    #endif

    extern void ModuleInstall_InitNet( );
    ModuleInstall_InitNet( );
    extern s32 ModuleInstall_Audio(void);
    ModuleInstall_Audio( );


    #if(CFG_STDIO_STDIOFILE == true)
    extern s32 ModuleInstall_STDIO(const char *in,const char *out, const char *err);
    ModuleInstall_STDIO(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME,CFG_STDIO_ERR_NAME);
    #endif


    evtt_main = Djy_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,0,
        __djy_main,NULL,CFG_MAINSTACK_LIMIT, "main function");
    //事件的两个参数暂设为0,如果用shell启动,可用来采集shell命令行参数

    Djy_EventPop(evtt_main,NULL,0,0,0,0);

    #if ((CFG_DYNAMIC_MEM == true))
    extern bool_t Heap_DynamicModuleInit(void);
    Heap_DynamicModuleInit ( );
    #endif






    return ;
}


